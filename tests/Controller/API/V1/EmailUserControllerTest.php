<?php

namespace App\Tests\Controller;

use App\Entity\Institute;
use App\Entity\Email;
use App\Entity\User;
use App\Entity\UserInstitute;
use App\Tests\Controller\API\APIGuzzleTestCase;

class EmailUserControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/users';
    private static $emailId;
    private static $userId;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = [
        'id', 'institute_id', 'user_id', 'email', 'is_primary', 'is_verified', 'role', 'date'
    ];
    private $data = [
        'email' => 'email@yahoo.fr',
        'isPrimary' => false,
        'role' => 'Service Examens',
    ];

    public function testGetUserEmail() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$userId . '/emails/' . self::$emailId );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetUserEmails() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$userId . '/emails' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewUserEmail() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->post( "$this->uriPrefix/" . self::$userId . '/emails', [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $this->data ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchUserEmail() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $data = [
            'isPrimary' => false,
            'email' => 'patched@gmail.com',
        ];

        $response = $this->client->patch( "$this->uriPrefix/" . self::$userId . '/emails/' . ++self::$emailId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutUserEmail() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $this->data['email'] = 'edited@gmail.com';
        $this->data['isPrimary'] = false;

        $response = $this->client->put( "$this->uriPrefix/" . self::$userId . '/emails/' . ++self::$emailId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $this->data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveUserEmail() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( "$this->uriPrefix/" . self::$userId . '/emails/' . ++self::$emailId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setCin( '09799221' );
        $user->setRoles( [ 'ROLE_INS_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );

        $em->persist( $user );

        $userEmail = new Email();
        $userEmail->setEmail('user@gmail.com');
        $userEmail->setUser($user);
        $userEmail->setIsPrimary(true);

        $userEmail2 = new Email();
        $userEmail2->setEmail('user2@gmail.com');
        $userEmail2->setConfirmationToken( self::getService( 'fos_user.util.token_generator' )->generateToken() );
        $userEmail2->setUser($user);

        $em->persist( $userEmail );
        $em->persist( $userEmail2 );
        $em->flush();

        $userEmail = $em->getRepository( 'App:Email')->findOneBy( [ 'email' => 'user@gmail.com' ] );
        $user = $em->getRepository( 'App:User')->findOneBy( [ 'username' => self::$username ] );
        self::$emailId = $userEmail->getId();
        self::$userId = $user->getId();
    }
}

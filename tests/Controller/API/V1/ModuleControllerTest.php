<?php

namespace App\Tests\Controller;

use App\Entity\Department;
use App\Entity\Module;
use App\Entity\Institute;
use App\Entity\Speciality;
use App\Entity\User;
use App\Entity\UserInstitute;
use App\Tests\Controller\API\APIGuzzleTestCase;

class ModuleControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/institutes/';
    private static $id;
    private static $specId;
    private static $insId;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = [
        'id', 'speciality_id', 'name', 'description', 'coefficient', 'date'
    ];

    public function testGetModule() {

        $response = $this->client->get( $this->uriPrefix . self::$insId . '/specialities/' . self::$specId . '/modules/' . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetModules() {

        $response = $this->client->get( $this->uriPrefix . self::$insId . '/specialities/' . self::$specId . '/modules' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewModule() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->post( $this->uriPrefix . self::$insId . '/specialities/' . self::$specId . '/modules', [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( [
                'name' => 'Module 2',
                'description' => 'Module 2 little description... Module 2 little description... Module 2 little description...'
            ] ),
        ] );
        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchModule() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->patch( $this->uriPrefix . self::$insId . '/specialities/' . self::$specId . '/modules/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( [
                'name' => 'Patched Module',
            ] ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutModule() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->put( $this->uriPrefix . self::$insId . '/specialities/' . self::$specId . '/modules/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( [
                'name' => 'Module Edited',
                'description' => 'Module edited little description... Module edited little description... Module edited '
            ] ),
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveModule() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . self::$insId . '/specialities/' . self::$specId . '/modules/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $institute = new Institute();
        $institute->setName( 'Esprit' );
        $institute->setDescription( 'Esprit description' );
        $institute->setSlogan( 'Se former autrement' );
        $institute->setWebsite( 'http://www.esprit.com.tn' );
        $institute->setFoundationDate( new \DateTime( '2009-02-15' ) );

        $em->persist( $institute );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setCin( '09799221' );
        $user->setRoles( [ 'ROLE_INS_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $em->persist( $user );

        $userIns = new UserInstitute();
        $userIns->setUser( $user );
        $userIns->setInstitute( $institute );
        $userIns->setRole( 'ROLE_INS_ADMIN' );
        $em->persist( $userIns );

        $speciality = new Speciality();
        $speciality->setName( 'Ingenieur Informatique' );
        $speciality->setDescription( 'Ingenieur Informatique speciality Description' );
        $speciality->setYears( 3 );
        $speciality->setStudyCycle( 'ENGINEER_CYCLE' );
        $speciality->setType( 'SPECIALITY' );
        $speciality->setInstitute( $institute );
        $em->persist( $speciality );

        $module = new Module();
        $module->setSpeciality( $speciality );
        $module->setName( 'Module Management' );
        $module->setCoefficient( 7 );
        $module->setDescription( 'Module Management little description...' );
        $em->persist( $module );

        $em->flush();

        /**
         * @var Module $module
         */
        $module = $em->getRepository( 'App:Module' )->findOneBy( [ 'coefficient' => 7 ] );
        self::$id = $module->getId();
        self::$specId = $module->getSpeciality()->getId();
        self::$insId = $module->getSpeciality()->getInstitute()->getId();
    }
}

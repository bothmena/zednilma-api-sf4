<?php

namespace App\Tests\Controller;

use App\Entity\Image;
use App\Entity\Institute;
use App\Tests\Controller\API\APIGuzzleTestCase;

class InstituteControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/institutes';
    private static $logoName = 'institute-logo.png';
    private static $coverName = 'institute-cover.jpg';
    private static $id;
    private static $slug;
    private $keysArray = [
        'context', 'id', 'name', 'slug', 'slogan', 'description', 'foundation_date', 'website',
        'type', 'parental_control', 'join_date', 'logo_url', 'cover_url',
    ];
    private $data = [
        'name'            => 'Esprit School Of Business',
        'slogan'          => 'Se Former Autrement!',
        'description'     => 'my little description about my institute the so called esprit...',
        'foundationDate'  => '2003-06-08',
        'website'         => 'http://www.esprit.com',
        'type'            => 'UNIVERSITY',
        'parentalControl' => 'NONE',
    ];

    public function testGetInstitute() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        $this->assertEquals( $contentArray[ 'cover_url' ], '/images/website/' . self::$coverName );
        $this->assertEquals( $contentArray[ 'logo_url' ], '/images/website/' . self::$logoName );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetInstituteFireData() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$id . '/fire-data' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $keyArray = [ 'id', 'slug', 'name', 'slogan', 'description', 'foundation_date', 'join_date', 'website',
            'type', 'parental_control', 'logo_url', 'cover_url' ];

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $keyArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetInstituteBySlug() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$slug );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetInstitutes() {

        $response = $this->client->get( $this->uriPrefix );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewInstitute() {

        $response = $this->client->post( $this->uriPrefix, [
            'body' => json_encode( [
                'name'    => 'Esprit School Of Business',
                'slogan'  => 'Esprit School Of Business Slogan',
                'website' => 'http://www.example.com',
            ] ),
        ] );
        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchInstitute() {

        $data = [
            'name'           => 'School Of Business Esprit',
            'foundationDate' => '2005-11-08',
        ];

        $response = $this->client->patch( "$this->uriPrefix/" . self::$id, [
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutInstitute() {

        $this->data[ 'name' ] = 'Esprit School Of Business edited';
        $this->data[ 'foundationDate' ] = '2001-06-20';
        $this->data[ 'website' ] = 'http://www.esprit.org';

        $response = $this->client->put( "$this->uriPrefix/" . self::$id, [
            'body' => json_encode( $this->data ),
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $em \Doctrine\ORM\EntityManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );

        $logo = new Image();
        $logo->setName( self::$logoName );
        $logo->setType( '' );
        $logo->setEntity( 'web' );
        $em->persist( $logo );

        $cover = new Image();
        $cover->setName( self::$coverName );
        $cover->setType( '' );
        $cover->setEntity( 'web' );
        $em->persist( $cover );

        $em->persist( $logo );
        $em->persist( $cover );

        $institute = new Institute();
        $institute->setName( 'Esprit' );
        $institute->setDescription( 'Esprit description' );
        $institute->setSlogan( 'Se former autrement' );
        $institute->setWebsite( 'http://www.esprit.com.tn' );
        $institute->setFoundationDate( new \DateTime( '2009-02-15' ) );
        $institute->setType( 'UNIVERSITY' );
        $institute->setParentalControl( 'NONE' );
        $institute->setProfileImage( $logo );
        $institute->setCoverImage( $cover );

        $em->persist( $institute );
        $em->flush();

        $institute = $em->getRepository( 'App:Institute' )
            ->findOneBy( [ 'name' => 'Esprit' ] );
        self::$id = $institute->getId();
        self::$slug = $institute->getSlug();
    }
}

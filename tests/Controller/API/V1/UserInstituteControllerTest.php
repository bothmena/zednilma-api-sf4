<?php

namespace App\Tests\Controller;

use App\Entity\Image;
use App\Entity\Institute;
use App\Entity\Location;
use App\Entity\User;
use App\Entity\UserInstitute;
use App\Tests\Controller\API\APIGuzzleTestCase;

class UserInstituteControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1';
    private static $id;
    private static $insId;
    private static $username = 'username';
    private static $imageName = 'professor-male.png';
    private static $stuImageName = 'student-male.png';
    private $keysArray = [
        'id', 'username', 'email', 'role', 'first_name', 'middle_name', 'last_name', 'location',
        'birthday', 'gender', 'cin', 'language', 'groups', 'image_url', 'institute_id', 'institute_slug',
    ];

    public function testGetUserFireData() {

        $response = $this->client->get( "$this->uriPrefix/users/" . self::$username . '/fire-data' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetinstitutestudents() {

        $client = static::createClient();

        $crawler = $client->request( 'GET', '/getInstituteStudents' );
    }

    public function testGetinstituteprofessors() {

        $client = static::createClient();

        $crawler = $client->request( 'GET', '/getInstituteProfessors' );
    }

    public function testGetinstitutestaff() {

        $client = static::createClient();

        $crawler = $client->request( 'GET', '/getInstituteStaff' );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $em \Doctrine\ORM\EntityManager
         * @var User $user
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $image = new Image();
        $image->setName( self::$imageName );
        $image->setType( '' );
        $image->setEntity( 'web' );
        $em->persist( $image );

        $studentImage = new Image();
        $studentImage->setName( self::$stuImageName );
        $studentImage->setType( '' );
        $studentImage->setEntity( 'web' );
        $em->persist( $studentImage );

        $institute = new Institute();
        $institute->setName( 'Esprit' );
        $institute->setDescription( 'Esprit description' );
        $institute->setSlogan( 'Se former autrement' );
        $institute->setWebsite( 'http://www.esprit.com.tn' );
        $institute->setFoundationDate( new \DateTime( '2009-02-15' ) );
        $institute->setType( 'UNIVERSITY' );
        $institute->setParentalControl( 'NONE' );

        $address = new Location();
        $address->setCountry( 'tn' );
        $address->setState( 'Nabeul' );
        $address->setCity( 'Nabeul' );
        $address->setAddress( 'my address in some place!' );
        $address->setZipCode( '8000' );

        $user = $userManager->createUser();
        $user->setEmail( 'mail@kjhkj.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setMiddleName( 'middle name' );
        $user->setLastName( 'last name' );
        $user->setBirthday( new \DateTime( '1992-02-15' ) );
        $user->setLanguage( 'ar' );
        $user->setGender( 'male' );
        $user->setCin( '09799221' );
        $user->setRoles( [ 'ROLE_PROFESSOR' ] );
        $user->setLocation( $address );
        $user->setPlainPassword( 'password' );
        $user->setProfileImage( $image );
        $user->setEnabled( true );
        $token = self::getService( 'fos_user.util.token_generator' )->generateToken();
        $user->setConfirmationToken( $token );

        $userInstitute = new UserInstitute();
        $userInstitute->setUser( $user );
        $userInstitute->setInstitute( $institute );
        $userInstitute->setRole( $user->getRoles()[ 0 ] );

        $em->persist( $institute );
        $em->persist( $user );
        $em->persist( $userInstitute );
        $em->flush();

        $institute = $em->getRepository( 'App:Institute' )
            ->findOneBy( [ 'name' => 'Esprit' ] );
        self::$insId = $institute->getId();

        $user = $userManager->findUserByEmail( 'mail@kjhkj.com' );
        self::$id = $user->getId();
    }
}

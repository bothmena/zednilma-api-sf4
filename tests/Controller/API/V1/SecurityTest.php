<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 01/06/17
 * Time: 18:49
 */

namespace App\Tests\Controller;

use App\Entity\Location;
use App\Entity\User;
use App\Tests\Controller\API\APIGuzzleTestCase;

class SecurityTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/users';
    private static $username = 'bothmena';
    private static $email = 'email@islamm.com';
    private static $password = 'bothmena';
    private static $token;
    private static $tokenRefresh;
    private static $id;

    public function testGetSecuredUri() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password
            ]
        ] );

        $data = json_decode( $response->getBody(), true );

        $this->assertArrayHasKey( 'token', $data );
        $this->assertArrayHasKey( 'refresh_token', $data );

        self::$token = $data['token'];
        self::$tokenRefresh = $data['refresh_token'];
        $data = [ 'firstName' => 'patch first' ];

        $response = $this->client->patch( "$this->uriPrefix/" . self::$id, [
            'headers' => [
                'Authorization'     => sprintf('Bearer %s', self::$token )
            ],
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManagerInterface
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $address = new Location();
        $address->setCountry( 'tn' );
        $address->setState( 'Nabeul' );
        $address->setCity( 'Nabeul' );
        $address->setAddress( 'my address in some place!' );
        $address->setZipCode( '8000' );

        $user = $userManager->createUser();
        $user->setEmail( self::$email );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setMiddleName( 'middle name' );
        $user->setLastName( 'last name' );
        $user->setLocation( $address );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );

        $em->persist( $user );
        $em->flush();

        self::$id = $userManager->findUserByEmail( self::$email )->getId();
    }
}

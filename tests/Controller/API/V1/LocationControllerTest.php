<?php

namespace App\Tests\Tests\Controller;

use App\Entity\Location;
use App\Entity\Institute;
use App\Tests\Controller\API\APIGuzzleTestCase;

class LocationControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/institutes/';
    private static $id;
    private static $uniId;
    private $keysArray = [
        'id', 'country', 'state', 'city', 'address', 'zip_code', 'map_lat', 'map_long', 'is_primary'
    ];
    private $data = [
        'country' => 'GB',
        'state'   => 'London',
        'city'    => 'London',
        'address' => 'my london address and this should be long enough',
        'zipCode' => '4879',
        'mapLat' => 79.254087,
        'mapLong' => -101.258987,
        'isPrimary' => true,
    ];

    public function testGetLocation() {

        $response = $this->client->get( '/index_test.php/v1/locations/' . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $this->keysArray ), sizeof( $contentArray ) );
    }

    public function testGetInstituteLocations() {

        $response = $this->client->get( $this->uriPrefix . self::$uniId . '/locations' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        $this->assertEquals( 2, sizeof( $contentArray ) );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
        $this->assertEquals( sizeof( $this->keysArray ), sizeof( $contentArray[ 0 ] ) );
    }

    public function testNewInstituteLocation() {

        $response = $this->client->post( $this->uriPrefix . self::$uniId . '/locations' , [
            'body' => json_encode( $this->data ),
        ] );
        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchInstituteLocation() {

        $data = [
            'isPrimary' => 'true',
            'country' => 'GB',
        ];

        $response = $this->client->patch( $this->uriPrefix . self::$uniId . '/locations/' . self::$id, [
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutInstituteLocation() {

        $response = $this->client->put( $this->uriPrefix . self::$uniId . '/locations/' . self::$id, [
            'body' => json_encode( $this->data ),
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveInstituteLocation() {

        $response = $this->client->delete( $this->uriPrefix . self::$uniId . '/locations/' . self::$id );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $em \Doctrine\ORM\EntityManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );

        $institute = new Institute();
        $institute->setName( 'Esprit' );
        $institute->setDescription( 'Esprit description' );
        $institute->setSlogan( 'Se former autrement' );
        $institute->setWebsite( 'http://www.esprit.com.tn' );
        $institute->setFoundationDate( new \DateTime('2009-02-15' ) );

        $em->persist( $institute );

        $location = new Location();
        $location->setCountry( 'tn' );
        $location->setState( 'Tunis' );
        $location->setCity( 'Charguia' );
        $location->setAddress( 'my address in some place!' );
        $location->setZipCode( '2000' );
        $location->setInstitute( $institute );
        $location->setIsPrimary( true );

        $location2 = new Location();
        $location2->setCountry( 'tn' );
        $location2->setState( 'Nabeul' );
        $location2->setCity( 'Nabeul' );
        $location2->setAddress( 'my address in some place!' );
        $location2->setZipCode( '8000' );

        $location3 = new Location();
        $location3->setCountry( 'tn' );
        $location3->setState( 'Ariana' );
        $location3->setCity( 'Ghazela' );
        $location3->setAddress( 'my address in some place!' );
        $location3->setZipCode( '3000' );
        $location3->setInstitute( $institute );

        $em->persist( $location );
        $em->persist( $location2 );
        $em->persist( $location3 );
        $em->flush();

        $location = $em->getRepository( 'App:Location' )
            ->findOneBy( [ 'institute' => $institute ] );
        self::$id = $location ? $location->getId() : 0;
        self::$uniId = $location ? $location->getInstitute()->getId() : 0;
    }
}

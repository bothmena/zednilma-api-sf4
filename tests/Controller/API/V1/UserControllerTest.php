<?php

namespace App\Tests\Controller;

use App\Entity\Image;
use App\Entity\Institute;
use App\Entity\Location;
use App\Entity\User;
use App\Tests\Controller\API\APIGuzzleTestCase;

class UserControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/users';
    private static $username = 'test_user';
    private static $email = 'email@islamm.com';
    private static $password = 'mySEC92pass';
    private static $imageName = 'professor-male.png';
    private static $stuImageName = 'student-male.png';
    private static $token = 'qlkjsfdqsdf354qsd3f645sq3fd54';
    private static $id;
    private static $insId;
    private $keysArray = [
        'id', 'email', 'username', 'first_name', 'middle_name', 'last_name', 'birthday',
        'gender', 'image_url', 'cin', 'language', 'location', 'role', 'groups',
    ];
    private $data = [
        'email'       => 'edit_user@email.tn',
        'username'       => 'edit_username',
        'firstName'   => 'edit first',
        'middleName'  => 'edit middle',
        'lastName'    => 'edit last',
        'birthday'    => '1992-06-02',
        'gender'      => 'male',
        'cin'         => '09799210',
        'language'    => 'ar',
        'location'    => [
            'country'   => 'TN',
            'state'     => 'Nabeul',
            'city'      => 'Nabeul',
            'address'   => 'my edit address and this should be long enough',
            'zipCode'   => '8000',
        ],
    ];

    public function testGetUser() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        $this->assertEquals( $contentArray['image_url'], '/images/website/' . self::$imageName );
        $this->assertEquals( sizeof($contentArray), sizeof($this->keysArray) );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetByUsernameUser() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$username );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testUserExistByToken() {

        $response = $this->client->get( "$this->uriPrefix/t/" . self::$token . '?enabled=false' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );
        $this->assertArrayHasKey( 'exist', json_decode( $response->getBody(), true ) );
    }

    public function testGetUsers() {

        $response = $this->client->get( $this->uriPrefix );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testInitAccountUser() {

        $data = [
            'username'      => 'username',
            'plainPassword' => [
                'password'         => 'mySTRONG92pass',
                'confirm_password' => 'mySTRONG92pass',
            ],
            'firstName'     => 'first',
            'lastName'      => 'last',

            'birthday'      => '02/06/1992',
            'language'      => 'en',
            'gender'        => 'male',
            'cin'           => '09799210',
            'location'      => [
                'country' => 'TN',
                'state'   => 'Nabeul',
                'city'    => 'Nabeul',
                'address' => 'my edit address and this should be long enough',
                'zipCode' => '8000',
            ],
        ];

        $response = $this->client->post( $this->uriPrefix . '/init-account/' . self::$token, [
            'body' => json_encode( $data ),
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testNewUser() {

        $data = [
            'username'  => 'username',
            'email'     => 'amail@gmail.com',
            'roles'     => [
                'ROLE_STUDENT',
            ],
            'institute' => $this::$insId,
        ];

        $response = $this->client->post( $this->uriPrefix, [
            'body' => json_encode( $data ),
        ] );
        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testConfirmEmailUser() {

        $response = $this->client->get( "$this->uriPrefix/confirm-email/" . self::$token );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertContains( 'token', $response->getBody()->getContents() );
    }

    public function testResendCodeUser() {

        $response = $this->client->post( "$this->uriPrefix/resend-code/" . self::$email );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPatchUser() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );

        $authData = json_decode( $response->getBody(), true );

        $data = [
            // 'firstName' => 'patch first',
            // 'lastName' => 'patch last',
            'birthday' => '02/06/1992'
        ];

        $response = $this->client->patch( "$this->uriPrefix/" . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutUser() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->put( "$this->uriPrefix/" . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $this->data ),
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testChangePasswordUser() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $newPass = 'MyNew5ecPa55_#';

        $response = $this->client->post( "$this->uriPrefix/" . self::$id . '/change-password', [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( [
                'oldPassword' => self::$password,
                'plainPassword' => [
                    'password' => $newPass,
                    'confirm_password' => $newPass,
                ]
            ] ),
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => $newPass,
            ],
        ] );
        $this->assertEquals( 200, $response->getStatusCode() );
    }

    public function testResetPasswordUser() {

        $newPass = 'MyNew5ecPa55_#';
        $response = $this->client->post( "$this->uriPrefix/" . self::$token . '/reset-password', [
            'body'    => json_encode( [
                'plainPassword' => [
                    'password' => $newPass,
                    'confirm_password' => $newPass,
                ]
            ] ),
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => $newPass,
            ],
        ] );
        $this->assertEquals( 200, $response->getStatusCode() );
    }

    public function testRequestPassResetAction() {

        $response = $this->client->post( "$this->uriPrefix/" . self::$email . "/request-password-reset" );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $image = new Image();
        $image->setName( self::$imageName );
        $image->setType( '' );
        $image->setEntity( 'web' );
        $em->persist( $image );

        $studentImage = new Image();
        $studentImage->setName( self::$stuImageName );
        $studentImage->setType( '' );
        $studentImage->setEntity( 'web' );
        $em->persist( $studentImage );

        $institute = new Institute();
        $institute->setName( 'Esprit' );
        $institute->setDescription( 'Esprit description' );
        $institute->setSlogan( 'Se former autrement' );
        $institute->setWebsite( 'http://www.esprit.com.tn' );
        $institute->setFoundationDate( new \DateTime( '2009-02-15' ) );
        $institute->setType( 'UNIVERSITY' );
        $institute->setParentalControl( 'NONE' );

        $address = new Location();
        $address->setCountry( 'tn' );
        $address->setState( 'Nabeul' );
        $address->setCity( 'Nabeul' );
        $address->setAddress( 'my address in some place!' );
        $address->setZipCode( '8000' );

        $user = $userManager->createUser();
        $user->setEmail( self::$email );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setMiddleName( 'middle name' );
        $user->setLastName( 'last name' );
        $user->setBirthday( new \DateTime( '1992-02-15' ) );
        $user->setLanguage( 'ar' );
        $user->setGender( 'male' );
        $user->setCin( '09799221' );
        $user->setRoles( [ 'ROLE_PROFESSOR' ] );
        $user->setLocation( $address );
        $user->setPlainPassword( self::$password );
        $user->setProfileImage( $image );
        $user->setEnabled( true );
        $user->setConfirmationToken( self::$token );

        $em->persist( $institute );
        $em->persist( $user );
        $em->flush();

        $institute = $em->getRepository( 'App:Institute' )
            ->findOneBy( [ 'name' => 'Esprit' ] );
        self::$insId = $institute->getId();

        $user = $userManager->findUserByEmail( self::$email );
        self::$id = $user->getId();
    }
}

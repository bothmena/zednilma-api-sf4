<?php

namespace App\Tests\Controller;

use App\Entity\Institute;
use App\Entity\Speciality;
use App\Entity\User;
use App\Entity\UserInstitute;
use GuzzleHttp\Client;
use App\Tests\Controller\API\APIGuzzleTestCase;

class SpecialityControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/institutes/';
    private static $id;
    private static $insId;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = [
        "id", "institute_id", 'parent_id', "name", "alias", "description", "study_cycle", "years", "type", "date",
    ];
    private $data = [
        'name'        => 'Data Science',
        'alias'        => 'DS',
        'description' => 'this is the language that you should learn.',
        'years' => '2',
        'type' => 'BRANCH',
        'parent' => NULL,
    ];

    public function testGetSpeciality() {

        $response = $this->client->get( $this->uriPrefix . self::$insId . '/specialities/' . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $this->keysArray ), sizeof( $contentArray ) );
    }

    public function testGetSpecialities() {

        $response = $this->client->get( $this->uriPrefix . self::$insId . '/specialities' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewSpeciality() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $this->data['parent'] = self::$id;

        $response = $this->client->post( $this->uriPrefix . self::$insId . '/specialities', [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $this->data ),
        ] );
        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchSpeciality() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $data = [
            'years' => 4,
        ];

        $response = $this->client->patch( $this->uriPrefix . self::$insId . '/specialities/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutSpeciality() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $this->data[ 'name' ] = 'Business Intelligence';
        $this->data[ 'alias' ] = 'BI';
        $response = $this->client->put( $this->uriPrefix . self::$insId . '/specialities/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $this->data ),
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveSpeciality() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . self::$insId . '/specialities/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        $baseUri = getenv( 'TEST_BASE_URI' );
        parent::$staticClient = new Client( [
            'base_uri' => $baseUri,
            'defaults' => [
                'exceptions' => false,
            ],
        ] );
        self::bootKernel();

        /**
         * @var $em \Doctrine\ORM\EntityManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );

        $specialities = $em->getRepository('App:Speciality')->findAll();
        foreach ( array_reverse( $specialities ) as $speciality ) {
            $em->remove( $speciality );
        }
        $em->flush();

        parent::purgeDatabase();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $institute = new Institute();
        $institute->setName( 'Esprit' );
        $institute->setDescription( 'Esprit description' );
        $institute->setSlogan( 'Se former autrement' );
        $institute->setWebsite( 'http://www.esprit.com.tn' );
        $institute->setFoundationDate( new \DateTime( '2009-02-15' ) );

        $em->persist( $institute );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setCin( '09799221' );
        $user->setRoles( [ 'ROLE_INS_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $em->persist( $user );

        $userIns = new UserInstitute();
        $userIns->setUser( $user );
        $userIns->setInstitute( $institute );
        $userIns->setRole( 'ROLE_INS_ADMIN' );
        $em->persist( $userIns );

        $speciality = new Speciality();
        $speciality->setName( 'Ingenieur Informatique' );
        $speciality->setDescription( 'Ingenieur Informatique speciality Description' );
        $speciality->setYears( 3 );
        $speciality->setStudyCycle( 'ENGINEER_CYCLE' );
        $speciality->setType( 'SPECIALITY' );
        $speciality->setInstitute( $institute );
        $em->persist( $speciality );

        $speciality2 = new Speciality();
        $speciality2->setName( 'Cursus B' );
        $speciality2->setAlias( 'B' );
        $speciality2->setDescription( 'Ingenieur Informatique Cursus B speciality Description' );
        $speciality2->setYears( 2 );
        $speciality2->setType( 'BRANCH' );
        $speciality2->setInstitute( $institute );
        $speciality2->setParent( $speciality );
        $em->persist( $speciality2 );

        $em->flush();

        /**
         * @var $speciality Speciality
         */
        $speciality = $em->getRepository( 'App:Speciality' )->findOneBy( [ 'type' => 'SPECIALITY' ] );
        self::$id = $speciality->getId();
        self::$insId = $speciality->getInstitute()->getId();
    }
}

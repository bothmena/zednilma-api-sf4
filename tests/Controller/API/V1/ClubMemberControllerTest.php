<?php

namespace App\Tests\Controller;

use App\Entity\ClubMember;
use App\Tests\Controller\API\APIGuzzleTestCase;

class ClubMemberControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/ClubMembers';
    private static $id;
//    private static $slug;
    private $keysArray = [
        'context', 'id',
    ];
    private $data = [
        'field1' => 'value1',
        'field2' => 'value2',
    ];

    public function testGetClubMember() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetClubMembers() {

        $response = $this->client->get( $this->uriPrefix );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewClubMember() {

        $response = $this->client->post( $this->uriPrefix, [
            'body' => json_encode( $this->data ),
        ] );
        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchClubMember() {

        $data = [
            'field' => 'value',
        ];

        $response = $this->client->patch( "$this->uriPrefix/" . self::$id, [
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutClubMember() {

        $response = $this->client->put( "$this->uriPrefix/" . self::$id, [
            'body' => json_encode( $this->data ),
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveClubMember() {

        $response = $this->client->delete( "$this->uriPrefix/" . self::$id );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $em \Doctrine\ORM\EntityManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );

//        $entity = new Entity();
//        setters...

//        $em->persist( $entity );
        $em->flush();

        $institute = $em->getRepository( 'App:Entity' )
            ->findOneBy( [ 'name' => 'Esprit' ] );
        self::$id = $institute->getId();
//        self::$slug = $institute->getSlug();
    }
}

<?php

namespace App\Tests\Controller;

use App\Entity\Institute;
use App\Entity\SocialProfile;
use App\Entity\User;
use App\Entity\UserInstitute;
use App\Tests\Controller\API\APIGuzzleTestCase;

class SocialProfileInstituteControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/institutes/';
    private static $profileId;
    private static $insId;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = [
        'context', 'id', 'institute_id', 'user_id', 'website', 'username', 'url', 'date',
    ];
    private $data = [
        'url'     => 'https://www.linkedin.com/in/bothmena',
        'website' => 'linkedin',
        'username' => 'bothmena',
    ];

    public function testGetInstituteSocialProfile() {

        $response = $this->client->get( $this->uriPrefix . self::$insId . '/social-profiles/' . self::$profileId );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetInstituteSocialProfiles() {

        $response = $this->client->get( $this->uriPrefix . self::$insId . '/social-profiles' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewInstituteSocialProfile() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->post( $this->uriPrefix . self::$insId . '/social-profiles', [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $this->data ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchInstituteSocialProfile() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $data = [
            'url' => 'https://www.twitter.com/bothmena.2',
        ];

        $response = $this->client->patch( $this->uriPrefix . self::$insId . '/social-profiles/' . self::$profileId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutInstituteSocialProfile() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->put( $this->uriPrefix . self::$insId . '/social-profiles/' . self::$profileId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body'    => json_encode( $this->data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveInstituteSocialProfile() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . self::$insId . '/social-profiles/' . self::$profileId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $institute = new Institute();
        $institute->setName( 'Esprit' );
        $institute->setDescription( 'Esprit description' );
        $institute->setSlogan( 'Se former autrement' );
        $institute->setWebsite( 'http://www.esprit.com.tn' );
        $institute->setFoundationDate( new \DateTime( '2009-02-15' ) );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setCin( '09799221' );
        $user->setRoles( [ 'ROLE_INS_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );

        $userIns = new UserInstitute();
        $userIns->setUser( $user );
        $userIns->setInstitute( $institute );
        $userIns->setRole( 'ROLE_INS_ADMIN' );

        $em->persist( $user );
        $em->persist( $institute );
        $em->persist( $userIns );

        $profile = new SocialProfile();
        $profile->setWebsite( 'twitter' );
        $profile->setUsername( 'bothmena' );
        $profile->setUrl( 'https://www.twitter.com/bothmena' );
        $profile->setInstitute( $institute );

        $profile2 = new SocialProfile();
        $profile2->setWebsite( 'facebook' );
        $profile2->setUsername( 'bothmena' );
        $profile2->setUrl( 'https://www.facebook.com/bothmena' );
        $profile2->setInstitute( $institute );

        $em->persist( $profile );
        $em->persist( $profile2 );
        $em->flush();

        $profile = $em->getRepository( 'App:SocialProfile' )
            ->findOneBy( [ 'institute' => $institute ] );
        self::$profileId = $profile->getId();
        self::$insId = $profile->getInstitute()->getId();
    }
}

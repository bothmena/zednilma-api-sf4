<?php

namespace App\Tests\Controller;

use App\Entity\Department;
use App\Entity\Infrastructure;
use App\Entity\Institute;
use App\Entity\User;
use App\Entity\UserInstitute;
use GuzzleHttp\Client;
use App\Tests\Controller\API\APIGuzzleTestCase;

class InfrastructureControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/institutes/';
    private static $blockId;
    private static $floorId;
    private static $classId;
    private static $insId;
    private static $insSlug;
    private static $depId;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = [
        "id", "institute_id", "parent_id", "department_id", "name", "alias","index", "capacity", "type", "date"
    ];

    public function testGetInfrastructure() {

        $response = $this->client->get( $this->uriPrefix . self::$insId . '/infrastructures/' . self::$classId );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $this->keysArray ), sizeof( $contentArray ) );
    }

    public function testGetInfrastructures() {

        $response = $this->client->get( $this->uriPrefix . self::$insId . '/infrastructures' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testGetInfrastructuresByInsSlug() {

        $response = $this->client->get( $this->uriPrefix . self::$insSlug . '/infrastructures' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testInfrastructureAutoNaming() {

        $block = new Infrastructure();
        $block->setName( 'Block H' );
        $block->setType( 'BLOCK' );
        $block->setAlias( 'H' );

        $floor = new Infrastructure();
        $floor->setType( 'FLOOR' );
        $floor->setIndex( 0 );
        $floor->setAlias( '5' );
        $floor->setParent( $block );
        $floor->onPrePersist();
        $this->assertEquals( $floor->getName(), 'Ground floor' );

        $floor->setIndex( 5 );
        $floor->onPrePersist();
        $this->assertEquals( $floor->getName(), 'B-H Floor 5' );

        $class = new Infrastructure();
        $class->setIndex( 9 );
        $class->setParent( $floor );
        $class->onPrePersist();
        $this->assertEquals( $class->getName(), 'H509' );
        $class->setIndex( 29 );
        $class->onPrePersist();
        $this->assertEquals( $class->getName(), 'H529' );
        $class->setIndex( 149 );
        $class->onPrePersist();
        $this->assertEquals( $class->getName(), 'H5149' );
    }

    public function testNewInfrastructure() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        // BLOCK, FLOOR, [CLASSROOM, LABORATORY, WORKSHOP, LIBRARY]
        $blockData = [
            'name' => 'Block H',
            'alias' => 'H',
            'type' => 'BLOCK',
        ];
        $floorData = [
            'name' => 'Etage',
            'alias' => '3',
            'index' => 3,
            'parent' => self::$blockId,
            'type' => 'FLOOR',
        ];
        $classData = [
            'name' => 'H302',
            'index' => 2,
            'capacity' => 15,
            'type' => 'CLASSROOM',
            'department' => self::$depId,
            'parent' => self::$floorId,
        ];

        $response = $this->client->post( $this->uriPrefix . self::$insId . '/infrastructures', [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $floorData ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );

        $response = $this->client->post( $this->uriPrefix . self::$insId . '/infrastructures', [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $blockData ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );

        $response = $this->client->post( $this->uriPrefix . self::$insId . '/infrastructures', [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $classData ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchInfrastructure() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->patch( $this->uriPrefix . self::$insId . '/infrastructures/' . self::$classId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( [ 'name' => 'H106', 'index' => 6 ] ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutInfrastructure() {

        $data = [
            'name' => 'Floor',
            'alias' => '2',
            'index' => 2,
            'parent' => self::$blockId,
            'type' => 'FLOOR',
        ];
        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->put( $this->uriPrefix . self::$insId . '/infrastructures/' . self::$floorId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveInfrastructure() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . self::$insId . '/infrastructures/' . self::$floorId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ]
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );

        $response = $this->client->delete( $this->uriPrefix . self::$insId . '/infrastructures/' . self::$blockId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ]
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        $baseUri = getenv( 'TEST_BASE_URI' );
        parent::$staticClient = new Client( [
            'base_uri' => $baseUri,
            'defaults' => [
                'exceptions' => false,
            ],
        ] );
        self::bootKernel();

        /**
         * @var $em \Doctrine\ORM\EntityManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );

        $infras = $em->getRepository('App:Infrastructure')->findAll();
        foreach ( array_reverse( $infras ) as $infra ) {
            $em->remove( $infra );
        }
        $em->flush();

        parent::purgeDatabase();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $institute = new Institute();
        $institute->setName( 'Esprit' );
        $institute->setDescription( 'Esprit description' );
        $institute->setSlogan( 'Se former autrement' );
        $institute->setWebsite( 'http://www.esprit.com.tn' );
        $institute->setFoundationDate( new \DateTime('2009-02-15' ) );

        $em->persist( $institute );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setCin( '09799221' );
        $user->setRoles( [ 'ROLE_INS_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $em->persist( $user );

        $userIns = new UserInstitute();
        $userIns->setUser($user);
        $userIns->setInstitute($institute);
        $userIns->setRole('ROLE_INS_ADMIN');
        $em->persist( $userIns );

        $department = new Department();
        $department->setInstitute( $institute );
        $department->setName( 'Departement Management' );
        $department->setDescription( 'Departement Management little description...' );
        $em->persist( $department );

        $block = new Infrastructure();
        $block->setInstitute( $institute );
        $block->setName( 'Block H' );
        $block->setAlias( 'H' );
        $block->setType( 'BLOCK' );
        $em->persist( $block );

        $floor = new Infrastructure();
        $floor->setInstitute( $institute );
        $floor->setParent( $block );
        $floor->setName( 'Etage 1' );
        $floor->setAlias( '1' );
        $floor->setType( 'FLOOR' );
        $floor->setIndex( 1 );
        $em->persist( $floor );

        $class = new Infrastructure();
        $class->setInstitute( $institute );
        $class->setDepartment( $department );
        $class->setParent( $floor );
        $class->setName( 'H105' );
        $class->setType( 'CLASSROOM' );
        $class->setIndex( 5 );
        $class->setCapacity( 30 );
        $em->persist( $class );

        $em->flush();

        /**
         * @var $class Infrastructure
         */
        $class = $em->getRepository( 'App:Infrastructure' )
            ->findOneBy( [ 'type' => 'CLASSROOM' ] );
        self::$classId = $class->getId();
        self::$insId = $class->getInstitute()->getId();
        self::$insSlug = $class->getInstitute()->getSlug();
        self::$depId = $class->getDepartment()->getId();
        self::$blockId = $em->getRepository( 'App:Infrastructure' )
            ->findOneBy( [ 'type' => 'BLOCK' ] )->getId();
        self::$floorId = $em->getRepository( 'App:Infrastructure' )
            ->findOneBy( [ 'type' => 'FLOOR' ] )->getId();
    }
}

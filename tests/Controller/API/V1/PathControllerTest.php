<?php

namespace App\Tests\Controller;

use App\Entity\Institute;
use App\Entity\Path;
use App\Entity\PathBranch;
use App\Entity\Speciality;
use App\Entity\User;
use App\Entity\UserInstitute;
use GuzzleHttp\Client;
use App\Tests\Controller\API\APIGuzzleTestCase;

class PathControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/institutes/';
    private static $id;
    private static $specId;
    private static $insId;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = [
        "id", "speciality_id", "name", "description", "date"
    ];
    private $data = [
        'name' => 'English',
        'description' => 'this is the language that you should learn.',
        'coefficient' => '2.95',
    ];

    public function testGetPath() {

        $response = $this->client->get( $this->uriPrefix . self::$insId . '/specialities/' . self::$specId . '/paths/' . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $this->keysArray ), sizeof( $contentArray ) );
    }

    public function testGetPaths() {

        $response = $this->client->get( $this->uriPrefix . self::$insId . '/specialities/' . self::$specId . '/paths' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( ['id','name','description','date','children'] as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ self::$id ] );
        }
        $this->assertEquals( sizeof( $contentArray[ self::$id ] ), 5 );
    }

    public function testNewPath() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->post( $this->uriPrefix . self::$insId . '/specialities/' . self::$specId . '/paths', [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( [
                'name' => 'New Test Path',
                'description' => 'New Test Path little description.',
            ] ),
        ] );
        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchPath() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->patch( $this->uriPrefix . self::$insId . '/specialities/' . self::$specId . '/paths/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( [
                'name' => 'edited path name'
            ] ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutPath() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $this->data['coefficient'] = '5.3';
        $response = $this->client->put( $this->uriPrefix . self::$insId . '/specialities/' . self::$specId . '/paths/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( [
                'name' => 'edited path name',
                'description' => 'New edited path name Path little description.',
            ] ),
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemovePath() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . self::$insId . '/specialities/' . self::$specId . '/paths/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        $baseUri = getenv( 'TEST_BASE_URI' );
        parent::$staticClient = new Client( [
            'base_uri' => $baseUri,
            'defaults' => [
                'exceptions' => false,
            ],
        ] );
        parent::bootKernel();

        /**
         * @var $em \Doctrine\ORM\EntityManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );

        $specialities = $em->getRepository('App:PathBranch')->findAll();
        foreach ( array_reverse( $specialities ) as $speciality ) {
            $em->remove( $speciality );
        }
        $em->flush();

        $specialities = $em->getRepository('App:Path')->findAll();
        foreach ( array_reverse( $specialities ) as $speciality ) {
            $em->remove( $speciality );
        }
        $em->flush();

        $specialities = $em->getRepository('App:Speciality')->findAll();
        foreach ( array_reverse( $specialities ) as $speciality ) {
            $em->remove( $speciality );
        }
        $em->flush();

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $institute = new Institute();
        $institute->setName( 'Esprit' );
        $institute->setDescription( 'Esprit description' );
        $institute->setSlogan( 'Se former autrement' );
        $institute->setWebsite( 'http://www.esprit.com.tn' );
        $institute->setFoundationDate( new \DateTime( '2009-02-15' ) );

        $em->persist( $institute );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setCin( '09799221' );
        $user->setRoles( [ 'ROLE_INS_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $em->persist( $user );

        $userIns = new UserInstitute();
        $userIns->setUser( $user );
        $userIns->setInstitute( $institute );
        $userIns->setRole( 'ROLE_INS_ADMIN' );
        $em->persist( $userIns );

        $speciality = new Speciality();
        $speciality->setInstitute( $institute );
        $speciality->setName( 'Ing Informatique' );
        $speciality->setType('SPECIALITY');
        $speciality->setYears(3);
        $speciality->setDescription( 'Speciality Informatique little description...' );
        $em->persist( $speciality );

        $cursusB3 = new Speciality();
        $cursusB3->setInstitute( $institute );
        $cursusB3->setParent( $speciality );
        $cursusB3->setName( 'Cursus B ( 3B )' );
        $cursusB3->setType('BRANCH');
        $cursusB3->setYears(1);
        $em->persist( $cursusB3 );

        $cursusB4 = new Speciality();
        $cursusB4->setInstitute( $institute );
        $cursusB4->setParent( $speciality );
        $cursusB4->setName( 'Cursus B ( 4info )' );
        $cursusB4->setType('BRANCH');
        $cursusB4->setYears(1);
        $em->persist( $cursusB4 );

        $bi = new Speciality();
        $bi->setInstitute( $institute );
        $bi->setParent( $speciality );
        $bi->setName( 'Business Intelligence' );
        $bi->setType('OPTION');
        $bi->setYears(1);
        $em->persist( $bi );

        $opWeb = new Speciality();
        $opWeb->setInstitute( $institute );
        $opWeb->setParent( $speciality );
        $opWeb->setName( 'Option Web Dev' );
        $opWeb->setType('OPTION');
        $opWeb->setYears(1);
        $em->persist( $opWeb );

        $ds = new Speciality();
        $ds->setInstitute( $institute );
        $ds->setParent( $speciality );
        $ds->setName( 'Date Science' );
        $ds->setType('BRANCH');
        $ds->setYears(2);
        $em->persist( $ds );

        $cursusA = new Speciality();
        $cursusA->setInstitute( $institute );
        $cursusA->setParent( $speciality );
        $cursusA->setName( 'Cursus A' );
        $cursusA->setType('BRANCH');
        $cursusA->setYears(1);
        $em->persist( $cursusA );

        $nids = new Speciality();
        $nids->setInstitute( $institute );
        $nids->setParent( $speciality );
        $nids->setName( 'NIDS' );
        $nids->setType('BRANCH');
        $nids->setYears(2);
        $em->persist( $nids );

        $path1 = new Path();
        $path1->setName( 'Data Science From Cursus B' );
        $path1->setSpeciality( $speciality );
        $em->persist( $path1 );

        $path2 = new Path();
        $path2->setName( 'Data Science From Cursus A' );
        $path2->setSpeciality( $speciality );
        $em->persist( $path2 );

        $path3 = new Path();
        $path3->setName( 'Option Web Dev CB' );
        $path3->setSpeciality( $speciality );
        $em->persist( $path3 );

        $path2Brach1 = new PathBranch();
        $path2Brach1->setBranch( $cursusA );
        $path2Brach1->setPath($path2);
        $path2Brach1->setIndex( 1 );
        $em->persist( $path2Brach1 );

        $path3Brach1 = new PathBranch();
        $path3Brach1->setBranch( $cursusB3 );
        $path3Brach1->setPath($path3);
        $path3Brach1->setIndex( 1 );
        $em->persist( $path3Brach1 );

        $path1Brach2 = new PathBranch();
        $path1Brach2->setBranch( $ds );
        $path1Brach2->setPath($path1);
        $path1Brach2->setIndex( 2 );
        $em->persist( $path1Brach2 );

        $path3Brach3 = new PathBranch();
        $path3Brach3->setBranch( $opWeb );
        $path3Brach3->setPath($path3);
        $path3Brach3->setIndex( 3 );
        $em->persist( $path3Brach3 );

        $path1Brach1 = new PathBranch();
        $path1Brach1->setBranch( $cursusB3 );
        $path1Brach1->setPath($path1);
        $path1Brach1->setIndex( 1 );
        $em->persist( $path1Brach1 );

        $path3Brach2 = new PathBranch();
        $path3Brach2->setBranch( $cursusB4 );
        $path3Brach2->setPath($path3);
        $path3Brach2->setIndex( 2 );
        $em->persist( $path3Brach2 );

        $path2Brach2 = new PathBranch();
        $path2Brach2->setBranch( $ds );
        $path2Brach2->setPath($path2);
        $path2Brach2->setIndex( 2 );
        $em->persist( $path2Brach2 );

        $path4 = new Path();
        $path4->setName( 'Option BI CB' );
        $path4->setSpeciality( $speciality );
        $em->persist( $path4 );

        $path4Brach3 = new PathBranch();
        $path4Brach3->setBranch( $bi );
        $path4Brach3->setPath($path4);
        $path4Brach3->setIndex( 3 );
        $em->persist( $path4Brach3 );

        $path4Brach2 = new PathBranch();
        $path4Brach2->setBranch( $cursusB4 );
        $path4Brach2->setPath($path4);
        $path4Brach2->setIndex( 2 );
        $em->persist( $path4Brach2 );

        $path4Brach1 = new PathBranch();
        $path4Brach1->setBranch( $cursusB3 );
        $path4Brach1->setPath($path4);
        $path4Brach1->setIndex( 1 );
        $em->persist( $path4Brach1 );

        $path5 = new Path();
        $path5->setName( 'NIDS Path' );
        $path5->setSpeciality( $speciality );
        $em->persist( $path5 );

        $path5Brach2 = new PathBranch();
        $path5Brach2->setBranch( $nids );
        $path5Brach2->setPath($path5);
        $path5Brach2->setIndex( 2 );
        $em->persist( $path5Brach2 );

        $path5Brach1 = new PathBranch();
        $path5Brach1->setBranch( $cursusA );
        $path5Brach1->setPath($path5);
        $path5Brach1->setIndex( 1 );
        $em->persist( $path5Brach1 );

        $em->flush();

        /**
         * @var $path Path
         */
        $path = $em->getRepository( 'App:Path' )->findOneBy( [ 'name' => 'Data Science From Cursus A' ] );
        self::$id = $path->getId();
        self::$specId = $path->getSpeciality()->getId();
        self::$insId = $path->getSpeciality()->getInstitute()->getId();
    }
}

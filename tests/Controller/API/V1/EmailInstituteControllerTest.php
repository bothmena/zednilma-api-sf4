<?php

namespace App\Tests\Controller;

use App\Entity\Institute;
use App\Entity\Email;
use App\Entity\User;
use App\Entity\UserInstitute;
use App\Tests\Controller\API\APIGuzzleTestCase;

class EmailInstituteControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/institutes';
    private static $emailId;
    private static $userId;
    private static $insId;
    private static $token;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = [
        'id', 'institute_id', 'user_id', 'email', 'is_primary', 'is_verified', 'role', 'date'
    ];
    private $data = [
        'email' => 'email@yahoo.fr',
        'isPrimary' => true,
        'role' => 'Service Examens',
    ];

    public function testGetInstituteEmail() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$insId . '/emails/' . self::$emailId );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetInstituteEmails() {

        $response = $this->client->get( "$this->uriPrefix/" . self::$insId . '/emails' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewInstituteEmail() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->post( "$this->uriPrefix/" . self::$insId . '/emails', [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $this->data ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchInstituteEmail() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $data = [
            'isPrimary' => true,
            'email' => 'patched@gmail.com',
        ];

        $response = $this->client->patch( "$this->uriPrefix/" . self::$insId . '/emails/' . self::$emailId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutInstituteEmail() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $this->data['email'] = 'edited@gmail.com';

        $response = $this->client->put( "$this->uriPrefix/" . self::$insId . '/emails/' . self::$emailId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $this->data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testConfirmInstituteEmail() {

        $response = $this->client->get( '/index_test.php/v1/confirm-email/' . self::$token );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveInstituteEmail() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( "$this->uriPrefix/" . self::$insId . '/emails/' . ++self::$emailId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $institute = new Institute();
        $institute->setName( 'Esprit' );
        $institute->setDescription( 'Esprit description' );
        $institute->setSlogan( 'Se former autrement' );
        $institute->setWebsite( 'http://www.esprit.com.tn' );
        $institute->setFoundationDate( new \DateTime('2009-02-15' ) );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setCin( '09799221' );
        $user->setRoles( [ 'ROLE_INS_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );

        $userIns = new UserInstitute();
        $userIns->setUser($user);
        $userIns->setInstitute($institute);
        $userIns->setRole('ROLE_INS_ADMIN');

        $em->persist( $user );
        $em->persist( $institute );
        $em->persist( $userIns );

        $uniEmail = new Email();
        $uniEmail->setEmail('institute@gmail.com');
        $uniEmail->setRole('Service Scolarité');
        $uniEmail->setInstitute($institute);
        $uniEmail->setIsPrimary(true);

        $uniEmail2 = new Email();
        $uniEmail2->setEmail('institute2@gmail.com');
        $uniEmail2->setRole('Service Emploi');
        self::$token = self::getService( 'fos_user.util.token_generator' )->generateToken();
        $uniEmail2->setConfirmationToken( self::$token );
        $uniEmail2->setInstitute($institute);

        $em->persist( $uniEmail );
        $em->persist( $uniEmail2 );
        $em->flush();

        $uniEmail = $em->getRepository( 'App:Email')->findOneBy( [ 'email' => 'institute@gmail.com' ] );
        $userIns = $em->getRepository( 'App:UserInstitute')->findOneBy( [ 'role' => 'ROLE_INS_ADMIN' ] );
        self::$emailId = $uniEmail->getId();
        self::$insId = $userIns->getInstitute()->getId();
        self::$userId = $userIns->getUser()->getId();
    }
}

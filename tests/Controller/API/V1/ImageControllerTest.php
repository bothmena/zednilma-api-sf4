<?php

namespace App\Tests\Controller\API\V1;

use App\Entity\Article;
use App\Entity\Image;
use App\Entity\Institute;
use App\Entity\Paragraph;
use App\Entity\User;
use App\Tests\Controller\API\APIGuzzleTestCase;

/**
 * Created by PhpStorm.
 * User: Aymen Ben Othmen
 * Date: 08/09/16
 * Time: 08:15
 */
class ImageControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/images';
    private static $id;
    private static $userId;
    private static $instituteId;
    private $imageArray = [
        'id',
        'name',
        'entity',
        'type',
        'date',
    ];

    public function testGetImage() {

        $response = $this->client->get("$this->uriPrefix/" . self::$id);

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue($response->hasHeader('Content-Type'));
        $this->assertEquals( $response->getHeader('Content-Type')[0], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->imageArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetImages() {

        $response = $this->client->get($this->uriPrefix);

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue($response->hasHeader('Content-Type'));
        $this->assertEquals( $response->getHeader('Content-Type')[0], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->imageArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[0] );
        }
    }

    public function testNewImage() {

        $path = '/home/bothmena/Pictures/screenshot.png';
        $entities = [ 'user' => self::$userId, 'institute' => self::$instituteId ];

        foreach ( $entities as $entity => $id ) {

            $url = $this->uriPrefix . '/' . $entity . '/' . $id . '/prf';
            $response = $this->client->request('POST', $url, [
                'multipart' => [
                    /*[
                        'name'     => 'foo',
                        'contents' => 'data',
                    ],*/
                    [
                        'name'     => 'app_image[file]',
                        'contents' => fopen($path, 'r'),
                        'filename' => 'screenshot.png'
                    ],
                ]
            ]);

            $this->assertEquals( 201, $response->getStatusCode() );
            $contentArray = json_decode( $response->getBody(), true );
            foreach ( $this->imageArray as $key ) {
                $this->assertArrayHasKey( $key, $contentArray );
            }
        }
    }

    public function testEditPatchImage() {

        $data = array(
            'entity' => 'usr',
        );

        $response = $this->client->patch("$this->uriPrefix/" . self::$id, [
            'body'=>json_encode($data)
        ]);
        $this->assertEquals(204, $response->getStatusCode());
    }

    public function testEditPutImage() {

        $data = array(
            'entity' => 'usr',
            'type' => 'cvr',
        );

        $response = $this->client->put("$this->uriPrefix/" . self::$id, [
            'body'=>json_encode($data)
        ]);
        $this->assertEquals(204, $response->getStatusCode());
    }

    public function testRemoveImage() {

        $response = $this->client->delete( "$this->uriPrefix/" . self::$id );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService('doctrine.orm.entity_manager');
        $userManager = self::getService( 'fos_user.user_manager' );

        $institute = new Institute();
        $institute->setName( 'Esprit' );
        $institute->setDescription( 'Esprit description' );
        $institute->setSlogan( 'Se former autrement' );
        $institute->setWebsite( 'http://www.esprit.com.tn' );
        $institute->setFoundationDate( new \DateTime( '2009-02-15' ) );
        $institute->setType( 'UNIVERSITY' );
        $institute->setParentalControl( 'NONE' );

        $user = $userManager->createUser();
        $user->setEmail( 'art_username@gmail.com' );
        $user->setUsername( 'art_username' );
        $user->setFirstName( 'art name' );
        $user->setLastName( 'art last name' );
        $user->setCin( '09799221' );
        $user->setRoles( [ 'ROLE_PROFESSOR' ] );
        $user->setEnabled(true);
        $user->setPlainPassword( 'password' );

        $name = 'A713RRJfX4nJSia5609O1t2jAkaqrLsfKFGzTR7oQsc.png';
        $image = new Image();
        $image->setName($name);
        $image->setType('prf');
        $image->setEntity('ins');

        $em->persist( $user );
        $em->persist( $institute );
        $em->persist( $image );
        $em->flush();

        $image = $em->getRepository( 'App:Image' )->findOneBy(['name'=>$name]);
        self::$id = $image ? $image->getId() : 0;

        $institute = $em->getRepository( 'App:Institute' )
            ->findOneBy( [ 'name' => 'Esprit' ] );
        self::$instituteId = $institute ? $institute->getId() : 0;

        $user = $userManager->findUserByEmail('art_username@gmail.com');
        self::$userId = $user ? $user->getId() : 0;
    }
}

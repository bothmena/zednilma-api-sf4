<?php

namespace App\Tests\Controller;

use App\Entity\Department;
use App\Entity\Institute;
use App\Entity\Subject;
use App\Entity\User;
use App\Entity\UserInstitute;
use App\Tests\Controller\API\APIGuzzleTestCase;

class SubjectControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/institutes/';
    private static $id;
    private static $depId;
    private static $insId;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = [
        "id", "department_id", "name", "description", "coefficient", "date",
    ];
    private $data = [
        'name' => 'English',
        'description' => 'this is the language that you should learn.',
        'coefficient' => '2.95',
    ];

    public function testGetSubject() {

        $response = $this->client->get( $this->uriPrefix . self::$insId . '/departments/' . self::$depId . '/subjects/' . self::$id );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
        $this->assertEquals( sizeof( $this->keysArray ), sizeof( $contentArray ) );
    }

    public function testGetSubjects() {

        $response = $this->client->get( $this->uriPrefix . self::$insId . '/departments/' . self::$depId . '/subjects' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewSubject() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->post( $this->uriPrefix . self::$insId . '/departments/' . self::$depId . '/subjects', [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $this->data ),
        ] );
        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchSubject() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $data = [
            'coefficient' => 1.3,
        ];

        $response = $this->client->patch( $this->uriPrefix . self::$insId . '/departments/' . self::$depId . '/subjects/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutSubject() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $this->data['coefficient'] = '5.3';
        $response = $this->client->put( $this->uriPrefix . self::$insId . '/departments/' . self::$depId . '/subjects/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $this->data ),
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveSubject() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( $this->uriPrefix . self::$insId . '/departments/' . self::$depId . '/subjects/' . self::$id, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
        ] );
        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $institute = new Institute();
        $institute->setName( 'Esprit' );
        $institute->setDescription( 'Esprit description' );
        $institute->setSlogan( 'Se former autrement' );
        $institute->setWebsite( 'http://www.esprit.com.tn' );
        $institute->setFoundationDate( new \DateTime( '2009-02-15' ) );

        $em->persist( $institute );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setCin( '09799221' );
        $user->setRoles( [ 'ROLE_INS_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );
        $em->persist( $user );

        $userIns = new UserInstitute();
        $userIns->setUser( $user );
        $userIns->setInstitute( $institute );
        $userIns->setRole( 'ROLE_INS_ADMIN' );
        $em->persist( $userIns );

        $department = new Department();
        $department->setInstitute( $institute );
        $department->setName( 'Departement Management' );
        $department->setDescription( 'Departement Management little description...' );
        $em->persist( $department );

        $subject = new Subject();
        $subject->setName( 'Electronique Analogique' );
        $subject->setCoefficient( '3.5' );
        $subject->setDescription( 'this is the subject description, and its a ton of bullshit!.' );
        $subject->setDepartment( $department );
        $em->persist( $subject );

        $em->flush();

        /**
         * @var $subject Subject
         */
        $subject = $em->getRepository( 'App:Subject' )->findOneBy( [ 'name' => 'Electronique Analogique' ] );
        self::$id = $subject->getId();
        self::$depId = $subject->getDepartment()->getId();
        self::$insId = $subject->getDepartment()->getInstitute()->getId();
    }
}

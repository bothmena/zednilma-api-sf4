<?php

namespace App\Tests\Controller;

use App\Entity\User;
use App\Entity\Phone;
use App\Tests\Controller\API\APIGuzzleTestCase;

class PhoneUserControllerTest extends APIGuzzleTestCase {

    private $uriPrefix = '/index_test.php/v1/users';
    private static $phoneId;
    private static $userId;
    private static $code;
    private static $username = 'username';
    private static $password = 'my92PASS';
    private $keysArray = [
        'id', 'institute_id', 'user_id', 'is_verified', 'type', 'ccode', 'number', 'role', 'date'
    ];
    private $data = [
        'type'       => 'MBL',
        'ccode'      => '00216',
        'number'     => '55 404 157',
        'role'     => 'Director Office',
    ];

    public function testGetUserPhone() {

        $response = $this->client->get( $this->uriPrefix . '/' . self::$userId . '/phones/' . self::$phoneId );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray );
        }
    }

    public function testGetUserPhones() {

        $response = $this->client->get( $this->uriPrefix . '/' . self::$userId . '/phones' );

        $this->assertEquals( 200, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) );
        $this->assertEquals( $response->getHeader( 'Content-Type' )[ 0 ], 'application/json' );

        $contentArray = json_decode( $response->getBody(), true );
        foreach ( $this->keysArray as $key ) {
            $this->assertArrayHasKey( $key, $contentArray[ 0 ] );
        }
    }

    public function testNewUserPhone() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->post( "$this->uriPrefix/" . self::$userId . '/phones', [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $this->data ),
        ] );

        $this->assertEquals( 201, $response->getStatusCode() );
        $this->assertTrue( $response->hasHeader( 'Location' ) );
    }

    public function testEditPatchUserPhone() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $data = [
            'number' => '98 434 012',
        ];

        $response = $this->client->patch( "$this->uriPrefix/" . self::$userId . '/phones/' . self::$phoneId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testEditPutUserPhone() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );
        $this->data[ 'role' ] = 'Aymen Ben Othmen\'s Office';

        $response = $this->client->put( "$this->uriPrefix/" . self::$userId . '/phones/' . self::$phoneId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
            'body' => json_encode( $this->data ),
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testConfirmUserPhone() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->post( "$this->uriPrefix/" . self::$userId . '/phones/' . self::$phoneId . '/' . self::$code, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public function testRemoveUserPhone() {

        $response = $this->client->post( '/index_test.php/login_check', [
            'form_params' => [
                '_username' => self::$username,
                '_password' => self::$password,
            ],
        ] );
        $authData = json_decode( $response->getBody(), true );

        $response = $this->client->delete( "$this->uriPrefix/" . self::$userId . '/phones/' . self::$phoneId, [
            'headers' => [
                'Authorization' => sprintf( 'Zednilma %s', $authData[ 'token' ] ),
            ],
        ] );

        $this->assertEquals( 204, $response->getStatusCode() );
    }

    public static function setUpBeforeClass() {

        parent::setUpBeforeClass();
        self::loadFixtures();
    }

    public static function loadFixtures() {

        /**
         * @var $user User
         * @var $em \Doctrine\ORM\EntityManager
         * @var $userManager \FOS\UserBundle\Model\UserManager
         */
        $em = self::getService( 'doctrine.orm.entity_manager' );
        $userManager = self::getService( 'fos_user.user_manager' );

        $user = $userManager->createUser();
        $user->setEmail( 'usermail@gmail.com' );
        $user->setUsername( self::$username );
        $user->setFirstName( 'first name' );
        $user->setLastName( 'last name' );
        $user->setCin( '09799221' );
        $user->setRoles( [ 'ROLE_INS_ADMIN' ] );
        $user->setPlainPassword( self::$password );
        $user->setEnabled( true );

        $em->persist( $user );

        $phone1 = new Phone();
        $phone1->setUser( $user );
        $phone1->setType( 'MBL' );
        $phone1->setCcode( '00216' );
        $phone1->setNumber( '50 434 012' );
        $phone1->setVerificationCode();
        self::$code = $phone1->getVerificationCode();
        $phone1->setRole( 'Service Scolarité' );

        $phone2 = new Phone();
        $phone1->setUser( $user );
        $phone2->setType( 'FIX' );
        $phone2->setCcode( '00216' );
        $phone2->setNumber( '31 434 012' );
        $phone2->setRole( 'Service Examens' );

        $em->persist( $phone1 );
        $em->persist( $phone2 );
        $em->flush();

        $user = $em->getRepository('App:User')->findOneBy(['username'=>self::$username]);
        self::$userId = $user->getId();
        $phone = $em->getRepository( 'App:Phone')->findOneBy( [ 'type' => 'MBL' ] );
        self::$phoneId = $phone->getId();
    }
}

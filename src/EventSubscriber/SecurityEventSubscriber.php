<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 08/02/17
 * Time: 19:49
 */

namespace App\EventSubscriber;

use App\AppEvents;
use App\Entity\Email;
use App\Entity\Image;
use App\Event\UserConfirmEmailSuccessEvent;
use App\Event\UserInitAccountSuccessEvent;
use App\Event\UserRegistrationSuccessEvent;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class SecurityEventSubscriber implements EventSubscriberInterface {

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;
    private $userManager;
    private $tokenGenerator;
    private $jwtManager;

    public function __construct( EntityManagerInterface $entityManager, UserManagerInterface $userManager, TokenGeneratorInterface $tokenGenerator,
                                 JWTTokenManagerInterface $jwtManager ) {

        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->tokenGenerator = $tokenGenerator;
        $this->jwtManager = $jwtManager;
    }

    public static function getSubscribedEvents() {

        return [
            AppEvents::REGISTRATION_SUCCESS       => [
                [ 'onRegistrationSuccess' ],
            ],
            AppEvents::USER_CONFIRM_EMAIL_SUCCESS => [
                [ 'onUserConfirmEmailSuccess' ],
            ],
            AppEvents::USER_INIT_ACCOUNT_SUCCESS  => [
                [ 'onUserInitAccountSuccess' ],
            ],
        ];
    }

    public function onRegistrationSuccess( UserRegistrationSuccessEvent $event ) {

        $user = $event->getUser();
        $user->setEnabled( false );
        $user->setConfirmationToken( $this->tokenGenerator->generateToken() );
        $user->setProfileImage( $this->getProfileImage( $user->getGender(), $user->getRoles()[0] ) );

        $email = new Email();
        $email->setUser( $user );
        $email->setEmail( $user->getEmail() );
        $email->setIsPrimary( true );

        $this->userManager->updateUser( $user );
        $this->entityManager->persist( $email );
        $this->entityManager->flush();
    }

    public function onUserConfirmEmailSuccess( UserConfirmEmailSuccessEvent $event ) {

        $user = $event->getUser();
        $user->setEnabled( true );
        $user->setConfirmationToken( NULL );
        $this->userManager->updateUser( $user );

        $token = $this->jwtManager->create( $user );
        $response = new JsonResponse( [ 'token' => $token ], 200 );
        $event->setResponse( $response );
    }

    public function onUserInitAccountSuccess( UserInitAccountSuccessEvent $event ) {

        $user = $event->getUser();
        $user->setEnabled( true );
        $user->setConfirmationToken( NULL );
        $user->setProfileImage( $this->getProfileImage( $user->getGender(), $user->getRoles()[0] ) );
        $this->userManager->updateUser( $user );
    }

    private function getProfileImage( $gender, string $role ): Image {

        if ( $gender !== 'male' && $gender !== 'female' ) {

            $gender = 'male';
        }

        switch ( $role ) {

            case 'ROLE_STUDENT':
                $role = 'student';
                break;
            case 'ROLE_PARENT':
                $role = 'parents';
                $gender = 'icon';
                break;
            case 'ROLE_PROFESSOR':
                $role = 'professor';
                break;
            case 'ROLE_INS_STAFF':
                $role = 'institute';
                break;
            case 'ROLE_INS_ADMIN':
                $role = 'institute';
                break;
            case 'ROLE_ADMIN':
                $role = 'admin';
                $gender = 'male';
                break;
        }

        return $this->entityManager->getRepository( 'App:Image' )->findOneBy( [
            'entity' => 'web',
            'name' => $role . '-' . $gender . '.png',
        ] );
    }
}

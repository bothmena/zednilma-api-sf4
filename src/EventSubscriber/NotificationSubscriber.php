<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 17/06/17
 * Time: 09:04
 */

namespace App\EventSubscriber;

use App\AppEvents;
use App\Entity\NotificationUser;
use App\Event\UserPushNotificationEvent;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NotificationSubscriber implements EventSubscriberInterface {


    private $entityManager;

    /**
     * NotificationSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager) {

        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents() {

        return [
            AppEvents::USER_PUSH_NOTIFICATION => [
                ['onPushNotification'],
            ],
        ];
    }

    /**
     * @param UserPushNotificationEvent $event
     *
     * @return void
     */
    public function onPushNotification(UserPushNotificationEvent $event) {

        $this->entityManager->persist($event->getNotification());
        if ( $event->getNotified() ) {

            $notificationUser = new NotificationUser($event->getNotified(), $event->getNotification());
            $notificationUser->setDate($event->getShowAfter());
            $this->entityManager->persist($notificationUser);
        }
        if ( $event->getNotifiedGroup() ) {

            $groupUsers = $this->entityManager->getRepository('App:UserGroup')->findBy(['group' => $event->getNotifiedGroup()]);
            foreach ($groupUsers as $groupUser) {

                $notificationUser = new NotificationUser($groupUser->getUser(), $event->getNotification());
                $notificationUser->setDate($event->getShowAfter());
                $this->entityManager->persist($notificationUser);
            }
        }
        $this->entityManager->flush();
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 08/02/17
 * Time: 19:49
 */

namespace App\EventSubscriber;

use App\AppEvents;
use App\Entity\ImageInstitute;
use App\Entity\ImageUser;
use App\Event\InstituteImageUploadSuccessEvent;
use App\Event\UserImageUploadSuccessEvent;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;

class ImageUploadEventSubscriber implements EventSubscriberInterface {

    private $entityManage;
    private $serializer;

    public function __construct( EntityManagerInterface $manager, SerializerInterface $serializer ) {

        $this->entityManage = $manager;
        $this->serializer = $serializer;
    }

    public static function getSubscribedEvents () {

        return array(
            AppEvents::INSTITUTE_IMAGE_UPLOAD_SUCCESS => array(
                array('onInstituteImageUpload'),
            ),
            AppEvents::USER_IMAGE_UPLOAD_SUCCESS      => array(
                array('onUserImageUpload'),
            ),
        );
    }

    public function onInstituteImageUpload( InstituteImageUploadSuccessEvent $event ) {

        $institute = $event->getInstitute();
        $image = $event->getImage();

        if ( $image->getType() == 'prf' )
            $institute->setProfileImage( $image );
        elseif ( $image->getType() == 'cvr' )
            $institute->setCoverImage( $image );

        $imageMosque = new ImageInstitute();
        $imageMosque->setImage( $image );
        $imageMosque->setInstitute( $institute );
        $this->entityManage->persist( $imageMosque );
        $this->entityManage->flush();

        $json = $this->serializer->serialize( $image, 'json' );
        $event->setResponse( new Response( $json, 201 ) );
    }

    public function onUserImageUpload( UserImageUploadSuccessEvent $event ) {

        $user = $event->getUser();
        $image = $event->getImage();

        if ( $image->getType() == 'prf' )
            $user->setProfileImage( $image );

        $imageUser = new ImageUser();
        $imageUser->setImage( $image );
        $imageUser->setUser( $user );
        $this->entityManage->persist( $imageUser );
        $this->entityManage->flush();

        $json = $this->serializer->serialize( $image, 'json' );
        $event->setResponse( new Response( $json, 201 ) );
    }
}

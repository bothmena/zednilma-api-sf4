<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 17/06/17
 * Time: 09:04
 */

namespace App\EventSubscriber;

use FOS\UserBundle\Model\UserInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class JWTEventSubscriber implements EventSubscriberInterface {

    public static function getSubscribedEvents() {

        return [
            Events::JWT_CREATED => [
                [ 'onJWTCreated' ],
            ],
        ];
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated( JWTCreatedEvent $event ) {

        $user = $event->getUser();
        $payload = $event->getData();

        if ( !$user instanceof UserInterface ) {

            $payload[ 'role' ] = 'anon';
            $payload[ 'uid' ] = 'null';
        } else {

            $payload[ 'role' ] = strtolower( substr($user->getRoles()[0], 5) );
            /*if ( $user->hasRole( 'ROLE_ADMIN' ) )
                $payload[ 'role' ] = 'admin';
            else if ( $user->hasRole( 'ROLE_PROFESSOR' ) )
                $payload[ 'role' ] = 'professor';
            else if ( $user->hasRole( 'ROLE_INS_ADMIN' ) )
                $payload[ 'role' ] = 'ins_admin';
            else if ( $user->hasRole( 'ROLE_INS_STAFF' ) )
                $payload[ 'role' ] = 'ins_staff';
            else if ( $user->hasRole( 'ROLE_STUDENT' ) )
                $payload[ 'role' ] = 'student';
            else
                $payload[ 'role' ] = 'user';*/

            $payload[ 'uid' ] = (string) $user->getId();
        }

        $event->setData( $payload );
    }
}

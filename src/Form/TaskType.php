<?php

namespace App\Form;

use App\Entity\SubjectClasse;
use App\Entity\Task;
use App\Entity\TaskList;
use App\Form\DataTransformer\DateTimeToStringTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Url;

class TaskType extends AbstractType {

    private $typeChoices = [1, 2, 3, 4, 5];

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new Length(['min' => 3, 'max' => 60])
                ]
            ])
            ->add('description', TextType::class, [
                'constraints' => [
                    new Length(['min' => 45, 'max' => 1000])
                ]
            ])
            ->add('dueDate', TextType::class, [
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('repeat', TextType::class, [
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('type', ChoiceType::class, [
                'choices' => $this->typeChoices,
                'constraints' => [
                    new Choice(['choices' => $this->typeChoices])
                ]
            ])
            ->add('link', UrlType::class, [
                'constraints' => [
                    new Url()
                ]
            ])
            ->add('list', EntityType::class, [
                'class' => TaskList::class,
            ])
            ->add('subjectClasse', EntityType::class, [
                'class' => SubjectClasse::class,
            ]);

        $builder->get('dueDate')
            ->addModelTransformer(new DateTimeToStringTransformer());
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);
    }
}

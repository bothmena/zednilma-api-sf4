<?php

namespace App\Form;

use App\Entity\SessionContainer;
use App\Form\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotNull;

class SessionContainerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startAt', TextType::class, [])
            ->add('endAt', TextType::class, [])
            ->add('day', ChoiceType::class , [
                'choices' => [1, 2, 3, 4, 5, 6, 7],
                'constraints'=> [
                    new NotNull(),
                    new Choice(['choices'=>[1, 2, 3, 4, 5, 6, 7]])
                ]
            ]);

        $builder->get('startAt')
            ->addModelTransformer(new DateTimeToStringTransformer('H:i:s'));

        $builder->get('endAt')
            ->addModelTransformer(new DateTimeToStringTransformer('H:i:s'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SessionContainer::class,
        ]);
    }
}

<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class SocialProfileType extends AbstractType {

    private $websites = [
        'linkedin', 'github', 'gitlab', 'bitbucket', 'facebook', 'twitter',
        'youtube', 'google-plus', 'instagram', 'vimeo', 'skype', 'whatsapp',
    ];

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('url', UrlType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Url(), // ['checkDNS'=>true]
                ],
            ])
            ->add('username', TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length(['min' => 5, 'max' => 71]),
                    new Assert\Regex(['pattern' => '/^[\w.+-\/@]+$/']),
                ],
            ])
            ->add('website', ChoiceType::class, [
                'choices' => $this->websites,
                'constraints' => [
                    new Assert\NotNull(['message' => 'value is null!']),
                    new Assert\Choice(['choices' => $this->websites, 'message' => 'value not in array !']),
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults([
            'data_class' => 'App\Entity\SocialProfile',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_social_profile';
    }

}

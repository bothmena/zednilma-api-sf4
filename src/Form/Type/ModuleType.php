<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ModuleType extends AbstractType {

    private $terms = ['y1', 's1', 's2', 't1', 't2', 't3', 'q1', 'q2', 'q3', 'q4'];

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length(['min' => 5, 'max' => 61]),
                ],
            ])
            ->add('description', TextareaType::class, [
                'constraints' => [
                    new Assert\Length(['min' => 45, 'max' => 500]),
                ],
            ])
            ->add('year', NumberType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\LessThanOrEqual(['value' => $options['max_years']]),
                    new Assert\GreaterThanOrEqual(['value' => 1]),
                ],
            ])
            ->add('term', ChoiceType::class, [
                'choices' => $this->terms,
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Choice(['choices'=> $this->terms])
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults([
            'data_class' => 'App\Entity\Module',
            'max_years' => 1,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_module';
    }

}

<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

class DepartmentType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'name', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new Length( [ 'min' => 5, 'max' => 101 ] ),
                ],
            ] )
            ->add( 'description', TextareaType::class, [
                'constraints' => [
                    new Length( [ 'min' => 45, 'max' => 500 ] ),
                ],
            ] );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'App\Entity\Department',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_department';
    }

}

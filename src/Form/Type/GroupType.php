<?php

namespace App\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class GroupType extends AbstractType {


    private $types = ['class', 'class_p', 'sub_class', 'speciality', 'speciality_p', 'department', 'sub_dep', 'container', 'post', 'sub_post'];

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'name', TextType::class, [
                'constraints' => [
                ],
            ] )
            ->add( 'type', ChoiceType::class, [
                'constraints' => [
                ],
            ] );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'App\Entity\Group',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_group';
    }

}

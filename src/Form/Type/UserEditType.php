<?php

namespace App\Form\Type;

use App\Form\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class UserEditType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'username', Type\TextType::class, [
                'constraints' => [
                    new Assert\Length( [ 'min' => 5, 'max' => 31 ] ),
                    new Assert\Regex( [ 'pattern' => '/^[a-zA-Z]{1}[\w.-]+$/' ] ),
                ],
            ] )
            ->add( 'email', Type\EmailType::class, [
                'constraints' => [
                    new Assert\Email(),//['checkMX'=>true, 'checkHost'=>true]
                ],
            ] )
            ->add( 'firstName', Type\TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length( [ 'min' => 3, 'max' => 31 ] ),
                ],
            ] )
            ->add( 'middleName', Type\TextType::class, [
                'constraints' => [
                    new Assert\Length( [ 'min' => 3, 'max' => 31 ] ),
                ],
            ] )
            ->add( 'lastName', Type\TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length( [ 'min' => 3, 'max' => 31 ] ),
                ],
            ] )
            ->add('birthday', Type\TextType::class, [] )
            ->add( 'language', Type\LanguageType::class, [
                'constraints' => [
                    new Assert\Language(),
                ],
            ] )
            ->add( 'gender', Type\ChoiceType::class, [
                'choices' => ['male', 'female'],
                'constraints' => [
                    new Assert\Choice( ['choices' => ['male', 'female']] )
                ],
            ] )
            ->add( 'cin', Type\TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                ],
            ] )
            ->add( 'location', LocationType::class, [
                'constraints' => [
                    new Assert\Valid(),
                ],
            ] )
        ;


        $builder->get('birthday')
            ->addModelTransformer(new DateTimeToStringTransformer( 'd/m/Y' ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'App\Entity\User',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'app_user_edit';
    }

}

<?php

namespace App\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class SpecialityType extends AbstractType {

    private $types = ['SPECIALITY', 'BRANCH'];
    private $cycles = ['ENGINEER_CYCLE', 'ENG_PREPARATORY_CYCLE', 'LICENCE_CYCLE', 'DOCTORATE_CYCLE', 'MASTERE_CYCLE'];

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'name', TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length( [ 'min' => 5, 'max' => 41 ] ),
                ],
            ] )
            ->add( 'alias', TextType::class, [
                'constraints' => [
                    new Assert\Length( [ 'min' => 1, 'max' => 10 ] ),
                ],
            ] )
            ->add( 'description', TextareaType::class, [
                'constraints' => [
                    new Assert\Length( [ 'min' => 15, 'max' => 500 ] ),
                ],
            ] )
            ->add( 'studyCycle', ChoiceType::class, [
                'choices' => $this->cycles,
                'constraints' => [
                    new Assert\Choice( [ 'choices' => $this->cycles ] ),
                ],
            ] )
            ->add( 'years', NumberType::class, [
                'scale'       => 2,
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\GreaterThanOrEqual( [ 'value' => 1 ] ),
                    new Assert\LessThanOrEqual( [ 'value' => 10 ] ),
                ],
            ] )
            ->add( 'type', ChoiceType::class, [
                'choices' => $this->types,
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Choice( [ 'choices' => $this->types ] ),
                ],
            ] )
            ->add( 'parent', EntityType::class, [
                'class' => 'App\Entity\Speciality',
            ] );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'App\Entity\Speciality',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_speciality';
    }

}

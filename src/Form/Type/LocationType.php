<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class LocationType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'country', CountryType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Country(),
                ],
            ] )
            ->add( 'state', TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length(['min'=>3,'max'=>31]),
                ],
            ] )
            ->add( 'city', TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length(['min'=>3,'max'=>31]),
                ],
            ] )
            ->add( 'zipCode', TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length(['min'=>4,'max'=>4]),
                    new Assert\Regex( [
                        'pattern' => '/^\d+$/',
                        'message' => 'postal code can only contain numbers.',
                    ] ),
                ],
            ] )
            ->add( 'address', TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length(['min'=>10,'max'=>101]),
                ],
            ] )
            ->add( 'mapLat', NumberType::class, [
                'constraints' => [
                    new Assert\Type(['type'=>'double']),
                    new Assert\GreaterThanOrEqual(['value'=> -90]),
                    new Assert\LessThanOrEqual(['value'=> 90]),
                ],
            ] )
            ->add( 'mapLong', NumberType::class, [
                'constraints' => [
                    new Assert\Type(['type'=>'double']),
                    new Assert\GreaterThanOrEqual(['value'=> -180]),
                    new Assert\LessThanOrEqual(['value'=> 180]),
                ],
            ] )
            ->add('isPrimary', CheckboxType::class, [
                'required' => false
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'App\Entity\Location',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_location';
    }

}

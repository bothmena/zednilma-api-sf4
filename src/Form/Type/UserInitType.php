<?php

namespace App\Form\Type;

use App\Form\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class UserInitType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'username', Type\TextType::class, [
                'constraints' => [
                    new Assert\Length( [ 'min' => 5, 'max' => 31 ] ),
                    new Assert\Regex( [ 'pattern' => '/^[a-zA-Z]{1}[\w.-]+$/' ] ),
                ],
            ] )
            ->add( 'plainPassword', Type\RepeatedType::class, [
                    'required'        => true,
                    'type'            => Type\PasswordType::class,
                    'first_name'      => 'password',
                    'second_name'     => 'confirm_password',
                    'first_options'   => [
                        'constraints' => [
                            new Assert\Length( [ 'min' => 8, 'max' => 51 ] ),
                            new Assert\Regex( [
                                'pattern' => '/^(?=\S*\d)(?=\S*[a-z])(?=\S*[A-Z])\S*$/',
                                'message' => 'password must contain at least 1 number, 1 uppercase, 1, lowercase',
                            ] ),
                        ],
                    ],
                    'invalid_message' => 'The password fields must match.',
                ]
            )
            ->add( 'firstName', Type\TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length( [ 'min' => 3, 'max' => 31 ] ),
                ],
            ] )
            ->add( 'lastName', Type\TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length( [ 'min' => 3, 'max' => 31 ] ),
                ],
            ] )
            ->add( 'birthday', Type\TextType::class, [] )
            ->add( 'language', Type\LanguageType::class, [
                'constraints' => [ // language codes like 'en', local codes are optional like 'US'
                    new Assert\NotNull(),
                    new Assert\Language(),
                ],
            ] )
            ->add( 'gender', Type\ChoiceType::class, [
                'choices'     => [ 'male', 'female' ],
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Choice( [ 'choices' => [ 'male', 'female' ] ] ),
                ],
            ] )
            ->add( 'cin', Type\TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length( [ 'min' => 8, 'max' => 8 ] ),
                    new Assert\Regex( [
                        'pattern' => '/^\d+$/',
                        'message' => 'cin can only contain numbers.',
                    ] ),
                ],
            ] )
            ->add( 'location', LocationType::class, [
                'constraints' => [
                    new Assert\Valid(),
                ],
            ] );

        $builder->get( 'birthday' )
            ->addModelTransformer( new DateTimeToStringTransformer( 'd/m/Y' ) ); // Y-m-d
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'App\Entity\User',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'app_user_edit';
    }

}

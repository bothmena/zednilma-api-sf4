<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType as EmType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class EmailType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'email', EmType::class, [
                'constraints' => [
                    new Assert\Email(), // ['checkMX'=>true, 'checkHost'=>true]
                ],
            ] )
            ->add( 'isPrimary', CheckboxType::class, [
                'required' => false,
            ] )
            ->add( 'role', TextType::class, [
                'constraints' => [
                    new Assert\Length( [ 'min' => 3, 'max' => 31 ] ),
                ],
            ] );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'App\Entity\Email',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_email';
    }

}

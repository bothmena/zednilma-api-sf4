<?php

namespace App\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotNull;

class AttendanceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isPresent', CheckboxType::class, [
                'constraints' => [
                    new NotNull(),
                    new Choice(['choices'=>[true, false]])
                ]
            ])
            ->add('role', ChoiceType::class, [
                'choices'=>[1, 2, 3],
                'constraints' => [
                    new NotNull(),
                    new Choice(['choices'=>[1, 2, 3]])
                ]
            ])
            ->add('user', EntityType::class, [
                'class' => 'App\Entity\User',
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('session', EntityType::class, [
                'class' => 'App\Entity\Session',
                'constraints' => [
                    new NotNull()
                ]
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Attendance'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_attendance';
    }


}

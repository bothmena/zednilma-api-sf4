<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class SubjectType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'name', TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length( [ 'min' => 5, 'max' => 61 ] ),
                ],
            ] )
            ->add( 'description', TextareaType::class, [
                'constraints' => [
                    new Assert\Length( [ 'min' => 15, 'max' => 500 ] ),
                ],
            ] );

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'App\Entity\Subject',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_subject';
    }

}

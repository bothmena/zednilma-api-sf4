<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class PhoneType extends AbstractType {

    private $typeChoices = [ 'MBL', 'FIX', 'FAX' ];

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'type', ChoiceType::class, [
                'choices'     => $this->typeChoices,
                'constraints' => [
                    new Assert\Choice( [ 'choices' => $this->typeChoices ] ),
                    new Assert\NotNull(),
                ],
            ] )
            ->add( 'ccode', TextType::class, [
                'constraints' => [
                    new Assert\Regex( [ 'pattern' => '/^(\+|00)\d{1,3}$/' ] ),
                ],
            ] )
            ->add( 'role', TextType::class, [
                'constraints' => [
                    new Assert\Length( ['min'=>3, 'max'=>31] ),
                ],
            ]  )
            ->add( 'number', TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Regex( [ 'pattern' => '/^\d{2}\s?(\d{3}\s?\d{3}|\d{2}\s\d{2}\s\d{2})$/' ] ),
                ],
            ] );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'App\Entity\Phone',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_phone';
    }

}

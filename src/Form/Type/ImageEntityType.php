<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ImageEntityType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $types = [ 'prf', 'cvr', 'glr' ];
        $entities = [ 'ins', 'usr' ];

        $builder
            ->add( 'type', ChoiceType::class, [
                'choices'     => $types,
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Choice( [ 'choices' => $types ] ),
                ],
            ] )
            ->add( 'entity', ChoiceType::class, [
                'choices'     => $entities,
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Choice( [ 'choices' => $entities ] ),
                ],
            ] );
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class'      => 'App\Entity\Image',
            'csrf_protection' => false,
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'app_image_entity';
    }

}

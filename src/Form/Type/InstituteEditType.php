<?php

namespace App\Form\Type;

use App\Form\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class InstituteEditType extends AbstractType {

    private $types = ['PRIMARY_SCHOOL', 'BASIC_SCHOOL', 'SECONDARY_SCHOOL', 'UNIVERSITY'];//PRE_SCHOOL
    private $parentalControls = ['NONE', 'MEDIUM', 'FULL', 'CUSTOM'];

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length(['min' => 5, 'max' => 101]),
                ],
            ])
            ->add('slogan', TextType::class, [
                'constraints' => [
                    new Assert\Length(['min' => 10, 'max' => 101]),
                ],
            ])
            ->add('description', TextareaType::class, [
                'constraints' => [
                    new Assert\Length(['min' => 45, 'max' => 500]),
                ],
            ])
            ->add('foundationDate', TextType::class, [])
            ->add('website', UrlType::class, [
                'constraints' => [
                    new Assert\Url(), // [ 'checkDNS' => true ]
                ],
            ])
            ->add('type', ChoiceType::class, [
                'choices' => $this->types,
                'constraints' => [
                    new Assert\Choice(['choices' => $this->types]),
                ],
            ])
            ->add('parentalControl', ChoiceType::class, [
                'choices' => $this->parentalControls,
                'constraints' => [
                    new Assert\Choice(['choices' => $this->parentalControls]),
                ],
            ]);

        /**
         * @todo foundationDate validation, in the setters or in the transformer.
         */
        $builder->get('foundationDate')
            ->addModelTransformer(new DateTimeToStringTransformer('d/m/Y'));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults([
            'data_class' => 'App\Entity\Institute',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_institute';
    }

}

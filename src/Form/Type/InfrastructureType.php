<?php

namespace App\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class InfrastructureType extends AbstractType {

    private $types = [ 'BLOCK', 'FLOOR', 'CLASSROOM', 'LABORATORY', 'WORKSHOP', 'LIBRARY' ];

    /**
     * {@inheritdoc}
     */
    public function buildForm( FormBuilderInterface $builder, array $options ) {

        $builder
            ->add( 'name', TextType::class, [
                'constraints' => [
                    new Assert\Length(['min'=>3, 'max'=>15])
                ],
            ] )
            ->add( 'alias', TextType::class, [
                'constraints' => [
                    new Assert\Length(['min'=>1, 'max'=>5])
                ],
            ] )
            ->add( 'index', IntegerType::class, [
            ] )
            ->add( 'capacity', IntegerType::class, [
            ] )
            ->add( 'type', ChoiceType::class, [
                'choices' => $this->types,
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Choice( ['choices'=>$this->types] )
                ],
            ] )
            ->add( 'parent', EntityType::class, [
                'class' => 'App\Entity\Infrastructure',
                'constraints' => [
                ],
            ] )
            ->add( 'department', EntityType::class, [
                'class' => 'App\Entity\Department',
                'constraints' => [
                ],
            ] );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions( OptionsResolver $resolver ) {

        $resolver->setDefaults( [
            'data_class' => 'App\Entity\Infrastructure',
        ] );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {

        return 'appbundle_infrastructure';
    }

}

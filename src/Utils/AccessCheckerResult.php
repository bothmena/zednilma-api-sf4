<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 13/07/17
 * Time: 14:31
 */

namespace App\Utils;

use App\Entity\Institute;
use App\Entity\User;
use App\Entity\UserInstitute;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

class AccessCheckerResult {

    /**
     * @var User $user
     * @var Institute $institute
     * @var UserInstitute $userInstitute
     * @var Response $response
     */
    private $user;
    private $institute;
    private $userInstitute;
    private $response;

    /**
     * AccessCheckerResult constructor.
     */
    public function __construct() {
    }

    /**
     * @return User|UserInterface
     */
    public function getUser() {

        return $this->user;
    }

    /**
     * @param UserInterface $user
     */
    public function setUser( UserInterface $user ) {

        $this->user = $user;
    }

    /**
     * @return Institute
     */
    public function getInstitute() {

        return $this->institute;
    }

    /**
     * @param Institute $institute
     */
    public function setInstitute( Institute $institute ) {

        $this->institute = $institute;
    }

    /**
     * @return Institute
     */
    public function getUserInstitute() {

        return $this->userInstitute;
    }

    /**
     * @param UserInstitute $userInstitute
     */
    public function setUserInstitute( UserInstitute $userInstitute ) {

        $this->userInstitute = $userInstitute;
    }

    /**
     * @return Response
     */
    public function getResponse() {

        return $this->response;
    }

    /**
     * @param Response $response
     */
    public function setResponse( Response $response ) {

        $this->response = $response;
    }
}
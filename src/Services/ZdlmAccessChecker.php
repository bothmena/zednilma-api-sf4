<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 19/03/17
 * Time: 15:13
 */

namespace App\Services;

use App\Utils\AccessCheckerResult;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ZdlmAccessChecker {

    private $manager;
    private $authChecker;
    private $tokenStorage;

    public function __construct(EntityManagerInterface $manager, AuthorizationCheckerInterface $checker, TokenStorageInterface $tokenStorage) {

        $this->manager = $manager;
        $this->authChecker = $checker;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param int $insId
     * @return AccessCheckerResult
     */
    public function checkForInstitute(int $insId): AccessCheckerResult {

        $result = new AccessCheckerResult();

        if (false === $this->authChecker->isGranted('IS_AUTHENTICATED_FULLY')) {

            $result->setResponse(
                new JsonResponse([
                    'status_code' => 401,
                    'error_code' => 'not_authenticated',
                    'message' => 'you are not authenticated',
                ], 401)
            );
            return $result;
        }

        if (false === $this->authChecker->isGranted('ROLE_INS_STAFF')) {

            $result->setResponse(
                new JsonResponse([
                    'status_code' => 403,
                    'error_code' => 'access_denied.role_not_granted',
                    'message' => 'you are not authorised',
                ], 403)
            );
            return $result;
        }

        $institute = $this->manager->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            $result->setResponse(
                new JsonResponse([
                    'status_code' => 404,
                    'error_code' => 'not_found.institute',
                    'message' => 'Institute was not found',
                ], 404)
            );
            return $result;
        }
        $result->setInstitute($institute);

        $user = $this->tokenStorage->getToken()->getUser();
        $result->setUser($user);
        $userInstitute = $this->manager->getRepository('App:UserInstitute')
            ->getUserInsByIds($insId, $user->getId());
        if (!$userInstitute) {

            $result->setResponse(
                new JsonResponse([
                    'status_code' => 403,
                    'error_code' => 'access_denied.not_user_property',
                    'message' => 'you are not authorised',
                ], 403)
            );
            return $result;
        }
        $result->setUserInstitute($userInstitute);

        return $result;
    }

    /**
     * @param int $userId
     * @param string|null $roles
     * @return AccessCheckerResult
     */
    public function checkForUser(int $userId = NULL, $roles = NULL): AccessCheckerResult {

        $result = new AccessCheckerResult();

        if (false === $this->authChecker->isGranted('ROLE_USER')) {

            $result->setResponse(
                new JsonResponse([
                    'status_code' => 401,
                    'error_code' => 'not_authenticated',
                    'message' => 'you are not authenticated',
                ], 401)
            );
            return $result;
        }

        if ($roles) {

            $granted = false;
            if (is_array($roles)) {
                foreach ($roles as $role) {
                    if (true === $this->authChecker->isGranted($role)) {
                        $granted = true;
                        break;
                    }
                }
            } else
                $granted = $this->authChecker->isGranted($roles);

            if (false === $granted) {

                $result->setResponse(
                    new JsonResponse([
                        'status_code' => 403,
                        'error_code' => 'access_denied.role_not_granted',
                        'message' => 'you are not authorised',
                    ], 403)
                );
                return $result;
            }
        }

        if ($userId) {

            $user = $this->manager->getRepository('App:User')->find($userId);
            if (!$user) {

                $result->setResponse(
                    new JsonResponse([
                        'status_code' => 404,
                        'error_code' => 'not_found.user',
                        'message' => 'User was not found',
                    ], 404)
                );
                return $result;
            }
            $result->setUser($user);

            if ($user->getId() !== $this->tokenStorage->getToken()->getUser()->getId()) {

                $result->setResponse(
                    new JsonResponse([
                        'status_code' => 403,
                        'error_code' => 'access_denied.not_user_property',
                        'message' => 'you are not authorised',
                    ], 403)
                );
                return $result;
            }

            return $result;
        }

        return $result;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 19/03/17
 * Time: 15:13
 */

namespace App\Services;

use App\Entity\Image;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;

class ABOImageUpload {

    private $requestStack;
    private $tokenGenerator;
    private $kernelDir;
    private $mimeTypes = array( 'image/png', 'image/jpeg', 'image/gif' );
    private $maxFileSize = 3145728;

    public function __construct( RequestStack $requestStack, TokenGeneratorInterface $tokenGenerator, string $kerDir ) {

        $this->tokenGenerator = $tokenGenerator;
        $this->requestStack = $requestStack;
        $this->kernelDir = $kerDir;
    }

    public function uploadImage( string $entity, string $type ): Image {

        /**
         * @var $file UploadedFile
         */
        $request = $this->requestStack->getCurrentRequest();
        if ( $request->files->count() > 0 ) {

            $files = $request->files->all();

            if ( !empty( $file = $files['app_image']['file'] ) ) {

                if ( $this->isFileValid( $file ) ) {

                    $fileName = $this->tokenGenerator->generateToken().'.'.$file->guessExtension();
                    $dir = $this->kernelDir . '/../web/images/' . $entity . '/' . $type;

                    try {

                        $file->move( $dir, $fileName );
                        $image = new Image();
                        $image->setName( $fileName );
                        if ( $entity === 'user' )
                            $image->setEntity( 'usr' );
                        if ( $entity === 'institute' )
                            $image->setEntity( 'ins' );
                        $image->setType( $type );
                        return $image;

                    } catch ( FileException $fileException ) {

                        throw $fileException;
                    }
                }
                else
                    return NULL;
            }
        }
    }

    private function isFileValid( UploadedFile $file ) : bool {

        if ( $file->getClientSize() > $this->maxFileSize )
            return false;
        if ( !in_array( $file->getClientMimeType(), $this->mimeTypes ) )
            return false;

        return $file->isValid();
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 19/03/17
 * Time: 15:13
 */

namespace App\Services;

use App\Entity\Session;
use App\Entity\Task;
use App\Entity\TaskUser;
use App\Entity\User;
use App\Entity\UserClasse;
use App\Entity\UserDepartment;
use App\Entity\UserGroup;
use App\Entity\UserPost;
use JMS\Serializer\SerializerInterface;

class ABONotificationDescriber {


    private $serializer;

    public function __construct(SerializerInterface $serializer) {

        $this->serializer = $serializer;
    }

    public function describeAttendance(Session $session, User $student, bool $isPresent) {

        // you have been marked absent in the session of -subject- that goes from -start- to -end- by prof. -firstName- -lastName-
        // -student.firstName- -student.lastName- has been marked absent in the session of -subject- that goes from -start- to -end- by prof. -firstName- -lastName-
        $data = [
            'v' => '1.0',
            'isPresent' => $isPresent,
            'session' => [
                'id' => $session->getId(),
                'start' => $session->getContainer()->getStartAt(),
                'end' => $session->getContainer()->getEndAt(),
            ],
            'prof' => [
                'id' => $session->getProfessor()->getId(),
                'username' => $session->getProfessor()->getUsername(),
                'firstName' => $session->getProfessor()->getFirstName(),
                'lastName' => $session->getProfessor()->getLastName(),
            ],
            'subject' => [
                'id' => $session->getSubject()->getId(),
                'name' => $session->getSubject()->getName(),
            ],
            'student' => [
                'id' => $student->getId(),
                'username' => $student->getUsername(),
                'firstName' => $student->getFirstName(),
                'lastName' => $student->getLastName(),
            ]
        ];

        return $this->serialize($data);
    }

    public function describePost(UserPost $userPost) {

        // starting from -start- you occupy a new post: -name-
        $data = [
            'v' => '1.0',
            'post' => [
                'id' => $userPost->getId(),
                'name' => $userPost->getPost()->getName(),
                'start' => $userPost->getDateStarted(),
                'end' => $userPost->getDateEnded(),
            ]
        ];

        return $this->serialize($data);
    }

    public function describeSessionReminder(Session $session) {

        // (-start- - now) [unit: min/seconds] // -subject.name- starts in less than -time- -unit-, in -name- classroom.
        $data = [
            'v' => '1.0',
            'session' => [
                'id' => $session->getId(),
                'start' => $session->getContainer()->getStartAt(),
                'end' => $session->getContainer()->getEndAt(),
            ],
            'classroom' => [
                'id' => $session->getClassroom()->getId(),
                'name' => $session->getClassroom()->getName(),
            ],
            'subject' => [
                'id' => $session->getSubject()->getId(),
                'name' => $session->getSubject()->getName(),
            ]
        ];

        return $this->serialize($data);
    }

    public function describeSessionReport(Session $session) {

        // prof. -firstName- -lastName- has submitted the -subject.name- session report
        $data = [
            'v' => '1.0',
            'session' => [
                'id' => $session->getId(),
                'start' => $session->getContainer()->getStartAt(),
                'end' => $session->getContainer()->getEndAt(),
                'report' => $session->getReport(),
            ],
            'prof' => [
                'id' => $session->getProfessor()->getId(),
                'username' => $session->getProfessor()->getUsername(),
                'firstName' => $session->getProfessor()->getFirstName(),
                'lastName' => $session->getProfessor()->getLastName(),
            ],
            'subject' => [
                'id' => $session->getSubject()->getId(),
                'name' => $session->getSubject()->getName(),
            ]
        ];

        return $this->serialize($data);
    }

    public function describeTaskUserReminder(TaskUser $taskUser) {

        // -task.name- assigned by -firstName- -lastName- is due in less than 1 hour
        // -task.name- assigned by [prof.] -firstName- -lastName- is overdue!
        // or if the above is too long,
        // -task.name- is overdue! | is due in less than 1 hour
        $data = [
            'v' => '1.0',
            'task' => [
                'id' => $taskUser->getTask()->getId(),
                'name' => $taskUser->getTask()->getName(),
            ],
            'assigner' => [
                'id' => $taskUser->getTask()->getUser()->getId(),
                'username' => $taskUser->getTask()->getUser()->getUsername(),
                'firstName' => $taskUser->getTask()->getUser()->getFirstName(),
                'lastName' => $taskUser->getTask()->getUser()->getLastName(),
            ],
            'assignee' => [
                'id' => $taskUser->getUser()->getId(),
                'username' => $taskUser->getUser()->getUsername(),
                'firstName' => $taskUser->getUser()->getFirstName(),
                'lastName' => $taskUser->getUser()->getLastName(),
            ],
            'taskUser' => [
                'id' => $taskUser->getId(),
                'dueDate' => $taskUser->getDueDate(),
            ]
        ];

        return $this->serialize($data);
    }

    public function describeAssignmentReminder(Task $task) {

        // -task.name- assigned to -classe- [-subject-] is due at -dueDate-
        $data = [
            'v' => '1.0',
            'task' => [
                'id' => $task->getId(),
                'name' => $task->getName(),
                'dueDate' => $task->getDueDate(),
            ]
        ];
        if ($task->getSubjectClasse()) {
            $data['subject'] = [
                'id' => $task->getSubjectClasse()->getSubject()->getId(),
                'name' => $task->getSubjectClasse()->getSubject()->getName(),
            ];
            $data['classe'] = [
                'id' => $task->getSubjectClasse()->getClasse()->getId(),
                'name' => $task->getSubjectClasse()->getClasse()->getName(),
                'level' => $task->getSubjectClasse()->getClasse()->getLevel(),
            ];
        }

        return $this->serialize($data);
    }

    public function describeNewAssignment(Task $task, User $student) {

        // a new assignment -task.name-, assigned by -task.user.fullName- and it will be due at -dueDate-
        $data = [
            'v' => '1.0',
            'task' => [
                'id' => $task->getId(),
                'name' => $task->getName(),
                'dueDate' => $task->getDueDate(),
            ],
            'assigner' => [
                'id' => $task->getUser()->getId(),
                'username' => $task->getUser()->getUsername(),
                'firstName' => $task->getUser()->getFirstName(),
                'lastName' => $task->getUser()->getLastName(),
            ],
            'assignee' => [
                'id' => $student->getId(),
                'username' => $student->getUsername(),
                'firstName' => $student->getFirstName(),
                'lastName' => $student->getLastName(),
            ]
        ];

        return $this->serialize($data);
    }

    public function describeGroup(UserGroup $userGroup) {

        // you are now a member of the -group.name- group
        // you are now a member of the -speciality.name- group, level -level-
        $data = [
            'v' => '1.0',
            'userGroup' => [
                'id' => $userGroup->getId(),
                'date' => $userGroup->getDate(),
                'level' => $userGroup->getLevel(),
            ],
            'group' => [
                'id' => $userGroup->getGroup()->getId(),
                'name' => $userGroup->getGroup()->getName(),
                'type' => $userGroup->getGroup()->getType(),
            ],
        ];

        return $this->serialize($data);
    }

    public function describeUserClasse(UserClasse $userClasse) {

        // you were assigned to -class.name- class
        $data = [
            'v' => '1.0',
            'userClasse' => [
                'id' => $userClasse->getId(),
                'dateIn' => $userClasse->getDateIn(),
                'dateOut' => $userClasse->getDateOut(),
            ],
            'classe' => [
                'id' => $userClasse->getClasse()->getId(),
                'name' => $userClasse->getClasse()->getName(),
                'level' => $userClasse->getClasse()->getLevel(),
                'branch' => $userClasse->getClasse()->getBranch()->getName(),
            ]
        ];

        return $this->serialize($data);
    }

    public function describeUserDepartment(UserDepartment $userDepartment) {

        // you are now a member of -department.name- department and you occupy the -post.name- post
        $data = [
            'v' => '1.0',
            'userDepartment' => [
                'id' => $userDepartment->getId(),
                'dateIn' => $userDepartment->getDateStarted(),
                'dateOut' => $userDepartment->getDateEnded(),
            ],
            'department' => [
                'id' => $userDepartment->getDepartment()->getId(),
                'slug' => $userDepartment->getDepartment()->getSlug(),
                'name' => $userDepartment->getDepartment()->getName(),
            ],
            'post' => [
                'id' => $userDepartment->getPost()->getId(),
                'name' => $userDepartment->getPost()->getName(),
            ]
        ];

        return $this->serialize($data);
    }

    private function serialize($data) {

        $serialized = $this->serializer->serialize($data, 'json');
        return preg_replace("/\s+(?=(?:[^\\'\"]*[\\'\"][^\\'\"]*[\\'\"])*[^\\'\"]*$)/", '', $serialized);
    }
}

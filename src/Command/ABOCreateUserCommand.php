<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 05/07/17
 * Time: 05:23
 */

namespace App\Command;

use App\Entity\Institute;
use App\Entity\User;
use App\Entity\UserInstitute;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

class ABOCreateUserCommand extends ContainerAwareCommand {


    private $entityManager;
    private $tokenGenerator;

    private $roles = [
        'professor' => 'ROLE_PROFESSOR',
        'student' => 'ROLE_STUDENT',
        'insadmin' => 'ROLE_INS_ADMIN',
        'insstaff' => 'ROLE_INS_STAFF',
        'admin' => 'ROLE_ADMIN',
        'parent' => 'ROLE_PARENT',
    ];
    private $roleImage = [
        'admin' => 'admin-male.png',
        'insadmin' => 'institute-male.png',
        'insstaff' => 'institute-female.png',
        'parent' => 'parents-icon.png',
        'professor' => 'professor-male.png',
        'student' => 'student-male.png',
    ];

    /**
     * ABOCreateUserCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function __construct(EntityManagerInterface $entityManager, TokenGeneratorInterface $tokenGenerator) {

        parent::__construct();
        $this->entityManager = $entityManager;
        $this->tokenGenerator = $tokenGenerator;
    }

    protected function configure() {

        $this
            // the name of the command (the part after "bin/console")
            ->setName('abo:create:user')
            // the short description shown while running "php bin/console list"
            ->setDescription('Create 6 users for each role, Admin, InsAdmin, InsStaff, Professor, Student and Parent.')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to create a user...');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output) {

        // admin, insadmin, insstaff, professor, student, parent
        $helper = $this->getHelper('question');

        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln(['User(s) Generation', '========================',]);

        $question = new ChoiceQuestion(
            'Please the user\'s roles to be generated (default: all)',
            ['all', 'admin', 'insadmin', 'insstaff', 'professor', 'student', 'parent'],
            0
        );
        $question->setErrorMessage('User %s is invalid.');
        $question->setMultiselect(true);

        $roles = $helper->ask($input, $output, $question);
        if (in_array('all', $roles))
            $roles = ['admin', 'insadmin', 'insstaff', 'professor', 'student', 'parent'];

        $i = 0;
        foreach ($roles as $role) {

            $this->createUser($role);
            $i++;
        }

        // outputs a message without adding a "\n" at the end of the line
        $output->writeln("$i users have been created with the following roles:");
        $output->writeln(implode(', ', $roles));
    }

    /**
     * @param string $role
     * @throws \Exception
     */
    private function createUser(string $role) {

        $image = $this->entityManager->getRepository('App:Image')->findOneBy([
            'entity' => 'web',
            'name' => $this->roleImage[$role],
        ]);

        if ($image) {

            $userManager = $this->getContainer()->get('fos_user.user_manager');
            /**
             * @var User $user
             */
            $user = $userManager->createUser();
            $user->setUsername($role);
            $user->setFirstName('First');
            $user->setLastName('Last Name');
            $user->setEmail($role . '@gmail.com');
            $user->setPlainPassword($role . 'PASS');
            $user->addRole($this->roles[$role]);
            $user->setCin($role);
            $user->setProfileImage($image);

            if ($role !== 'admin') {

                $user->setConfirmationToken($this->tokenGenerator->generateToken());
                $userManager->updateUser($user);

                $institute = $this->entityManager->getRepository('App:Institute')->find(1);
                if (!$institute) {

                    $institute = new Institute();
                    $institute->setName('Demo Institute');
                    $institute->setFoundationDate(new \DateTime());
                    $institute->setDescription('Demo Institute Description');
                    $institute->setParentalControl('Demo Institute');
                    $institute->setSlogan('Demo Institute Slogan');
                    $institute->setType('HIGH_SCHOOL');
                    $institute->setWebsite('http://www.demo-institute.com');

                    $this->entityManager->persist($institute);
                    $this->entityManager->flush();
                }

                $userInstitute = new UserInstitute();
                $userInstitute->setUser($user);
                $userInstitute->setInstitute($institute);
                $userInstitute->setRole($this->roles[$role]);

                $this->entityManager->persist($userInstitute);
                $this->entityManager->flush();
            } else {

                $user->setEnabled(true);
                $userManager->updateUser($user);
            }
        } else {

            throw new \Exception('Profile Image was not found! make sure you installed images for all users');
        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 05/07/17
 * Time: 05:23
 */

namespace App\Command;

use App\Entity\Image;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

class ABOCreateImageCommand extends ContainerAwareCommand {

    protected function configure() {

        $this
            // the name of the command (the part after "bin/console")
            ->setName( 'abo:create:image' )
            // the short description shown while running "php bin/console list"
            ->setDescription( 'Create default users and institute profile/cover images.' )
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp( 'This command allows you to create an image...' );
    }

    protected function execute( InputInterface $input, OutputInterface $output ) {

        $helper = $this->getHelper( 'question' );
        $output->writeln( [ 'Image(s) Generation', '========================', ] );

        $question = new ChoiceQuestion(
            'Please the images to be created (default: all)',
            [ 'all', 'admin', 'institute', 'parent', 'professor', 'student', 'university' ],
            0
        );
        $question->setErrorMessage( '%s is not a valid choice.' );
        $question->setMultiselect( true );

        $roles = $helper->ask( $input, $output, $question );
        if ( in_array( 'all', $roles ) )
            $roles = [ 'admin', 'institute', 'parent', 'professor', 'student', 'university' ];

        $i = 0;
        foreach ( $roles as $role ) {

            $this->createImage( $role );
            if ( in_array( $role, [ 'admin', 'parent' ] ) )
                $i++;
            else
                $i += 2;
        }

        // outputs a message without adding a "\n" at the end of the line
        $output->writeln( "$i images have been created for the following roles:" );
        $output->writeln( implode( ', ', $roles ) );
    }

    private function createImage( string $role ) {

        $images = [
            'admin'      => [ 'admin-male.png' ],
            'institute'  => [ 'institute-male.png', 'institute-female.png' ],
            'parent'     => [ 'parents-icon.png' ],
            'professor'  => [ 'professor-male.png', 'professor-female.png' ],
            'student'    => [ 'student-male.png', 'student-female.png' ],
            'university' => [ 'institute-logo.png', 'institute-cover.jpg' ],
        ];

        $em = $this->getContainer()->get( 'doctrine.orm.entity_manager' );
        /**
         * @var User $user
         */
        forEach ( $images[ $role ] as $name ) {

            $image = new Image();
            $image->setName( $name );
            $image->setType( '' );
            $image->setEntity( 'web' );
            $em->persist( $image );
        }
        $em->flush();
    }
}

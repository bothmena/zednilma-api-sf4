<?php

namespace App\Controller\API\V1;

use App\AppEvents;
use App\Entity\Attendance;
use App\Entity\Notification;
use App\Event\UserPushNotificationEvent;
use App\Form\Type\AttendanceType;
use App\Services\ABONotificationDescriber;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AttendanceController extends FOSRestController {


    private $accessChecker;
    private $describer;

    public function __construct(ZdlmAccessChecker $checker, ABONotificationDescriber $describer) {

        $this->accessChecker = $checker;
        $this->describer = $describer;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/attendances/{id}", requirements={"insId": "\d+", "id": "\d+"}, name="api_attendance_read", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Attendance",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function getAction(int $insId, int $id) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $attendance = $this->getDoctrine()->getRepository('App:Attendance')->find($id);
        if (!$attendance) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.attendance',
                'message' => 'Attendance was not found',
            ], 404);
        }

        $view = $this->view($attendance, 200);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/attendances", requirements={"insId": "\d+"}, name="api_attendance_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Attendance",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @return Response
     */
    public function cgetAction(int $insId) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $attendances = $this->getDoctrine()->getRepository('App:Attendance')->findAll();
        return $this->handleView($this->view($attendances, 200));
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/attendances", name="api_attendance_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Attendance",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function newSessionAttendanceAction(Request $request) {

        if ($this->accessChecker->checkForUser(NULL, 'ROLE_PROFESSOR')->getResponse())
            return $this->accessChecker->checkForUser()->getResponse();

        $data = json_decode($request->getContent(), true);
        $response = [];
        $em = $this->getDoctrine()->getManager();

        foreach ($data as $datum) {

            $attendance = new Attendance();
            $form = $this->createForm(AttendanceType::class, $attendance);
            $form->submit($datum);

            if ($form->isValid()) {

                $this->pushNotification($attendance);
                $em->persist($attendance);
            } else {

                array_push($response, ['data' => $datum, 'errors' => $form->getErrors(true)]);
            }
        }
        $em->flush();

        return new JsonResponse($response, 201);
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/attendances/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_attendance_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Attendance",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function editAction(Request $request, int $insId, int $id) {

        if ($this->accessChecker->checkForUser(NULL, 'ROLE_PROFESSOR')->getResponse())
            return $this->accessChecker->checkForUser()->getResponse();

        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();

        $attendance = $em->getRepository('App:Attendance')->find($id);
        if (!$attendance) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.attendance',
                'message' => 'Attendance was not found',
            ], 404);
        }
        if (in_array($data['isPresent'], [true, false])) {

            $attendance->setIsPresent($data['isPresent']);
            $this->pushNotification($attendance);
            $em->flush();

            return new JsonResponse(NULL, 204);
        } else {

            return new JsonResponse([
                'status_code' => 400,
                'error_code' => 'form.invalid',
                'message' => 'Submitted form is not valid',
            ], 400);
        }
    }

    private function pushNotification(Attendance $attendance) {

        $dispatcher = $this->get('event_dispatcher');
        $notifParent = new Notification();
        $notifParent->setType(2);
        $notifParent->setDescription($this->describer->describeAttendance($attendance->getSession(), $attendance->getUser(), $attendance->getIsPresent()));
        $notifParent->setNotifier($attendance->getSession()->getProfessor());
        $event = new UserPushNotificationEvent($notifParent, $attendance->getUser()->getParent());
        $dispatcher->dispatch(AppEvents::USER_PUSH_NOTIFICATION, $event);

        if (!$attendance->getIsPresent()) {
            $notifStudent = new Notification();
            $notifStudent->setType(1);
            $notifStudent->setDescription($this->describer->describeAttendance($attendance->getSession(), $attendance->getUser(), false));
            $notifStudent->setNotifier($attendance->getSession()->getProfessor());
            $event = new UserPushNotificationEvent($notifStudent, $attendance->getUser());
            $dispatcher->dispatch(AppEvents::USER_PUSH_NOTIFICATION, $event);
        }
    }
}

<?php

namespace App\Controller\API\V1;

use App\Entity\FileAccess;
use App\Form\Type\FileAccessType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FileAccessController extends FOSRestController {


    private $accessChecker;

    /**
     * UserDepartmentController constructor.
     * @param ZdlmAccessChecker $accessChecker
     */
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/fileAccesss/{id}", requirements={"id": "\d+"}, name="api_file_access_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="FileAccess",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function getAction ( int $id ) {

        $fileAccess = $this->getDoctrine()->getRepository( 'App:FileAccess' )->find( $id );
        if ( !$fileAccess ) {
            throw $this->createNotFoundException( 'FileAccess not found.' );
        }

        $view = $this->view( $fileAccess, 200 );
        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/fileAccesss", name="api_file_access_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="FileAccess",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function cgetAction () {

        $fileAccesss = $this->getDoctrine()->getRepository( 'App:FileAccess' )->findAll();
        return $this->handleView( $this->view( $fileAccesss, 200 ) );
    }

    /**
     * @Rest\Post(path="/fileAccesss", name="api_file_access_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="FileAccess",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request) {

        /**
         * @todo check for access restrictions
         */
        $fileAccess = new FileAccess();

        return $this->processForm( $request, $fileAccess, true );
    }

    /**
     * @Rest\Route(path="/fileAccesss/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_file_access_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="FileAccess",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function editAction ( Request $request, int $id ) {

        /**
         * @todo check for access restrictions
         */
        /*if ( !$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED') ) {

            throw $this->createAccessDeniedException('No Anonymous allowed here!!');
        }*/
        $fileAccess = $this->getDoctrine()->getRepository( 'App:FileAccess' )->find( $id );
        if ( !$fileAccess ) {
            throw $this->createNotFoundException( 'FileAccess not found.' );
        }

        return $this->processForm( $request, $fileAccess );
    }

    private function processForm ( Request $request, FileAccess $fileAccess, bool $isNew = false ) {

        $form = $this->createForm( FileAccessType::class, $fileAccess );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $fileAccess );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_file_access_read', [
                    'version' => 'v1',
                    'id' => $fileAccess->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }
}

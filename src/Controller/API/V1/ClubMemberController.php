<?php

namespace App\Controller\API\V1;

use App\Entity\ClubMember;
use App\Form\Type\ClubMemberType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ClubMemberController extends FOSRestController {


    private $accessChecker;

    /**
     * UserDepartmentController constructor.
     * @param ZdlmAccessChecker $accessChecker
     */
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/clubMembers/{id}", requirements={"id": "\d+"}, name="api_club_member_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="ClubMember",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function getAction ( int $id ) {

        $clubMember = $this->getDoctrine()->getRepository( 'App:ClubMember' )->find( $id );
        if ( !$clubMember ) {
            throw $this->createNotFoundException( 'ClubMember not found.' );
        }

        $view = $this->view( $clubMember, 200 );
        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/clubMembers", name="api_club_member_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="ClubMember",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function cgetAction () {

        $clubMembers = $this->getDoctrine()->getRepository( 'App:ClubMember' )->findAll();
        return $this->handleView( $this->view( $clubMembers, 200 ) );
    }

    /**
     * @Rest\Post(path="/clubMembers", name="api_club_member_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="ClubMember",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request) {

        /**
         * @todo check for access restrictions
         */
        $clubMember = new ClubMember();

        return $this->processForm( $request, $clubMember, true );
    }

    /**
     * @Rest\Route(path="/clubMembers/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_club_member_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="ClubMember",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function editAction ( Request $request, int $id ) {

        /**
         * @todo check for access restrictions
         */
        /*if ( !$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED') ) {

            throw $this->createAccessDeniedException('No Anonymous allowed here!!');
        }*/
        $clubMember = $this->getDoctrine()->getRepository( 'App:ClubMember' )->find( $id );
        if ( !$clubMember ) {
            throw $this->createNotFoundException( 'ClubMember not found.' );
        }

        return $this->processForm( $request, $clubMember );
    }

    private function processForm ( Request $request, ClubMember $clubMember, bool $isNew = false ) {

        $form = $this->createForm( ClubMemberType::class, $clubMember );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $clubMember );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_club_member_read', [
                    'version' => 'v1',
                    'id' => $clubMember->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }
}

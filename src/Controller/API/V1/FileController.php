<?php

namespace App\Controller\API\V1;

use App\Entity\File;
use App\Form\Type\FileType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FileController extends FOSRestController {


    private $accessChecker;

    /**
     * UserDepartmentController constructor.
     * @param ZdlmAccessChecker $accessChecker
     */
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/files/{id}", requirements={"id": "\d+"}, name="api_file_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="File",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function getAction ( int $id ) {

        $file = $this->getDoctrine()->getRepository( 'App:File' )->find( $id );
        if ( !$file ) {
            throw $this->createNotFoundException( 'File not found.' );
        }

        $view = $this->view( $file, 200 );
        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/files", name="api_file_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="File",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function cgetAction () {

        $files = $this->getDoctrine()->getRepository( 'App:File' )->findAll();
        return $this->handleView( $this->view( $files, 200 ) );
    }

    /**
     * @Rest\Post(path="/files", name="api_file_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="File",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request) {

        /**
         * @todo check for access restrictions
         */
        $file = new File();

        return $this->processForm( $request, $file, true );
    }

    /**
     * @Rest\Route(path="/files/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_file_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="File",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function editAction ( Request $request, int $id ) {

        /**
         * @todo check for access restrictions
         */
        /*if ( !$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED') ) {

            throw $this->createAccessDeniedException('No Anonymous allowed here!!');
        }*/
        $file = $this->getDoctrine()->getRepository( 'App:File' )->find( $id );
        if ( !$file ) {
            throw $this->createNotFoundException( 'File not found.' );
        }

        return $this->processForm( $request, $file );
    }

    private function processForm ( Request $request, File $file, bool $isNew = false ) {

        $form = $this->createForm( FileType::class, $file );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $file );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_file_read', [
                    'version' => 'v1',
                    'id' => $file->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }
}

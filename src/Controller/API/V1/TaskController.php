<?php

namespace App\Controller\API\V1;

use App\AppEvents;
use App\Entity\Notification;
use App\Entity\Task;
use App\Entity\TaskUser;
use App\Entity\User;
use App\Event\UserPushNotificationEvent;
use App\Form\TaskType;
use App\Services\ABONotificationDescriber;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class TaskController extends FOSRestController {


    private $authChecker;
    private $accessChecker;
    private $describer;

    public function __construct(AuthorizationCheckerInterface $checker, ZdlmAccessChecker $accessChecker, ABONotificationDescriber $describer) {

        $this->authChecker = $checker;
        $this->accessChecker = $accessChecker;
        $this->describer = $describer;
    }

    /**
     * @Rest\Get(path="/users/{userId}/tasks/{id}", requirements={"userId": "\d+", "id": "\d+"}, name="api_task_read", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Task",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @param int $id
     * @return Response
     */
    public function getAction(int $userId, int $id) {

        $result = $this->accessChecker->checkForUser($userId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $task = $this->getDoctrine()->getRepository('App:Task')->find($id);
        return $this->handleView($this->view($task, 200));
    }

    /**
     * @Rest\Get(path="/users/{userId}/tasks", requirements={"userId": "\d+"}, name="api_task_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Task",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @return Response
     */
    public function cgetAction(int $userId) {

        $result = $this->accessChecker->checkForUser($userId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $userTasks = $this->getDoctrine()->getRepository('App:TaskUser')->getUserTasks($userId);
        return $this->handleView($this->view($userTasks, 200));
    }

    /**
     * @Rest\Get(path="/users/{username}/tasks", name="api_task_read_all_username", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Task",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $username
     * @return Response
     */
    public function cgetByUsernameAction(string $username) {

        /**
         * @var $user User
         */
        $user = $this->getDoctrine()->getRepository('App:User')->findOneBy(['username' => $username]);
        if (!$user) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user',
                'message' => 'User was not found',
            ], 404);
        }

        if (true === $this->authChecker->isGranted('ROLE_PARENT')) {

            if ($this->getUser()->getId() !== $user->getParent()->getId()) {
                return new JsonResponse([
                    'status_code' => 403,
                    'error_code' => 'access_denied.not_user_property',
                    'message' => 'you are not allowed to access this resource',
                ], 403);
            }
        } else if (true === $this->authChecker->isGranted('ROLE_PROFESSOR') || true === $this->authChecker->isGranted('ROLE_STUDENT')) {

            if ($this->getUser()->getId() !== $user->getId()) {
                return new JsonResponse([
                    'status_code' => 403,
                    'error_code' => 'access_denied.not_user_property',
                    'message' => 'you are not allowed to access this resource',
                ], 403);
            }
        } else {

            return new JsonResponse([
                'status_code' => 403,
                'error_code' => 'access_denied.role_not_granted',
                'message' => 'you are not authorised',
            ], 403);
        }

        $userTasks = $this->getDoctrine()->getRepository('App:TaskUser')->getUserTasks($user->getId());
        return $this->handleView($this->view($userTasks, 200));
    }

    /**
     * @Rest\Post(path="/users/{userId}/task-user/{id}/{isDone}", requirements={"userId": "\d+", "id": "\d+", "isDone": "done|not-done"}, name="api_task_user_mark_done",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="TaskUser",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     *
     * @param int $userId
     * @param int $id
     * @param string $isDone
     * @return Response
     */
    public function markAsDoneAction(int $userId, int $id, string $isDone) {

        $result = $this->accessChecker->checkForUser($userId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $taskUser = $this->getDoctrine()->getRepository('App:TaskUser')->findOneBy(['id' => $id, 'user' => $result->getUser()]);
        if (!$taskUser) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.task',
                'message' => 'Task was not found',
            ], 404);
        }

        if ($isDone === 'done')
            $taskUser->setDoneAt(new \DateTime());
        else
            $taskUser->setDoneAt(NULL);

        $this->getDoctrine()->getManager()->flush();

        return $this->handleView($this->view(NULL, 204));
    }

    /**
     * @Rest\Post(path="/users/{userId}/tasks", requirements={"userId": "\d+"}, name="api_task_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Task",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @param int $userId
     * @return Response
     */
    public function newAction(Request $request, int $userId) {

        $result = $this->accessChecker->checkForUser($userId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }
        $task = new Task();
        $task->setUser($result->getUser());

        return $this->processForm($request, $task, true);
    }

    /**
     * @Rest\Route(path="/users/{userId}/tasks/{id}", requirements={"userId": "\d+", "id": "\d+"}, methods={"PUT", "PATCH"}, name="api_task_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Task",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $userId
     * @param int $id
     * @return Response
     */
    public function editAction(Request $request, int $userId, int $id) {

        $result = $this->accessChecker->checkForUser($userId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $task = $this->getDoctrine()->getRepository('App:Task')->findOneBy(['id' => $id, 'user' => $result->getUser()]);
        if (!$task) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.task',
                'message' => 'Task was not found',
            ], 404);
        }

        return $this->processForm($request, $task);
    }

    private function processForm(Request $request, Task $task, bool $isNew = false) {

        $form = $this->createForm(TaskType::class, $task);

        $data = json_decode($request->getContent(), true);
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            if ($task->getSubjectClasse()) {
                $task->setDescription($task->getDescription() . ' - 1: task has subCls');
                $this->setTaskList($task);
            }

            if ($isNew) {

                if ($task->getSubjectClasse())
                    $this->setClassTask($task);
                else
                    $this->selfTask($task);
                $view = $this->view(NULL, 201);

                $response = $this->handleView($view);
                $response->headers->set('Location', $this->generateUrl('api_task_read', [
                    'version' => 'v1',
                    'userId' => $task->getUser()->getId(),
                    'id' => $task->getId(),
                ]));
                return $response;
            }

            $em->flush();
            /**
             * @todo create notification to remind user IF dueDate changed.
             */
            $view = $this->view(NULL, 204);
            return $this->handleView($view);
        }

        $view = $this->view($form, 400);
        return $this->handleView($view);
    }

    /**
     * @param Task $task
     */
    private function setClassTask(Task $task) {

        $em = $this->getDoctrine()->getManager();
        $group = $task->getSubjectClasse()->getClasse()->getGroupe();

        $studentGrps = $em->getRepository('App:UserGroup')->findBy(['group' => $group]);
        foreach ($studentGrps as $studentGrp) {

            $taskUser = new TaskUser($task, $studentGrp->getUser());
            $this->pushNotification($taskUser);
            $this->pushNewAssignmentNotification($taskUser);
            $em->persist($taskUser);
        }

        $this->pushAssignmentNotification($task);
        $em->flush();
    }

    private function pushNotification(TaskUser $taskUser, $remindParent = true) {

        $times = [
            (clone $taskUser->getDueDate())->modify('-3 hours'),
            (clone $taskUser->getDueDate())->modify('-1 day'),
            (clone $taskUser->getDueDate())->modify('-3 day'),
            (clone $taskUser->getDueDate())->modify('-7 day'),
        ];

        foreach ($times as $time) {
            if ($time > (new \DateTime())) {

                $dispatcher = $this->get('event_dispatcher');
                $notification = new Notification();
                $notification->setType(158);
                $notification->setDescription($this->describer->describeTaskUserReminder($taskUser));
                $notification->setNotifier($taskUser->getTask()->getUser());
                $event = new UserPushNotificationEvent($notification, $taskUser->getUser(), NULL, $time);
                $dispatcher->dispatch(AppEvents::USER_PUSH_NOTIFICATION, $event);
            }
        }

        if ($remindParent && $times[2] > (new \DateTime())) {

            $dispatcher = $this->get('event_dispatcher');
            $notification = new Notification();
            $notification->setType(159);
            $notification->setDescription($this->describer->describeTaskUserReminder($taskUser));
            $notification->setNotifier($taskUser->getTask()->getUser());
            $event = new UserPushNotificationEvent($notification, $taskUser->getUser()->getParent(), NULL, $times[2]);
            $dispatcher->dispatch(AppEvents::USER_PUSH_NOTIFICATION, $event);
        }
    }

    private function pushNewAssignmentNotification(TaskUser $taskUser) {

        $dispatcher = $this->get('event_dispatcher');
        $notification = new Notification();
        $notification->setType(156);
        $notification->setDescription($this->describer->describeNewAssignment($taskUser->getTask(), $taskUser->getUser()));
        $notification->setNotifier($taskUser->getTask()->getUser());
        $event = new UserPushNotificationEvent($notification, $taskUser->getUser());
        $dispatcher->dispatch(AppEvents::USER_PUSH_NOTIFICATION, $event);

        if ($taskUser->getUser()->getParent()) {
            $notification = new Notification();
            $notification->setType(157);
            $notification->setDescription($this->describer->describeNewAssignment($taskUser->getTask(), $taskUser->getUser()));
            $notification->setNotifier($taskUser->getTask()->getUser());
            $event = new UserPushNotificationEvent($notification, $taskUser->getUser()->getParent());
            $dispatcher->dispatch(AppEvents::USER_PUSH_NOTIFICATION, $event);
        }
    }

    private function pushAssignmentNotification(Task $task) {

        $dispatcher = $this->get('event_dispatcher');
        $notification = new Notification();
        $notification->setType(141);
        $notification->setDescription($this->describer->describeAssignmentReminder($task));
        $notification->setNotifier($task->getUser());
        $event = new UserPushNotificationEvent($notification, $task->getUser(), NULL, $task->getDueDate());
        $dispatcher->dispatch(AppEvents::USER_PUSH_NOTIFICATION, $event);
    }

    /**
     * @param Task $task
     */
    private function selfTask(Task $task) {

        $em = $this->getDoctrine()->getManager();
        $userTask = new TaskUser($task, $task->getUser());
        $this->pushNotification($userTask, false);
        $em->persist($userTask);
        $em->flush();
    }

    private function setTaskList(Task $task) {

        $task->setDescription($task->getDescription() . ' - 2: task type = ' . $task->getType());
        if ($task->getType() == 1)
            $name = 'exams';
        elseif ($task->getType() == 2)
            $name = 'homework';
        elseif ($task->getType() == 3)
            $name = 'file_submission';
        elseif ($task->getType() == 4)
            $name = 'project';

        if (isset($name)) {
            $task->setDescription($task->getDescription() . ' - 3: list name = ' . $name);
            $list = $this->getDoctrine()->getRepository('App:TaskList')->findOneBy(['user'=>NULL, 'name'=>$name]);
            if ($list) {
                $task->setDescription($task->getDescription() . ' - 4: list id = ' . $list->getId());
                $task->setList($list);
                $task->setDescription($task->getDescription() . ' - 3: task list set = true');
            }
        }
    }
}

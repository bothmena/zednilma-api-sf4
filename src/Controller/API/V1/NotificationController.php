<?php

namespace App\Controller\API\V1;

use App\AppEvents;
use App\Entity\Group;
use App\Entity\Notification;
use App\Entity\NotificationUser;
use App\Entity\User;
use App\Event\UserPushNotificationEvent;
use App\Form\Type\NotificationType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NotificationController extends FOSRestController {


    private $accessChecker;

    public function __construct(ZdlmAccessChecker $checker) {

        $this->accessChecker = $checker;
    }

    /**
     * @Rest\Get(path="/users/{userId}/notifications/{id}", requirements={"id": "\d+", "userId": "\d+"}, name="api_notification_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="Notification",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @param int $id
     * @return Response
     */
    public function getAction ( int $userId, int $id ) {


        if ($this->accessChecker->checkForUser(NULL, 'ROLE_USER')->getResponse())
            return $this->accessChecker->checkForUser()->getResponse();

        $user = $this->getUser();
        if ($user->getId() !== $userId) {

            new JsonResponse([
                'status_code' => 403,
                'error_code' => 'access_denied.not_user_property',
                'message' => 'This Notification does not belong to this user',
            ], 403);
        }

        $notification = $this->getDoctrine()->getRepository( 'App:Notification' )->find( $id );
        if ( !$notification ) {

            new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.notification',
                'message' => 'Notification was not found',
            ], 404);
        }

        $notificationUser = $this->getDoctrine()->getRepository( 'App:NotificationUser' )->findOneBy( ['notified'=>$user, 'notification'=>$notification] );
        if ( !$notificationUser ) {

            new JsonResponse([
                'status_code' => 403,
                'error_code' => 'access_denied.not_user_property',
                'message' => 'This Notification does not belong to this user',
            ], 403);
        }

        $view = $this->view( $notification, 200 );
        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/users/{userId}/notifications", requirements={"userId": "\d+"}, name="api_notification_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Notification",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @return Response
     */
    public function cgetAction (int $userId) {

        if ($this->accessChecker->checkForUser(NULL, 'ROLE_USER')->getResponse())
            return $this->accessChecker->checkForUser()->getResponse();

        $user = $this->getUser();
        if ($user->getId() !== $userId) {

            new JsonResponse([
                'status_code' => 403,
                'error_code' => 'access_denied.not_user_property',
                'message' => 'This Notification does not belong to this user',
            ], 403);
        }

        /*$notification = new Notification();
        $notification->setNotifier($this->getUser());
        $notification->setType(0);
        $notification->setTitle('This is a notification from the administration!');
        $notification->setDescription('This is the description of the notification from the administration!');
        $notifUser = new NotificationUser($user, $notification);
        $this->getDoctrine()->getManager()->persist($notification);
        $this->getDoctrine()->getManager()->persist($notifUser);
        $this->getDoctrine()->getManager()->flush();*/

        $notifications = $this->getDoctrine()->getRepository( 'App:NotificationUser' )->getUserLastNotifications($userId);
        return $this->handleView( $this->view( $notifications, 200 ) );
    }

    /**
     * @Rest\Post(path="/users/{userId}/notifications/read", requirements={"userId": "\d+"}, name="api_mark_all_notification_read", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Notification",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @return Response
     */
    public function markAllReadAction (int $userId) {

        if ($this->accessChecker->checkForUser(NULL, 'ROLE_USER')->getResponse())
            return $this->accessChecker->checkForUser()->getResponse();

        $user = $this->getUser();
        if ($user->getId() !== $userId) {

            new JsonResponse([
                'status_code' => 403,
                'error_code' => 'access_denied.not_user_property',
                'message' => 'This Notification does not belong to this user',
            ], 403);
        }

        /**
         * @var $notifications NotificationUser[]
         */
        $notifications = $this->getDoctrine()->getRepository( 'App:NotificationUser' )->getUserUnreadNotifications($userId);
        foreach ($notifications as $notification) {

            $notification->setIsRead(true);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @Rest\Post(path="/users/{userId}/notifications/{notifId}/{mark}", requirements={"id": "\d+", "userId": "\d+", "mark": "read|not-read"}, name="api_mark_notification_read",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Notification",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @param int $notifId
     * @param string $mark
     * @return Response
     */
    public function markAsAction (int $userId, int $notifId, string $mark) {

        if ($this->accessChecker->checkForUser(NULL, 'ROLE_USER')->getResponse())
            return $this->accessChecker->checkForUser()->getResponse();

        $user = $this->getUser();
        if ($user->getId() !== $userId) {

                new JsonResponse([
                    'status_code' => 403,
                    'error_code' => 'access_denied.not_user_property',
                    'message' => 'This Notification does not belong to this user',
                ], 403);
        }

        $notification = $this->getDoctrine()->getRepository( 'App:Notification' )->find( $notifId );
        if ( !$notification ) {

            new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.notification',
                'message' => 'Notification was not found',
            ], 404);
        }

        $notification = $this->getDoctrine()->getRepository( 'App:NotificationUser' )->findOneBy(['notification'=>$notification, 'notified'=>$user]);
        if ( !$notification ) {

            new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.notification',
                'message' => 'Notification was not found',
            ], 404);
        }

        $notification->setIsRead($mark === 'read');
        $this->getDoctrine()->getManager()->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @Rest\Post(path="/users/{userId}/notifications", name="api_notification_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Notification",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @param int $userId
     * @return Response
     */
    public function newAction(Request $request, int $userId) {

        /**
         * @todo check for access restrictions
         * @todo call pushNotification method with either user or group
         */
        $notification = new Notification();
        $this->pushNotification($notification);

//        return $this->processForm( $request, $notification, true );
        return null;
    }

    private function processForm ( Request $request, Notification $notification, bool $isNew = false ) {

        $form = $this->createForm( NotificationType::class, $notification );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $notification );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_notification_read', [
                    'version' => 'v1',
                    'id' => $notification->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }

    private function pushNotification(Notification $notification, User $user = NULL, Group $group = NULL) {

        $dispatcher = $this->get('event_dispatcher');
        $event = new UserPushNotificationEvent($notification, $user, $group);
        $dispatcher->dispatch(AppEvents::USER_PUSH_NOTIFICATION, $event);
    }
}

<?php

namespace App\Controller\API\V1;

use App\Entity\Module;
use App\Form\Type\ModuleType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ModuleController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/specialities/{specId}/modules/{id}",
     *     requirements={
     *          "insId": "\d+", "specId": "\d+",
     *          "id": "\d+"
     *     }, name="api_module_read", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Module",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $specId
     * @param int $id
     * @return Response
     */
    public function getAction( int $insId, int $specId, int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $speciality = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findOneBy( [ 'id' => $specId, 'institute' => $institute ] );
        if ( !$speciality ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.speciality',
                'message'     => 'Speciality was not found',
            ], 404 );
        }

        $module = $this->getDoctrine()->getRepository( 'App:Module' )
            ->findOneBy( [ 'id' => $id, 'speciality' => $speciality ] );
        if ( !$module ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.speciality_module',
                'message'     => 'Speciality module was not found',
            ], 404 );
        }

        return $this->handleView( $this->view( $module, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/specialities/{specId}/modules", requirements={
     *     "insId": "\d+", "specId": "\d+"}, name="api_module_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Module",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $specId
     * @return Response
     */
    public function cgetAction( int $insId, int $specId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $speciality = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findOneBy( [ 'id' => $specId, 'institute' => $institute ] );
        if ( !$speciality ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.speciality',
                'message'     => 'Speciality was not found',
            ], 404 );
        }

        $modules = $this->getDoctrine()->getRepository( 'App:Module' )
            ->findBy( [ 'speciality' => $speciality ] );

        return $this->handleView( $this->view( $modules, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/specialities/{specId}/modules", requirements={"insId": "\d+", "specId": "\d+"},
     *     name="api_module_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Module",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $specId
     * @return Response
     */
    public function newAction( Request $request, int $insId, int $specId ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $speciality = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findOneBy( [ 'id' => $specId, 'institute' => $result->getInstitute() ] );
        if ( !$speciality ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.speciality',
                'message'     => 'Speciality was not found',
            ], 404 );
        }

        $module = new Module();
        $module->setSpeciality( $speciality );

        return $this->processForm( $request, $module, $result->getInstitute()->getId(), true );
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/specialities/{specId}/modules/{id}", requirements={"insId": "\d+", "specId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_module_update",options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Module",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Modules"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $specId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $insId, int $specId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $speciality = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findOneBy( [ 'id' => $specId, 'institute' => $result->getInstitute() ] );
        if ( !$speciality ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.speciality',
                'message'     => 'Speciality was not found',
            ], 404 );
        }

        $module = $this->getDoctrine()->getRepository( 'App:Module' )
            ->findOneBy( [ 'id' => $id, 'speciality' => $speciality ] );
        if ( !$module ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_module',
                'message'     => 'Institute module was not found',
            ], 404 );
        }
        $moduleSubjects = $this->getDoctrine()->getRepository('App:ModuleSubject')->findBy(['module' => $module]);
        $coefSum = 0;
        foreach ($moduleSubjects as $moduleSubject)
            $coefSum += $moduleSubject->getCoefficient();
        $module->setCoefficient($coefSum);

        return $this->processForm( $request, $module, $result->getInstitute()->getId(), false );
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/specialities/{specId}/modules/{id}", requirements={"insId": "\d+", "specId": "\d+","id": "\d+"},
     *     name="api_module_delete", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Module",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "Modules"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $specId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $insId, int $specId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $speciality = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findOneBy( [ 'id' => $specId, 'institute' => $result->getInstitute() ] );
        if ( !$speciality ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.speciality',
                'message'     => 'Speciality was not found',
            ], 404 );
        }

        $module = $this->getDoctrine()->getRepository( 'App:Module' )
            ->findOneBy( [ 'id' => $id, 'speciality' => $speciality ] );
        if ( !$module ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_module',
                'message'     => 'Institute module was not found',
            ], 404 );
        }

        $em = $this->getDoctrine()->getManager();
        $moduleSubjects = $this->getDoctrine()->getRepository('App:ModuleSubject')->findBy(['module'=> $module]);
        foreach ($moduleSubjects as $moduleSubject)
            $em->remove($moduleSubject);

        $em->remove( $module );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Module $module
     * @param int $insId
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Module $module, int $insId, bool $isNew = false ) {

        $form = $this->createForm( ModuleType::class, $module, [
            'max_years' => $module->getSpeciality()->getParent()->getYears()
        ] );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $module );
            $em->flush();

            if ( $isNew ) {

                $response = $this->handleView( $this->view( NULL, 201 ) );
                $response->headers->set( 'Location', $this->generateUrl( 'api_module_read', [
                    'version' => 'v1',
                    'insId'   => $insId,
                    'specId'  => $module->getSpeciality()->getId(),
                    'id'      => $module->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}

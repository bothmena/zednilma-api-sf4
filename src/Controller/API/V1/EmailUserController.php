<?php

namespace App\Controller\API\V1;

use App\Entity\Email;
use App\Form\Type\EmailType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EmailUserController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/users/{userId}/emails/{id}", requirements={"userId": "\d+", "id": "\d+"},
     *     name="api_user_email_read", options={ "method_prefix" =false })
     * @ ApiDoc(
     *     section="Email",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @param int $id
     * @return JsonResponse|Response
     */
    public function getUserEmailAction( int $userId, int $id ) {

        $user = $this->getDoctrine()->getRepository( 'App:User' )
            ->find( $userId );
        if ( !$user ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.user',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $userEmail = $this->getDoctrine()->getRepository( 'App:Email' )->find( $id );
        if ( !$userEmail ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.user_email',
                'message'     => 'User email was not found',
            ], 404 );
        }

        $view = $this->view( $userEmail, 200 );

        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/users/{userId}/emails", requirements={"userId": "\d+"}, name="api_user_email_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Email",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $userId
     * @return JsonResponse|Response
     */
    public function getUserEmailsAction( Request $request, int $userId ) {

        $user = $this->getDoctrine()->getRepository( 'App:User' )->find( $userId );
        if ( !$user ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.user',
                'message'     => 'User was not found',
            ], 404 );
        }

        $enabled = $request->query->get( 'verified' );
        $verified = NULL;
        if ( $enabled === 'true' ) {

            $verified = true;
        } else if ( $enabled === 'false' ) {

            $verified = false;
        }

        $userEmails = $this->getDoctrine()->getRepository( 'App:Email' )
            ->getUserEmails( $userId, $verified );

        return $this->handleView( $this->view( $userEmails, 200 ) );
    }

    /**
     * @Rest\Post(path="/users/{userId}/emails", requirements={"userId": "\d+"}, name="api_user_email_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Email",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $userId
     * @return Response
     */
    public function newUserEmailAction( Request $request, int $userId ) {

        $result = $this->accessChecker->checkForUser( $userId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $email = new Email();
        $email->setUser( $result->getUser() );

        return $this->processForm( $request, $email, true );
    }

    /**
     * @Rest\Route(path="/users/{userId}/emails/{id}", requirements={"userId": "\d+", "id": "\d+"}, methods={"PUT", "PATCH"}, name="api_user_email_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Email",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $userId
     * @param int $id
     * @return mixed|JsonResponse|Response
     */
    public function editAction( Request $request, int $userId, int $id ) {

        $result = $this->accessChecker->checkForUser( $userId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $email = $this->getDoctrine()->getRepository( 'App:Email' )
            ->find( $id );
        if ( !$email ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.email',
                'message'     => 'Email was not found',
            ], 404 );
        }

        if ( $email->getConfirmationToken() === NULL && $email->getIsPrimary() ) {

            return new JsonResponse( [
                'status_code' => 412,
                'error_code'  => 'precondition_failed.vp_email_not_editable',
                'message'     => 'This is a primary, verified email, you can not modify it, you need to make new verified email primary in order to change this email.',
            ], 412 );
        }

        return $this->processForm( $request, $email );
    }

    /**
     * @Rest\Delete(path="/users/{userId}/emails/{id}", requirements={"userId": "\d+", "id": "\d+"}, name="api_user_email_delete", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Email",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @param int $id
     * @return mixed|JsonResponse|Response
     */
    public function deleteAction( int $userId, int $id ) {

        $em = $this->getDoctrine()->getManager();
        $result = $this->accessChecker->checkForUser( $userId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $email = $em->getRepository( 'App:Email' )->find( $id );
        if ( !$email ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.email',
                'message'     => 'Email was not found',
            ], 404 );
        }

        if ( $email->getIsPrimary() ) {

            $newPrimaryEmail = $em->getRepository( 'App:Email' )
                ->getUserAVerifiedEmail( $result->getUser()->getId(), $email->getId() );

            if ( $newPrimaryEmail ) {

                $newPrimaryEmail->setIsPrimary( true );
            } else {

                return new JsonResponse( [
                    'status_code' => 412,
                    'error_code'  => 'precondition_failed.no_verified_email',
                    'message'     => 'We can\'t remove this email because it\'s primary and you have no verified email to replace it.',
                ], 412 );
            }
        }

        $em->remove( $email );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Email $email
     * @param bool $isNew
     * @return JsonResponse|Response
     */
    private function processForm( Request $request, Email $email, bool $isNew = false ) {

        $data = json_decode( $request->getContent(), true );
        if ( isset( $data[ 'email' ] ) && $data[ 'email' ] != $email->getEmail() ) {

            $email->setConfirmationToken(
                $this->get( 'fos_user.util.token_generator' )->generateToken()
            );
        }
        if ( $data[ 'isPrimary' ] && $email->getConfirmationToken() !== NULL ) {

            return new JsonResponse( [
                'status_code' => 412,
                'error_code'  => 'precondition_failed.email_not_verified',
                'message'     => 'you can not set an unverified email primary, please verify it first',
            ], 412 );
        }

        $form = $this->createForm( EmailType::class, $email );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();

            if ( $email->getIsPrimary() ) {

                $userEmails = $em->getRepository( 'App:Email' )->findBy( [ 'user' => $email->getUser() ] );
                foreach ( $userEmails as $userEmail ) {
                    $userEmail->setIsPrimary( false );
                }
                $email->setIsPrimary( true );
                $email->getUser()->setEmail( $email->getEmail() );
            }

            $em->persist( $email->getUser() );
            $em->persist( $email );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_user_email_read', [
                    'version' => 'v1',
                    'userId'  => $email->getUser()->getId(),
                    'id'      => $email->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}

<?php

namespace App\Controller\API\V1;

use App\AppEvents;
use App\Entity\Classe;
use App\Entity\Group;
use App\Entity\Notification;
use App\Entity\User;
use App\Entity\UserClasse;
use App\Entity\UserGroup;
use App\Event\UserPushNotificationEvent;
use App\Services\ABONotificationDescriber;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserClasseController extends FOSRestController {


    private $accessChecker;
    private $describer;

    /**
     * UserDepartmentController constructor.
     * @param ZdlmAccessChecker $accessChecker
     * @param ABONotificationDescriber $describer
     */
    public function __construct(ZdlmAccessChecker $accessChecker, ABONotificationDescriber $describer) {

        $this->accessChecker = $accessChecker;
        $this->describer = $describer;
    }

    /**
     * @Rest\Get(path="/users/{slug}/classes", name="api_user_classe_by_slug", options={ "method_prefix"=false })
     * @ ApiDoc(
     *     section="UserClasse",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $id
     * @return Response
     */
    public function getAction(string $classe) {

        $user = $this->getDoctrine()->getRepository('App:User')->find($id);
        if (!$user instanceof User) {
            throw $this->createNotFoundException('User not found.');
        }

        $view = $this->view($user, 200);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/classes/{classId}/students", requirements={"insId": "\d+", "classId": "\d+"}, name="api_spec_classe_create",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Classe",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $classId
     * @return Response
     */
    public function newUsersClasseAction(Request $request, int $insId, int $classId) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        /**
         * @var Classe $classe
         */
        $classe = $this->getDoctrine()->getRepository('App:Classe')->find($classId);
        if (!$classe) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.classe',
                'message' => 'Class was not found',
            ], 404);
        }

        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);
        if (!empty($data['students'])) {
            $students = $this->getDoctrine()->getRepository('App:User')->findBy(['id'=>$data['students']]);
            if (!empty($data['students'])) {

                foreach ($students as $student) {

                    $userClasse = new UserClasse($student, $classe);
                    $clsGrp = new UserGroup($student, $classe->getGroupe());
                    $branchGrp = new UserGroup($student, $classe->getBranch()->getGroupe());
                    $specGrp = new UserGroup($student, $classe->getBranch()->getParent()->getGroupe());

                    if ($student->getParent()) {

                        $this->addParentToGroups($student->getParent(), $classe->getGroupe(), $classe->getBranch()->getGroupe(), $classe->getBranch()->getParent()->getGroupe());
                    }

                    $this->pushNotification($userClasse);
                    $em->persist($userClasse);
                    $em->persist($clsGrp);
                    $em->persist($branchGrp);
                    $em->persist($specGrp);
                }
            }
        }
        $em->flush();

        return $this->handleView($this->view(NULL, 201));
    }

    private function addParentToGroups(User $parent, Group $clsGrp, Group $branchGroup, Group $specGroup) {

        $em = $this->getDoctrine()->getManager();
        $clsParentGrp = $this->getDoctrine()->getRepository('App:Group')->findOneBy(['parent'=>$clsGrp, 'type'=>'class_p']);
        if ($clsParentGrp) {
            $usrClsParentGrp = new UserGroup($parent, $clsParentGrp);
            $em->persist($usrClsParentGrp);
        }

        $branchParentGrp = $this->getDoctrine()->getRepository('App:Group')->findOneBy(['parent'=>$branchGroup, 'type'=>'speciality_p']);
        if ($branchParentGrp) {
            $usrBranchParentGrp = new UserGroup($parent, $branchParentGrp);
            $em->persist($usrBranchParentGrp);
        }

        $specParentGrp = $this->getDoctrine()->getRepository('App:Group')->findOneBy(['parent'=>$specGroup, 'type'=>'speciality_p']);
        if ($specParentGrp) {
            $usrSpecParentGrp = new UserGroup($parent, $specParentGrp);
            $em->persist($usrSpecParentGrp);
        }
    }

    private function pushNotification(UserClasse $userClasse) {

        $dispatcher = $this->get('event_dispatcher');
        $notification = new Notification();
        $notification->setType(171);
        $notification->setDescription($this->describer->describeUserClasse($userClasse));
        $notification->setNotifier($this->getUser());
        $event = new UserPushNotificationEvent($notification, $userClasse->getUser());
        $dispatcher->dispatch(AppEvents::USER_PUSH_NOTIFICATION, $event);
    }
}

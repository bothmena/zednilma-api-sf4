<?php

namespace App\Controller\API\V1;

use App\Entity\Department;
use App\Entity\Group;
use App\Entity\Institute;
use App\Form\Type\DepartmentType;
use App\Services\ZdlmAccessChecker;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DepartmentController extends FOSRestController {


    private $accessChecker;

    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/departments/{id}",
     *     requirements={
     *          "insId": "\d+",
     *          "id": "\d+"
     *     }, name="api_department_read", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Department",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function getAction(int $insId, int $id) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')
            ->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $department = $this->getDoctrine()->getRepository('App:Department')
            ->getInstituteDepartment($insId, $id);
        if (!$department) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute_department',
                'message' => 'Institute department was not found',
            ], 404);
        }

        return $this->handleView($this->view($department, 200));
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/departments", requirements={
     *     "insId": "\d+"}, name="api_department_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Department",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @return Response
     */
    public function cgetAction(Request $request, int $insId) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        return $this->cgetResponse($request, $institute);
    }

    private function cgetResponse(Request $request, Institute $institute) {

        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $departments = $this->getDoctrine()->getRepository('App:Department')
            ->findBy(['institute' => $institute]);

        $loadSubjects = $request->query->get('subjects');
        $loadMembers = $request->query->get('members');
        if ($loadSubjects === 'true') {

            foreach ($departments as $department) {

                $subjects = $this->getDoctrine()->getRepository('App:Subject')
                    ->findBy(['department' => $department]);
                $department->setSubjects(new ArrayCollection($subjects));
            }
        }
        if ($loadMembers === 'true') {

            foreach ($departments as $department) {

                $members = $this->getDoctrine()->getRepository('App:UserDepartment')
                    ->getDepartmentMembers($department->getId());
                $department->setMembers(new ArrayCollection($members));
            }
        }

        return $this->handleView($this->view($departments, 200));
    }

    /**
     * @Rest\Get(path="/institutes/{insSlug}/departments", name="api_department_read_all_slug", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Department",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @return Response
     */
    public function cgetByInsSlugAction(Request $request, string $insSlug) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->findOneBySlug($insSlug);
        return $this->cgetResponse($request, $institute);
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/departments", requirements={"insId": "\d+"},
     *     name="api_department_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Department",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return Response
     */
    public function newAction(Request $request, int $insId) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $department = new Department();
        $department->getGroupe()->setInstitute($result->getInstitute());
        $department->setInstitute($result->getInstitute());
        return $this->processForm($request, $department, true);
    }

    /**
     * @param Request $request
     * @param Department $department
     * @param bool $isNew
     * @return Response
     */
    private function processForm(Request $request, Department $department, bool $isNew = false) {

        $form = $this->createForm(DepartmentType::class, $department);

        $data = json_decode($request->getContent(), true);
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($department);
            $em->flush();

            if ($isNew) {

                $view = $this->view(NULL, 201);

                $response = $this->handleView($view);
                $response->headers->set('Location', $this->generateUrl('api_department_read', [
                    'version' => 'v1',
                    'insId' => $department->getInstitute()->getId(),
                    'id' => $department->getId(),
                ]));

                return $response;
            }

            $view = $this->view(NULL, 204);

            return $this->handleView($view);
        }

        $view = $this->view($form, 400);

        return $this->handleView($view);
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/departments/{id}", requirements={"insId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_department_update",options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Department",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Departments"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function editAction(Request $request, int $insId, int $id) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $department = $this->getDoctrine()->getRepository('App:Department')
            ->getInstituteDepartment($insId, $id);
        if (!$department) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute_department',
                'message' => 'Institute department was not found',
            ], 404);
        }

        return $this->processForm($request, $department, false);
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/departments/{id}", requirements={"insId": "\d+","id": "\d+"},
     *     name="api_department_delete", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Department",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "Departments"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function deleteAction(int $insId, int $id) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $department = $this->getDoctrine()->getRepository('App:Department')
            ->getInstituteDepartment($insId, $id);
        if (!$department) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute_department',
                'message' => 'Institute department was not found',
            ], 404);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($department);
        $em->flush();

        return $this->handleView($this->view(NULL, 204));
    }
}

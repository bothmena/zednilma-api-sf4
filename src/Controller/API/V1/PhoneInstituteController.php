<?php

namespace App\Controller\API\V1;

use App\Entity\Phone;
use App\Form\Type\PhoneType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PhoneInstituteController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/phones/{id}", requirements={"insId": "\d+", "id": "\d+"}, name="api_institute_phone_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="Phone",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return JsonResponse|Response
     */
    public function getInstitutePhoneAction( int $insId, int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )
            ->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $phone = $this->getDoctrine()->getRepository( 'App:Phone' )->find( $id );
        if ( !$phone ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_phone',
                'message'     => 'Institute phone was not found',
            ], 404 );
        }

        $view = $this->view( $phone, 200 );

        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/phones", requirements={"insId": "\d+"}, name="api_institute_phone_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Email",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return JsonResponse|Response
     */
    public function getInstitutePhonesAction( Request $request, int $insId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )
            ->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $enabled = $request->query->get( 'verified' );
        $verified = NULL;
        if ( $enabled === 'true' ) {

            $verified = true;
        } else if ( $enabled === 'false' ) {

            $verified = false;
        }

        $phones = $this->getDoctrine()->getRepository( 'App:Phone' )
            ->getInstitutePhones( $insId, $verified );

        return $this->handleView( $this->view( $phones, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/phones", requirements={"insId": "\d+"}, name="api_institute_phone_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Phone",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @param int $insId
     * @return Response
     */
    public function newInstitutePhoneAction( Request $request, int $insId ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $phone = new Phone();
        $phone->setInstitute( $result->getInstitute() );

        return $this->processForm( $request, $phone, true );
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/phones/{id}", requirements={"insId": "\d+", "id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_institute_phone_update",options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Phone",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $phone = $this->getDoctrine()->getRepository( 'App:Phone' )->find( $id );
        if ( !$phone ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.phone',
                'message'     => 'Phone was not found',
            ], 404 );
        }

        return $this->processForm( $request, $phone );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/confirm-phone/{code}", requirements={"insId": "\d+", "code": "\d+"}, name="api_institute_confirm_phone", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Phone",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @param int $code
     * @return mixed|JsonResponse|Response
     */
    public function confirmPhoneAction( int $insId, int $code ) {

        $em = $this->getDoctrine()->getManager();
        $phone = $em->getRepository('App:Phone')->getInstituteUnconfirmedPhone( $insId, $code );

        if ( !$phone ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.phone',
                'message'     => 'Phone was not found',
            ], 404 );
        }

        $phone->setVCodeToNull();
        $em = $this->getDoctrine()->getManager();
        $em->persist( $phone );
        $em->flush();
        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/phones/{id}", requirements={"insId": "\d+", "id": "\d+"},
     *     name="api_institute_phone_delete", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Phone",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $phone = $this->getDoctrine()->getRepository( 'App:Phone' )->find( $id );
        if ( !$phone ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.phone',
                'message'     => 'Phone was not found',
            ], 404 );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove( $phone );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Phone $phone
     * @param bool $isNew
     * @return Response
     */
    private function processForm ( Request $request, Phone $phone, bool $isNew = false ) {

        $data = json_decode( $request->getContent(), true );

        if ( isset( $data[ 'number' ] ) ) {

            $dataNumber = preg_replace( '/\s/', '', $data[ 'number' ] );
            $phoneNumber = preg_replace( '/\s/', '', $phone->getNumber() );
            if ( $dataNumber !== $phoneNumber ) {

                $phone->setVerificationCode();
            }
        }

        $form = $this->createForm( PhoneType::class, $phone );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $phone );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_institute_phone_read', [
                    'version' => 'v1',
                    'insId' => $phone->getInstitute()->getId(),
                    'id' => $phone->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }
}

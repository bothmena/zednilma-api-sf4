<?php

namespace App\Controller\API\V1;

use App\Entity\Institute;
use App\Entity\User;
use App\Entity\UserInstitute;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserInstituteController extends FOSRestController {

    private $fields = [
        'id' => 'id',
        'username' => 'username',
        'first_name' => 'firstName',
        'last_name' => 'lastName',
        'email' => 'email',
        'last_login' => 'lastLogin',
    ];
    private $roles = [
        'professor' => 'ROLE_PROFESSOR',
        'ins_admin' => 'ROLE_INS_ADMIN',
        'ins_staff' => 'ROLE_INS_STAFF',
        'student' => 'ROLE_STUDENT',
        'parent' => 'ROLE_PARENT',
    ];
    private $checker;

    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->checker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/users/{username}/fire-data", name="api_user_institute_read",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="UserInstitute",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $username
     * @return JsonResponse
     */
    public function getUserFireDataAction(string $username) {

        /**
         * @var UserInstitute $userInstitute
         */
        $userInstitute = $this->getDoctrine()->getRepository('App:UserInstitute')->getUserInstitute($username);

        if (!$userInstitute) {

            $user = $this->getDoctrine()->getRepository('App:User')->findOneBy([
                'username' => $username
            ]);
            if ($user) {

                $data = $this->getUserData($user);
                return new JsonResponse($data, 200);
            }
            return new JsonResponse(['code' => 404, 'message' => 'user_institute.user_404',
                'params' => ['username' => $username]], 404);
        }

        $data = $this->getUserData($userInstitute->getUser(), $userInstitute->getInstitute());
        return new JsonResponse($data, 200);
    }

    private function getUserData(User $user, Institute $institute = NULL): array {

        $location = $user->getLocation();
        $data = [
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'email' => $user->getEmail(),
            'role' => $user->getRole(),
            'first_name' => $user->getFirstName(),
            'middle_name' => $user->getMiddleName(),
            'last_name' => $user->getLastName(),
            'birthday' => $user->getBirthdayStr(),
            'gender' => $user->getGender(),
            'cin' => $user->getCin(),
            'language' => $user->getLanguage(),
            'groups' => $user->getGroups(),
            'image_url' => $user->getImageUrl(),
            'location' => $location ? [
                'id' => $location->getId(),
                'country' => $location->getCountry(),
                'state' => $location->getState(),
                'city' => $location->getCity(),
                'address' => $location->getAddress(),
                'zip_code' => $location->getZipCode(),
                'map_lat' => $location->getMapLat(),
                'map_long' => $location->getMapLong(),
                'is_primary' => $location->getIsPrimary()
            ] : [
                'id' => '',
                'country' => '',
                'state' => '',
                'city' => '',
                'address' => '',
                'zip_code' => '',
                'map_lat' => '',
                'map_long' => '',
                'is_primary' => ''
            ],
            'institute_id' => 0,
            'institute_slug' => '',
        ];

        if ($institute) {

            $data['institute_id'] = $institute->getId();
            $data['institute_slug'] = $institute->getSlug();
        }

        return $data;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/members", requirements={"insId": "\d+"}, name="api_institute_members_read_all", options={ "method_prefix" = false })
     * @Rest\View()
     * @ ApiDoc(
     *     section="UserInstitute",
     *     description="Institute members, could be filtered with role and gender and ordered by some fields such as first/last name, email, username...",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return Response
     */
    public function cgetMembersAction(Request $request, int $insId) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute instanceof Institute) {

            return new JsonResponse(['code' => 404, 'error' => 'institute.not_found sdsd'], 404);
        }
        $role = $request->query->get('role');
        $orderBy = $request->query->get('order_by');
        $gender = $request->query->get('gender');
        $orderBy = array_key_exists($orderBy, $this->fields) ? $this->fields[$orderBy] : NULL;
        $role = array_key_exists($role, $this->roles) ? $this->roles[$role] : NULL;

        /**
         * @var $insUsers UserInstitute[]
         */
        $insUsers = $this->getDoctrine()->getRepository('App:UserInstitute')->getInstituteMembers($role, $gender, $orderBy);
        $members = [];
        foreach ($insUsers as $insUser) {
            array_push($members, $insUser->getUser());
        }

        $view = $this->view($members, 200);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get(path="/institutes/{insSlug}/members", name="api_institute_members_read_all_by_slug", options={ "method_prefix" = false })
     * @Rest\View()
     * @ ApiDoc(
     *     section="UserInstitute",
     *     description="Institute members, could be filtered with role and gender and ordered by some fields such as first/last name, email, username...",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param string $insSlug
     * @return Response
     */
    public function cgetMembersBySlugAction(Request $request, string $insSlug) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->findOneBy(['slug' => $insSlug]);
        if (!$institute instanceof Institute) {

            return new JsonResponse(['code' => 404, 'error' => 'institute.not_found'], 404);
        }
        $role = $request->query->get('role');
        $orderBy = $request->query->get('order_by');
        $gender = $request->query->get('gender');
        $orderBy = array_key_exists($orderBy, $this->fields) ? $this->fields[$orderBy] : NULL;
        $role = array_key_exists($role, $this->roles) ? $this->roles[$role] : NULL;

        /**
         * @var $insUsers UserInstitute[]
         */
        $insUsers = $this->getDoctrine()->getRepository('App:UserInstitute')->getInstituteMembers($role, $gender, $orderBy);
        $members = [];
        foreach ($insUsers as $insUser) {
            array_push($members, $insUser->getUser());
        }

        $view = $this->view($members, 200);
        return $this->handleView($view);
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/members/{role}", requirements={"insId": "\d+", "role": "student|professor|staff"}, name="api_institute_member_create"
     * , options={ "method_prefix" = false })
     * @Rest\View()
     * @ ApiDoc(
     *     section="UserInstitute",
     *     description="Institute members, could be filtered with role and gender and ordered by some fields such as first/last name, email, username...",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return Response
     */
    public function newMembersAction(Request $request, int $insId) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute instanceof Institute) {

            return new JsonResponse(['code' => 404, 'error' => 'institute.not_found'], 404);
        }
        $role = $request->query->get('role');
        $orderBy = $request->query->get('order_by');
        $gender = $request->query->get('gender');
        $orderBy = array_key_exists($orderBy, $this->fields) ? $this->fields[$orderBy] : NULL;
        $role = array_key_exists($role, $this->roles) ? $this->roles[$role] : NULL;

        /**
         * @var $insUsers UserInstitute[]
         */
        $insUsers = $this->getDoctrine()->getRepository('App:UserInstitute')->getInstituteMembers($role, $gender, $orderBy);
        $members = [];
        foreach ($insUsers as $insUser) {
            array_push($members, $insUser->getUser());
        }

        $view = $this->view($members, 200);
        return $this->handleView($view);
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/members/{memberId}/{status}", requirements={"insId": "\d+", "status": "enable|disable"}, name="api_institute_change_member_status",
     *     options={ "method_prefix" = false })
     * @Rest\View()
     * @ ApiDoc(
     *     section="UserInstitute",
     *     description="Institute members, could be filtered with role and gender and ordered by some fields such as first/last name, email, username...",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $memberId
     * @param string $status
     * @return Response
     */
    public function changeMemberStatusAction(Request $request, int $insId, int $memberId, string $status) {

        $result = $this->checker->checkForInstitute($insId);
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $user = $this->getDoctrine()->getRepository('App:User')->find($memberId);
        if (!$user instanceof User)
            return new JsonResponse(['code' => 404, 'error' => 'user.not_found', 'message' => 'User was not found.'], 404);

        $userInstitue = $this->getDoctrine()->getRepository('App:UserInstitute')->findOneBy(['institute'=>$result->getInstitute(), 'user'=>$user, 'dateOut'=>Null]);
        if (!$userInstitue instanceof UserInstitute)
            return new JsonResponse(['code' => 404, 'error' => 'user_institute.not_found', 'message' => 'Institute Member was not found.'], 404);

        $user->setEnabled($status === 'enable');
        $this->getDoctrine()->getManager()->flush();
        return $this->handleView($this->view(NULL, 200));
    }
}

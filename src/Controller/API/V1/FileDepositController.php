<?php

namespace App\Controller\API\V1;

use App\Entity\FileDeposit;
use App\Form\Type\FileDepositType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FileDepositController extends FOSRestController {


    private $accessChecker;

    /**
     * UserDepartmentController constructor.
     * @param ZdlmAccessChecker $accessChecker
     */
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/fileDeposits/{id}", requirements={"id": "\d+"}, name="api_file_deposit_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="FileDeposit",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function getAction ( int $id ) {

        $fileDeposit = $this->getDoctrine()->getRepository( 'App:FileDeposit' )->find( $id );
        if ( !$fileDeposit ) {
            throw $this->createNotFoundException( 'FileDeposit not found.' );
        }

        $view = $this->view( $fileDeposit, 200 );
        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/fileDeposits", name="api_file_deposit_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="FileDeposit",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function cgetAction () {

        $fileDeposits = $this->getDoctrine()->getRepository( 'App:FileDeposit' )->findAll();
        return $this->handleView( $this->view( $fileDeposits, 200 ) );
    }

    /**
     * @Rest\Post(path="/fileDeposits", name="api_file_deposit_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="FileDeposit",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request) {

        /**
         * @todo check for access restrictions
         */
        $fileDeposit = new FileDeposit();

        return $this->processForm( $request, $fileDeposit, true );
    }

    /**
     * @Rest\Route(path="/fileDeposits/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_file_deposit_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="FileDeposit",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function editAction ( Request $request, int $id ) {

        /**
         * @todo check for access restrictions
         */
        /*if ( !$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED') ) {

            throw $this->createAccessDeniedException('No Anonymous allowed here!!');
        }*/
        $fileDeposit = $this->getDoctrine()->getRepository( 'App:FileDeposit' )->find( $id );
        if ( !$fileDeposit ) {
            throw $this->createNotFoundException( 'FileDeposit not found.' );
        }

        return $this->processForm( $request, $fileDeposit );
    }

    private function processForm ( Request $request, FileDeposit $fileDeposit, bool $isNew = false ) {

        $form = $this->createForm( FileDepositType::class, $fileDeposit );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $fileDeposit );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_file_deposit_read', [
                    'version' => 'v1',
                    'id' => $fileDeposit->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }
}

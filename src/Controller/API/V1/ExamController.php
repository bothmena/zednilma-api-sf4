<?php

namespace App\Controller\API\V1;

use App\Entity\Exam;
use App\Form\Type\ExamType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ExamController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/subjects/{subId}/exams/{id}", requirements={"insId": "\d+", "subId": "\d+", "id": "\d+"}, name="api_exam_read",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Exam",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $id
     * @return Response
     */
    public function getAction ( int $id ) {

        $exam = $this->getDoctrine()->getRepository( 'App:Exam' )->find( $id );
        if ( !$exam ) {
            throw $this->createNotFoundException( 'Exam not found.' );
        }

        $view = $this->view( $exam, 200 );
        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/subjects/{subId}/exams", requirements={"insId": "\d+", "subId": "\d+"}, name="api_exam_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Exam",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $subId
     * @return JsonResponse|Response
     */
    public function cgetAction (int $insId, int $subId) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $subject = $this->getDoctrine()->getRepository( 'App:Subject' )->getInstituteSubject( $insId, $subId );
        if ( !$subject ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.subject',
                'message'     => 'Subject was not found',
            ], 404 );
        }

        $exams = $this->getDoctrine()->getRepository('App:Exam')->findBy(['subject'=>$subject], ['coefficient'=>'DESC']);

        return $this->handleView( $this->view( $exams, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/subjects/{subId}/exams", requirements={"insId": "\d+", "subId": "\d+"}, name="api_exam_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Exam",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @param int $insId
     * @param int $subId
     * @return Response
     */
    public function newAction(Request $request, int $insId, int $subId) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $subject = $this->getDoctrine()->getRepository( 'App:Subject' )->getInstituteSubject( $insId, $subId );
        if ( !$subject ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.subject',
                'message'     => 'Subject was not found',
            ], 404 );
        }

        $exam = new Exam();
        $exam->setSubject($subject);

        return $this->processForm( $request, $exam, $insId, true );
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/subjects/{subId}/exams/{id}", requirements={"insId": "\d+", "subId": "\d+", "id": "\d+"}, methods={"PUT", "PATCH"}, name="api_exam_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Exam",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $subId
     * @param int $id
     * @return Response
     */
    public function editAction ( Request $request, int $insId, int $subId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $subject = $this->getDoctrine()->getRepository( 'App:Subject' )->getInstituteSubject( $insId, $subId );
        if ( !$subject ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.department',
                'message'     => 'Department was not found',
            ], 404 );
        }

        $exam = $this->getDoctrine()->getRepository( 'App:Exam' )->find( $id );
        if ( !$subject ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.exam',
                'message'     => 'Exam was not found',
            ], 404 );
        }

        return $this->processForm( $request, $exam );
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/subjects/{subId}/exams/{id}", requirements={"insId": "\d+", "subId": "\d+", "id": "\d+"}, name="api_exam_delete",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Exam",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $subId
     * @param int $id
     * @return Response
     */
    public function deleteAction ( int $insId, int $subId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $subject = $this->getDoctrine()->getRepository( 'App:Subject' )->getInstituteSubject( $insId, $subId );
        if ( !$subject ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.department',
                'message'     => 'Department was not found',
            ], 404 );
        }

        $exam = $this->getDoctrine()->getRepository( 'App:Exam' )->find( $id );
        if ( !$subject ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.exam',
                'message'     => 'Exam was not found',
            ], 404 );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($exam);
        $em->flush();

        return $this->handleView($this->view(NULL, 204));
    }

    private function processForm ( Request $request, Exam $exam, int $insId = 0, bool $isNew = false ) {

        $form = $this->createForm( ExamType::class, $exam );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $exam );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_exam_read', [
                    'version' => 'v1',
                    'insId' => $insId,
                    'subId' => $exam->getSubject()->getId(),
                    'id' => $exam->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }
}

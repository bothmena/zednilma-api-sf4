<?php

namespace App\Controller\API\V1;

use App\Entity\Group;
use App\Entity\Post;
use App\Form\Type\PostType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/posts/{id}",
     *     requirements={
     *          "insId": "\d+",
     *          "id": "\d+"
     *     }, name="api_post_read", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Post",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function getAction( int $insId, int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )
            ->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $post = $this->getDoctrine()->getRepository( 'App:Post' )
            ->getInstitutePost( $insId, $id );
        if ( !$post ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_post',
                'message'     => 'Institute post was not found',
            ], 404 );
        }

        return $this->handleView( $this->view( $post, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/posts", requirements={
     *     "insId": "\d+"}, name="api_post_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Post",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return Response
     */
    public function cgetAction( Request $request, int $insId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

//        $grp1 = new Group();
//        $grp1->setName('Department Chief');
//        $grp1->setType('department');
//        $grp1->setInstitute($institute);
//        $post1 = new Post();
//        $post1->setType('department');
//        $post1->setName('Department Chief');
//        $post1->setDescription('Department Chief Description');
//        $post1->setGroupe($grp1);
//
//        $grp2 = new Group();
//        $grp2->setName('Department Member');
//        $grp2->setType('department');
//        $grp2->setInstitute($institute);
//        $post2 = new Post();
//        $post2->setType('department');
//        $post2->setName('Department Member');
//        $post2->setDescription('Department Member Description');
//        $post2->setGroupe($grp2);
//
//        $em = $this->getDoctrine()->getManager();
//        $em->persist($grp1);
//        $em->persist($grp2);
//        $em->persist($post1);
//        $em->persist($post2);
//        $em->flush();

        $type = $request->query->get('type');
        $posts = $this->getDoctrine()->getRepository( 'App:Post' )->getInstitutePosts( $insId, $type );

        return $this->handleView( $this->view( $posts, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/posts", requirements={"insId": "\d+"},
     *     name="api_post_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Post",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return Response
     */
    public function newAction( Request $request, int $insId ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $post = new Post();
        $group = new Group();
        $group->setInstitute($result->getInstitute());
        $post->setGroupe($group);
        $post->setInstitute( $result->getInstitute() );
        return $this->processForm( $request, $post, true );
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/posts/{id}", requirements={"insId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_post_update",options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Post",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Posts"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $post = $this->getDoctrine()->getRepository( 'App:Post' )
            ->getInstitutePost( $insId, $id );
        if ( !$post ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_post',
                'message'     => 'Institute post was not found',
            ], 404 );
        }

        return $this->processForm( $request, $post, false );
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/posts/{id}", requirements={"insId": "\d+","id": "\d+"},
     *     name="api_post_delete", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Post",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "Posts"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $post = $this->getDoctrine()->getRepository( 'App:Post' )
            ->getInstitutePost( $insId, $id );
        if ( !$post ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_post',
                'message'     => 'Institute post was not found',
            ], 404 );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove( $post );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Post $post
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Post $post, bool $isNew = false ) {

        $form = $this->createForm( PostType::class, $post );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $post );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_post_read', [
                    'version'  => 'v1',
                    'insId'    => $post->setInstitute()->getId(),
                    'id'       => $post->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}

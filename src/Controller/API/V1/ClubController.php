<?php

namespace App\Controller\API\V1;

use App\Entity\Club;
use App\Form\Type\ClubType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ClubController extends FOSRestController {


    private $accessChecker;

    /**
     * UserDepartmentController constructor.
     * @param ZdlmAccessChecker $accessChecker
     */
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/clubs/{id}", requirements={"id": "\d+"}, name="api_club_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="Club",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $id
     * @return Response
     */
    public function getAction ( int $id ) {

        $club = $this->getDoctrine()->getRepository( 'App:Club' )->find( $id );
        if ( !$club ) {
            throw $this->createNotFoundException( 'Club not found.' );
        }

        $view = $this->view( $club, 200 );
        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/clubs", name="api_club_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Club",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function cgetAction () {

        $clubs = $this->getDoctrine()->getRepository( 'App:Club' )->findAll();
        return $this->handleView( $this->view( $clubs, 200 ) );
    }

    /**
     * @Rest\Post(path="/clubs", name="api_club_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Club",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request) {

        /**
         * @todo check for access restrictions
         */
        $club = new Club();

        return $this->processForm( $request, $club, true );
    }

    /**
     * @Rest\Route(path="/clubs/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_club_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Club",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function editAction ( Request $request, int $id ) {

        /**
         * @todo check for access restrictions
         */
        /*if ( !$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED') ) {

            throw $this->createAccessDeniedException('No Anonymous allowed here!!');
        }*/
        $club = $this->getDoctrine()->getRepository( 'App:Club' )->find( $id );
        if ( !$club ) {
            throw $this->createNotFoundException( 'Club not found.' );
        }

        return $this->processForm( $request, $club );
    }

    private function processForm ( Request $request, Club $club, bool $isNew = false ) {

        $form = $this->createForm( ClubType::class, $club );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $club );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_club_read', [
                    'version' => 'v1',
                    'id' => $club->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }
}

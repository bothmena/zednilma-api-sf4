<?php

namespace App\Controller\API\V1;

use App\Entity\Phone;
use App\Form\Type\PhoneType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PhoneUserController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/users/{userId}/phones/{id}", requirements={"userId": "\d+", "id": "\d+"}, name="api_user_phone_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="Phone",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @param int $id
     * @return JsonResponse|Response
     */
    public function getUserPhoneAction( int $userId, int $id ) {

        $user = $this->getDoctrine()->getRepository( 'App:User' )
            ->find( $userId );
        if ( !$user ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.user',
                'message'     => 'User was not found',
            ], 404 );
        }

        $phone = $this->getDoctrine()->getRepository( 'App:Phone' )->find( $id );
        if ( !$phone ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.user_phone',
                'message'     => 'User phone was not found',
            ], 404 );
        }

        $view = $this->view( $phone, 200 );

        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/users/{userId}/phones", requirements={"userId": "\d+"}, name="api_user_phone_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Phone",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $userId
     * @return JsonResponse|Response
     */
    public function getUserPhonesAction( Request $request, int $userId ) {

        $user = $this->getDoctrine()->getRepository( 'App:User' )
            ->find( $userId );
        if ( !$user ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.user',
                'message'     => 'User was not found',
            ], 404 );
        }

        $enabled = $request->query->get( 'verified' );
        $verified = NULL;
        if ( $enabled === 'true' ) {

            $verified = true;
        } else if ( $enabled === 'false' ) {

            $verified = false;
        }

        $phones = $this->getDoctrine()->getRepository( 'App:Phone' )
            ->getUserPhones( $userId, $verified );

        return $this->handleView( $this->view( $phones, 200 ) );
    }

    /**
     * @Rest\Post(path="/users/{userId}/phones", requirements={"userId": "\d+"}, name="api_user_phone_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Phone",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $userId
     * @return Response
     */
    public function newUserPhoneAction( Request $request, int $userId ) {

        $result = $this->accessChecker->checkForUser( $userId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $phone = new Phone();
        $phone->setUser( $result->getUser() );

        return $this->processForm( $request, $phone, true );
    }

    /**
     * @Rest\Route(path="/users/{userId}/phones/{id}", requirements={"userId": "\d+", "id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_user_phone_update",options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Phone",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $userId
     * @param int $id
     * @return JsonResponse|Response
     */
    public function editAction( Request $request, int $userId, int $id ) {

        $result = $this->accessChecker->checkForUser( $userId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $phone = $this->getDoctrine()->getRepository( 'App:Phone' )->find( $id );
        if ( !$phone ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.phone',
                'message'     => 'Phone was not found',
            ], 404 );
        }

        return $this->processForm( $request, $phone );
    }

    /**
     * @Rest\Post(path="/users/{userId}/confirm-phone/{code}", requirements={"userId": "\d+", "code": "\d+"}, name="api_user_confirm_phone",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Phone",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @param int $code
     * @return JsonResponse|Response
     */
    public function confirmPhoneAction( int $userId, int $code ) {

        $em = $this->getDoctrine()->getManager();
        $phone = $em->getRepository('App:Phone')->getUserUnconfirmedPhone( $userId, $code );

        if ( !$phone ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.phone',
                'message'     => 'Phone was not found',
            ], 404 );
        }

        if ( $phone->getVerificationCode() === $code ) {

            $phone->setVCodeToNull();
            $em = $this->getDoctrine()->getManager();
            $em->persist( $phone );
            $em->flush();
            return $this->handleView( $this->view( NULL, 204 ) );
        } else {

            return new JsonResponse( [
                'status_code' => 412,
                'error_code'  => 'precondition_failed.verification_code_mismatch',
                'message'     => 'The code must be equal to the verification code send via SMS',
            ], 412 );
        }
    }

    /**
     * @Rest\Delete(path="/users/{userId}/phones/{id}", requirements={"insId": "\d+", "id": "\d+"},
     *     name="api_user_phone_delete", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Phone",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @param int $id
     * @return JsonResponse|Response
     */
    public function deleteAction( int $userId, int $id ) {

        $result = $this->accessChecker->checkForUser( $userId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $phone = $this->getDoctrine()->getRepository( 'App:Phone' )->find( $id );
        if ( !$phone ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.phone',
                'message'     => 'Phone was not found',
            ], 404 );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove( $phone );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Phone $phone
     * @param bool $isNew
     * @return Response
     */
    private function processForm ( Request $request, Phone $phone, bool $isNew = false ) {

        $data = json_decode( $request->getContent(), true );

        if ( isset( $data[ 'number' ] ) ) {

            $dataNumber = preg_replace( '/\s/', '', $data[ 'number' ] );
            $phoneNumber = preg_replace( '/\s/', '', $phone->getNumber() );
            if ( $dataNumber !== $phoneNumber ) {

                $phone->setVerificationCode();
            }
        }

        $form = $this->createForm( PhoneType::class, $phone );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $phone );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_user_phone_read', [
                    'version' => 'v1',
                    'userId' => $phone->getUser()->getId(),
                    'id' => $phone->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }
}

<?php

namespace App\Controller\API\V1;

use App\Entity\Group;
use App\Form\Type\GroupType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GroupController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/groups/{id}",
     *     requirements={
     *          "insId": "\d+",
     *          "id": "\d+"
     *     }, name="api_group_read", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Group",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function getAction( int $insId, int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )
            ->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $group = $this->getDoctrine()->getRepository( 'App:Group' )
            ->getInstituteGroup( $insId, $id );
        if ( !$group ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_group',
                'message'     => 'Institute group was not found',
            ], 404 );
        }

        return $this->handleView( $this->view( $group, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/groups", requirements={
     *     "insId": "\d+"}, name="api_group_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Group",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @return Response
     */
    public function cgetAction( int $insId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $groups = $this->getDoctrine()->getRepository( 'App:Group' )
            ->findBy( [ 'institute' => $institute ] );

        return $this->handleView( $this->view( $groups, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/groups", requirements={"insId": "\d+"},
     *     name="api_group_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Group",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return Response
     */
    public function newAction( Request $request, int $insId ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $group = new Group();
        $group->setInstitute( $result->getInstitute() );
        return $this->processForm( $request, $group, true );
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/groups/{id}", requirements={"insId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_group_update",options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Group",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Groups"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $group = $this->getDoctrine()->getRepository( 'App:Group' )
            ->getInstituteGroup( $insId, $id );
        if ( !$group ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_group',
                'message'     => 'Institute group was not found',
            ], 404 );
        }

        return $this->processForm( $request, $group, false );
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/groups/{id}", requirements={"insId": "\d+","id": "\d+"},
     *     name="api_group_delete", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Group",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "Groups"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        /**
         * @var Group $group
         */
        $group = $this->getDoctrine()->getRepository( 'App:Group' )->getInstituteGroup( $insId, $id );
        if ( !$group ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_group',
                'message'     => 'Institute group was not found',
            ], 404 );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove( $group );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Group $group
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Group $group, bool $isNew = false ) {

        $form = $this->createForm( GroupType::class, $group );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $group );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_group_read', [
                    'version'  => 'v1',
                    'insId'    => $group->getInstitute()->getId(),
                    'id'       => $group->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}

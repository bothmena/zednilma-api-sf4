<?php

namespace App\Controller\API\V1;

use App\Entity\Curriculum;
use App\Form\Type\CurriculumType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CurriculumController extends FOSRestController {


    private $accessChecker;

    /**
     * UserDepartmentController constructor.
     * @param ZdlmAccessChecker $accessChecker
     */
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/curriculums/{id}", requirements={"id": "\d+"}, name="api_curriculum_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="Curriculum",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function getAction ( int $id ) {

        $curriculum = $this->getDoctrine()->getRepository( 'App:Curriculum' )->find( $id );
        if ( !$curriculum ) {
            throw $this->createNotFoundException( 'Curriculum not found.' );
        }

        $view = $this->view( $curriculum, 200 );
        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/curriculums", name="api_curriculum_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Curriculum",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function cgetAction () {

        $curriculums = $this->getDoctrine()->getRepository( 'App:Curriculum' )->findAll();
        return $this->handleView( $this->view( $curriculums, 200 ) );
    }

    /**
     * @Rest\Post(path="/curriculums", name="api_curriculum_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Curriculum",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request) {

        /**
         * @todo check for access restrictions
         */
        $curriculum = new Curriculum();

        return $this->processForm( $request, $curriculum, true );
    }

    /**
     * @Rest\Route(path="/curriculums/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_curriculum_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Curriculum",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function editAction ( Request $request, int $id ) {

        /**
         * @todo check for access restrictions
         */
        /*if ( !$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED') ) {

            throw $this->createAccessDeniedException('No Anonymous allowed here!!');
        }*/
        $curriculum = $this->getDoctrine()->getRepository( 'App:Curriculum' )->find( $id );
        if ( !$curriculum ) {
            throw $this->createNotFoundException( 'Curriculum not found.' );
        }

        return $this->processForm( $request, $curriculum );
    }

    private function processForm ( Request $request, Curriculum $curriculum, bool $isNew = false ) {

        $form = $this->createForm( CurriculumType::class, $curriculum );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $curriculum );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_curriculum_read', [
                    'version' => 'v1',
                    'id' => $curriculum->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }
}

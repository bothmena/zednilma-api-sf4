<?php

namespace App\Controller\API\V1;

use App\Entity\Institute;
use App\Entity\Timetable;
use App\Form\Type\InstituteType;
use App\Form\Type\InstituteEditType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Time;

class InstituteController extends FOSRestController {


    private $accessChecker;

    /**
     * UserDepartmentController constructor.
     * @param ZdlmAccessChecker $accessChecker
     */
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/institutes/{id}", requirements={"id": "\d+"}, name="api_institute_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="Institute",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $id
     * @return Response
     */
    public function getAction( int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $id );
        if ( !$institute ) {
            throw $this->createNotFoundException( 'Institute not found.' );
        }

        $view = $this->view( $institute, 200 );

        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/institutes/{slug}", name="api_institute_read_slug", options={ "method_prefix" = false })
     * @Rest\View()
     * @ ApiDoc(
     *     section="Institute",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $slug
     * @return Response
     */
    public function getBySlugAction( string $slug ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->findOneBy( ['slug'=>$slug] );

        if ( !$institute ) {
            throw $this->createNotFoundException( 'Institute not found' );
        }

        $view = $this->view( $institute, 200 );

        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/institutes", name="api_institute_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Institute",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @return Response
     */
    public function cgetAction( Request $request ) {

        $instituteFields = [ 'context', 'id', 'name', 'slug', 'slogan', 'description',
            'foundation_date', 'website', 'type', 'parental_control', 'join_date' ];

        $institutes = $this->getDoctrine()->getRepository( 'App:Institute' )->findAll();

        if ( $request->query->get( 'fields' ) ) {
            $fields = explode( ',', $request->query->get( 'fields' ) );
            $result = [];

            foreach ( $institutes as $institute ) {
                $inst = [];
                foreach ( $fields as $field ) {
                    if ( in_array( $field, $instituteFields ) ) {
                        $inst[ $field ] = $institute->getValueByKey( $field );
                    }
                }
                array_push( $result, $inst );
            }

            return $this->handleView( $this->view( $result, 200 ) );
        }

        return $this->handleView( $this->view( $institutes, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes", name="api_institute_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Institute",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @return Response
     * @throws \LogicException
     */
    public function newAction( Request $request ) {

        $result = $this->accessChecker->checkForUser(NULL, 'ROLE_ADMIN');
        if ($result->getResponse())
            return $result->getResponse();

        $institute = new Institute();
        $logo = $this->getDoctrine()->getRepository( 'App:Image' )->findOneBy( [
            'entity' => 'web',
            'name'   => 'institute-logo.png',
        ] );
        $cover = $this->getDoctrine()->getRepository( 'App:Image' )->findOneBy( [
            'entity' => 'web',
            'name'   => 'institute-cover.jpg',
        ] );
        $institute->setProfileImage( $logo );
        $institute->setCoverImage( $cover );

        return $this->processForm( $request, $institute, true );
    }

    /**
     * @Rest\Route(path="/institutes/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_institute_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Institute",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $id ) {

        $result = $this->accessChecker->checkForInstitute($id);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $id );
        if ( !$institute ) {
            throw $this->createNotFoundException( 'Institute not found.' );
        }

        return $this->processForm( $request, $institute );
    }

    private function processForm( Request $request, Institute $institute, bool $isNew = false ) {

        if ( $isNew )
            $form = $this->createForm( InstituteType::class, $institute );
        else
            $form = $this->createForm( InstituteEditType::class, $institute );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $institute );

            if ( $isNew ) {

                $timetable = new Timetable();
                $timetable->setInstitute($institute);
                $timetable->setName('timetable.default');
                $em->persist( $timetable );
                $em->flush();

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_institute_read', [
                    'version' => 'v1',
                    'id'      => $institute->getId(),
                ] ) );

                return $response;
            }

            $em->flush();

            return $this->handleView( $this->view( NULL, 204 ) );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}
 
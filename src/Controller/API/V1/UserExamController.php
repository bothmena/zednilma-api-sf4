<?php

namespace App\Controller\API\V1;

use App\Entity\UserExam;
use App\Form\Type\UserExamType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserExamController extends FOSRestController {


    private $accessChecker;

    /**
     * UserDepartmentController constructor.
     * @param ZdlmAccessChecker $accessChecker
     */
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/userExams/{id}", requirements={"id": "\d+"}, name="api_user_exam_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="UserExam",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function getAction ( int $id ) {

        $userExam = $this->getDoctrine()->getRepository( 'App:UserExam' )->find( $id );
        if ( !$userExam ) {
            throw $this->createNotFoundException( 'UserExam not found.' );
        }

        $view = $this->view( $userExam, 200 );
        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/userExams", name="api_user_exam_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="UserExam",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function cgetAction () {

        $userExams = $this->getDoctrine()->getRepository( 'App:UserExam' )->findAll();
        return $this->handleView( $this->view( $userExams, 200 ) );
    }

    /**
     * @Rest\Post(path="/userExams", name="api_user_exam_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="UserExam",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request) {

        /**
         * @todo check for access restrictions
         */
        $userExam = new UserExam();

        return $this->processForm( $request, $userExam, true );
    }

    /**
     * @Rest\Route(path="/userExams/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_user_exam_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="UserExam",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function editAction ( Request $request, int $id ) {

        /**
         * @todo check for access restrictions
         */
        /*if ( !$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED') ) {

            throw $this->createAccessDeniedException('No Anonymous allowed here!!');
        }*/
        $userExam = $this->getDoctrine()->getRepository( 'App:UserExam' )->find( $id );
        if ( !$userExam ) {
            throw $this->createNotFoundException( 'UserExam not found.' );
        }

        return $this->processForm( $request, $userExam );
    }

    private function processForm ( Request $request, UserExam $userExam, bool $isNew = false ) {

        $form = $this->createForm( UserExamType::class, $userExam );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $userExam );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_user_exam_read', [
                    'version' => 'v1',
                    'id' => $userExam->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }
}

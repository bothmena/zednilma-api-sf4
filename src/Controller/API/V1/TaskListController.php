<?php

namespace App\Controller\API\V1;

use App\Entity\TaskList;
use App\Entity\User;
use App\Form\TaskListType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class TaskListController extends FOSRestController {


    private $authChecker;
    private $accessChecker;

    public function __construct(AuthorizationCheckerInterface $checker, ZdlmAccessChecker $accessChecker) {

        $this->authChecker = $checker;
        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/users/{userId}/task-lists/{id}", requirements={"userId": "\d+", "id": "\d+"}, name="api_task_list_read", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="TaskList",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @param int $id
     * @return Response
     */
    public function getUserListAction(int $userId, int $id) {

        $result = $this->accessChecker->checkForUser($userId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $taskList = $this->getDoctrine()->getRepository('App:TaskList')->findOneBy(['user' => $result->getUser(), 'id' => $id]);
        return $this->handleView($this->view($taskList, 200));
    }

    /**
     * @Rest\Get(path="/users/{userId}/task-lists", requirements={"userId": "\d+"}, name="api_task_list_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="TaskList",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @return Response
     */
    public function getUserListsAction(int $userId) {

        $result = $this->accessChecker->checkForUser($userId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $taskLists = $this->getDoctrine()->getRepository('App:TaskList')->findBy(['user' => $result->getUser()]);
        return $this->handleView($this->view($taskLists, 200));
    }

    /**
     * @Rest\Get(path="/users/{username}/task-lists", name="api_task_list_read_all_username", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="TaskList",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $username
     * @return Response
     */
    public function getUserListsByUsernameAction(string $username) {

        /**
         * @var $user User
         */
        $user = $this->getDoctrine()->getRepository('App:User')->findOneBy(['username' => $username]);
        if (!$user) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user',
                'message' => 'User was not found',
            ], 404);
        }

        if (true === $this->authChecker->isGranted('ROLE_PARENT')) {

            if ($this->getUser()->getId() !== $user->getParent()->getId()) {
                return new JsonResponse([
                    'status_code' => 403,
                    'error_code' => 'access_denied.not_user_property',
                    'message' => 'you are not allowed to access this resource',
                ], 403);
            }
        } else if (true === $this->authChecker->isGranted('ROLE_PROFESSOR') || true === $this->authChecker->isGranted('ROLE_STUDENT')) {

            if ($this->getUser()->getId() !== $user->getId()) {
                return new JsonResponse([
                    'status_code' => 403,
                    'error_code' => 'access_denied.not_user_property',
                    'message' => 'you are not allowed to access this resource',
                ], 403);
            }
        } else {

            return new JsonResponse([
                'status_code' => 403,
                'error_code' => 'access_denied.role_not_granted',
                'message' => 'you are not authorised',
            ], 403);
        }

        $taskLists = $this->getDoctrine()->getRepository('App:TaskList')->findBy(['user' => $user]);
        return $this->handleView($this->view($taskLists, 200));
    }

    /**
     * @Rest\Post(path="/users/{userId}/task-lists", requirements={"userId": "\d+"}, name="api_task_list_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="TaskList",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @param int $userId
     * @return Response
     */
    public function newAction(Request $request, int $userId) {

        $result = $this->accessChecker->checkForUser($userId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }
        $taskList = new TaskList();
        $taskList->setUser($result->getUser());

        return $this->processForm($request, $taskList, true);
    }

    /**
     * @Rest\Route(path="/users/{userId}/task-lists/{id}", requirements={"userId": "\d+", "id": "\d+"}, methods={"PUT", "PATCH"}, name="api_task_list_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="TaskList",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $userId
     * @param int $id
     * @return Response
     */
    public function editAction(Request $request, int $userId, int $id) {

        $result = $this->accessChecker->checkForUser($userId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $taskList = $this->getDoctrine()->getRepository('App:TaskList')->findOneBy(['user' => $result->getUser(), 'id' => $id]);
        if (!$taskList) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.task_list',
                'message' => 'Tasks List was not found',
            ], 404);
        }

        return $this->processForm($request, $taskList);
    }

    /**
     * @Rest\Delete(path="/users/{userId}/task-lists/{id}", requirements={"userId": "\d+", "id": "\d+"}, name="api_task_list_delete",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="TaskList",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @param int $id
     * @return Response
     */
    public function deleteAction(int $userId, int $id) {

        $result = $this->accessChecker->checkForInstitute($userId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $subject = $this->getDoctrine()->getRepository('App:Subject')->find($userId);
        if (!$subject) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.department',
                'message' => 'Department was not found',
            ], 404);
        }

        $exam = $this->getDoctrine()->getRepository('App:Exam')->find($id);
        if (!$subject) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.exam',
                'message' => 'Exam was not found',
            ], 404);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($exam);
        $em->flush();

        return $this->handleView($this->view(NULL, 204));
    }

    private function processForm(Request $request, TaskList $taskList, bool $isNew = false) {

        $form = $this->createForm(TaskListType::class, $taskList);

        $data = json_decode($request->getContent(), true);
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($taskList);
            $em->flush();

            if ($isNew) {

                $view = $this->view(NULL, 201);

                $response = $this->handleView($view);
                $response->headers->set('Location', $this->generateUrl('api_task_list_read', [
                    'version' => 'v1',
                    'userId' => $taskList->getUser()->getId(),
                    'id' => $taskList->getId(),
                ]));
                return $response;
            }

            $view = $this->view(NULL, 204);
            return $this->handleView($view);
        }

        $view = $this->view($form, 400);
        return $this->handleView($view);
    }
}

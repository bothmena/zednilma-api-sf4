<?php

namespace App\Controller\API\V1;

use App\Entity\Location;
use App\Form\Type\LocationType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class LocationController extends FOSRestController {


    private $accessChecker;

    /**
     * UserDepartmentController constructor.
     * @param ZdlmAccessChecker $accessChecker
     */
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/locations/{id}", requirements={"id": "\d+"}, name="api_location_read",
     *     options={"method_prefix" = false })
     * @ ApiDoc(
     *     section="Location",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "locations"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $id
     *
     * @return Response
     */
    public function getAction( int $id ) {

        $location = $this->getDoctrine()->getRepository( 'App:Location' )->find( $id );

        if ( !$location ) {
            throw $this->createNotFoundException( printf( "Location with id: %d was not found!", $id ) );
        }

        return $this->handleView( $this->view( $location, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{uniId}/locations", requirements={"uniId": "\d+"}, name="api_location_read_all",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Location",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Locations"},
     *     tags={"v1" = "#4A7023"},
     * )
     *
     * @param int $uniId
     * @return Response
     */
    public function getInstituteLocationsAction( int $uniId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )
            ->find( $uniId );
        if ( !$institute ) {
            throw $this->createNotFoundException( 'Institute not found.' );
        }

        $locations = $this->getDoctrine()->getRepository( 'App:Location' )
            ->findBy( [ 'institute' => $institute ] );

        return $this->handleView( $this->view( $locations, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/locations", requirements={"insId": "\d+"}, name="api_institute_location_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Location",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Locations"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     *
     * @return Response
     */
    public function newInstituteLocationAction( Request $request, int $insId ) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse())
            return $result->getResponse();

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )
            ->find( $insId );
        if ( !$institute ) {
            throw $this->createNotFoundException( 'Institute not found.' );
        }

        $location = new Location();
        $location->setInstitute( $institute );

        return $this->processForm( $request, $location, true );
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/locations/{id}", requirements={"uniId": "\d+", "id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_institute_location_update", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Location",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse())
            return $result->getResponse();

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {
            throw $this->createNotFoundException( 'Institute not found.' );
        }

        $location = $this->getDoctrine()->getRepository( 'App:Location' )->find( $id );
        if ( !$location ) {
            throw $this->createNotFoundException( 'Location not found.' );
        }

        return $this->processForm( $request, $location );
    }

    /**
     * @Rest\Delete(path="/institutes/{uniId}/locations/{id}", requirements={"uniId": "\d+", "id": "\d+"},
     *     name="api_institute_location_delete", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Location",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "Locations"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return JsonResponse|Response
     */
    public function deleteAction ( int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse())
            return $result->getResponse();

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {
            throw $this->createNotFoundException( 'Institute not found.' );
        }

        $location = $this->getDoctrine()->getRepository( 'App:Location' )
            ->find( $id );
        if ( !$location ) {
            throw $this->createNotFoundException( 'Location not found.' );
        }

        $em = $this->getDoctrine()->getManager();

        if ( $location->getIsPrimary() ) {

            $newPrimaryLocation = $em->getRepository('App:Location')->findOneBy([
                'isPrimary'=>false,
                'institute'=>$institute,
            ]);
            if ( $newPrimaryLocation )
                $newPrimaryLocation->setIsPrimary( true );
            else {
                return new JsonResponse( [
                    'code' => 412,
                    'error' => 'no_primary_location',
                    'message' => 'this is you only location'
                ], 412 );
            }
        }

        $em->remove( $location );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Location $location
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Location $location, bool $isNew = false ) {

        $form = $this->createForm( LocationType::class, $location );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();

            if ( $location->getIsPrimary() ) {

                $uniLocations = $em->getRepository('App:Location')
                    ->findBy(['institute'=>$location->getInstitute()]);
                foreach ( $uniLocations as $uniLocation ) {
                    $uniLocation->setIsPrimary(false);
                }
                $location->setIsPrimary(true);
            }

            $em->persist( $location );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_location_read', [
                    'version' => 'v1',
                    'id'      => $location->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}

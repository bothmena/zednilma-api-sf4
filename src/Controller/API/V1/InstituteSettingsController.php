<?php

namespace App\Controller\API\V1;

use App\Entity\InstituteSettings;
use App\Form\InstituteSettingsType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class InstituteSettingsController extends FOSRestController {


    /**
     * @var $accessChecker ZdlmAccessChecker
     * @var $authChecker AuthorizationCheckerInterface
     */
    private $accessChecker;
    private $authChecker;

    public function __construct(ZdlmAccessChecker $accessChecker, AuthorizationCheckerInterface $checker) {

        $this->accessChecker = $accessChecker;
        $this->authChecker = $checker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/settings", requirements={"insId": "\d+"}, name="api_institute_settings_read", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="InstituteSettings",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @return JsonResponse|Response
     */
    public function getAction ( int $insId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if (!$institute) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        if (false === $this->authChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            return new JsonResponse([
                'status_code' => 401,
                'error_code' => 'not_authenticated',
                'message' => 'you are not authenticated',
            ], 401);
        }

        $userInstitute = $this->getDoctrine()->getRepository( 'App:UserInstitute' )->findOneBy(['user'=>$this->getUser(), 'institute'=>$institute,
            'role'=>'ROLE_INS_ADMIN', 'dateOut' => NULL]);
        if (!$userInstitute) {
            return new JsonResponse([
                'status_code' => 403,
                'error_code' => 'access_denied.not_ins_admin',
                'message' => 'you are not allowed to do this action',
            ], 403);
        }

        $view = $this->view( $institute->getSettings(), 200 );
        return $this->handleView( $view );
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/settings", requirements={"insId": "\d+"}, methods={"PUT", "PATCH"}, name="api_institute_settings_update", options={"method_prefix"=false})
     * @ ApiDoc(
     *     section="InstituteSettings",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return JsonResponse|Response
     */
    public function editAction ( Request $request, int $insId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if (!$institute) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        if (false === $this->authChecker->isGranted('IS_AUTHENTICATED_FULLY')) {

            return new JsonResponse([
                'status_code' => 401,
                'error_code' => 'not_authenticated',
                'message' => 'you are not authenticated',
            ], 401);
        }

        $userInstitute = $this->getDoctrine()->getRepository( 'App:UserInstitute' )->findOneBy(['user'=>$this->getUser(), 'institute'=>$institute,
            'role'=>'ROLE_INS_ADMIN', 'dateOut' => NULL]);
        if (!$userInstitute) {
            return new JsonResponse([
                'status_code' => 403,
                'error_code' => 'access_denied.not_ins_admin',
                'message' => 'you are not allowed to do this action',
            ], 403);
        }

        return $this->processForm( $request, $institute->getSettings() );
    }

    private function processForm ( Request $request, InstituteSettings $insSettings, bool $isNew = false ) {

        $form = $this->createForm( InstituteSettingsType::class, $insSettings );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $insSettings );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_institute_settings_read', [
                    'version' => 'v1',
                    'id' => $insSettings->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }
}

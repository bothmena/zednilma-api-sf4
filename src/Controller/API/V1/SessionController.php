<?php

namespace App\Controller\API\V1;

use App\AppEvents;
use App\Entity\Institute;
use App\Entity\Notification;
use App\Entity\Session;
use App\Entity\User;
use App\Entity\UserClasse;
use App\Event\UserPushNotificationEvent;
use App\Form\Type\SessionType;
use App\Services\ABONotificationDescriber;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SessionController extends FOSRestController {


    private $accessChecker;
    private $describer;

    public function __construct(ZdlmAccessChecker $checker, ABONotificationDescriber $describer) {

        $this->accessChecker = $checker;
        $this->describer = $describer;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/sessions/{id}", requirements={"insId": "\d+", "id": "\d+"}, name="api_session_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="Session",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function getAction(int $insId, int $id) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $session = $this->getDoctrine()->getRepository('App:Session')->find($id);
        if (!$session) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.session',
                'message' => 'Session was not found',
            ], 404);
        }

        $view = $this->view($session, 200);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/sessions/current", requirements={"insId": "\d+"}, name="api_session_current_read", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Session",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function getCurrentAction() {

        if ($this->accessChecker->checkForUser(NULL, 'ROLE_PROFESSOR')->getResponse())
            return $this->accessChecker->checkForUser()->getResponse();

        $now = new \DateTime();
//        $now->setDate(2018, 8, 14);
        $now->setTime(10, 0, 0);

        /**
         * @var $session Session
         * @todo use current date and time.
         */
//        $session = $this->getDoctrine()->getRepository('App:Session')->getUserCurrent($this->getUser()->getId(), $now->format('Y-m-d H:i:s'));
//        11:00:00 15:00:00 16:00:00
        $session = $this->getDoctrine()->getRepository('App:Session')->getUserCurrent($this->getUser()->getId(), '10:00:00');
        if (!$session) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.session',
                'message' => 'Session was not found',
            ], 404);
        }

        $usersClasse = $this->getDoctrine()->getRepository('App:UserClasse')->findBy(['classe' => $session->getClasse()]);
        $subjectClasses = $this->getDoctrine()->getRepository('App:SubjectClasse')->findBy(['subject' => $session->getSubject(), 'classe' => $session->getClasse(), 'role' => 2]);

        $result = ['students' => [], 'assistants' => [], 'session' => $session];

        foreach ($usersClasse as $userClasse)
            array_push($result['students'], $this->getUserData($userClasse->getUser()));

        foreach ($subjectClasses as $subjectClass)
            array_push($result['assistants'], $this->getUserData($subjectClass->getProfessor()));

        $view = $this->view($result, 200);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/sessions", name="api_session_read_all", requirements={"insId": "\d+"}, options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Session",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @return Response
     */
    public function cgetAction(int $insId) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $sessions = $this->getDoctrine()->getRepository('App:Session')->findAll();
        return $this->handleView($this->view($sessions, 200));
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/professors/{profId}/reports", name="api_session_read_last_prof_all", requirements={"insId": "\d+", "profId": "\d+"},
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Session",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $profId
     * @return Response
     */
    public function cgetProfAction(int $insId, int $profId) {

        /**
         * @var $sessions Session[]
         */
        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $result = $this->accessChecker->checkForUser($profId, 'ROLE_PROFESSOR');
        if ($result->getResponse())
            return $result->getResponse();

        $sessions = $this->getDoctrine()->getRepository('App:Session')->getLastProfSessions($profId);
        foreach ($sessions as $session)
            $session->setClassroom(NULL);

        return $this->handleView($this->view($sessions, 200));
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/professors/{profId}/sessions/{week}", requirements={"insId": "\d+", "profId": "\d+", "week": "\d+"}, defaults={"week": "-1"},
     *     name="api_session_read_prof_week_tt", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Session",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $profId
     * @param int $week
     * @return Response
     */
    public function cgetProfTTAction(int $insId, int $profId, int $week) {

        $result = $this->accessChecker->checkForUser($profId, 'ROLE_PROFESSOR');
        if ($result->getResponse())
            return $result->getResponse();

        /**
         * @var $student User
         * @var $institute Institute
         */
        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        if ($week == -1) {

            $lastWeek = $this->getDoctrine()->getRepository('App:Session')->findOneBy(['professor'=>$result->getUser(),
                'schoolYear'=>$institute->getSettings()->getSchoolYear()], ['week'=>'DESC']);

            if ($lastWeek)
                $week = $lastWeek->getWeek();
            else
                return new JsonResponse([]);
        }

        $sessions = $this->getDoctrine()->getRepository('App:Session')->findBy(['professor'=>$result->getUser(),
            'schoolYear'=>$institute->getSettings()->getSchoolYear(), 'week' => $week]);
        $containers = [];
        if (!empty($sessions))
            $containers = $this->getDoctrine()->getRepository('App:SessionContainer')->findBy(['timetable'=>$sessions[0]->getContainer()->getTimetable()]);

        return $this->handleView($this->view(['containers'=>$containers, 'sessions'=>$sessions], 200));
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/students/{username}/reports", name="api_session_read_last_std_all", requirements={"insId": "\d+", "profId": "\d+"},
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Session",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param string $username
     * @return Response
     */
    public function cgetStudentAction(int $insId, string $username) {

        /**
         * @var $student User
         * @var $studentClasse UserClasse
         * @var $sessions Session[]
         */
        $result = $this->accessChecker->checkForUser(NULL, ['ROLE_PARENT', 'ROLE_STUDENT']);
        if ($result->getResponse())
            return $result->getResponse();

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $student = $this->getDoctrine()->getRepository('App:User')->findOneBy(['username' => $username]);
        if ($student) {
            if ($this->getUser()->getId() !== $student->getId() && $this->getUser()->getId() !== $student->getParent()->getId())
                return new JsonResponse([
                    'status_code' => 403,
                    'error_code' => 'access_denied.not_user_property',
                    'message' => 'you are not allowed to access this resource',
                ], 403);
        } else
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user',
                'message' => 'User was not found',
            ], 404);

        $studentClasse = $this->getDoctrine()->getRepository('App:UserClasse')->findOneBy(['user' => $student, 'dateOut' => NULL]);
        if (!$studentClasse) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user_classe',
                'message' => 'User\'s Class was not found',
            ], 403);
        }

        $sessions = $this->getDoctrine()->getRepository('App:Session')->getLastClasseSessions($studentClasse->getClasse()->getId());
        foreach ($sessions as $session)
            $session->setClassroom(NULL);

        return $this->handleView($this->view($sessions, 200));
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/students/{username}/sessions/{week}", requirements={"insId": "\d+", "week": "\d+"}, defaults={"week": "-1"}, name="api_session_read_std_week_tt",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Session",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param string $username
     * @param int $week
     * @return Response
     */
    public function cgetStudentTTAction(int $insId, string $username, int $week) {

        $result = $this->accessChecker->checkForUser(NULL, ['ROLE_PARENT', 'ROLE_STUDENT']);
        if ($result->getResponse())
            return $result->getResponse();

        /**
         * @var $student User
         * @var $institute Institute
         */
        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $student = $this->getDoctrine()->getRepository('App:User')->findOneBy(['username' => $username]);
        if ($student) {
            if ($this->getUser()->getId() !== $student->getId() && $this->getUser()->getId() !== $student->getParent()->getId())
                return new JsonResponse([
                    'status_code' => 403,
                    'error_code' => 'access_denied.not_user_property',
                    'message' => 'you are not allowed to access this resource',
                ], 403);
        } else
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user',
                'message' => 'User was not found',
            ], 404);

        $studentClasse = $this->getDoctrine()->getRepository('App:UserClasse')->findOneBy(['user' => $student, 'dateOut' => NULL]);
        if (!$studentClasse) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user_classe',
                'message' => 'User\'s Class was not found',
            ], 403);
        }

        if ($week == -1) {

            $lastWeek = $this->getDoctrine()->getRepository('App:Session')->findOneBy(['classe'=>$studentClasse->getClasse(),
                'schoolYear'=>$institute->getSettings()->getSchoolYear()], ['week'=>'DESC']);

            if ($lastWeek)
                $week = $lastWeek->getWeek();
            else
                return new JsonResponse([]);
        }

        $sessions = $this->getDoctrine()->getRepository('App:Session')->findBy(['classe'=>$studentClasse->getClasse(),
            'schoolYear'=>$institute->getSettings()->getSchoolYear(), 'week' => $week]);
        $containers = [];
        if (!empty($sessions))
            $containers = $this->getDoctrine()->getRepository('App:SessionContainer')->findBy(['timetable'=>$sessions[0]->getContainer()->getTimetable()]);

        return $this->handleView($this->view(['containers'=>$containers, 'sessions'=>$sessions], 200));
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/sessions/{classSlug}/{week}", requirements={"insId": "\d+", "week": "\d+"}, name="api_session_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Session",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param string $classSlug
     * @param int $week
     * @return Response
     */
    public function cgetByClasseWeekAction(int $insId, string $classSlug, int $week) {

        /**
         * @var Institute $institute
         */
        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }
        $classe = $this->getDoctrine()->getRepository('App:Classe')->findOneBy(['slug' => $classSlug]);
        if (!$classe) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.class',
                'message' => 'Class was not found',
            ], 404);
        }

        $sessions = $this->getDoctrine()->getRepository('App:Session')->findBy(['week' => $week, 'classe' => $classe,
            'schoolYear' => $institute->getSettings()->getSchoolYear()]);
        return $this->handleView($this->view($sessions, 200));
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/sessions", requirements={"insId": "\d+"}, name="api_session_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Session",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @param int $insId
     * @return Response
     */
    public function newAction(Request $request, int $insId) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        return $this->processForm($request, $result->getInstitute());
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/sessions/{id}", requirements={"insId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_session_update",options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Session",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Timetables"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function editAction(Request $request, int $insId, int $id) {

        $result = $this->accessChecker->checkForUser(NULL, 'ROLE_PROFESSOR');
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $session = $this->getDoctrine()->getRepository('App:Session')->findOneBy(['professor' => $this->getUser(), 'id' => $id]);
        if (!$session) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.session',
                'message' => 'Session was not found',
            ], 404);
        }

        $form = $this->createForm(SessionType::class, $session);

        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->pushReportNotification($session);

            return $this->handleView($this->view(NULL, 204));
        }

        return $this->handleView($this->view($form, 400));
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/sessions/{id}", requirements={"insId": "\d+", "id": "\d+"}, requirements={"id": "\d+"}, name="api_session_delete",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Session",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function deleteAction(int $insId, int $id) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $session = $this->getDoctrine()->getRepository('App:Session')->find($id);
        if (!$session) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.session',
                'message' => 'Session was not found',
            ], 404);
        }

        $sessions = $this->getDoctrine()->getRepository('App:Session')->findBy(['classe' => $session->getClasse(), 'container' => $session->getContainer(),
            'week' => $session->getWeek(), 'schoolYear' => $session->getSchoolYear()]);

        $em = $this->getDoctrine()->getManager();
        foreach ($sessions as $session) {
            $em->remove($session);
        }
        $em->flush();

        return $this->handleView($this->view(NULL, 204));
    }

    private function processForm(Request $request, Institute $institute) {

        $data = json_decode($request->getContent(), true);
        if (empty($data['classe']) || empty($data['classroom']) || empty($data['container']) || empty($data['professor']) || empty($data['subject']) || empty($data['week']) ||
            $data['week'] > 53 || $data['week'] < 1) {

            return new JsonResponse([
                'status_code' => 400,
                'error_code' => 'form.not_valid',
                'message' => 'Some data is missing or not valid',
            ], 400);
        }
        $subCls = $this->getDoctrine()->getRepository('App:SubjectClasse')->findOneBySubClsProf($data['subject'], $data['classe'], $data['professor']);
        if (!$subCls) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.subject_classe',
                'message' => 'Subject-Classe relationship was not found',
            ], 404);
        }
        $classroom = $this->getDoctrine()->getRepository('App:Infrastructure')->find($data['classroom']);
        if (!$classroom) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.infrastructure',
                'message' => 'Classroom was not found',
            ], 404);
        }
        $container = $this->getDoctrine()->getRepository('App:SessionContainer')->find($data['container']);
        if (!$container) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.session_container',
                'message' => 'Session container was not found',
            ], 404);
        }

        $subjectClasses = $this->getDoctrine()->getRepository('App:SubjectClasse')->findBy(['subject' => $data['subject'], 'classe' => $data['classe'], 'role' => 2]);
        array_push($subjectClasses, $subCls);
        $em = $this->getDoctrine()->getManager();
        foreach ($subjectClasses as $subjectClass) {

            $session = new Session();
            $session->setSubject($subjectClass->getSubject());
            $session->setClasse($subjectClass->getClasse());
            $session->setProfessor($subjectClass->getProfessor());
            $session->setClassroom($classroom);
            $session->setContainer($container);
            $session->setWeek($data['week']);
            $session->setSchoolYear($institute->getSettings()->getSchoolYear());
            $em->persist($session);
        }

        $em->flush();
        return $this->handleView($this->view(NULL, 201));
    }

    private function pushReportNotification(Session $session) {

        $parentGrp = $this->getDoctrine()->getRepository('App:Group')->findOneBy(['parent' => $session->getClasse()->getGroupe(), 'type' => 'class_p']);
        if ($parentGrp) {

            $dispatcher = $this->get('event_dispatcher');
            $notification = new Notification();
            $notification->setType(121);
            $notification->setDescription($this->describer->describeSessionReport($session));
            $notification->setNotifier($this->getUser());
            $event = new UserPushNotificationEvent($notification, NULL, $parentGrp);
            $dispatcher->dispatch(AppEvents::USER_PUSH_NOTIFICATION, $event);
        }
    }

    private function getUserData(User $user) {

        return [
            'id' => $user->getId(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'email' => $user->getEmail(),
            'image_url' => $user->getImageUrl(),
        ];
    }
}

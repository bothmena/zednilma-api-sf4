<?php

namespace App\Controller\API\V1;

use App\Entity\Subject;
use App\Form\Type\SubjectType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SubjectController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/departments/{depId}/subjects/{id}",
     *     requirements={
     *          "insId": "\d+", "depId": "\d+",
     *          "id": "\d+"
     *     }, name="api_subject_read", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Subject",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $depId
     * @param int $id
     * @return Response
     */
    public function getAction( int $insId, int $depId, int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $department = $this->getDoctrine()->getRepository( 'App:Department' )
            ->findOneBy( ['id' => $depId, 'institute'=>$institute] );
        if ( !$department ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.department',
                'message'     => 'Department was not found',
            ], 404 );
        }

        $subject = $this->getDoctrine()->getRepository( 'App:Subject' )
            ->findOneBy( ['id' => $id, 'department'=>$department] );
        if ( !$subject ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.department_subject',
                'message'     => 'Department subject was not found',
            ], 404 );
        }

        return $this->handleView( $this->view( $subject, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/departments/{depSlug}/subjects/{id}", requirements={"insId": "\d+", "id": "\d+"}, name="api_subject_read_by_slug",
     *     options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Subject",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param string $depSlug
     * @param int $id
     * @return Response
     */
    public function getByDepSlugAction( int $insId, string $depSlug, int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $department = $this->getDoctrine()->getRepository( 'App:Department' )
            ->findOneBy( ['slug' => $depSlug, 'institute'=>$institute] );
        if ( !$department ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.department',
                'message'     => 'Department was not found',
            ], 404 );
        }

        $subject = $this->getDoctrine()->getRepository( 'App:Subject' )
            ->findOneBy( ['id' => $id, 'department'=>$department] );
        if ( !$subject ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.department_subject',
                'message'     => 'Department subject was not found',
            ], 404 );
        }

        return $this->handleView( $this->view( $subject, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/departments/{depId}/subjects", requirements={
     *     "insId": "\d+", "depId": "\d+"}, name="api_subject_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Subject",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $depId
     * @return Response
     */
    public function cgetAction( Request $request, int $insId, int $depId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $department = $this->getDoctrine()->getRepository( 'App:Department' )
            ->findOneBy( ['id' => $depId, 'institute'=>$institute] );
        if ( !$department ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.department',
                'message'     => 'Department was not found',
            ], 404 );
        }

        $subjects = $this->getDoctrine()->getRepository( 'App:Subject' )
            ->findBy( [ 'department' => $department ] );

        return $this->handleView( $this->view( $subjects, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/subjects", requirements={
     *     "insId": "\d+"}, name="api_subject_search", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Subject",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return Response
     */
    public function searchAction( Request $request, int $insId) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $query = $request->query->get('query');
        if (!empty($query)) {

            $subjects = $this->getDoctrine()->getRepository( 'App:Subject' )
                ->searchForinstitueSubjects( $institute->getId(), $query );
        } else {

            $subjects = $this->getDoctrine()->getRepository( 'App:Subject' )
                ->getInstituteSubjects( $institute->getId() );
        }

        return $this->handleView( $this->view( $subjects, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/departments/{depId}/subjects", requirements={"insId": "\d+", "depId": "\d+"},
     *     name="api_subject_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Subject",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $depId
     * @return Response
     */
    public function newAction( Request $request, int $insId, int $depId ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $department = $this->getDoctrine()->getRepository( 'App:Department' )
            ->findOneBy( ['id' => $depId, 'institute'=>$result->getInstitute()] );
        if ( !$department ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.department',
                'message'     => 'Department was not found',
            ], 404 );
        }

        $subject = new Subject();
        $subject->setDepartment( $department );
        return $this->processForm( $request, $subject, $result->getInstitute()->getId(), true );
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/departments/{depId}/subjects/{id}", requirements={"insId": "\d+", "depId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_subject_update",options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Subject",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Subjects"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $depId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $insId, int $depId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $department = $this->getDoctrine()->getRepository( 'App:Department' )
            ->findOneBy( ['id' => $depId, 'institute'=>$result->getInstitute()] );
        if ( !$department ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.department',
                'message'     => 'Department was not found',
            ], 404 );
        }

        $subject = $this->getDoctrine()->getRepository( 'App:Subject' )
            ->findOneBy( ['id' => $id, 'department'=>$department] );
        if ( !$subject ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_subject',
                'message'     => 'Institute subject was not found',
            ], 404 );
        }

        return $this->processForm( $request, $subject, $result->getInstitute()->getId(), false );
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/departments/{depId}/subjects/{id}", requirements={"insId": "\d+", "depId": "\d+","id": "\d+"},
     *     name="api_subject_delete", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Subject",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "Subjects"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $depId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $insId, int $depId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $department = $this->getDoctrine()->getRepository( 'App:Department' )
            ->findOneBy( ['id' => $depId, 'institute'=>$result->getInstitute()] );
        if ( !$department ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.department',
                'message'     => 'Department was not found',
            ], 404 );
        }

        $subject = $this->getDoctrine()->getRepository( 'App:Subject' )
            ->findOneBy( ['id' => $id, 'department'=>$department] );
        if ( !$subject ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_subject',
                'message'     => 'Institute subject was not found',
            ], 404 );
        }

        $em = $this->getDoctrine()->getManager();
        $exams = $this->getDoctrine()->getRepository('App:Exam')->findBy(['subject'=>$subject]);
        foreach ($exams as $exam) {
            $em->remove($exam);
        }
        $curriculum = $this->getDoctrine()->getRepository('App:Curriculum')->findBy(['subject'=>$subject]);
        foreach ($curriculum as $curricula) {
            $em->remove($curricula);
        }
        $exams = $this->getDoctrine()->getRepository('App:ModuleSubject')->findBy(['subject'=>$subject]);
        foreach ($exams as $exam) {
            $em->remove($exam);
        }
        $exams = $this->getDoctrine()->getRepository('App:SubjectClasse')->findBy(['subject'=>$subject]);
        foreach ($exams as $exam) {
            $em->remove($exam);
        }

        $em->remove( $subject );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Subject $subject
     * @param int $insId
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Subject $subject, int $insId, bool $isNew = false ) {

        $form = $this->createForm( SubjectType::class, $subject );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $subject );
            $em->flush();

            if ( $isNew ) {

                $response = $this->handleView( $this->view( NULL, 201 ) );
                $response->headers->set( 'Location', $this->generateUrl( 'api_subject_read', [
                    'version'  => 'v1',
                    'insId'    => $insId,
                    'depId'    => $subject->getDepartment()->getId(),
                    'id'       => $subject->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}

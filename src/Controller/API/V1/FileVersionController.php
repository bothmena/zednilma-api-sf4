<?php

namespace App\Controller\API\V1;

use App\Entity\FileVersion;
use App\Form\Type\FileVersionType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FileVersionController extends FOSRestController {


    private $accessChecker;

    /**
     * UserDepartmentController constructor.
     * @param ZdlmAccessChecker $accessChecker
     */
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/fileVersions/{id}", requirements={"id": "\d+"}, name="api_file_version_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="FileVersion",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function getAction ( int $id ) {

        $fileVersion = $this->getDoctrine()->getRepository( 'App:FileVersion' )->find( $id );
        if ( !$fileVersion ) {
            throw $this->createNotFoundException( 'FileVersion not found.' );
        }

        $view = $this->view( $fileVersion, 200 );
        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/fileVersions", name="api_file_version_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="FileVersion",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function cgetAction () {

        $fileVersions = $this->getDoctrine()->getRepository( 'App:FileVersion' )->findAll();
        return $this->handleView( $this->view( $fileVersions, 200 ) );
    }

    /**
     * @Rest\Post(path="/fileVersions", name="api_file_version_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="FileVersion",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request) {

        /**
         * @todo check for access restrictions
         */
        $fileVersion = new FileVersion();

        return $this->processForm( $request, $fileVersion, true );
    }

    /**
     * @Rest\Route(path="/fileVersions/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_file_version_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="FileVersion",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function editAction ( Request $request, int $id ) {

        /**
         * @todo check for access restrictions
         */
        /*if ( !$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED') ) {

            throw $this->createAccessDeniedException('No Anonymous allowed here!!');
        }*/
        $fileVersion = $this->getDoctrine()->getRepository( 'App:FileVersion' )->find( $id );
        if ( !$fileVersion ) {
            throw $this->createNotFoundException( 'FileVersion not found.' );
        }

        return $this->processForm( $request, $fileVersion );
    }

    private function processForm ( Request $request, FileVersion $fileVersion, bool $isNew = false ) {

        $form = $this->createForm( FileVersionType::class, $fileVersion );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $fileVersion );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_file_version_read', [
                    'version' => 'v1',
                    'id' => $fileVersion->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }
}

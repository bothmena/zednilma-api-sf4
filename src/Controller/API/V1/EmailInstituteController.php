<?php

namespace App\Controller\API\V1;

use App\Entity\Email;
use App\Form\Type\EmailType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EmailInstituteController extends FOSRestController {


    private $accessChecker;
    private $tokenGenerator;
    public function __construct(ZdlmAccessChecker $accessChecker, TokenGeneratorInterface $tokenGenerator) {

        $this->accessChecker = $accessChecker;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/emails/{id}", requirements={"insId": "\d+", "id": "\d+"}, name="api_institute_email_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="Email",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return JsonResponse|Response
     */
    public function getInstituteEmailAction( int $insId, int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )
            ->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $instituteEmail = $this->getDoctrine()->getRepository( 'App:Email' )->find( $id );
        if ( !$instituteEmail ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_email',
                'message'     => 'Institute email was not found',
            ], 404 );
        }

        $view = $this->view( $instituteEmail, 200 );

        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/emails", requirements={"insId": "\d+"}, name="api_institute_email_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Email",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return JsonResponse|Response
     */
    public function getInstituteEmailsAction( Request $request, int $insId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )
            ->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $enabled = $request->query->get( 'verified' );
        $verified = NULL;
        if ( $enabled === 'true' ) {

            $verified = true;
        } else if ( $enabled === 'false' ) {

            $verified = false;
        }

        $instituteEmails = $this->getDoctrine()->getRepository( 'App:Email' )
            ->getInstituteEmails( $insId, $verified );

        return $this->handleView( $this->view( $instituteEmails, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/emails", requirements={"insId": "\d+"}, name="api_institute_email_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Email",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @param int $insId
     * @return Response
     */
    public function newInstituteEmailAction( Request $request, int $insId ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $email = new Email();
        $email->setInstitute( $result->getInstitute() );

        return $this->processForm( $request, $email, true );
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/emails/{id}", requirements={"insId": "\d+", "id": "\d+"}, methods={"PUT", "PATCH"}, name="api_institute_email_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Email",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $id
     * @return mixed|JsonResponse|Response
     */
    public function editAction( Request $request, int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $email = $this->getDoctrine()->getRepository( 'App:Email' )->find( $id );
        if ( !$email ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.email',
                'message'     => 'Email was not found',
            ], 404 );
        }

        if ( $email->getConfirmationToken() === NULL && $email->getIsPrimary() ) {

            return new JsonResponse( [
                'status_code' => 412,
                'error_code'  => 'precondition_failed.vp_email_not_editable',
                'message'     => 'This is a primary, verified email, you can not modify it, you need to make new verified email primary in order to change this email.',
            ], 412 );
        }

        return $this->processForm( $request, $email );
    }

    /**
     * @Rest\Get(path="/confirm-email/{token}", name="api_email_confirm", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Email",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $token
     * @return JsonResponse|Response
     */
    public function confirmAction( string $token ) {

        $em = $this->getDoctrine()->getManager();
        $email = $em->getRepository( 'App:Email' )->findOneBy( [ 'confirmationToken' => $token ] );
        if ( !$email ) {
            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.email',
                'message'     => 'Email with token \'' . $token . '\' was not found',
                'params'      => [
                    'token' => $token,
                ],
            ], 404 );
        }

        $email->setConfirmationToken( NULL );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/emails/{id}", requirements={"insId": "\d+", "id": "\d+"}, name="api_institute_email_delete", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Email",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return mixed|JsonResponse|Response
     */
    public function deleteAction( int $insId, int $id ) {

        $em = $this->getDoctrine()->getManager();
        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $email = $em->getRepository( 'App:Email' )->find( $id );
        if ( !$email ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.email',
                'message'     => 'Email was not found',
            ], 404 );
        }

        if ( $email->getIsPrimary() ) {

            $newPrimaryEmail = $em->getRepository('App:Email')
                ->getInstituteAVerifiedEmail( $result->getInstitute()->getId(), $email->getId() );

            if ( $newPrimaryEmail ) {

                $newPrimaryEmail->setIsPrimary( true );
            } else {

                return new JsonResponse( [
                    'status_code' => 412,
                    'error_code'  => 'precondition_failed.no_verified_email',
                    'message'     => 'We can\'t remove this email because it\'s primary and you have no verified email to replace it.',
                ], 412 );
            }
        }

        $em->remove( $email );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    private function processForm( Request $request, Email $email, bool $isNew = false ) {

        $data = json_decode( $request->getContent(), true );
        if ( isset( $data[ 'email' ] ) && $data[ 'email' ] != $email->getEmail() ) {

            $email->setConfirmationToken(
                $this->tokenGenerator->generateToken()
            );
        }
        if ( $data[ 'isPrimary' ] && $email->getConfirmationToken() !== NULL ) {

            return new JsonResponse( [
                'status_code' => 412,
                'error_code'  => 'precondition_failed.email_not_verified',
                'message'     => 'you can not set an unverified email primary, please verify it first',
            ], 412 );
        }

        $form = $this->createForm( EmailType::class, $email );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();

            if ( $email->getIsPrimary() ) {

                $insEmails = $em->getRepository( 'App:Email' )->findBy( [ 'institute' => $email->getInstitute() ] );
                foreach ( $insEmails as $uniEmail ) {
                    $uniEmail->setIsPrimary( false );
                }
                $email->setIsPrimary( true );
            }

            $em->persist( $email );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_institute_email_read', [
                    'version' => 'v1',
                    'insId'   => $email->getInstitute()->getId(),
                    'id'      => $email->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}

<?php

namespace App\Controller\API\V1;

use App\Entity\Infrastructure;
use App\Form\Type\InfrastructureType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class InfrastructureController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/infrastructures/{id}", requirements={"insId": "\d+", "id": "\d+"},
     *     name="api_infrastructure_read", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Infrastructure",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function getAction( int $insId, int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )
            ->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $infrastructure = $this->getDoctrine()->getRepository( 'App:Infrastructure' )
            ->getInstituteInfrastructure( $insId, $id );
        if ( !$infrastructure ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_infrastructure',
                'message'     => 'Institute infrastructure was not found',
            ], 404 );
        }

        return $this->handleView( $this->view( $infrastructure, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/infrastructures", requirements={"insId": "\d+"},
     *     name="api_infrastructure_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Infrastructure",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return Response
     */
    public function cgetAction( Request $request, int $insId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $parent = $request->query->get( 'parent' );
        $types = $request->query->get( 'type' );

        if (is_array($types))
            $Infrastructure = $this->getDoctrine()->getRepository( 'App:Infrastructure' )->getInstituteInfrastructures( $institute->getId(), NULL, $types );
        else
            $Infrastructure = $this->getDoctrine()->getRepository( 'App:Infrastructure' )->getInstituteInfrastructures( $institute->getId(), $parent );

        return $this->handleView( $this->view( $Infrastructure, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insSlug}/infrastructures", name="api_infrastructure_read_all_by_slug",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Infrastructure",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param string $insSlug
     * @return Response
     */
    public function cgetByInsSlugAction( Request $request, string $insSlug ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->findOneBy( [ 'slug' => $insSlug ] );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $parent = $request->query->get( 'parent' );
        $types = $request->query->get( 'type' );

        if (is_array($types))
            $Infrastructure = $this->getDoctrine()->getRepository( 'App:Infrastructure' )->getInstituteInfrastructures( $institute->getId(), NULL, $types );
        else
            $Infrastructure = $this->getDoctrine()->getRepository( 'App:Infrastructure' )->getInstituteInfrastructures( $institute->getId(), $parent );

        return $this->handleView( $this->view( $Infrastructure, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/infrastructures", requirements={"insId": "\d+"},
     *     name="api_infrastructure_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Infrastructure",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return Response
     */
    public function newAction( Request $request, int $insId ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $infrastructure = new Infrastructure();
        $infrastructure->setInstitute( $result->getInstitute() );

        return $this->processForm( $request, $infrastructure, true );
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/infrastructures/{id}", requirements={"insId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_infrastructure_update",options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Infrastructure",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Infrastructures"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $infrastructure = $this->getDoctrine()->getRepository( 'App:Infrastructure' )
            ->getInstituteInfrastructure( $insId, $id );
        if ( !$infrastructure ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_infrastructure',
                'message'     => 'Institute infrastructure was not found',
            ], 404 );
        }

        return $this->processForm( $request, $infrastructure, false );
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/infrastructures/{id}", requirements={"insId": "\d+","id": "\d+"},
     *     name="api_infrastructure_delete", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Infrastructure",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "Infrastructures"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $infrastructure = $this->getDoctrine()->getRepository( 'App:Infrastructure' )
            ->getInstituteInfrastructure( $insId, $id );
        if ( !$infrastructure ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_infrastructure',
                'message'     => 'Institute infrastructure was not found',
            ], 404 );
        }

        $children = $this->getDoctrine()->getRepository( 'App:Infrastructure' )->findBy( [ 'parent' => $infrastructure ] );

        if ( $infrastructure->getType() === 'BLOCK' && sizeof( $children ) > 0 ) {
            return new JsonResponse( [
                'status_code' => 412,
                'error_code'  => 'precondition_failed.block_not_empty',
                'message'     => 'If this block is not empty we can not delete it.',
            ], 412 );
        }
        $em = $this->getDoctrine()->getManager();
        foreach ( $children as $child )
            $em->remove( $child );
        $em->remove( $infrastructure );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Infrastructure $infrastructure
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Infrastructure $infrastructure, bool $isNew = false ) {

        $form = $this->createForm( InfrastructureType::class, $infrastructure );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $infrastructure );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_infrastructure_read', [
                    'version' => 'v1',
                    'insId'   => $infrastructure->getInstitute()->getId(),
                    'id'      => $infrastructure->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}

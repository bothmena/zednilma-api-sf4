<?php

namespace App\Controller\API\V1;

use App\Entity\Classe;
use App\Entity\Group;
use App\Entity\Institute;
use App\Entity\Speciality;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ClasseController extends FOSRestController {


    private $accessChecker;

    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/users/{id}/class", requirements={"id": "\d+"}, name="api_user_classe_read", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Classe",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function getUserAction(int $id) {

        $user = $this->getDoctrine()->getRepository('App:User')
            ->find($id);
        if (!$user) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $classe = $this->getDoctrine()->getRepository('App:Classe')
            ->findOneBy(['']);
        if (!$classe) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute_classe',
                'message' => 'Institute classe was not found',
            ], 404);
        }

        return $this->handleView($this->view($classe, 200));
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/specialities/{specId}/classes", requirements={"insId": "\d+", "specId": "\d+"}, name="api_spec_classe_read_all",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Classe",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $specId
     * @return Response
     */
    public function cgetSpecClassesAction(int $insId, int $specId) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $speciality = $this->getDoctrine()->getRepository('App:Speciality')->find($specId);
        if (!$speciality) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.speciality',
                'message' => 'Speciality was not found',
            ], 404);
        }

        $classes = $this->getDoctrine()->getRepository('App:Classe')->getSpecialityClasses($specId);

        return $this->handleView($this->view($classes, 200));
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/classes", requirements={"insId": "\d+", "specId": "\d+"}, name="api_ins_classe_read_all",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Classe",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @return Response
     */
    public function cgetInstituteClassesAction(int $insId) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $classes = $this->getDoctrine()->getRepository('App:Classe')->getInstituteClasses($insId);

        return $this->handleView($this->view($classes, 200));
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/specialities/{specId}/classes", requirements={"insId": "\d+", "specId": "\d+"}, name="api_spec_classe_create",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Classe",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $specId
     * @return Response
     */
    public function newAction(Request $request, int $insId, int $specId) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        /**
         * @var Speciality $speciality
         */
        $speciality = $this->getDoctrine()->getRepository('App:Speciality')->find($specId);
        if (!$speciality) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.speciality',
                'message' => 'Speciality was not found',
            ], 404);
        }

        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);
        if ($data['quantity'] > 0 && $data['quantity'] < 36 && $data['level'] >= 1 && $data['level'] <= $speciality->getParent()->getYears()) {
            for ($i = 0; $i < $data['quantity']; $i++) {

                $classe = new Classe();
                $classe->getGroupe()->setInstitute($result->getInstitute());
                $classe->getGroupe()->setSuffixedName($speciality->getName());

                $parentGrp = new Group();
                $parentGrp->setType('class_p');
                $parentGrp->setName($speciality->getName());
                $parentGrp->setInstitute($result->getInstitute());
                $parentGrp->setParent($classe->getGroupe());

                $classe->setSchoolYear($result->getInstitute()->getSettings()->getSchoolYear());
                $classe->setName($data['level'] . $speciality->getAlias());
                $classe->setBranch($speciality);
                $classe->setLevel($data['level']);
                $em->persist($classe);
                $em->persist($parentGrp);
            }
        }
        $em->flush();
        $this->renameClasses($speciality);

        return $this->handleView($this->view(NULL, 201));
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/specialities/{specId}/python-classes/{quantity}/{level}",
     *     requirements={"insId": "\d+", "specId": "\d+", "quantity": "\d+"}, name="api_spec_classe_create_py", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Classe",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $specId
     * @param int $quantity
     * @param int $level
     * @return Response
     */
    public function newPythonAction(Request $request, int $insId, int $specId, int $quantity, int $level) {

        if ($request->getClientIp() !== '127.0.0.1') {

            return new JsonResponse([
                'status_code' => 403,
                'error_code' => 'access_denied.action_not_allowed',
                'message' => 'you are not allowed to do this action',
            ], 403);
        }

        /**
         * @var Institute $institute
         */
        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $speciality = $this->getDoctrine()->getRepository('App:Speciality')->find($specId);
        if (!$speciality) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.speciality',
                'message' => 'Speciality was not found',
            ], 404);
        }

        $em = $this->getDoctrine()->getManager();
        $id_arr = [];
        if ($quantity > 0 && $quantity < 100) {
            for ($i = 0; $i < $quantity; $i++) {

                $classe = new Classe();
                $classe->getGroupe()->setInstitute($institute);
                $classe->getGroupe()->setName($speciality->getName());

                $parentGrp = new Group();
                $parentGrp->setType('class_p');
                $parentGrp->setName($speciality->getName());
                $parentGrp->setInstitute($institute);
                $parentGrp->setParent($classe->getGroupe());

                $classe->setSchoolYear($institute->getSettings()->getSchoolYear());
                $classe->setName($level . $speciality->getAlias());
                $classe->setBranch($speciality);
                $classe->setLevel($level);
                $em->persist($classe);
                $em->persist($parentGrp);

                $em->flush();
                array_push($id_arr, $classe->getId());
            }

            $this->renameClasses($speciality);
            return $this->handleView($this->view($id_arr, 201));
        }

        return new JsonResponse([
            'status_code' => 400,
            'error_code' => 'form.invalid',
            'message' => 'the given data is not valid.',
        ], 400);
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/specialities/{specId}/classes/{id}", requirements={"insId": "\d+","specId": "\d+","id": "\d+"},
     *     name="api_spec_classe_delete", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Classe",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "Classes"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $specId
     * @param int $id
     * @return Response
     */
    public function deleteAction(int $insId, int $specId, int $id) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }

        $speciality = $this->getDoctrine()->getRepository('App:Speciality')->find($specId);
        if (!$speciality) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.speciality',
                'message' => 'Speciality was not found',
            ], 404);
        }

        /**
         * @var $classe Classe
         */
        $classe = $this->getDoctrine()->getRepository('App:Classe')->findOneBy(['branch' => $speciality, 'id' => $id]);
        if (!$classe) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute_classe',
                'message' => 'Institute classe was not found',
            ], 404);
        }

        if ($classe->getStudents() === 0) {
            $em = $this->getDoctrine()->getManager();
            $parentGrp = $this->getDoctrine()->getRepository('App:Group')->findOneBy(['parent'=>$classe->getGroupe(), 'type'=> 'class_p']);
            if ($parentGrp)
                $em->remove($parentGrp);

            $em->remove($classe);
            $em->flush();

            $this->renameClasses($classe->getBranch());

            return $this->handleView($this->view(NULL, 204));
        }

        return new JsonResponse([
            'status_code' => 400,
            'error_code' => 'error.classe_not_empty',
            'message' => 'You can not delete a class unless it is empty.',
        ], 400);
    }

    private function renameClasses(Speciality $speciality) {

        for ( $level = 1; $level <= $speciality->getParent()->getYears(); $level++) {

            $classes = $this->getDoctrine()->getRepository('App:Classe')->findBy(['branch' => $speciality, 'level'=>$level,
                'schoolYear'=>$speciality->getInstitute()->getSettings()->getSchoolYear()], ['id' => 'ASC']);
            if (count($classes) >= 100)
                $format = "%u%s-%'.03d";
            else if (count($classes) >= 10)
                $format = "%u%s-%'.02d";
            else
                $format = "%u%s-%d";

            foreach ($classes as $i => $classe) {
                $name = sprintf($format, $level, $speciality->getAlias(), $i + 1);
                $classe->setName($name);
                $classe->getGroupe()->setSuffixedName($name);
                $parentGrp = $this->getDoctrine()->getRepository('App:Group')->findOneBy(['parent'=>$classe->getGroupe(), 'type'=> 'class_p']);
                if ($parentGrp)
                    $parentGrp->setSuffixedName($name);
            }
        }

        $this->getDoctrine()->getManager()->flush();
    }
}

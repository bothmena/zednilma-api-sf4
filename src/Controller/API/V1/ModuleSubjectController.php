<?php

namespace App\Controller\API\V1;

use App\Entity\Module;
use App\Entity\ModuleSubject;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ModuleSubjectController extends FOSRestController {


    /**
     * @var $accessChecker ZdlmAccessChecker
     * @var $authChecker AuthorizationCheckerInterface
     */
    private $accessChecker;
    private $authChecker;

    public function __construct(ZdlmAccessChecker $accessChecker, AuthorizationCheckerInterface $checker) {

        $this->accessChecker = $accessChecker;
        $this->authChecker = $checker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/specialities/{specId}/module-subjects/{id}", name="api_module_subject_read", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="ModuleSubject",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $specId
     * @param int $id
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getAction(int $insId, int $specId, int $id) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $speciality = $this->getDoctrine()->getRepository('App:Speciality')->findOneBy(['id' => $specId, 'institute' => $institute]);
        if (!$speciality) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.speciality',
                'message' => 'Speciality was not found',
            ], 404);
        }

        $moduleSubject = $this->getDoctrine()->getRepository('App:ModuleSubject')->find($id);
        return $this->handleView($this->view($moduleSubject, 200));
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/specialities/{specId}/module-subjects", name="api_specialities_modules_subjects_read", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="SubjectClasse",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $specId
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getSpecModulesSubjectsAction(int $insId, int $specId) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 401);
        }

        $speciality = $this->getDoctrine()->getRepository('App:Speciality')->findOneBy(['id' => $specId, 'institute' => $institute]);
        if (!$speciality) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.speciality',
                'message' => 'Speciality was not found',
            ], 404);
        }

        $modules = $this->getDoctrine()->getRepository('App:Module')->findBy(['speciality' => $speciality]);
        $moduleSubjects = $this->getDoctrine()->getRepository('App:ModuleSubject')->getSpecModulesSubjects($specId);

        $detachedModules = [];
        foreach ($modules as $i => $module) {
            foreach ($moduleSubjects as $moduleSubject) {
                if ($moduleSubject->getModule()->getId() === $module->getId()) {
                    continue 2;
                }
            }
            array_push($detachedModules, $module);
        }
        foreach ($detachedModules as $module)
            array_push($moduleSubjects, new ModuleSubject($module));
        return $this->handleView($this->view($moduleSubjects, 200));
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/specialities/{specId}/module-subjects", name="api_specialities_modules_subjects_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="ModuleSubject",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $specId
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, int $insId, int $specId) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $speciality = $this->getDoctrine()->getRepository('App:Speciality')->findOneBy(['id' => $specId, 'institute' => $institute]);
        if (!$speciality) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.speciality',
                'message' => 'Speciality was not found',
            ], 404);
        }

        $data = json_decode($request->getContent(), true);
        if ($data['module'] > 0 && $data['subject'] > 0 && $data['coefficient'] <= 10 && $data['coefficient'] > 0) {

            $module = $this->getDoctrine()->getRepository('App:Module')->find($data['module']);
            if (!$module) {
                return new JsonResponse([
                    'status_code' => 404,
                    'error_code' => 'not_found.module',
                    'message' => 'Module was not found',
                ], 404);
            }

            $subject = $this->getDoctrine()->getRepository('App:Subject')->find($data['subject']);
            if (!$subject) {
                return new JsonResponse([
                    'status_code' => 404,
                    'error_code' => 'not_found.subject',
                    'message' => 'Subject was not found',
                ], 404);
            }

            $mdlSubject = $this->getDoctrine()->getRepository('App:ModuleSubject')->findOneBy(['module' => $module, 'subject' => $subject]);
            if ($mdlSubject) {
                return new JsonResponse([
                    'status_code' => 400,
                    'error_code' => 'already_exist.module_subject',
                    'message' => 'Module-Subject already exists in the database.',
                ], 400);
            }

            $mdlSubject = new ModuleSubject($module, $subject, $data['coefficient']);
            $module->setCoefficient($module->getCoefficient() + $data['coefficient']);
            $this->getDoctrine()->getManager()->persist($mdlSubject);
            $this->getDoctrine()->getManager()->flush();

            $response = $this->handleView($this->view(NULL, 201));
            $response->headers->set('Location', $this->generateUrl('api_module_subject_read', [
                'version' => 'v1',
                'insId' => $insId,
                'specId' => $specId,
                'id' => $mdlSubject->getId(),
            ]));

            return $response;
        }

        return $this->handleView($this->view([
            'status_code' => 400,
            'error_code' => 'data.not_valid',
            'message' => 'The data was not valid.',], 400));
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/specialities/{specId}/module-subjects/{id}", name="api_specialities_modules_subjects_edit", methods={"PUT", "PATCH"},
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="ModuleSubject",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $specId
     * @param int $id
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, int $insId, int $specId, int $id) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $speciality = $this->getDoctrine()->getRepository('App:Speciality')->findOneBy(['id' => $specId, 'institute' => $institute]);
        if (!$speciality) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.speciality',
                'message' => 'Speciality was not found',
            ], 404);
        }

        $mdlSubject = $this->getDoctrine()->getRepository('App:ModuleSubject')->find($id);
        if (!$mdlSubject) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.module_subject',
                'message' => 'Module-Subject was not found',
            ], 404);
        }

        $data = json_decode($request->getContent(), true);
        if ($data['coefficient'] <= 10 && $data['coefficient'] > 0) {

            /**
             * @var Module $module
             */
            $module = $mdlSubject->getModule();
            $module->setCoefficient($module->getCoefficient() - $mdlSubject->getCoefficient() + $data['coefficient']);
            $mdlSubject->setCoefficient($data['coefficient']);
            $this->getDoctrine()->getManager()->flush();

            return $this->handleView($this->view(NULL, 204));
        }

        return $this->handleView($this->view([
            'status_code' => 400,
            'error_code' => 'data.not_valid',
            'message' => 'The data was not valid.',], 400));
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/specialities/{specId}/module-subjects/{id}", name="api_specialities_modules_subjects_delete", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="ModuleSubject",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $specId
     * @param int $id
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(int $insId, int $specId, int $id) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $speciality = $this->getDoctrine()->getRepository('App:Speciality')->findOneBy(['id' => $specId, 'institute' => $institute]);
        if (!$speciality) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.speciality',
                'message' => 'Speciality was not found',
            ], 404);
        }

        $mdlSubject = $this->getDoctrine()->getRepository('App:ModuleSubject')->find($id);
        if (!$mdlSubject) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.module_subject',
                'message' => 'Module-Subject was not found',
            ], 404);
        }

        /**
         * @var Module $module
         */
        $module = $mdlSubject->getModule();
        $module->setCoefficient($module->getCoefficient() - $mdlSubject->getCoefficient());
        $this->getDoctrine()->getManager()->remove($mdlSubject);
        $this->getDoctrine()->getManager()->flush();

        return $this->handleView($this->view(NULL, 204));
    }
}

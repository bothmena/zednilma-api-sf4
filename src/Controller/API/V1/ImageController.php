<?php

namespace App\Controller\API\V1;

use App\AppEvents;
use App\Entity\Image;
use App\Event\InstituteImageUploadSuccessEvent;
use App\Event\UserImageUploadSuccessEvent;
use App\Form\Type\ImageEntityType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageController extends FOSRestController {

    /**
     * @Rest\Get(path="/images/{id}", requirements={"id": "\d+"}, name="api_image_read", options={
     *     "method_prefix" = false })
     * @ ApiDoc(
     *     section="Image",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "images"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $id
     *
     * @return Response
     */
    public function getAction( int $id ) {

        $image = $this->getDoctrine()->getRepository( 'App:Image' )->find( $id );

        if ( !$image ) {
            return new JsonResponse( [ 'code' => 404, 'error' => 'Image was not found' ], 404 );
        }

        return $this->handleView( $this->view( $image, 200 ) );
    }

    /**
     * @Rest\Get(path="/images", name="api_image_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Image",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "images"},
     *     tags={"v1" = "#4A7023"},
     * )
     *
     * @return Response
     */
    public function cgetAction() {

        $images = $this->getDoctrine()->getRepository( 'App:Image' )->findAll();

        return $this->handleView( $this->view( $images, 200 ) );
    }

    /**
     * @Rest\Post(path="/images/{entity}/{id}/{type}", name="api_image_create", requirements={ "entity": "user|institute",
     *         "id": "\d+","type": "prf|cvr|glr" }, options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Image",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "images"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function newAction( Request $request, string $entity, int $id, string $type ) {

        try {

            $image = $this->get( 'abo.img_upload' )->uploadImage( $entity, $type );

            return $this->processEvent( $image, $id );
        } catch ( FileException $fileException ) {
            return new JsonResponse( [
                'message' => 'we couldn\'t process the image upload.',
                'error'   => $fileException->getMessage(),
            ], 500 );
        }
    }

    /**
     * @Rest\Route(path="/images/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"},
     *     name="api_image_update", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Image",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "images"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @param int $id
     *
     * @return Response
     */
    public function editAction( Request $request, int $id ) {

        $image = $this->getDoctrine()->getRepository( 'App:Image' )->find( $id );

        if ( !$image ) {
            return new JsonResponse( [ 'code' => 404, 'error' => 'Image was not found' ], 404 );
        }

        return $this->processForm( $request, $image );
    }

    /**
     * @Rest\Delete(path="/images/{id}", requirements={"id": "\d+"}, name="api_image_delete", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Image",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "images"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $id
     *
     * @return Response
     * @internal param Request $request
     *
     */
    public function removeAction( int $id ) {

        $image = $this->getDoctrine()->getRepository( 'App:Image' )->find( $id );

        if ( $image ) {

            $em = $this->getDoctrine()->getManager();
            $em->remove( $image );
            $em->flush();
            $path = $this->get( 'kernel' )->getRootDir() . '/../web' . $image->getSource();
            unlink( $path );
        } else {
            return new JsonResponse( [ 'code' => 404, 'error' => 'Image was not found' ], 404 );
        }

        return new Response( NULL, 204 );
    }

    private function processEvent( Image $image, int $id ) {

        $dbEntity = $this->getEntityFromDB( $image->getEntity(), $id );

        if ( $dbEntity ) {

            $dispatcher = $this->get( 'event_dispatcher' );
            switch ( $image->getEntity() ) {

                case 'ins':
                    $event = new InstituteImageUploadSuccessEvent( $dbEntity, $image );
                    $dispatcher->dispatch( AppEvents::INSTITUTE_IMAGE_UPLOAD_SUCCESS, $event );

                    return $event->getResponse();
                case 'usr':
                    $event = new UserImageUploadSuccessEvent( $dbEntity, $image );
                    $dispatcher->dispatch( AppEvents::USER_IMAGE_UPLOAD_SUCCESS, $event );

                    return $event->getResponse();
                default:
                    return new Response( 'Entity not valid: '.$image->getEntity(), 400 );
            }
        } else
            return new Response( 'Entity not found: '.$image->getEntity(), 404 );
    }

    /**
     * @param Request $request
     * @param Image $image
     * @return Response
     */
    private function processForm( Request $request, Image $image ) {

        $form = $this->createForm( ImageEntityType::class, $image );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $image );
            $em->flush();

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }

    /**
     * @param string $entity
     * @param int $id
     * @return \App\Entity\Institute|\App\Entity\User|NULL
     */
    private function getEntityFromDB( string $entity, int $id ) {

        if ( $entity === 'ins' ) {

            return $this->getDoctrine()->getRepository( 'App:Institute' )->find( $id );
        } else if ( $entity === 'usr' ) {

            return $this->getDoctrine()->getRepository( 'App:User' )->find( $id );
        }
        return NULL;
    }
}

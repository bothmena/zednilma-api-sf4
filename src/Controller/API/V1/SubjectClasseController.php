<?php

namespace App\Controller\API\V1;

use App\Entity\SubjectClasse;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class SubjectClasseController extends FOSRestController {


    /**
     * @var $accessChecker ZdlmAccessChecker
     * @var $authChecker AuthorizationCheckerInterface
     */
    private $accessChecker;
    private $authChecker;

    public function __construct(ZdlmAccessChecker $accessChecker, AuthorizationCheckerInterface $checker) {

        $this->accessChecker = $accessChecker;
        $this->authChecker = $checker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/professors/{id}/subject-classes", name="api_subject_classes_prof_sub_cls", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="SubjectClasse",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getProfessorSubClassesAction(int $insId, int $id) {

        $this->accessChecker->checkForInstitute($insId);

        $professor = $this->getDoctrine()->getRepository('App:User')->find($id);
        if (!$professor) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user',
                'message' => 'This user was not found.',
            ], 401);
        }

        $subjectClasses = $this->getDoctrine()->getRepository('App:SubjectClasse')->findBy(['professor' => $professor]);
        return $this->handleView($this->view($subjectClasses, 200));
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/class/{slug}/subjects", name="api_subject_classes_class_subs_by_slug", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="SubjectClasse",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param string $slug
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getClasseSubjectsBySlugAction(int $insId, string $slug) {

        $this->accessChecker->checkForInstitute($insId);

        $classe = $this->getDoctrine()->getRepository('App:Classe')->findOneBy(['slug'=>$slug]);
        if (!$classe) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user',
                'message' => 'This user was not found.',
            ], 401);
        }

        $subjectClasses = $this->getDoctrine()->getRepository('App:SubjectClasse')->findBy(['classe' => $classe, 'role'=>1]);
        return $this->handleView($this->view($subjectClasses, 200));
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/professors/{id}/subject-classes", name="api_subject_classes_prof_sub_cls_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="SubjectClasse",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $id
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newProfessorSubClasseAction(Request $request, int $insId, int $id) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        if ( false === $this->authChecker->isGranted('ROLE_INS_ADMIN') && $this->getUser()->getId() !== $id) {

            return new JsonResponse([
                'status_code' => 401,
                'error_code' => 'not_authenticated',
                'message' => 'you are not authenticated',
            ], 401);
        }

        $userIns = $this->getDoctrine()->getRepository('App:UserInstitute')->findOneBy(['user' => $this->getUser(), 'institute' => $institute, 'dateOut' => NULL]);
        if (!$userIns) {

            return new JsonResponse([
                'status_code' => 403,
                'error_code' => 'access_denied.role_not_granted',
                'message' => 'You do not belong to this institute',
            ], 403);
        }

        $professor = $this->getDoctrine()->getRepository('App:User')->find($id);
        if (!$professor) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user',
                'message' => 'This user was not found.',
            ], 404);
        }

        $data = json_decode( $request->getContent(), true );
        $subject = $this->getDoctrine()->getRepository('App:Subject')->find($data['subject']);
        if (!$subject) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.subject',
                'message' => 'Subject was not found.',
            ], 404);
        }
        foreach ($data['classe'] as $classeId) {
            $classe = $this->getDoctrine()->getRepository('App:Classe')->find($classeId);
            if (!$classe) {

                return new JsonResponse([
                    'status_code' => 404,
                    'error_code' => 'not_found.class',
                    'message' => 'Class was not found.',
                ], 404);
            }
            $subClass = $this->getDoctrine()->getRepository('App:SubjectClasse')->findOneBy(['subject'=>$subject, 'classe'=>$classe, 'professor'=>$professor]);
            if (!$subClass) {
                $subClass = new SubjectClasse($subject, $classe, $professor);
                $this->getDoctrine()->getManager()->persist($subClass);
            } else {
                return new JsonResponse([
                    'status_code' => 400,
                    'error_code' => 'subject_class.data_already_exist',
                    'message' => 'This professor already have the subject/class'
                ], 400);
            }
        }
        $this->getDoctrine()->getManager()->flush();

        return $this->handleView($this->view(NULL, 201));
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/subject-classes/{id}", name="api_subject_classes_delete", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="SubjectClasse",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(int $insId, int $id) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        if ( false === $this->authChecker->isGranted('ROLE_INS_ADMIN')) {

            return new JsonResponse([
                'status_code' => 401,
                'error_code' => 'not_authenticated',
                'message' => 'you are not authenticated',
            ], 401);
        }

        $subClass = $this->getDoctrine()->getRepository('App:SubjectClasse')->find($id);
        if (!$subClass) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.subject_class',
                'message' => 'Subject/Class was not found',
            ], 404);
        } else if ( $subClass->getClasse()->getBranch()->getInstitute()->getId() !== $insId ) {
            return new JsonResponse([
                'status_code' => 403,
                'error_code' => 'access_denied.not_institute_property',
                'message' => 'you are not allowed to do this action',
            ], 403);
        }
        $this->getDoctrine()->getManager()->remove($subClass);
        $this->getDoctrine()->getManager()->flush();

        return $this->handleView($this->view(NULL, 204));
    }
}

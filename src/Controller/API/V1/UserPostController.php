<?php

namespace App\Controller\API\V1;

use App\Entity\UserGroup;
use App\Entity\UserPost;
use App\Form\Type\UserPostType;
use App\Services\ABONotificationDescriber;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserPostController extends FOSRestController {


    private $accessChecker;
    private $describer;

    public function __construct(ZdlmAccessChecker $checker, ABONotificationDescriber $describer) {

        $this->accessChecker = $checker;
        $this->describer = $describer;
    }

    /**
     * @Rest\Get(path="/userPosts/{id}", requirements={"id": "\d+"}, name="api_user_post_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="UserPost",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $id
     * @return Response
     */
    public function getAction(int $id) {

        $userPost = $this->getDoctrine()->getRepository('App:UserPost')->find($id);
        if (!$userPost) {
            throw $this->createNotFoundException('UserPost not found.');
        }

        $view = $this->view($userPost, 200);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get(path="/userPosts", name="api_user_post_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="UserPost",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function cgetAction() {

        $userPosts = $this->getDoctrine()->getRepository('App:UserPost')->findAll();
        return $this->handleView($this->view($userPosts, 200));
    }

    /**
     * @Rest\Post(path="/userPosts", name="api_user_post_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="UserPost",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request) {

        /**
         * @todo check for access restrictions
         */
        $userPost = new UserPost();

        return $this->processForm($request, $userPost, true);
    }

    private function processForm(Request $request, UserPost $userPost, bool $isNew = false) {

        $form = $this->createForm(UserPostType::class, $userPost);

        $data = json_decode($request->getContent(), true);
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($userPost);
            $em->flush();

            if ($isNew) {

                $userPostGrp = new UserGroup($userPost->getUser(), $userPost->getPost()->getGroupe());
                $em->persist($userPostGrp);
                $em->flush();
                $view = $this->view(NULL, 201);

                $response = $this->handleView($view);
                $response->headers->set('Location', $this->generateUrl('api_user_post_read', [
                    'version' => 'v1',
                    'id' => $userPost->getId(),
                ]));
                return $response;
            }

            $view = $this->view(NULL, 204);
            return $this->handleView($view);
        }

        $view = $this->view($form, 400);
        return $this->handleView($view);
    }

    /**
     * @Rest\Route(path="/userPosts/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_user_post_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="UserPost",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editAction(Request $request, int $id) {

        /**
         * @todo check for access restrictions
         */
        /*if ( !$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED') ) {

            throw $this->createAccessDeniedException('No Anonymous allowed here!!');
        }*/
        $userPost = $this->getDoctrine()->getRepository('App:UserPost')->find($id);
        if (!$userPost) {
            throw $this->createNotFoundException('UserPost not found.');
        }

        return $this->processForm($request, $userPost);
    }
}

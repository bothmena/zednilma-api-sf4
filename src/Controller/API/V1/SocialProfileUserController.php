<?php

namespace App\Controller\API\V1;

use App\Entity\SocialProfile;
use App\Form\Type\SocialProfileType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SocialProfileUserController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/users/{userId}/social-profiles/{id}", requirements={ "userId": "\d+", "id": "\d+"},
     *     name="api_social_profile_user_read", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="SocialProfile",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @param int $id
     * @return Response
     */
    public function getAction( int $userId, int $id ) {

        $user = $this->getDoctrine()->getRepository( 'App:User' )
            ->find( $userId );
        if ( !$user ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.user',
                'message'     => 'User was not found',
            ], 404 );
        }

        $socialProfile = $this->getDoctrine()->getRepository( 'App:SocialProfile' )
            ->getUserSocialProfile( $userId, $id );
        if ( !$socialProfile ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_social_profile',
                'message'     => 'User social profile was not found',
            ], 404 );
        }

        return $this->handleView( $this->view( $socialProfile, 200 ) );
    }

    /**
     * @Rest\Get(path="/users/{userId}/social-profiles", requirements={"userId": "\d+"},
     *     name="api_social_profile_user_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="SocialProfile",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @return Response
     */
    public function cgetAction( int $userId ) {

        $user = $this->getDoctrine()->getRepository( 'App:User' )
            ->find( $userId );
        if ( !$user ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.user',
                'message'     => 'User was not found',
            ], 404 );
        }

        $socialProfiles = $this->getDoctrine()->getRepository( 'App:SocialProfile' )
            ->findBy( [ 'user' => $user ] );

        return $this->handleView( $this->view( $socialProfiles, 200 ) );
    }

    /**
     * @Rest\Post(path="/users/{userId}/social-profiles", requirements={"userId": "\d+"},
     *     name="api_social_profile_user_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="SocialProfile",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $userId
     * @return Response
     */
    public function newAction( Request $request, int $userId ) {

        $result = $this->accessChecker->checkForUser( $userId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $socialProfile = new SocialProfile();
        $socialProfile->setUser( $result->getUser() );
        return $this->processForm( $request, $socialProfile, true );
    }

    /**
     * @Rest\Route(path="/users/{userId}/social-profiles/{id}", requirements={"userId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_social_profile_user_update", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="SocialProfile",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "SocialProfiles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $userId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $userId, int $id ) {

        $result = $this->accessChecker->checkForUser( $userId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $socialProfile = $this->getDoctrine()->getRepository( 'App:SocialProfile' )
            ->getUserSocialProfile( $userId, $id );
        if ( !$socialProfile ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_social_profile',
                'message'     => 'User social profile was not found',
            ], 404 );
        }

        return $this->processForm( $request, $socialProfile, false );
    }

    /**
     * @Rest\Delete(path="/users/{userId}/social-profiles/{id}", requirements={"userId": "\d+","id": "\d+"},
     *     name="api_social_profile_user_delete", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="SocialProfile",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "SocialProfiles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $userId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $userId, int $id ) {

        $result = $this->accessChecker->checkForUser( $userId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $socialProfile = $this->getDoctrine()->getRepository( 'App:SocialProfile' )
            ->getUserSocialProfile( $userId, $id );
        if ( !$socialProfile ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_social_profile',
                'message'     => 'User social profile was not found',
            ], 404 );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove( $socialProfile );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param SocialProfile $socialProfile
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, SocialProfile $socialProfile, bool $isNew = false ) {

        $form = $this->createForm( SocialProfileType::class, $socialProfile );

        $data = json_decode( $request->getContent(), true );

        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $socialProfile );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_social_profile_user_read', [
                    'version' => 'v1',
                    'userId'  => $socialProfile->getUser()->getId(),
                    'id'      => $socialProfile->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}

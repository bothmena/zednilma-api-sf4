<?php

namespace App\Controller\API\V1;

use App\Entity\SessionContainer;
use App\Entity\Timetable;
use App\Form\TimetableType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TimetableController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/timetables/{id}", requirements={"insId": "\d+", "id": "\d+"},
     *     name="api_timetable_read", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Timetable",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function getAction( int $insId, int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )
            ->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $timetable = $this->getDoctrine()->getRepository( 'App:Timetable' )->findOneBy(['institute'=>$institute, 'id'=>$id]);
        if ( !$timetable ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.timetable',
                'message'     => 'Timetable was not found',
            ], 404 );
        }

        return $this->handleView( $this->view( $timetable, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insSlug}/timetables/{id}", requirements={"id": "\d+"},
     *     name="api_timetable_by_ins_slug_read", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Timetable",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $insSlug
     * @param int $id
     * @return Response
     */
    public function getBySlugAction(string $insSlug, int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )
            ->findOneBy(['slug'=>$insSlug]);
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $timetable = $this->getDoctrine()->getRepository( 'App:Timetable' )->findOneBy(['institute'=>$institute, 'id'=>$id]);
        if ( !$timetable ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.timetable',
                'message'     => 'Timetable was not found',
            ], 404 );
        }

        return $this->handleView( $this->view( $timetable, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/timetables", requirements={"insId": "\d+"},
     *     name="api_timetable_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Timetable",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @return Response
     */
    public function cgetAction( int $insId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $timetables = $this->getDoctrine()->getRepository( 'App:Timetable' )->findBy(['institute'=>$institute]);

        return $this->handleView( $this->view( $timetables, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insSlug}/timetables", name="api_timetable_read_all_by_slug",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Timetable",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $insSlug
     * @return Response
     */
    public function cgetByInsSlugAction( string $insSlug ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->findOneBy( [ 'slug' => $insSlug ] );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $timetables = $this->getDoctrine()->getRepository( 'App:Timetable' )->findBy(['institute'=>$institute]);

        return $this->handleView( $this->view( $timetables, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/timetables", requirements={"insId": "\d+"}, name="api_timetable_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Timetable",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return Response
     */
    public function newAction( Request $request, int $insId ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $timetable = new Timetable();
        $timetable->setInstitute( $result->getInstitute() );

        return $this->processForm( $request, $timetable, true );
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/timetables/{id}", requirements={"insId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_timetable_update",options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Timetable",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Timetables"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $timetable = $this->getDoctrine()->getRepository( 'App:Timetable' )->findOneBy(['institute'=>$result->getInstitute(), 'id'=>$id]);
        if ( !$timetable ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.timetable',
                'message'     => 'Timetable was not found',
            ], 404 );
        }

        return $this->processForm( $request, $timetable, false );
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/timetables/{id}", requirements={"insId": "\d+","id": "\d+"},
     *     name="api_timetable_delete", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Timetable",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "Timetables"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $timetable = $this->getDoctrine()->getRepository( 'App:Timetable' )->findOneBy(['institute'=>$result->getInstitute(), 'id'=>$id]);
        if ( !$timetable ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.timetable',
                'message'     => 'Timetable was not found',
            ], 404 );
        }

        $containers = $timetable->getSessionContainers();

        $em = $this->getDoctrine()->getManager();
        foreach ( $containers as $container )
            $em->remove( $container );
        $em->remove( $timetable );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Timetable $timetable
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Timetable $timetable, bool $isNew = false ) {

        $form = $this->createForm( TimetableType::class, $timetable );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $timetable );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_timetable_read', [
                    'version' => 'v1',
                    'insId'   => $timetable->getInstitute()->getId(),
                    'id'      => $timetable->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}

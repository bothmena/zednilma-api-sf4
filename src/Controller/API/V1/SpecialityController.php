<?php

namespace App\Controller\API\V1;

use App\Entity\Group;
use App\Entity\Speciality;
use App\Form\Type\SpecialityType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SpecialityController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/specialities/{id}",
     *     requirements={
     *          "insId": "\d+",
     *          "id": "\d+"
     *     }, name="api_speciality_read", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Speciality",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function getAction( int $insId, int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $speciality = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findOneBy( ['id'=>$id, 'institute'=>$institute] );
        if ( !$speciality ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_speciality',
                'message'     => 'Institute speciality was not found',
            ], 404 );
        }

        return $this->handleView( $this->view( $speciality, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/specialities/{slug}", requirements={"insId": "\d+"}, name="api_speciality_by_slug_read", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Speciality",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param string $slug
     * @return Response
     */
    public function getBySlugAction( int $insId, string $slug ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $speciality = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findOneBy( ['slug'=>$slug, 'institute'=>$institute] );
        if ( !$speciality ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_speciality',
                'message'     => 'Institute speciality was not found',
            ], 404 );
        }

        return $this->handleView( $this->view( $speciality, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/specialities", requirements={
     *     "insId": "\d+"}, name="api_speciality_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Speciality",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @return Response
     */
    public function cgetAction( int $insId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $specialities = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findBy( [ 'institute' => $institute ] );

        return $this->handleView( $this->view( $specialities, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{slug}/specialities", name="api_speciality_read_all_slug", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Speciality",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $slug
     * @return Response
     */
    public function cgetByInsSlugAction( string $slug ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->findOneBy( ['slug' => $slug] );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $specialities = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findBy( [ 'institute' => $institute ] );

        return $this->handleView( $this->view( $specialities, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/specialities", requirements={"insId": "\d+"},
     *     name="api_speciality_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Speciality",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @return Response
     */
    public function newAction( Request $request, int $insId ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $speciality = new Speciality();
        $speciality->getGroupe()->setInstitute($result->getInstitute());
        $speciality->setInstitute( $result->getInstitute() );
        return $this->processForm( $request, $speciality, true );
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/specialities/{id}", requirements={"insId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_speciality_update",options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Speciality",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "Specialitys"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $speciality = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findOneBy( ['id' => $id, 'institute' => $result->getInstitute() ] );
        if ( !$speciality ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_speciality',
                'message'     => 'Institute speciality was not found',
            ], 404 );
        }

        return $this->processForm( $request, $speciality, false );
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/specialities/{id}", requirements={"insId": "\d+","id": "\d+"},
     *     name="api_speciality_delete", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Speciality",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "Specialitys"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $insId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $em = $this->getDoctrine()->getManager();
        $speciality = $em->getRepository( 'App:Speciality' )
            ->findOneBy( ['id' => $id, 'institute' => $result->getInstitute() ] );
        if ( !$speciality ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_speciality',
                'message'     => 'Institute speciality was not found',
            ], 404 );
        }

        if ($speciality->getParent() === NULL) {
            $paths = $em->getRepository('App:Path')->findBy(['speciality' => $speciality]);
            foreach ($paths as $path) {
                $pathsBranches = $em->getRepository('App:PathBranch')->findBy(['path'=> $path]);
                foreach ($pathsBranches as $pathBranch)
                    $em->remove($pathBranch);
                $em->remove($path);
            }
            $children = $em->getRepository('App:Speciality')->findBy(['parent' => $speciality]);
            foreach ( $children as $child ) {
                /**
                 * @todo delete subjects and other dependencies.
                 */
//                $subjects = $em->getRepository('App:ModuleSubject')->findBy(['speciality' => $child]);
                $em->remove( $child );
            }
        } else {
            $pathsBranches = $em->getRepository('App:PathBranch')->findBy(['branch'=> $speciality]);
            foreach ($pathsBranches as $pathBranch) {
                $pbs = $em->getRepository('App:PathBranch')->findBy(['path'=> $pathBranch->getPath()]);
                foreach($pbs as $pb)
                    $em->remove($pb);
                $em->remove($pathBranch->getPath());
            }
        }
        $parentGrp = $this->getDoctrine()->getRepository('App:Group')->findOneBy(['parent'=>$speciality->getGroupe(), 'type'=> 'speciality_p']);
        if ($parentGrp)
            $em->remove( $parentGrp );

        $em->remove( $speciality );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param Speciality $speciality
     * @param bool $isNew
     * @return Response
     */
    private function processForm( Request $request, Speciality $speciality, bool $isNew = false ) {

        $form = $this->createForm( SpecialityType::class, $speciality );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $speciality );
            $em->flush();

            if ( $isNew ) {

                $parentGrp = new Group();
                $parentGrp->setType('speciality_p');
                $parentGrp->setInstitute($speciality->getInstitute());
                $parentGrp->setSuffixedName($speciality->getName());
                $parentGrp->setParent($speciality->getGroupe());

                $em->persist( $parentGrp );
                $em->flush();

                $response = $this->handleView( $this->view( NULL, 201 ) );
                $response->headers->set( 'Location', $this->generateUrl( 'api_speciality_read', [
                    'version'  => 'v1',
                    'insId'    => $speciality->getInstitute()->getId(),
                    'id'       => $speciality->getId(),
                ] ) );

                return $response;
            } else {

                $parentGrp = $this->getDoctrine()->getRepository('App:Group')->findOneBy(['parent'=>$speciality->getGroupe(), 'type'=> 'speciality_p']);
                if ($parentGrp)
                    $parentGrp->setSuffixedName($speciality->getName());

                $em->flush();
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}

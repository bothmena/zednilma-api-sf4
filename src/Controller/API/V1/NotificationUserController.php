<?php

namespace App\Controller\API\V1;

use App\Entity\NotificationUser;
use App\Form\Type\NotificationUserType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NotificationUserController extends FOSRestController {


    private $accessChecker;

    /**
     * UserDepartmentController constructor.
     * @param ZdlmAccessChecker $accessChecker
     */
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/notificationUsers/{id}", requirements={"id": "\d+"}, name="api_notification_user_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="NotificationUser",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function getAction ( int $id ) {

        $notificationUser = $this->getDoctrine()->getRepository( 'App:NotificationUser' )->find( $id );
        if ( !$notificationUser ) {
            throw $this->createNotFoundException( 'NotificationUser not found.' );
        }

        $view = $this->view( $notificationUser, 200 );
        return $this->handleView( $view );
    }

    /**
     * @Rest\Get(path="/notificationUsers", name="api_notification_user_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="NotificationUser",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     */
    public function cgetAction () {

        $notificationUsers = $this->getDoctrine()->getRepository( 'App:NotificationUser' )->findAll();
        return $this->handleView( $this->view( $notificationUsers, 200 ) );
    }

    /**
     * @Rest\Post(path="/notificationUsers", name="api_notification_user_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="NotificationUser",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request) {

        /**
         * @todo check for access restrictions
         */
        $notificationUser = new NotificationUser();

        return $this->processForm( $request, $notificationUser, true );
    }

    /**
     * @Rest\Route(path="/notificationUsers/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_notification_user_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="NotificationUser",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editAction ( Request $request, int $id ) {

        /**
         * @todo check for access restrictions
         */
        /*if ( !$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED') ) {

            throw $this->createAccessDeniedException('No Anonymous allowed here!!');
        }*/
        $notificationUser = $this->getDoctrine()->getRepository( 'App:NotificationUser' )->find( $id );
        if ( !$notificationUser ) {
            throw $this->createNotFoundException( 'NotificationUser not found.' );
        }

        return $this->processForm( $request, $notificationUser );
    }

    private function processForm ( Request $request, NotificationUser $notificationUser, bool $isNew = false ) {

        $form = $this->createForm( NotificationUserType::class, $notificationUser );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $notificationUser );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set('Location', $this->generateUrl('api_notification_user_read', [
                    'version' => 'v1',
                    'id' => $notificationUser->getId(),
                ]));
                return $response;
            }

            $view = $this->view( NULL, 204 );
            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );
        return $this->handleView( $view );
    }
}

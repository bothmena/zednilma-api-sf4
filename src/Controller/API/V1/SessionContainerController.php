<?php

namespace App\Controller\API\V1;

use App\Entity\SessionContainer;
use App\Form\SessionContainerType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SessionContainerController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/timetables/{timeId}/containers/{id}", requirements={"insId": "\d+", "timeId": "\d+", "id": "\d+"},
     *     name="api_session_container_read", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="SessionContainer",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId , int $timeId
     * @param int $timeId
     * @param int $id
     * @return Response
     */
    public function getAction( int $insId, int $timeId, int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $timetable = $this->getDoctrine()->getRepository( 'App:Timetable' )->findOneBy(['institute'=>$institute, 'id'=>$timeId]);
        if ( !$timetable ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.timetable',
                'message'     => 'Timetable was not found',
            ], 404 );
        }

        $container = $this->getDoctrine()->getRepository( 'App:SessionContainer' )->findOneBy(['timetable'=>$timetable, 'id'=>$id]);
        if ( !$container ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.session_container',
                'message'     => 'Session container was not found',
            ], 404 );
        }

        return $this->handleView( $this->view( $container, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/timetables/{timeId}/containers", requirements={"insId": "\d+", "timeId": "\d+"},
     *     name="api_session_container_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="SessionContainer",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId , int $timeId
     * @param int $timeId
     * @return Response
     */
    public function cgetAction( int $insId, int $timeId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $timetable = $this->getDoctrine()->getRepository( 'App:Timetable' )->findOneBy(['institute'=>$institute, 'id'=>$timeId]);
        if ( !$timetable ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.timetable',
                'message'     => 'Timetable was not found',
            ], 404 );
        }

        $SessionContainer = $this->getDoctrine()->getRepository( 'App:SessionContainer' )->findOneBy(['timetable'=>$timetable]);

        return $this->handleView( $this->view( $SessionContainer, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insSlug}/timetables/{timeId}/containers", name="api_session_container_read_all_by_slug",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="SessionContainer",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $insSlug
     * @param int $timeId
     * @return Response
     */
    public function cgetByInsSlugAction( string $insSlug, int $timeId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->findOneBy( [ 'slug' => $insSlug ] );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $timetable = $this->getDoctrine()->getRepository( 'App:Timetable' )->findOneBy(['institute'=>$institute, 'id'=>$timeId]);
        if ( !$timetable ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.timetable',
                'message'     => 'Timetable was not found',
            ], 404 );
        }

        $SessionContainer = $this->getDoctrine()->getRepository( 'App:SessionContainer' )->findOneBy(['timetable'=>$timetable]);

        return $this->handleView( $this->view( $SessionContainer, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/timetables/{timeId}/containers", requirements={"insId": "\d+", "timeId": "\d+"},
     *     name="api_session_container_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="SessionContainer",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId , int $timeId
     * @param int $timeId
     * @return Response
     */
    public function newAction( Request $request, int $insId, int $timeId ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $timetable = $this->getDoctrine()->getRepository( 'App:Timetable' )->findOneBy(['institute'=>$result->getInstitute(), 'id'=>$timeId]);
        if ( !$timetable ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.timetable',
                'message'     => 'Timetable was not found',
            ], 404 );
        }
        $containers = $this->getDoctrine()->getRepository( 'App:SessionContainer' )->findBy(['timetable'=>$timetable]);
        $em = $this->getDoctrine()->getManager();
        foreach ($containers as $container)
            $em->remove($container);

        $arr = ['success'=>0, 'failure'=>0];
        $data = json_decode( $request->getContent(), true );

        foreach ($data as $scData) {

            $container = new SessionContainer();
            $container->setTimetable( $timetable );

            $form = $this->createForm( SessionContainerType::class, $container );
            $form->submit( $scData );

            if ( $form->isValid() ) {

                $em->persist($container);
                $arr['success']++;
            }else
                $arr['failure']++;
        }
        $em->flush();

        return $this->handleView( $this->view( $arr, 201 ) );
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/timetables/{timeId}/containers/{id}", requirements={"insId": "\d+", "timeId": "\d+","id": "\d+"},
     *     methods={"PUT", "PATCH"}, name="api_session_container_update",options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="SessionContainer",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "SessionContainers"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId, int $timeId
     * @param int $id
     * @return Response
     */
    public function editAction( Request $request, int $insId, int $timeId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $timetable = $this->getDoctrine()->getRepository( 'App:Timetable' )->findOneBy(['institute'=>$result->getInstitute(), 'id'=>$timeId]);
        if ( !$timetable ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.timetable',
                'message'     => 'Timetable was not found',
            ], 404 );
        }

        $container = $this->getDoctrine()->getRepository( 'App:SessionContainer' )->findOneBy(['timetable'=>$timetable, 'id'=>$id]);
        if ( !$container ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.session_container',
                'message'     => 'Session container was not found',
            ], 404 );
        }

        return $this->processForm( $request, $container, false );
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/timetables/{timeId}/containers/{id}", requirements={"insId": "\d+", "timeId": "\d+","id": "\d+"},
     *     name="api_session_container_delete", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="SessionContainer",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "SessionContainers"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId, int $timeId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $insId, int $timeId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $timetable = $this->getDoctrine()->getRepository( 'App:Timetable' )->findOneBy(['institute'=>$result->getInstitute(), 'id'=>$timeId]);
        if ( !$timetable ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.timetable',
                'message'     => 'Timetable was not found',
            ], 404 );
        }

        $container = $this->getDoctrine()->getRepository( 'App:SessionContainer' )->findOneBy(['timetable'=>$timetable, 'id'=>$id]);
        if ( !$container ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.session_container',
                'message'     => 'Session container was not found',
            ], 404 );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove( $container );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }

    /**
     * @param Request $request
     * @param SessionContainer $container
     * @param bool $isNew
     * @return Response
     */
    private function processForm(Request $request, SessionContainer $container, bool $isNew = false ) {

        $form = $this->createForm( SessionContainerType::class, $container );

        $data = json_decode( $request->getContent(), true );
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit( $data, $clearMissing );

        if ( $form->isValid() ) {

            $em = $this->getDoctrine()->getManager();
            $em->persist( $container );
            $em->flush();

            if ( $isNew ) {

                $view = $this->view( NULL, 201 );

                $response = $this->handleView( $view );
                $response->headers->set( 'Location', $this->generateUrl( 'api_session_container_read', [
                    'version' => 'v1',
                    'insId'   => $container->getTimetable()->getInstitute()->getId(),
                    'timeId'   => $container->getTimetable()->getId(),
                    'id'      => $container->getId(),
                ] ) );

                return $response;
            }

            $view = $this->view( NULL, 204 );

            return $this->handleView( $view );
        }

        $view = $this->view( $form, 400 );

        return $this->handleView( $view );
    }
}

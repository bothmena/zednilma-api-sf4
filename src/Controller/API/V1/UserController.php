<?php

namespace App\Controller\API\V1;

use App\AppEvents;
use App\Entity\User;
use App\Entity\UserInstitute;
use App\Event\UserConfirmEmailSuccessEvent;
use App\Event\UserInitAccountSuccessEvent;
use App\Event\UserRegistrationSuccessEvent;
use App\Form\Type\ChangePasswordType;
use App\Form\Type\ResetPasswordType;
use App\Form\Type\UserEditType;
use App\Form\Type\UserInitType;
use App\Form\Type\UserType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;

class UserController extends FOSRestController {

    private $tokenGen;
    private $userManager;
    private $dispatcher;
    private $accessChecker;

    private $fields = [
        'id' => 'id',
        'username' => 'username',
        'first_name' => 'firstName',
        'last_name' => 'lastName',
        'email' => 'email',
        'last_login' => 'lastLogin',
    ];
    private $roles = [
        'professor' => 'ROLE_PROFESSOR',
        'ins_admin' => 'ROLE_INS_ADMIN',
        'ins_staff' => 'ROLE_INS_STAFF',
        'student' => 'ROLE_STUDENT',
        'parent' => 'ROLE_PARENT',
    ];

    public function __construct(TokenGeneratorInterface $generator, UserManagerInterface $userManager, EventDispatcherInterface $dispatcher, ZdlmAccessChecker $accessChecker) {

        $this->tokenGen = $generator;
        $this->userManager = $userManager;
        $this->dispatcher = $dispatcher;
        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/users/{id}", requirements={"id": "\d+"}, name="api_user_read", options={ "method_prefix" =
     *                               false })
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $id
     * @return Response
     */
    public function getAction(int $id) {

        $user = $this->getDoctrine()->getRepository('App:User')->find($id);
        if (!$user instanceof User) {
            throw $this->createNotFoundException('User not found.');
        }

        $view = $this->view($user, 200);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get(path="/users/{username}", name="api_user_read_username", options={ "method_prefix" = false })
     * @Rest\View()
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $username
     * @return Response
     */
    public function getByUsernameAction(string $username) {

        $user = $this->getDoctrine()->getRepository('App:User')
            ->findOneBy(['username' => $username]);

        if (!$user instanceof User) {
            return new JsonResponse(['code' => 404, 'error' => 'user.not_found'], 404);
        }

        $view = $this->view($user, 200);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get(path="/user-institute/{username}", name="api_user_ins_read_username", options={ "method_prefix" = false })
     * @Rest\View()
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $username
     * @return Response
     */
    public function getUserInsByUsernameAction(string $username) {

        $userInstitute = $this->getDoctrine()->getRepository('App:UserInstitute')->getUserInstitute($username);
        if (!$userInstitute instanceof UserInstitute) {

            return new JsonResponse(['code' => 404, 'error' => 'user_institute.not_found'], 404);
        }
        $view = $this->view($userInstitute, 200);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get(path="/users/t/{token}", name="api_user_read_token", options={ "method_prefix" = false })
     * @Rest\View()
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param string $token
     * @return Response
     */
    public function existByTokenAction(Request $request, string $token) {

        $criteria = ['confirmationToken' => $token];
        $enabled = $request->query->get('enabled');
        if ($enabled === 'true') {

            $criteria['enabled'] = true;
        } else if ($enabled === 'false') {

            $criteria['enabled'] = false;
        }

        $user = $this->getDoctrine()->getRepository('App:User')->findOneBy($criteria);

        return $this->handleView($this->view(['exist' => $user !== NULL], 200));
    }

    /**
     * @Rest\Get(path="/users", name="api_user_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @return Response
     */
    public function cgetAction(Request $request) {

        $role = $request->query->get('role');
        $orderBy = $request->query->get('order_by');
        $gender = $request->query->get('gender');
        $orderBy = array_key_exists($orderBy, $this->fields) ? $this->fields[$orderBy] : NULL;
        $role = array_key_exists($role, $this->roles) ? $this->roles[$role] : NULL;

        $users = $this->getDoctrine()->getRepository('App:User')
            ->getFiltredUsers($role, $gender, $orderBy);

        return $this->handleView($this->view($users, 200));
    }

    /**
     * @Rest\Get(path="/users/{id}/children", name="api_user_read_all_children", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $id
     * @return Response
     */
    public function cgetChildrenAction(int $id) {

        $result = $this->accessChecker->checkForUser($id, 'ROLE_PARENT');

        $children = $this->getDoctrine()->getRepository('App:User')->findBy(['parent'=>$result->getUser()]);

        return $this->handleView($this->view($children, 200));
    }

    /**
     * @Rest\Post(path="/users/init-account/{token}", name="api_user_init_account", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param string $token
     * @return Response
     */
    public function initAccountAction(Request $request, string $token) {

        /**
         * @var User $user
         */
        $user = $this->userManager->findUserByConfirmationToken($token);
        if (!$user) {

            throw $this->createNotFoundException('User wasn\'t found');
        }
        $form = $this->createForm(UserInitType::class, $user);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if ($form->isValid()) {

            $event = new UserInitAccountSuccessEvent($user);
            $this->dispatcher->dispatch(AppEvents::USER_INIT_ACCOUNT_SUCCESS, $event);

            return $this->handleView($this->view(NULL, 204));
        }

        return $this->handleView($this->view($form, 400));
    }

    /**
     * @Rest\Post(path="/users", name="api_user_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function newAction(Request $request, \Swift_Mailer $mailer, TranslatorInterface $translator) {

        /**
         * @var User $user
         */
        $user = $this->userManager->createUser();
        $form = $this->createForm(UserType::class, $user);
        $data = json_decode($request->getContent(), true);
        $data['plainPassword'] = [
            'password' => 'myCORRECT6548PASS',
            'confirm_password' => 'myCORRECT6548PASS',
        ];
        $form->submit($data);

        if ($form->isValid()) {

            $institute = $this->getDoctrine()->getRepository('App:Institute')
                ->find($form->get('institute')->getData());

            if ($institute) {

                $event = new UserRegistrationSuccessEvent($user);
                $this->dispatcher->dispatch(AppEvents::REGISTRATION_SUCCESS, $event);
                $view = $this->view(NULL, 201);

                $userInstitute = new UserInstitute();
                $userInstitute->setUser($user);
                $userInstitute->setInstitute($institute);
                $userInstitute->setRole($user->getRoles()[0]);

                $em = $this->getDoctrine()->getManager();
                $em->persist($userInstitute);
                $em->flush();

                $lang = $request->query->get( 'language' );
                $lang = ($lang === 'en' || $lang === 'fr') ? $lang : 'en';
                $message = (new \Swift_Message($translator->trans('confirmation.subject', [], null, $lang)))
                    ->setFrom(array('aymenbenothmenabo@gmail.com' => $translator->trans('mail.zed_support', [], null, $lang)))
                    ->setTo($user->getEmail())
                    ->setBody($this->renderView(
                        'emails/security/activate_account.html.twig',
                        array(
                            'link_var' => 'https://www.zednilma.com/init-account/' . $user->getConfirmationToken(),
                            'lang' => $lang
                        )
                    ),'text/html');
                $mailer->send($message);

                $response = $this->handleView($view);
                $response->headers->set('Location', $this->generateUrl('api_user_read', [
                    'version' => 'v1',
                    'id' => $user->getId(),
                ]));

                return $response;
            }

            throw $this->createNotFoundException('Institute wasn\'t found');
        }

        $view = $this->view($form, 400);

        return $this->handleView($view);
    }

    /**
     * @Rest\Route(path="/users/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_user_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $id
     * @return JsonResponse|Response
     */
    public function editAction(Request $request, int $id) {

        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {

            return new JsonResponse([
                'status_code' => 401,
                'error_code' => 'not_authenticated',
                'message' => 'you are not authenticated',
            ], 401);
        }
        $user = $this->getUser();

        if (!$user instanceof User) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user',
                'message' => 'user was not found',
            ], 404);
        }

        if ($user->getId() !== $id) {

            return new JsonResponse([
                'status_code' => 403,
                'error_code' => 'access_denied.not_user_property',
                'message' => 'you are not allowed to do this action',
            ], 403);
        }

        return $this->processForm($request, $user);
    }

    /**
     * @Rest\Get(path="/users/confirm-email/{token}", name="api_user_confirm_email", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param $token
     * @return JsonResponse|Response
     */
    public function confirmEmailAction($token) {

        $user = $this->userManager->findUserByConfirmationToken($token);

        if (NULL === $user) {
            return new Response('The user with confirmation token "' . $token . '" does not exist', 404);
        }

        $event = new UserConfirmEmailSuccessEvent($user);
        $this->dispatcher->dispatch(AppEvents::USER_CONFIRM_EMAIL_SUCCESS, $event);

        if (NULL === $response = $event->getResponse()) {

            return new RedirectResponse('https://www.zednilma.com');
        }

        return $response;
    }

    /**
     * @ Rest\Post(path="/users/resend-code/{email}", name="api_user_resend_code", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param string $email
     * @param \Swift_Mailer $mailer
     * @param TranslatorInterface $translator
     * @return JsonResponse
     */
    public function resendCodeAction(Request $request, string $email, \Swift_Mailer $mailer, TranslatorInterface $translator) {

        $user = $this->userManager->findUserByEmail($email);

        if (NULL === $user) {
            throw new NotFoundHttpException(
                sprintf('The user with email "%s" does not exist', $email)
            );
        }

        $lang = $request->query->get( 'language' );
        $lang = ($lang === 'en' || $lang === 'fr') ? $lang : 'en';
        $message = (new \Swift_Message($translator->trans('confirmation.subject', [], null, $lang)))
            ->setFrom(array('aymenbenothmenabo@gmail.com' => $translator->trans('mail.zed_support', [], null, $lang)))
            ->setTo($user->getEmail())
            ->setBody($this->renderView(
                'emails/security/confirmation_code.html.twig',
                array(
                    'link_var' => $this->generateUrl('api_user_confirm_email', [
                        'version' => 'v1',
                        'token' => $user->getConfirmationToken()
                    ], UrlGeneratorInterface::ABSOLUTE_URL),
                    'lang' => $lang
                )
            ),'text/html');
        $mailer->send($message);

        return new JsonResponse(NULL, 204);
    }

    /**
     * @Rest\Post(path="/users/{id}/change-password", requirements={"id": "\d+"}, name="api_user_change_pass_code", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function changePasswordAction(Request $request, int $id) {

        $user = $this->userManager->findUserBy(['id' => $id]);
        if (!$user) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user',
                'message' => 'User was not found',
            ], 404);
        }
        $form = $this->createForm(ChangePasswordType::class, $user);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if ($form->isValid()) {

            $newPass = $form->get('plainPassword')->get('password')->getData();
            $user->setPlainPassword($newPass);
            $this->userManager->updatePassword($user);
            $this->userManager->updateUser($user);

            return $this->handleView($this->view(NULL, 204));
        }

        return $this->handleView($this->view($form, 400));
    }

    /**
     * @Rest\Post(path="/users/{token}/reset-password", name="api_user_reset_pass_code", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param string $token
     * @return Response
     */
    public function resetPasswordAction(Request $request, string $token) {

        $user = $this->userManager->findUserBy(['confirmationToken' => $token, 'enabled' => true]);
        if (!$user) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user',
                'message' => 'User was not found',
            ], 404);
        }
        $form = $this->createForm(ResetPasswordType::class, $user);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if ($form->isValid()) {

            $newPass = $form->get('plainPassword')->get('password')->getData();
            $user->setPlainPassword($newPass);
            $user->setConfirmationToken(null);
            $user->setPasswordRequestedAt(null);
            $this->userManager->updatePassword($user);
            $this->userManager->updateUser($user);

            return $this->handleView($this->view(NULL, 204));
        }

        return $this->handleView($this->view($form, 400));
    }

    /**
     * @Rest\Post(path="/users/{email}/request-password-reset", name="api_user_conf_reset", options={ "method_prefix" = false })
     * @param Request $request
     * @param string $email
     * @param \Swift_Mailer $mailer
     * @param TranslatorInterface $translator
     * @return JsonResponse
     */
    public function requestPassResetAction(Request $request, string $email, \Swift_Mailer $mailer, TranslatorInterface $translator) {

        $user = $this->getDoctrine()->getRepository('App:User')->findOneBy(['email' => $email, 'enabled' => true]);
        if (!$user instanceof User) {
            return new JsonResponse(['code' => 404, 'error' => 'user.not_found'], 404);
        }
        $user->setConfirmationToken($this->tokenGen->generateToken());
        $user->setPasswordRequestedAt(new \DateTime());
        $this->getDoctrine()->getManager()->flush();

        $lang = $request->query->get( 'language' );
        $lang = ($lang === 'en' || $lang === 'fr') ? $lang : 'en';
        $message = (new \Swift_Message($translator->trans('confirmation.subject', [], null, $lang)))
            ->setFrom(array('aymenbenothmenabo@gmail.com' => $translator->trans('mail.zed_support', [], null, $lang)))
            ->setTo($user->getEmail())
            ->setBody($this->renderView(
                'emails/security/confirmation_code.html.twig',
                array(
                    'link_var' => 'https://www.zednilma.com/reset-password/' . $user->getConfirmationToken(),
                    'lang' => $lang
                )
            ),'text/html');
        $mailer->send($message);

        return new JsonResponse(NULL, 204);
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/users/{role}/send-activation-email", requirements={"insId": "\d+", "role": "professor|ins_admin|ins_staff|student|parent"},
     *     name="api_user_init_email", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="User",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param string $role
     * @param \Swift_Mailer $mailer
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function sendActivationEmailAction(Request $request, int $insId, string $role, \Swift_Mailer $mailer, TranslatorInterface $translator) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        /**
         * @var UserInstitute[] $users
         */
        $users = $this->getDoctrine()->getRepository('App:UserInstitute')->getInsInactivatedUsers($insId, $this->roles[$role]);
        $i = 0;
        foreach ($users as $user) {

            if ($user->getUser()->getEmail() == 'aymenbenothmenabo@outlook.com' || $user->getUser()->getEmail() == 'abowebmaster@gmail.com'
                || $user->getUser()->getEmail() == 'aymen.benothmen@esprit.tn') {

                $lang = $request->query->get( 'language' );
                $lang = ($lang === 'en' || $lang === 'fr') ? $lang : 'en';
                $message = (new \Swift_Message($translator->trans('confirmation.subject', [], null, $lang)))
                    ->setFrom(array('aymenbenothmenabo@gmail.com' => $translator->trans('mail.zed_support', [], null, $lang)))
                    ->setTo($user->getUser()->getEmail())
                    ->setBody($this->renderView(
                        'emails/security/activate_account.html.twig',
                        array(
                            'link_var' => 'https://www.zednilma.com/init-account/' . $user->getUser()->getConfirmationToken(),
                            'lang' => $lang
                        )
                    ),'text/html');
                $mailer->send($message);

                $i++;
            }
        }

//        return new JsonResponse(NULL, 204);
        return new JsonResponse(['emails'=>$i, 'users'=>sizeof($users)], 200);
    }

    private function processForm(Request $request, User $user) {

        $form = $this->createForm(UserEditType::class, $user);

        $data = json_decode($request->getContent(), true);
        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $view = $this->view(NULL, 204);

            return $this->handleView($view);
        }

        $view = $this->view($form, 400);

        return $this->handleView($view);
    }
}

<?php

namespace App\Controller\API\V1;

use App\AppEvents;
use App\Entity\Notification;
use App\Entity\UserDepartment;
use App\Entity\UserGroup;
use App\Event\UserPushNotificationEvent;
use App\Services\ABONotificationDescriber;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserDepartmentController extends FOSRestController {

    private $accessChecker;
    private $describer;

    /**
     * UserDepartmentController constructor.
     * @param ZdlmAccessChecker $accessChecker
     * @param ABONotificationDescriber $describer
     */
    public function __construct(ZdlmAccessChecker $accessChecker, ABONotificationDescriber $describer) {

        $this->accessChecker = $accessChecker;
        $this->describer = $describer;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/departments/{depId}/members/{memId}", requirements={"memId": "\d+", "insId": "\d+", "depId": "\d+"}, name="api_department_user_read",
     *     options={ "method_prefix"=false })
     * @ ApiDoc(
     *     section="UserDepartment",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $depId
     * @param int $memId
     * @return Response
     */
    public function getAction(int $insId, int $depId, int $memId) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $department = $this->getDoctrine()->getRepository('App:Department')->find($depId);
        if (!$department) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.department',
                'message' => 'Department was not found',
            ], 404);
        }

        $userDepartment = $this->getDoctrine()->getRepository('App:UserDepartment')->find($memId);
        if (!$userDepartment) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user_department',
                'message' => 'User Department was not found',
            ], 404);
        }

        $view = $this->view($userDepartment, 200);
        return $this->handleView($view);
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/members/{memId}/department", requirements={"insId": "\d+", "memId": "\d+"}, name="api_user_department_read", options={ "method_prefix"=false })
     * @ ApiDoc(
     *     section="UserDepartment",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $memId
     * @return Response
     */
    public function getUserDepAction(int $insId, int $memId) {

        $institute = $this->getDoctrine()->getRepository('App:Institute')->find($insId);
        if (!$institute) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.institute',
                'message' => 'Institute was not found',
            ], 404);
        }

        $user = $this->getDoctrine()->getRepository('App:User')->find($memId);
        if (!$user) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user',
                'message' => 'User was not found',
            ], 404);
        }
        $userDepartment = $this->getDoctrine()->getRepository('App:UserDepartment')->findOneBy(['user'=>$user, 'dateEnded'=> NULL]);
        if (!$userDepartment) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user_department',
                'message' => 'User Department was not found',
            ], 404);
        }

        $view = $this->view($userDepartment, 200);
        return $this->handleView($view);
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/departments/{depId}/members", name="api_user_department_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="UserDepartment",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     *
     * @param int $insId
     * @param int $depId
     * @return Response
     */
    public function newAction(Request $request, int $insId, int $depId) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }
        $department = $this->getDoctrine()->getRepository('App:Department')->find($depId);
        if (!$department)
            return new JsonResponse(['status_code' => 404, 'error_code' => 'not_found.department', 'message' => 'Department was not found',], 404);

        $data = json_decode($request->getContent(), true);

        $user = $this->getDoctrine()->getRepository('App:User')->find($data['user']);
        if (!$user)
            return new JsonResponse(['status_code' => 404, 'error_code' => 'not_found.user', 'message' => 'User was not found',], 404);

        $post = $this->getDoctrine()->getRepository('App:Post')->find($data['post']);
        if (!$post)
            return new JsonResponse(['status_code' => 404, 'error_code' => 'not_found.post', 'message' => 'Post was not found',], 404);

        $userDepartment = $this->getDoctrine()->getRepository('App:UserDepartment')->checkIfUserHasPostInDep($user->getId(), $department->getId());
        if ($userDepartment)
            return new JsonResponse(['status_code' => 404, 'error_code' => 'user_department.exist', 'message' => 'This user already has a post in the department',], 400);

        $userDepartment = $this->getDoctrine()->getRepository('App:UserDepartment')->checkIfPostAvailableInDep($department->getId(), $post->getId());
        if ($userDepartment)
            return new JsonResponse(['status_code' => 404, 'error_code' => 'user_department.post_unavailable', 'message' => 'This post is already taken for this department',], 400);


        $userDepartment = new UserDepartment($user, $department, $post);
        $this->getDoctrine()->getManager()->persist($userDepartment);

        $userGrpDep = new UserGroup($user, $department->getGroupe());
        $this->getDoctrine()->getManager()->persist($userGrpDep);

        $userGrpPost = new UserGroup($user, $post->getGroupe());
        $this->getDoctrine()->getManager()->persist($userGrpPost);

        $this->getDoctrine()->getManager()->flush();

        $view = $this->view(NULL, 201);

        $this->pushNotification($userDepartment);

        $response = $this->handleView($view);
        $response->headers->set('Location', $this->generateUrl('api_user_department_read', [
            'version' => 'v1',
            'insId' => $user->getId(),
            'memId' => $userDepartment->getUser()->getId(),
            ]));
        return $response;
    }

    /**
     * @Rest\Route(path="/institutes/{insId}/departments/{depId}/members/{id}", requirements={"id": "\d+"}, methods={"PUT", "PATCH"}, name="api_user_department_update",
     *     options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="UserDepartment",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $depId
     * @param int $id
     * @return Response
     */
    public function editAction(Request $request, int $insId, int $depId, int $id) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }
        $department = $this->getDoctrine()->getRepository('App:Department')->find($depId);
        if (!$department) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.department',
                'message' => 'Department was not found',
            ], 404);
        }

        /**
         * @var UserDepartment $userDepartment
         */
        $userDepartment = $this->getDoctrine()->getRepository('App:UserDepartment')->find($id);
        if (!$userDepartment) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user_department',
                'message' => 'User Department was not found',
            ], 404);
        }

        $data = json_decode($request->getContent(), true);
        $post = $this->getDoctrine()->getRepository('App:Post')->find($data['post']);
        if (!$post)
            return new JsonResponse(['status_code' => 404, 'error_code' => 'not_found.post', 'message' => 'Post was not found',], 404);

        $userDep = $this->getDoctrine()->getRepository('App:UserDepartment')->checkIfPostAvailableInDep($department->getId(), $post->getId());
        if ($userDep)
            return new JsonResponse(['status_code' => 400, 'error_code' => 'user_department.post_unavailable', 'message' => 'This post is already taken for this department',], 400);

        $userDepartment->setPost($post);
        $this->pushNotification($userDepartment);
        $this->getDoctrine()->getManager()->flush();

        return $this->handleView($this->view(NULL, 204));
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/departments/{depId}/members/{id}", requirements={"id": "\d+"}, name="api_user_department_delete", options={ "method_prefix"=false })
     * @ ApiDoc(
     *     section="UserDepartment",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $depId
     * @param int $id
     * @return Response
     */
    public function deleteAction(int $insId, int $depId, int $id) {

        $result = $this->accessChecker->checkForInstitute($insId);
        if ($result->getResponse()) {
            return $result->getResponse();
        }
        $department = $this->getDoctrine()->getRepository('App:Department')->find($depId);
        if (!$department) {

            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.department',
                'message' => 'Department was not found',
            ], 404);
        }

        /**
         * @var $userDepartment UserDepartment
         */
        $userDepartment = $this->getDoctrine()->getRepository('App:UserDepartment')->findOneBy(['id'=>$id, 'department'=>$department]);
        if (!$userDepartment) {
            return new JsonResponse([
                'status_code' => 404,
                'error_code' => 'not_found.user_department',
                'message' => 'User Department was not found',
            ], 404);
        }

        $now = new \DateTime();
        $interval = (int) date_diff($now, $userDepartment->getDateStarted())->format('%m');

        $userDepGrp = $this->getDoctrine()->getRepository('App:UserGroup')->findOneBy(['user'=>$userDepartment->getUser(),
            'group'=>$userDepartment->getDepartment()->getGroupe()]);
        $this->getDoctrine()->getManager()->remove($userDepGrp);

        $userPostGrp = $this->getDoctrine()->getRepository('App:UserGroup')->findOneBy(['user'=>$userDepartment->getUser(),
            'group'=>$userDepartment->getPost()->getGroupe()]);
        $this->getDoctrine()->getManager()->remove($userPostGrp);

        if ( $interval < 1 ) {

            $this->getDoctrine()->getManager()->remove($userDepartment);
            $decision = 'delete';
        } else {

            $userDepartment->setDateEnded($now);
            $this->pushNotification($userDepartment);
            $decision = 'keep';
        }
        $this->getDoctrine()->getManager()->flush();

//        return $this->handleView($this->view(['interval' => $interval], 204));
        return new JsonResponse([
            'status_code' => 400,
            'interval' => $interval,
            'decision' => $decision,
        ], 200);
    }

    private function pushNotification(UserDepartment $userDepartment) {

        $dispatcher = $this->get('event_dispatcher');
        $notification = new Notification();
        $notification->setType(26);
        $notification->setDescription($this->describer->describeUserDepartment($userDepartment));
        $notification->setNotifier($this->getUser());
        $event = new UserPushNotificationEvent($notification, $userDepartment->getUser());
        $dispatcher->dispatch(AppEvents::USER_PUSH_NOTIFICATION, $event);
    }
}

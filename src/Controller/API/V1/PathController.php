<?php

namespace App\Controller\API\V1;

use App\Entity\Path;
use App\Entity\PathBranch;
use App\Form\Type\PathType;
use App\Services\ZdlmAccessChecker;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PathController extends FOSRestController {


    private $accessChecker;
    public function __construct(ZdlmAccessChecker $accessChecker) {

        $this->accessChecker = $accessChecker;
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/specialities/{specId}/paths/{id}",
     *     requirements={
     *          "insId": "\d+", "specId": "\d+", "id": "\d+"
     *     }, name="api_path_read", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Path",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $specId
     * @param int $id
     * @return Response
     */
    public function getAction( int $insId, int $specId, int $id ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $speciality = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findOneBy( ['id' => $specId, 'institute'=>$institute] );
        if ( !$speciality ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.speciality',
                'message'     => 'Speciality was not found',
            ], 404 );
        }

        $path = $this->getDoctrine()->getRepository( 'App:Path' )
            ->findOneBy( ['id' => $id, 'speciality'=>$speciality] );
        if ( !$path ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.speciality_path',
                'message'     => 'Speciality path was not found',
            ], 404 );
        }

        return $this->handleView( $this->view( $path, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/specialities/{specId}/paths", requirements={
     *     "insId": "\d+", "specId": "\d+"}, name="api_speciality_path_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Path",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $specId
     * @return Response
     */
    public function cgetSpecialityPathsAction( int $insId, int $specId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $speciality = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findOneBy( ['id' => $specId, 'institute'=>$institute] );
        if ( !$speciality ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.speciality',
                'message'     => 'Speciality was not found',
            ], 404 );
        }

        $paths = $this->getDoctrine()->getRepository( 'App:PathBranch' )
            ->getSpecialityPaths( $specId );

        $pathsArray = [];
        foreach ( $paths as $pathBranch ) {
            /**
             * @var PathBranch $pathBranch
             */
            if ( empty( $pathsArray[ $pathBranch->getPath()->getId() ] ) ) {
                $pathsArray[ $pathBranch->getPath()->getId() ] = [
                    'id' => $pathBranch->getPath()->getId(),
                    'date' => $pathBranch->getPath()->getDateStr(),
                    'children' => [ $pathBranch->getIndex() => $pathBranch->getBranch() ],
                ];
            } else {
                $pathsArray[ $pathBranch->getPath()->getId() ]['children'][$pathBranch->getIndex()] = $pathBranch->getBranch();
            }
        }

        return $this->handleView( $this->view( $pathsArray, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insId}/paths", requirements={
     *     "insId": "\d+", "specId": "\d+"}, name="api_ins_path_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Path",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @return Response
     */
    public function cgetInstitutePathsAction( int $insId ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->find( $insId );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $paths = $this->getDoctrine()->getRepository( 'App:PathBranch' )
            ->getInstitutePaths( $insId );

        return $this->handleView( $this->view( $paths, 200 ) );
    }

    /**
     * @Rest\Get(path="/institutes/{insSlug}/paths", name="api_ins_slug_path_read_all", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Path",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "users"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param string $insSlug
     * @return Response
     */
    public function cgetInstitutePathsBySlugAction( string $insSlug ) {

        $institute = $this->getDoctrine()->getRepository( 'App:Institute' )->findOneBy( ['slug' => $insSlug] );
        if ( !$institute ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute',
                'message'     => 'Institute was not found',
            ], 404 );
        }

        $paths = $this->getDoctrine()->getRepository( 'App:PathBranch' )
            ->getInstitutePaths( $institute->getId() );

        return $this->handleView( $this->view( $paths, 200 ) );
    }

    /**
     * @Rest\Post(path="/institutes/{insId}/specialities/{specId}/paths", requirements={"insId": "\d+", "specId": "\d+"},
     *     name="api_path_create", options={ "method_prefix" = false })
     * @ ApiDoc(
     *     section="Path",
     *     description="",
     *     statusCodes={200="Returned when successful"},
     *     views={"default", "v1", "articles"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param Request $request
     * @param int $insId
     * @param int $specId
     * @return Response
     */
    public function newAction( Request $request, int $insId, int $specId ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $speciality = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findOneBy( ['id' => $specId, 'institute'=>$result->getInstitute()] );
        if ( !$speciality ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.speciality',
                'message'     => 'Speciality was not found',
            ], 404 );
        }

        $data = json_decode( $request->getContent(), true );
        $path = new Path($speciality);
        $em = $this->getDoctrine()->getManager();
        $em->persist($path);
        foreach ($data as $index => $branchId) {
            $branch = $em->getRepository('App:Speciality')->findOneBy(['id'=>$branchId, 'institute'=>$result->getInstitute()]);
            if ( !$branch ){
                return new JsonResponse( [
                    'status_code' => 404,
                    'error_code'  => 'not_found.speciality',
                    'message'     => 'Branch was not found',
                ], 404 );
            }
            $pathBranch = new PathBranch($path, $branch, $index);
            $em->persist($pathBranch);
        }
        $em->flush();
        $response = $this->handleView( $this->view( NULL, 201 ) );
        $response->headers->set( 'Location', $this->generateUrl( 'api_path_read', [
            'version'  => 'v1',
            'insId'    => $insId,
            'specId'    => $specId,
            'id'       => $path->getId(),
        ] ) );

        return $response;
    }

    /**
     * @Rest\Delete(path="/institutes/{insId}/specialities/{specId}/paths/{id}", requirements={"insId": "\d+", "specId": "\d+","id": "\d+"},
     *     name="api_path_delete", options={ "method_prefix" = false })
     *
     * @ ApiDoc(
     *     section="Path",
     *     description="",
     *     statusCodes={201="Returned when user creation was successful"},
     *     views={"default", "v1", "Paths"},
     *     tags={"v1" = "#4A7023"},
     * )
     * @param int $insId
     * @param int $specId
     * @param int $id
     * @return Response
     */
    public function deleteAction( int $insId, int $specId, int $id ) {

        $result = $this->accessChecker->checkForInstitute( $insId );
        if ( $result->getResponse() ) {
            return $result->getResponse();
        }

        $speciality = $this->getDoctrine()->getRepository( 'App:Speciality' )
            ->findOneBy( ['id' => $specId, 'institute'=>$result->getInstitute()] );
        if ( !$speciality ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.speciality',
                'message'     => 'Speciality was not found',
            ], 404 );
        }

        $path = $this->getDoctrine()->getRepository( 'App:Path' )
            ->findOneBy( ['id' => $id, 'speciality'=>$speciality] );
        if ( !$path ) {

            return new JsonResponse( [
                'status_code' => 404,
                'error_code'  => 'not_found.institute_path',
                'message'     => 'Institute path was not found',
            ], 404 );
        }

        $pathBranches = $this->getDoctrine()->getRepository('App:PathBranch')->findBy(
            [ 'path' => $path ]
        );

        $em = $this->getDoctrine()->getManager();
        foreach ( $pathBranches as $pathBranch ) {
            $em->remove( $pathBranch );
        }
        $em->remove( $path );
        $em->flush();

        return $this->handleView( $this->view( NULL, 204 ) );
    }
}

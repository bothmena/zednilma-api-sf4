<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 08/02/17
 * Time: 19:56
 */

namespace App;

final class AppEvents {

    const REGISTRATION_SUCCESS = 'zdlm.regis_succ';
    const USER_CONFIRM_EMAIL_SUCCESS = 'zdlm.usr_cnfrm_mail';
    const USER_INIT_ACCOUNT_SUCCESS = 'zdlm.usr_init_account';

    const INSTITUTE_IMAGE_UPLOAD_SUCCESS = 'zdlm.ins_img_upload_success';
    const USER_IMAGE_UPLOAD_SUCCESS = 'zdlm.user_img_upload_success';

    const USER_PUSH_NOTIFICATION = 'zdlm.user_push_notification';
}

<?php

namespace App\Repository;

use App\Entity\TaskList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TaskList|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskList|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskList[]    findAll()
 * @method TaskList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskListRepository extends ServiceEntityRepository {

    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, TaskList::class);
    }

    /**
     * @return TaskList[] Returns an array of TaskList objects
     */
    public function getUserTaskLists($userId) {

        $qb = $this->createQueryBuilder('tl');

        $qb->join('tl.user', 'u')
//            ->where('u.id = :userId')
            ->where($qb->expr()->orX(
                $qb->expr()->eq('u.id', ':userId'),
                $qb->expr()->isNull('tl.user')
            ))
            ->setParameter('userId', $userId)
            ->orderBy('tl.date', 'ASC');

        return
        $qb->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?TaskList
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

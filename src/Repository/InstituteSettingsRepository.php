<?php

namespace App\Repository;

use App\Entity\InstituteSettings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InstituteSettings|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstituteSettings|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstituteSettings[]    findAll()
 * @method InstituteSettings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstituteSettingsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InstituteSettings::class);
    }

//    /**
//     * @return InstituteSettings[] Returns an array of InstituteSettings objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstituteSettings
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository;

use App\Entity\SessionContainer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SessionContainer|null find($id, $lockMode = null, $lockVersion = null)
 * @method SessionContainer|null findOneBy(array $criteria, array $orderBy = null)
 * @method SessionContainer[]    findAll()
 * @method SessionContainer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SessionContainerRepository extends ServiceEntityRepository {


    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, SessionContainer::class);
    }

    /**
     * @param int $insId
     * @return SessionContainer[] Returns an array of SessionContainer objects
     */
    public function getInstituteTimetables(int $insId) {

        return $this->createQueryBuilder('sc')
            ->join('sc.timetable', 't')
            ->join('t.institute', 'i')
            ->where('i.id = :insId')
            ->setParameter('insId', $insId)
            ->orderBy('t.id', 'ASC')
            ->addOrderBy('sc.id', 'ASC')
            ->getQuery()
            ->getResult();
    }
}

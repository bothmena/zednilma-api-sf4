<?php

namespace App\Repository;

use App\Entity\SubjectClasse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SubjectClasse|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubjectClasse|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubjectClasse[]    findAll()
 * @method SubjectClasse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubjectClasseRepository extends ServiceEntityRepository {
    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, SubjectClasse::class);
    }

    /**
     * @param int $subId
     * @param int $clsId
     * @param int $profId
     * @return SubjectClasse[] Returns an array of SubjectClasse objects
     */
    public function findOneBySubClsProf(int $subId, int $clsId, int $profId) {

        $qb = $this->createQueryBuilder('sc')
            ->andWhere('sc.subject = :subId')
            ->andWhere('sc.classe = :clsId')
            ->andWhere('sc.professor = :profId')
            ->setParameters(['subId' => $subId, 'clsId' => $clsId, 'profId' => $profId])
            ->setMaxResults(1);

        try {

            return $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {

            return null;
        }
    }


    /*
    public function findOneBySomeField($value): ?SubjectClasse
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

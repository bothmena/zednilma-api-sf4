<?php

namespace App\Repository;

use App\Entity\TaskUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TaskUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskUser[]    findAll()
 * @method TaskUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskUserRepository extends ServiceEntityRepository {
    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, TaskUser::class);
    }

    /**
     * @param int $userId
     * @return TaskUser[] Returns an array of TaskUser objects
     */

    public function getUserTasks(int $userId) {

        $now = new \DateTime();
        $qb = $this->createQueryBuilder('tu');

        return $qb->join('tu.task', 't')
            ->join('tu.user', 'u')
            ->where('u.id = :userId')
            ->andWhere('tu.dueDate >= :now')
            ->setParameters([':userId' => $userId, ':now' => $now])
            ->orderBy('tu.dueDate', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?TaskUser
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

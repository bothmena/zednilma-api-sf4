<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 08/02/17
 * Time: 19:47
 */

namespace App\Event;

use App\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserRegistrationSuccessEvent extends Event {

    private $user;

    public function __construct( User $user ){

        $this->user = $user;
    }

    public function getUser(){

        return $this->user;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 19/03/17
 * Time: 15:46
 */

namespace App\Event;

use App\Entity\Group;
use App\Entity\Notification;
use App\Entity\User;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;

class UserPushNotificationEvent extends Event {

    private $notified;
    private $notifiedGroup;
    private $notification;
    private $showAfter;
    private $response;

    public function __construct(Notification $notification, User $notified = NULL, Group $notifiedGroup = NULL, \DateTime $showAfter = NULL) {

        $this->notification = $notification;
        $this->notified = $notified;
        $this->notifiedGroup = $notifiedGroup;
        if ($showAfter)
            $this->showAfter = $showAfter;
        else
            $this->showAfter = new \DateTime();
    }

    /**
     * @return User
     */
    public function getNotified() {
        return $this->notified;
    }

    /**
     * @param User $notified
     */
    public function setNotified(User $notified) {
        $this->notified = $notified;
    }

    /**
     * @return Group
     */
    public function getNotifiedGroup() {
        return $this->notifiedGroup;
    }

    /**
     * @param Group $notifiedGroup
     */
    public function setNotifiedGroup(Group $notifiedGroup) {
        $this->notifiedGroup = $notifiedGroup;
    }

    /**
     * @return Notification
     */
    public function getNotification() {
        return $this->notification;
    }

    /**
     * @param Notification $notification
     */
    public function setNotification(Notification $notification) {
        $this->notification = $notification;
    }

    /**
     * @return mixed
     */
    public function getResponse() {

        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse(Response $response) {

        $this->response = $response;
    }

    /**
     * @return \DateTime
     */
    public function getShowAfter() {
        return $this->showAfter;
    }

    /**
     * @param \DateTime $showAfter
     */
    public function setShowAfter(\DateTime $showAfter) {
        $this->showAfter = $showAfter;
    }
}

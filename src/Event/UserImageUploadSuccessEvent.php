<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 19/03/17
 * Time: 15:46
 */

namespace App\Event;

use App\Entity\Image;
use App\Entity\Mosque;
use App\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserImageUploadSuccessEvent extends Event {

    private $image;
    private $user;
    private $response;

    public function __construct( User $user, Image $image ) {

        $this->image = $image;
        $this->user = $user;
    }

    /**
     * @return Image
     */
    public function getImage(): Image {

        return $this->image;
    }

    /**
     * @return User
     */
    public function getUser(): User {

        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getResponse() {

        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse( $response ) {

        $this->response = $response;
    }
}

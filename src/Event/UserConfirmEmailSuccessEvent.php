<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 08/02/17
 * Time: 19:47
 */

namespace App\Event;

use App\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;

class UserConfirmEmailSuccessEvent extends Event {

    private $user;
    private $response;

    public function __construct( UserInterface $user ){

        $this->user = $user;
    }

    /**
     * @return Response
     */
    public function getResponse () {

        return $this->response;
    }

    /**
     * @param Response $response
     */
    public function setResponse ( Response $response ) {

        $this->response = $response;
    }

    /**
     * @return UserInterface
     */
    public function getUser(){

        return $this->user;
    }
}

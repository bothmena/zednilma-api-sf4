<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 19/03/17
 * Time: 15:46
 */

namespace App\Event;

use App\Entity\Image;
use App\Entity\Institute;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;

class InstituteImageUploadSuccessEvent extends Event {

    private $image;
    private $institute;
    private $response;

    public function __construct( Institute $institute, Image $image ) {

        $this->image = $image;
        $this->institute = $institute;
    }

    /**
     * @return Image
     */
    public function getImage(): Image {

        return $this->image;
    }

    /**
     * @return Institute
     */
    public function getInstitute(): Institute {

        return $this->institute;
    }

    /**
     * @param mixed $response
     */
    public function setResponse( Response $response ) {

        $this->response = $response;
    }

    /**
     * @return mixed
     */
    public function getResponse() {

        return $this->response;
    }
}

<?php /** @noinspection PhpParamsInspection */

namespace App\DataFixtures\ORM;

use App\Entity\Institute;
use App\Entity\Location;
use App\Entity\User;
use App\Entity\UserInstitute;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class UserFixtures extends Fixture implements DependentFixtureInterface {

    public const ADMIN_USER = 'admin-user';
    public const INS_ADMIN_USER = 'ins-admin-user';
    public const PROFESSOR_USER = 'professor-user';
    public const STUDENT_USER = 'student-user';

    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);
        /**
         * @var Institute $institute
         */
        $institute = $this->getReference(InstituteFixtures::DEMO_INSTITUTE_REFERENCE);

        $admin = new User();
        $admin->setProfileImage($this->getReference(ImageFixtures::ADMIN_IMAGE));
        $admin->setUsername('admin');
        $admin->setEmail('admin@zednilma.com');
        $admin->setFirstName($faker->firstName);
        $admin->setLastName($faker->lastName);
        $admin->setBirthday($faker->dateTime('-25 years'));
        $admin->setCin($faker->randomNumber(8));
        $admin->setGender('male');
        $admin->setLocation($this->getLocation($faker));
        $admin->setEnabled(true);
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setPlainPassword('zedPASS18');
        $manager->persist($admin);
        $this->setReference(self::ADMIN_USER, $admin);

        $insAdmin = new User();
        $insAdmin->setProfileImage($this->getReference(ImageFixtures::INSTITUTE_M_IMAGE));
        $insAdmin->setUsername('insadmin');
        $insAdmin->setEmail('insadmin@zednilma.com');
        $insAdmin->setFirstName($faker->firstName);
        $insAdmin->setLastName($faker->lastName);
        $insAdmin->setBirthday($faker->dateTime('-25 years'));
        $insAdmin->setCin($faker->randomNumber(8));
        $insAdmin->setGender('male');
        $insAdmin->setLocation($this->getLocation($faker));
        $insAdmin->setEnabled(true);
        $insAdmin->setRoles(['ROLE_INS_ADMIN']);
        $insAdmin->setPlainPassword('zedPASS18');
        $userInstitute = new UserInstitute($insAdmin, $institute, 'ROLE_INS_ADMIN');
        $manager->persist($insAdmin);
        $manager->persist($userInstitute);
        $this->setReference(self::INS_ADMIN_USER, $insAdmin);

        for ( $i = 1; $i < 61; $i++) {

            $enabled = $faker->boolean;
            $gender = $faker->randomElement(['male', 'female']);

            $student = new User();
            if ($gender === 'male') {
                $student->setProfileImage($this->getReference(ImageFixtures::STUDENT_M_IMAGE));
                $student->setFirstName($faker->firstNameMale);
            } else{
                $student->setProfileImage($this->getReference(ImageFixtures::STUDENT_F_IMAGE));
                $student->setFirstName($faker->firstNameFemale);
            }
            $student->setUsername('student' . $i);
            $student->setEmail('student' . $i . '@zednilma.com');
            $student->setLastName($faker->lastName);
            $student->setBirthday($faker->dateTime('-6 years'));
            $student->setCin($faker->randomNumber(8));
            $student->setGender($gender);
            $student->setLocation($this->getLocation($faker));

            if ($i === 1) {
                $this->setReference(self::STUDENT_USER, $student);
                $enabled = true;
            }
            $student->setEnabled($enabled);
            if ( !$enabled ) {
                $student->setConfirmationToken($faker->uuid);
            }
            $student->setRoles(['ROLE_STUDENT']);
            $student->setPlainPassword('zedPASS18');
            $studentInstitute = new UserInstitute($student, $institute, 'ROLE_STUDENT');
            $manager->persist($student);
            $manager->persist($studentInstitute);
        }

        for ( $i = 1; $i < 11; $i++) {

            $enabled = $faker->boolean;
            $gender = $faker->randomElement(['male', 'female']);

            $professor = new User();
            if ($gender === 'male') {
                $professor->setProfileImage($this->getReference(ImageFixtures::PROFESSOR_M_IMAGE));
                $professor->setFirstName($faker->firstNameMale);
            } else{
                $professor->setProfileImage($this->getReference(ImageFixtures::PROFESSOR_F_IMAGE));
                $professor->setFirstName($faker->firstNameFemale);
            }
            $professor->setUsername('professor' . $i);
            $professor->setEmail('professor' . $i . '@zednilma.com');
            $professor->setLastName($faker->lastName);
            $professor->setBirthday($faker->dateTime('-25 years'));
            $professor->setCin($faker->randomNumber(8));
            $professor->setGender($gender);
            $professor->setLocation($this->getLocation($faker));
            if ($i === 1) {
                $this->setReference(self::PROFESSOR_USER, $professor);
                $enabled = true;
            }
            $professor->setEnabled($enabled);
            if ( !$enabled ) {
                $professor->setConfirmationToken($faker->uuid);
            }
            $professor->setRoles(['ROLE_PROFESSOR']);
            $professor->setPlainPassword('zedPASS18');
            $professorInstitute = new UserInstitute($professor, $institute, 'ROLE_PROFESSOR');
            $manager->persist($professor);
            $manager->persist($professorInstitute);
        }

        for ( $i = 1; $i < 11; $i++) {

            $enabled = $faker->boolean;
            $gender = $faker->randomElement(['male', 'female']);

            $parent = new User();
            if ($gender === 'male') {
                $parent->setFirstName($faker->firstNameMale);
            } else{
                $parent->setFirstName($faker->firstNameFemale);
            }
            $parent->setProfileImage($this->getReference(ImageFixtures::PARENTS_IMAGE));
            $parent->setUsername('parent' . $i);
            $parent->setEmail('parent' . $i . '@zednilma.com');
            $parent->setLastName($faker->lastName);
            $parent->setBirthday($faker->dateTime('-35 years'));
            $parent->setCin($faker->randomNumber(8));
            $parent->setGender($gender);
            $parent->setLocation($this->getLocation($faker));
            $parent->setEnabled($enabled);
            if ( !$enabled ) {
                $parent->setConfirmationToken($faker->uuid);
            }
            $parent->setRoles(['ROLE_PARENT']);
            $parent->setPlainPassword('zedPASS18');
            $parentInstitute = new UserInstitute($parent, $institute, 'ROLE_PARENT');
            $manager->persist($parent);
            $manager->persist($parentInstitute);
        }

        for ( $i = 1; $i < 11; $i++) {

            $enabled = $faker->boolean;
            $gender = $faker->randomElement(['male', 'female']);

            $insstuff = new User();
            if ($gender === 'male') {
                $insstuff->setFirstName($faker->firstNameMale);
                $insstuff->setProfileImage($this->getReference(ImageFixtures::INSTITUTE_M_IMAGE));
            } else{
                $insstuff->setFirstName($faker->firstNameFemale);
                $insstuff->setProfileImage($this->getReference(ImageFixtures::INSTITUTE_F_IMAGE));
            }
            $insstuff->setUsername('insstuff' . $i);
            $insstuff->setEmail('insstuff' . $i . '@zednilma.com');
            $insstuff->setLastName($faker->lastName);
            $insstuff->setBirthday($faker->dateTime('-25 years'));
            $insstuff->setCin($faker->randomNumber(8));
            $insstuff->setGender($gender);
            $insstuff->setLocation($this->getLocation($faker));
            $insstuff->setEnabled($enabled);
            if ( !$enabled ) {
                $insstuff->setConfirmationToken($faker->uuid);
            }
            $insstuff->setRoles(['ROLE_INS_STAFF']);
            $insstuff->setPlainPassword('zedPASS18');
            $insstuffInstitute = new UserInstitute($insstuff, $institute, 'ROLE_INS_STAFF');
            $manager->persist($insstuff);
            $manager->persist($insstuffInstitute);
        }

        $manager->flush();
    }

    public function getDependencies() {

        return array(
            LocationFixtures::class,
        );
    }

    private function getLocation($faker): Location {

        $location = new Location();
        $location->setCountry('TN');
        $location->setState($faker->state);
        $location->setCity($faker->city);
        $location->setAddress($faker->streetAddress);
        $location->setZipCode($faker->postcode);
        $location->setMapLat($faker->latitude);
        $location->setMapLong($faker->longitude);
        $location->setIsPrimary($faker->boolean);

        return $location;
    }
}

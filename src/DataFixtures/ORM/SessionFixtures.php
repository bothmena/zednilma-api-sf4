<?php

namespace App\DataFixtures\ORM;

use App\Entity\Classe;
use App\Entity\Infrastructure;
use App\Entity\Session;
use App\Entity\Subject;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class SessionFixtures extends Fixture implements DependentFixtureInterface {

//    public const DSA2_CLASSE = 'idsa2-classe';

    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);

        /**
         * @var User $professor
         * @var Classe $classe
         * @var Subject $subject
         * @var Infrastructure $classroom
         * @var User $professor
         */
        /*$professor = $this->getReference(UserFixtures::PROFESSOR_USER);
        $classe = $this->getReference(ClassesFixtures::DSA1_CLASSE);
        $classe2 = $this->getReference(ClassesFixtures::DSA2_CLASSE);
        $subject = $this->getReference(SubjectFixtures::DL_SUBJECT);
        $classroom = $this->getReference(InfrastructureFixtures::CLASSROOM_INFRASTRUCTURE);

        for ($i = 1; $i < 5; $i++) {

            $session = new Session();
            $session->setProfessor($professor);
            $session->setClasse($classe);
            $session->setSubject($subject);
            $session->setClassroom($classroom);
            $session->setSchoolYear('2018-2019');
            $session->setWeek(5);
            $startAt = new \DateTime();
            $startAt->setTime(6+2*$i, 0, 0);
            $startAt->setDate(2018, 8, 13);
            $session->setStartAt($startAt);
            $endAt = new \DateTime();
            $endAt->setTime(8+2*$i, 0, 0);
            $endAt->setDate(2018, 8, 13);
            $session->setEndAt($endAt);

            $manager->persist($session);
        }

        for ($i = 1; $i < 5; $i++) {

            $session = new Session();
            $session->setProfessor($professor);
            $session->setClasse($classe2);
            $session->setSubject($subject);
            $session->setClassroom($classroom);
            $session->setSchoolYear('2018-2019');
            $session->setWeek(5);
            $startAt = new \DateTime();
            $startAt->setTime(6+2*$i, 0, 0);
            $startAt->setDate(2018, 8, 14);
            $session->setStartAt($startAt);
            $endAt = new \DateTime();
            $endAt->setTime(8+2*$i, 0, 0);
            $endAt->setDate(2018, 8, 14);
            $session->setEndAt($endAt);

            $manager->persist($session);
        }

        $manager->flush();*/
    }

    public function getDependencies() {

        return array(
            UserFixtures::class,
            ClassesFixtures::class,
            SubjectFixtures::class,
            InfrastructureFixtures::class,
        );
    }
}

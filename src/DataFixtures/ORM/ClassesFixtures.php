<?php

namespace App\DataFixtures\ORM;

use App\Entity\Classe;
use App\Entity\Group;
use App\Entity\Speciality;
use App\Entity\UserClasse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class ClassesFixtures extends Fixture implements DependentFixtureInterface {

    public const DSA1_CLASSE = 'dsa1-classe';
    public const DSA2_CLASSE = 'dsa2-classe';
    public const DSA3_CLASSE = 'dsa3-classe';

    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);
        /**
         * @var Speciality $branch
         */

        $branch = $this->getReference(SpecialityFixtures::DSA_1_SPECIALITY);
        for ($i = 1; $i < 4; $i++) {

            $classe = new Classe();
            $classe->setBranch($branch);
            $classe->setName($branch->getAlias() . '-' . $i);
            $classe->setLevel($i);
            $classe->setSchoolYear('2018-2019');

            for ($j = ($i-1) * 20 + 1; $j < $i * 20 + 1; $j++) {

                $student = $manager->getRepository('App:User')->findOneBy(['username'=> 'student' . $j]);
                if ($student) {
                    $classUser = new UserClasse($student, $classe);
                    $manager->persist($classUser);
                }
            }
            $manager->persist($classe);
            if ($i == 1)
                $this->setReference(self::DSA1_CLASSE, $classe);
            if ($i == 2)
                $this->setReference(self::DSA2_CLASSE, $classe);
            else
                $this->setReference(self::DSA3_CLASSE, $classe);
        }


        $manager->flush();
    }

    public function getDependencies() {

        return array(
            SpecialityFixtures::class,
        );
    }
}

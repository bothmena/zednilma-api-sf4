<?php

namespace App\DataFixtures\ORM;

use App\Entity\Location;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class LocationFixtures extends Fixture implements DependentFixtureInterface {


    public const INSTITUTE_LOCATION = 'institute-location';

    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);

        $insLocation = new Location();
        $insLocation->setInstitute($this->getReference(InstituteFixtures::DEMO_INSTITUTE_REFERENCE));
        $insLocation->setCountry('TN');
        $insLocation->setState($faker->state);
        $insLocation->setCity($faker->city);
        $insLocation->setAddress($faker->streetAddress);
        $insLocation->setZipCode($faker->postcode);
        $insLocation->setMapLat($faker->latitude);
        $insLocation->setMapLong($faker->longitude);
        $insLocation->setIsPrimary(true);
        $manager->persist($insLocation);
        $this->setReference(self::INSTITUTE_LOCATION, $insLocation);

        $manager->flush();
    }

    public function getDependencies() {

        return array(
            InstituteFixtures::class,
        );
    }
}

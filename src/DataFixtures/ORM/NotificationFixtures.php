<?php

namespace App\DataFixtures\ORM;

use App\Entity\Notification;
use App\Entity\NotificationUser;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class NotificationFixtures extends Fixture implements DependentFixtureInterface {


    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);
        /**
         * @var User $professor
         * @var User $student
         */
        $professor = $this->getReference(UserFixtures::PROFESSOR_USER);
        $student = $this->getReference(UserFixtures::STUDENT_USER);

        for ($i = 0; $i < 20; $i++) {

            $notification = new Notification();
            $notification->setNotifier($professor);
            $notification->setType($faker->randomDigit);
            $notification->setTitle($faker->realText(100));
            $notification->setDescription($faker->realText(600));

            $notifUser = new NotificationUser($student, $notification);
            $manager->persist($notification);
            $manager->persist($notifUser);
        }

        $manager->flush();
    }

    public function getDependencies() {

        return array(
            SubjectClassesFixtures::class,
        );
    }
}

<?php

namespace App\DataFixtures\ORM;

use App\Entity\Group;
use App\Entity\Institute;
use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class PostFixtures extends Fixture implements DependentFixtureInterface {


    public const SPECIALITY_CHIEF = 'chief-post';
    public const PUBLIC_SPECIALITY_MAIN = 'public-speciality-post';
    public const DEPARTMENT_MANAGER = 'manager-post';
    public const PUBLIC_DEPARTMENT_MAIN = 'public-department-post';
    public const DEPARTMENT_MANAGER_ASSISTANT = 'manager-assistant-post';

    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);
        /**
         * @var Institute $institute
         */
        $institute = $this->getReference(InstituteFixtures::DEMO_INSTITUTE_REFERENCE);

        $depManager = new Post();
        $depManager->setName('Department Maintainer');
        $depManager->setDescription($faker->realText(250));
        $depManager->setType('department');

        $group0 = new Group();
        $group0->setInstitute($institute);
        $group0->setType('post');
        $depManager->setGroupe($group0);

        $this->setReference(self::PUBLIC_DEPARTMENT_MAIN, $depManager);
        $manager->persist($depManager);

        $depManager = new Post();
        $depManager->setName('Speciality Maintainer');
        $depManager->setDescription($faker->realText(250));
        $depManager->setType('speciality');

        $group1 = new Group();
        $group1->setInstitute($institute);
        $group1->setType('post');
        $depManager->setGroupe($group1);

        $this->setReference(self::PUBLIC_SPECIALITY_MAIN, $depManager);
        $manager->persist($depManager);

        $depManager = new Post();
        $depManager->setName('Department Manager');
        $depManager->setDescription($faker->realText(250));
        $depManager->setInstitute($institute);
        $depManager->setType('department');

        $group2 = new Group();
        $group2->setInstitute($institute);
        $group2->setType('post');
        $depManager->setGroupe($group2);

        $this->setReference(self::DEPARTMENT_MANAGER, $depManager);
        $manager->persist($depManager);

        $depManagerAssist = new Post();
        $depManagerAssist->setName('Department Manager Assistant');
        $depManagerAssist->setDescription($faker->realText(250));
        $depManagerAssist->setInstitute($institute);
        $depManagerAssist->setType('department');

        $group3 = new Group();
        $group3->setInstitute($institute);
        $group3->setType('post');
        $depManagerAssist->setGroupe($group3);

        $this->setReference(self::DEPARTMENT_MANAGER, $depManager);
        $manager->persist($depManagerAssist);

        $depManagerAssist = new Post();
        $depManagerAssist->setName('Speciality Chief');
        $depManagerAssist->setDescription($faker->realText(250));
        $depManagerAssist->setInstitute($institute);
        $depManagerAssist->setType('speciality');

        $group4 = new Group();
        $group4->setInstitute($institute);
        $group4->setType('post');
        $depManagerAssist->setGroupe($group4);

        $this->setReference(self::SPECIALITY_CHIEF, $depManager);
        $manager->persist($depManagerAssist);

        $manager->flush();
    }

    public function getDependencies() {

        return array(
            InstituteProfileFixtures::class,
        );
    }
}

<?php

namespace App\DataFixtures\ORM;

use App\Entity\Group;
use App\Entity\Institute;
use App\Entity\Speciality;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class SpecialityFixtures extends Fixture implements DependentFixtureInterface {

    public const PREPA_SPECIALITY = 'prepa-speciality';
    public const PREPA_MP_SPECIALITY = 'prepa-mp-speciality';
    public const PREPA_TECHNO_SPECIALITY = 'prepa-techno-speciality';
    public const PREPA_PC_SPECIALITY = 'prepa-pc-speciality';

    public const ING_INFO_1_SPECIALITY = 'ing-info-1-pc-speciality';
    public const INFO_B_1_SPECIALITY = 'info-b-1-speciality';
    public const BIB_1_SPECIALITY = 'bib-1-speciality';
    public const DSB_1_SPECIALITY = 'dsb-1-speciality';
    public const NIDSB_1_SPECIALITY = 'nidsb-1-speciality';
    public const INFO_A_1_SPECIALITY = 'info-a-1-speciality';
    public const BIA_1_SPECIALITY = 'bia-1-speciality';
    public const DSA_1_SPECIALITY = 'dsa-1-speciality';
    public const NIDSA_1_SPECIALITY = 'nidsa-1-speciality';

    public const ING_INFO_2_SPECIALITY = 'ing-info-2-pc-speciality';
    public const PREPA_2_SPECIALITY = 'prepa-2-speciality';
    public const INFO_A_2_SPECIALITY = 'info-a-2-speciality';
    public const BIA_2_SPECIALITY = 'bia-2-speciality';
    public const DSA_2_SPECIALITY = 'dsa-2-speciality';
    public const NIDSA_2_SPECIALITY = 'nidsa-2-speciality';

    public const MASTER_INFO_3_SPECIALITY = 'mastere-info-3-pc-speciality';
    public const PREPA_3_SPECIALITY = 'prepa-3-speciality';
    public const INFO_A_3_SPECIALITY = 'info-a-3-speciality';
    public const BI_3_SPECIALITY = 'bi-3-speciality';
    public const DS_3_SPECIALITY = 'ds-3-speciality';
    public const NIDS_3_SPECIALITY = 'nids-3-speciality';
    public const BIM_3_SPECIALITY = 'biM-3-speciality';
    public const DSM_3_SPECIALITY = 'dsM-3-speciality';
    public const NIDSM_3_SPECIALITY = 'nidsM-3-speciality';

    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);
        /**
         * @var Institute $institute
         */
        $institute = $this->getReference(InstituteFixtures::DEMO_INSTITUTE_REFERENCE);

        // Depth = 1

        $cyclePrep = new Speciality();
        $cyclePrep->setName('Cycle Preparatoire');
        $cyclePrep->setDescription($faker->realText(150));
        $cyclePrep->setYears(2);
        $cyclePrep->setStudyCycle('ENG_PREPARATORY_CYCLE');
        $cyclePrep->setType('SPECIALITY');
        $cyclePrep->setInstitute($institute);

        $manager->persist($cyclePrep);
        $this->setReference(self::PREPA_SPECIALITY, $cyclePrep);

        $mp = new Speciality();
        $mp->setName('Math-Physique');
        $mp->setAlias('MP');
        $mp->setDescription($faker->realText(150));
        $mp->setYears(2);
        $mp->setType('BRANCH');
        $mp->setInstitute($institute);
        $mp->setParent($cyclePrep);

        $manager->persist($mp);
        $this->setReference(self::PREPA_MP_SPECIALITY, $mp);

        $techno = new Speciality();
        $techno->setName('Technique');
        $techno->setAlias('T');
        $techno->setDescription($faker->realText(150));
        $techno->setYears(2);
        $techno->setType('BRANCH');
        $techno->setInstitute($institute);
        $techno->setParent($cyclePrep);

        $manager->persist($techno);
        $this->setReference(self::PREPA_TECHNO_SPECIALITY, $techno);

        $pc = new Speciality();
        $pc->setName('Physique-Chimie');
        $pc->setAlias('PC');
        $pc->setDescription($faker->realText(150));
        $pc->setYears(2);
        $pc->setType('BRANCH');
        $pc->setInstitute($institute);
        $pc->setParent($cyclePrep);

        $manager->persist($pc);
        $this->setReference(self::PREPA_PC_SPECIALITY, $pc);

        // Depth = 2

        $ingInfo = new Speciality();
        $ingInfo->setName('Ingenieur Informatique');
        $ingInfo->setDescription($faker->realText(150));
        $ingInfo->setYears(3);
        $ingInfo->setStudyCycle('ENGINEER_CYCLE');
        $ingInfo->setType('SPECIALITY');
        $ingInfo->setInstitute($institute);

        $manager->persist($ingInfo);
        $this->setReference(self::ING_INFO_1_SPECIALITY, $ingInfo);

        $infoB = new Speciality();
        $infoB->setName('Info B');
        $infoB->setAlias('B');
        $infoB->setDescription($faker->realText(150));
        $infoB->setYears(2);
        $infoB->setType('BRANCH');
        $infoB->setInstitute($institute);
        $infoB->setParent($ingInfo);

        $manager->persist($infoB);
        $this->setReference(self::INFO_B_1_SPECIALITY, $infoB);

        $bib = new Speciality();
        $bib->setName('Business Intelligence B');
        $bib->setAlias('BI-B');
        $bib->setDescription($faker->realText(150));
        $bib->setYears(1);
        $bib->setType('OPTION');
        $bib->setInstitute($institute);
        $bib->setParent($ingInfo);

        $manager->persist($bib);
        $this->setReference(self::BIB_1_SPECIALITY, $bib);

        $dsb = new Speciality();
        $dsb->setName('Data Science B');
        $dsb->setAlias('DS-B');
        $dsb->setDescription($faker->realText(150));
        $dsb->setYears(1);
        $dsb->setType('OPTION');
        $dsb->setInstitute($institute);
        $dsb->setParent($ingInfo);

        $manager->persist($dsb);
        $this->setReference(self::DSB_1_SPECIALITY, $dsb);

        $nidsb = new Speciality();
        $nidsb->setName('Network Infrastructure and Data Security B');
        $nidsb->setAlias('NIDS-B');
        $nidsb->setDescription($faker->realText(150));
        $nidsb->setYears(1);
        $nidsb->setType('OPTION');
        $nidsb->setInstitute($institute);
        $nidsb->setParent($ingInfo);

        $manager->persist($nidsb);
        $this->setReference(self::NIDSB_1_SPECIALITY, $nidsb);

        $infoA = new Speciality();
        $infoA->setName('Info A');
        $infoA->setAlias('A');
        $infoA->setDescription($faker->realText(150));
        $infoA->setYears(1);
        $infoA->setType('BRANCH');
        $infoA->setInstitute($institute);
        $infoA->setParent($ingInfo);

        $manager->persist($infoA);
        $this->setReference(self::INFO_A_1_SPECIALITY, $infoA);

        $bi = new Speciality();
        $bi->setName('Business Intelligence');
        $bi->setAlias('BI');
        $bi->setDescription($faker->realText(150));
        $bi->setYears(2);
        $bi->setType('BRANCH');
        $bi->setInstitute($institute);
        $bi->setParent($ingInfo);

        $manager->persist($bi);
        $this->setReference(self::BIA_1_SPECIALITY, $bi);

        $ds = new Speciality();
        $ds->setName('Data Science');
        $ds->setAlias('DS');
        $ds->setDescription($faker->realText(150));
        $ds->setYears(2);
        $ds->setType('BRANCH');
        $ds->setInstitute($institute);
        $ds->setParent($ingInfo);

        $manager->persist($ds);
        $this->setReference(self::DSA_1_SPECIALITY, $ds);

        $nids = new Speciality();
        $nids->setName('Network Infrastructure and Data Security');
        $nids->setAlias('NIDS');
        $nids->setDescription($faker->realText(150));
        $nids->setYears(2);
        $nids->setType('BRANCH');
        $nids->setInstitute($institute);
        $nids->setParent($ingInfo);

        $manager->persist($nids);
        $this->setReference(self::NIDSA_1_SPECIALITY, $nids);

        // Depth 3

        $ingInfo5 = new Speciality();
        $ingInfo5->setName('Ingenieur Informatique');
        $ingInfo5->setDescription($faker->realText(150));
        $ingInfo5->setYears(5);
        $ingInfo5->setStudyCycle('ENGINEER_CYCLE');
        $ingInfo5->setType('SPECIALITY');
        $ingInfo5->setInstitute($institute);

        $manager->persist($ingInfo5);
        $this->setReference(self::ING_INFO_2_SPECIALITY, $ingInfo5);

        $prep = new Speciality();
        $prep->setName('Preparatoire');
        $prep->setAlias('P');
        $prep->setDescription($faker->realText(150));
        $prep->setYears(2);
        $prep->setType('BRANCH');
        $prep->setInstitute($institute);
        $prep->setParent($ingInfo5);

        $manager->persist($prep);
        $this->setReference(self::PREPA_2_SPECIALITY, $prep);

        $infoA5 = new Speciality();
        $infoA5->setName('Info A');
        $infoA5->setAlias('BTS');
        $infoA5->setDescription($faker->realText(150));
        $infoA5->setYears(1);
        $infoA5->setType('BRANCH');
        $infoA5->setInstitute($institute);
        $infoA5->setParent($ingInfo5);

        $manager->persist($infoA5);
        $this->setReference(self::INFO_A_2_SPECIALITY, $infoA5);

        $bi2 = new Speciality();
        $bi2->setName('Business Intelligence');
        $bi2->setAlias('BI');
        $bi2->setDescription($faker->realText(150));
        $bi2->setYears(1);
        $bi2->setType('BRANCH');
        $bi2->setInstitute($institute);
        $bi2->setParent($ingInfo5);

        $manager->persist($bi2);
        $this->setReference(self::BIA_2_SPECIALITY, $bi2);

        $ds2 = new Speciality();
        $ds2->setName('Data Science');
        $ds2->setAlias('DS');
        $ds2->setDescription($faker->realText(150));
        $ds2->setYears(2);
        $ds2->setType('BRANCH');
        $ds2->setInstitute($institute);
        $ds2->setParent($ingInfo5);

        $manager->persist($ds2);
        $this->setReference(self::DSA_2_SPECIALITY, $ds2);

        $nids2 = new Speciality();
        $nids2->setName('Network Infrastructure and Data Security');
        $nids2->setAlias('NIDS');
        $nids2->setDescription($faker->realText(150));
        $nids2->setYears(2);
        $nids2->setType('BRANCH');
        $nids2->setInstitute($institute);
        $nids2->setParent($ingInfo5);

        $manager->persist($nids2);
        $this->setReference(self::NIDSA_2_SPECIALITY, $nids2);

        // Depth 4

        $ingInfo6 = new Speciality();
        $ingInfo6->setName('Master Informatique');
        $ingInfo6->setDescription($faker->realText(150));
        $ingInfo6->setYears(6);
        $ingInfo6->setStudyCycle('MASTERE_CYCLE');
        $ingInfo6->setType('SPECIALITY');
        $ingInfo6->setInstitute($institute);

        $manager->persist($ingInfo6);
        $this->setReference(self::MASTER_INFO_3_SPECIALITY, $ingInfo6);

        $prep2 = new Speciality();
        $prep2->setName('Preparatoire');
        $prep2->setAlias('P');
        $prep2->setDescription($faker->realText(150));
        $prep2->setYears(2);
        $prep2->setType('BRANCH');
        $prep2->setInstitute($institute);
        $prep2->setParent($ingInfo6);

        $manager->persist($prep2);
        $this->setReference(self::PREPA_3_SPECIALITY, $prep2);

        $infoA6 = new Speciality();
        $infoA6->setName('Info A');
        $infoA6->setAlias('BTS');
        $infoA6->setDescription($faker->realText(150));
        $infoA6->setYears(1);
        $infoA6->setType('BRANCH');
        $infoA6->setInstitute($institute);
        $infoA6->setParent($ingInfo6);

        $manager->persist($infoA6);
        $this->setReference(self::INFO_A_3_SPECIALITY, $infoA6);

        $bi3 = new Speciality();
        $bi3->setName('Business Intelligence');
        $bi3->setAlias('BI');
        $bi3->setDescription($faker->realText(150));
        $bi3->setYears(2);
        $bi3->setType('BRANCH');
        $bi3->setInstitute($institute);
        $bi3->setParent($ingInfo6);

        $manager->persist($bi3);
        $this->setReference(self::BI_3_SPECIALITY, $bi3);

        $bi3m = new Speciality();
        $bi3m->setName('Mastere Business Intelligence');
        $bi3m->setAlias('BI');
        $bi3m->setDescription($faker->realText(150));
        $bi3m->setYears(1);
        $bi3m->setType('BRANCH');
        $bi3m->setInstitute($institute);
        $bi3m->setParent($ingInfo6);

        $manager->persist($bi3m);
        $this->setReference(self::BIM_3_SPECIALITY, $bi3m);

        $ds3 = new Speciality();
        $ds3->setName('Data Science');
        $ds3->setAlias('DS');
        $ds3->setDescription($faker->realText(150));
        $ds3->setYears(2);
        $ds3->setType('BRANCH');
        $ds3->setInstitute($institute);
        $ds3->setParent($ingInfo6);

        $manager->persist($ds3);
        $this->setReference(self::DS_3_SPECIALITY, $ds3);

        $ds3m = new Speciality();
        $ds3m->setName('Mastere Data Science');
        $ds3m->setAlias('DS');
        $ds3m->setDescription($faker->realText(150));
        $ds3m->setYears(1);
        $ds3m->setType('BRANCH');
        $ds3m->setInstitute($institute);
        $ds3m->setParent($ingInfo6);

        $manager->persist($ds3m);
        $this->setReference(self::DSM_3_SPECIALITY, $ds3m);

        $nids3 = new Speciality();
        $nids3->setName('Network Infrastructure and Data Security');
        $nids3->setAlias('NIDS');
        $nids3->setDescription($faker->realText(150));
        $nids3->setYears(2);
        $nids3->setType('BRANCH');
        $nids3->setInstitute($institute);
        $nids3->setParent($ingInfo6);

        $manager->persist($nids3);
        $this->setReference(self::NIDS_3_SPECIALITY, $nids3);

        $nids3m = new Speciality();
        $nids3m->setName('Mastere Securite');
        $nids3m->setAlias('NIDS');
        $nids3m->setDescription($faker->realText(150));
        $nids3m->setYears(1);
        $nids3m->setType('BRANCH');
        $nids3m->setInstitute($institute);
        $nids3m->setParent($ingInfo6);

        $manager->persist($nids3m);
        $this->setReference(self::NIDSM_3_SPECIALITY, $nids3m);

        $manager->flush();
    }

    public function getDependencies() {

        return array(
            DepartmentFixtures::class,
        );
    }
}

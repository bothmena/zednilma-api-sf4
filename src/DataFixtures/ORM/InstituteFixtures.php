<?php

namespace App\DataFixtures\ORM;

use App\Entity\Institute;
use App\Entity\InstituteSettings;
use App\Entity\Timetable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class InstituteFixtures extends Fixture implements DependentFixtureInterface {

    public const DEMO_INSTITUTE_REFERENCE = 'demo-institute';

    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);

        $institute = new Institute();
        $institute->setProfileImage($this->getReference(ImageFixtures::LOGO_IMAGE));
        $institute->setCoverImage($this->getReference(ImageFixtures::COVER_IMAGE));
        $institute->setName('Demo Institute');
        $institute->setSlogan($faker->sentence());
        $institute->setDescription($faker->sentence(20));
        $institute->setFoundationDate($faker->dateTime('-10 years'));
        $institute->setWebsite('http://www.demoinstitute.edu.tn');
        $institute->setType('UNIVERSITY');
        $institute->setParentalControl('NONE');

        $settings = new InstituteSettings();
        $settings->setTerms('s');
        $settings->setSchoolYear('2018-2019');
        $settings->setMinClassSize(20);
        $settings->setMaxClassSize(30);

        $timetable = new Timetable();
        $timetable->setName('timetable.default');
        $timetable->setInstitute($institute);

        $institute->setSettings($settings);

        $this->setReference(self::DEMO_INSTITUTE_REFERENCE, $institute);

        $manager->persist($institute);
        $manager->persist($timetable);
        $manager->flush();
    }

    public function getDependencies() {

        return array(
            ImageFixtures::class,
        );
    }
}

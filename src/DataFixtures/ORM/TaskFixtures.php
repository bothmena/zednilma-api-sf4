<?php

namespace App\DataFixtures\ORM;

use App\Entity\Classe;
use App\Entity\NotificationUser;
use App\Entity\Speciality;
use App\Entity\Task;
use App\Entity\TaskList;
use App\Entity\TaskUser;
use App\Entity\User;
use App\Entity\UserClasse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use phpDocumentor\Reflection\Types\Parent_;

class TaskFixtures extends Fixture implements DependentFixtureInterface {

    public const DSA1_CLASSE = 'dsa1-classe';
    public const DSA2_CLASSE = 'dsa2-classe';
    public const DSA3_CLASSE = 'dsa3-classe';

    private $faker;

    public function __construct() {

        $this->faker = Faker\Factory::create('en_UK');
        $this->faker->seed(123);
    }

    public function load(ObjectManager $manager) {



        foreach ([1=>'exams', 2=>'homework', 3=>'file_submission', 4=>'project'] as $type => $listName) {

            $this->createListAndTasks($manager, $type, $listName);
        }

        foreach (['Personal List 1', 'Personal List 2', 'Personal List 3'] as $listName) {

            $this->createListAndTasks($manager, 5, $listName);
        }

        $manager->flush();
    }

    private function createListAndTasks(ObjectManager $manager, int $type, string $listName) {


        /**
         * @var User $professor
         * @var User $student
         */
        $student = $this->getReference(UserFixtures::STUDENT_USER);
        $professor = $this->getReference(UserFixtures::PROFESSOR_USER);

        $tl = new TaskList();
        $tl->setName($listName);
        if ($type === 5)
            $tl->setUser($student);
        $manager->persist($tl);

        $taskNbr = $this->faker->randomDigit;
        for ($i = 0; $i < $taskNbr; $i++) {

            $task = new Task();
            $task->setName($this->faker->realText(20));
            $task->setDescription($this->faker->realText(200));
            $task->setType($type);
            if ($type === 5)
                $task->setUser($student);
            else
                $task->setUser($professor);
            $task->setList($tl);
            $task->setDueDate($this->faker->dateTimeBetween('+1 days', '+2 months'));

            $taskUser = new TaskUser($task, $student);

            $manager->persist($task);
            $manager->persist($taskUser);
        }
    }

    public function getDependencies() {

        return array(
            NotificationFixtures::class,
        );
    }
}

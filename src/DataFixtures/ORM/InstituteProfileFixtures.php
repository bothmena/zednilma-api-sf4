<?php

namespace App\DataFixtures\ORM;

use App\Entity\Email;
use App\Entity\Institute;
use App\Entity\Location;
use App\Entity\Phone;
use App\Entity\SocialProfile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class InstituteProfileFixtures extends Fixture implements DependentFixtureInterface {


    private $sps = [
        [
            'url' => 'https://gitlab.com/',
            'website' => 'gitlab',
        ],
        [
            'url' => 'https://www.facebook.com/',
            'website' => 'facebook',
        ],
        [
            'url' => 'https://bitbucket.org/',
            'website' => 'bitbucket',
        ],
    ];

    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);
        /**
         * @var $institute Institute
         */
        $institute = $this->getReference(InstituteFixtures::DEMO_INSTITUTE_REFERENCE);

        for ( $i = 0; $i < 3; $i++ ) {

            $insLocation = new Location();
            $insLocation->setInstitute($institute);
            $insLocation->setCountry('TN');
            $insLocation->setState($faker->state);
            $insLocation->setCity($faker->city);
            $insLocation->setAddress($faker->streetAddress);
            $insLocation->setZipCode($faker->postcode);
            $insLocation->setMapLat($faker->latitude);
            $insLocation->setMapLong($faker->longitude);
            $insLocation->setIsPrimary(false);
            $manager->persist($insLocation);
        }

        for ( $i = 0; $i < 3; $i++ ) {
            $phone = new Phone();
            $phone->setInstitute($institute);
            $phone->setType($faker->randomElement(['MBL', 'FIX', 'FAX']));
            $phone->setCcode('+' . $faker->randomNumber(3));
            $phone->setNumber($faker->randomNumber(8));
            $phone->setVerificationCode();
            $manager->persist($phone);
        }

        for ( $i = 0; $i < 3; $i++ ) {

            $email = new Email();
            $email->setInstitute($institute);
            $email->setEmail($faker->email);
            $email->setIsPrimary($i === 0);
            $email->setRole($faker->realText(20));
            $email->setConfirmationToken($faker->uuid);
            $manager->persist($email);
        }

        foreach ($this->sps as $sp) {

            $sP = new SocialProfile();
            $sP->setInstitute($institute);
            $sP->setUsername($institute->getSlug());
            $sP->setWebsite($sp['website']);
            $sP->setUrl($sp['url'] . $institute->getSlug());
            $manager->persist($sP);
        }

        $manager->flush();
    }

    public function getDependencies() {

        return array(
            LocationFixtures::class,
        );
    }
}

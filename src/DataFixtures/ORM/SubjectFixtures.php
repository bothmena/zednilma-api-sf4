<?php

namespace App\DataFixtures\ORM;

use App\Entity\Department;
use App\Entity\Institute;
use App\Entity\Subject;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class SubjectFixtures extends Fixture implements DependentFixtureInterface {

    private $subjects = [
        'math' => [
            'Probability',
            'Statistics',
            'Linear Algebra',
        ],
        'ai' => [
            'Machine Learning',
            'Deep Learning',
            'Tensorflow & PyTorch',
        ],
        'cloud' => [
            'AWS Could Storage',
            'Google Cloud Storage',
        ],
        'fineco' => [
            'Finance',
            'Economy',
        ],
        'embedded' => [
            'Linux System Administration',
        ],
        'physics' => [
            'Quantum Physics',
        ],
    ];
    public const PROBABILITY_SUBJECT = 'probability-subject';
    public const DL_SUBJECT = 'dl-subject';

    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);
        /**
         * @var Department $department
         */

        foreach ($this->subjects as $dep => $depSubjects) {


            switch ($dep) {
                case 'math':
                    $department = $this->getReference(DepartmentFixtures::MATH_DEPARTMENT);
                    break;
                case 'physics':
                    $department = $this->getReference(DepartmentFixtures::PHYSIC_DEPARTMENT);
                    break;
                case 'embedded':
                    $department = $this->getReference(DepartmentFixtures::EMBEDDED_DEPARTMENT);
                    break;
                case 'fineco':
                    $department = $this->getReference(DepartmentFixtures::FINECO_DEPARTMENT);
                    break;
                case 'ai':
                    $department = $this->getReference(DepartmentFixtures::AI_DEPARTMENT);
                    break;
                case 'cloud':
                    $department = $this->getReference(DepartmentFixtures::CLOUD_DEPARTMENT);
                    break;
            }
            foreach ($depSubjects as $depSubject) {
                $subject = new Subject();
                $subject->setName($depSubject);
                $subject->setDepartment($department);
                $subject->setDescription($faker->realText(250));
                $manager->persist($subject);

                if ($depSubject === 'Probability')
                    $this->setReference(SubjectFixtures::PROBABILITY_SUBJECT, $subject);
                else if ($depSubject === 'Deep Learning')
                    $this->setReference(SubjectFixtures::DL_SUBJECT, $subject);
            }
        }

        $manager->flush();
    }

    public function getDependencies() {

        return array(
            PathFixtures::class,
        );
    }
}

<?php

namespace App\DataFixtures\ORM;

use App\Entity\Institute;
use App\Entity\Path;
use App\Entity\PathBranch;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class PathFixtures extends Fixture implements DependentFixtureInterface {

    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);
        /**
         * @var Institute $institute
         */
        $institute = $this->getReference(InstituteFixtures::DEMO_INSTITUTE_REFERENCE);

        $arr = [SpecialityFixtures::PREPA_MP_SPECIALITY, SpecialityFixtures::PREPA_PC_SPECIALITY, SpecialityFixtures::PREPA_TECHNO_SPECIALITY];
        foreach ($arr as $index => $speciality) {
            $path = new Path($this->getReference(SpecialityFixtures::PREPA_SPECIALITY));
            $pathBranch = new PathBranch($path, $this->getReference($speciality), 1);
            $manager->persist($path);
            $manager->persist($pathBranch);
        }

        $arr = [
            [SpecialityFixtures::INFO_B_1_SPECIALITY, SpecialityFixtures::BIB_1_SPECIALITY],
            [SpecialityFixtures::INFO_B_1_SPECIALITY, SpecialityFixtures::DSB_1_SPECIALITY],
            [SpecialityFixtures::INFO_B_1_SPECIALITY, SpecialityFixtures::NIDSB_1_SPECIALITY],
            [SpecialityFixtures::INFO_A_1_SPECIALITY, SpecialityFixtures::BIA_1_SPECIALITY],
            [SpecialityFixtures::INFO_A_1_SPECIALITY, SpecialityFixtures::DSA_1_SPECIALITY],
            [SpecialityFixtures::INFO_A_1_SPECIALITY, SpecialityFixtures::NIDSA_1_SPECIALITY],
        ];
        foreach ($arr as $path) {
            $learningPath = new Path($this->getReference(SpecialityFixtures::ING_INFO_1_SPECIALITY));
            $manager->persist($learningPath);
            foreach ($path as $index => $speciality) {
                $pathBranch = new PathBranch($learningPath, $this->getReference($speciality), $index + 1);
                $manager->persist($pathBranch);
            }
        }

        $arr = [
            [SpecialityFixtures::PREPA_2_SPECIALITY, SpecialityFixtures::INFO_A_2_SPECIALITY, SpecialityFixtures::BIA_2_SPECIALITY],
            [SpecialityFixtures::PREPA_2_SPECIALITY, SpecialityFixtures::INFO_A_2_SPECIALITY, SpecialityFixtures::DSA_2_SPECIALITY],
            [SpecialityFixtures::PREPA_2_SPECIALITY, SpecialityFixtures::INFO_A_2_SPECIALITY, SpecialityFixtures::NIDSA_2_SPECIALITY],
        ];
        foreach ($arr as $path) {
            $learningPath = new Path($this->getReference(SpecialityFixtures::ING_INFO_2_SPECIALITY));
            $manager->persist($learningPath);
            foreach ($path as $index => $speciality) {
                $pathBranch = new PathBranch($learningPath, $this->getReference($speciality), $index + 1);
                $manager->persist($pathBranch);
            }
        }

        $arr = [
            [SpecialityFixtures::PREPA_3_SPECIALITY, SpecialityFixtures::INFO_A_3_SPECIALITY, SpecialityFixtures::BI_3_SPECIALITY, SpecialityFixtures::BIM_3_SPECIALITY],
            [SpecialityFixtures::PREPA_3_SPECIALITY, SpecialityFixtures::INFO_A_3_SPECIALITY, SpecialityFixtures::DS_3_SPECIALITY, SpecialityFixtures::DSM_3_SPECIALITY],
            [SpecialityFixtures::PREPA_3_SPECIALITY, SpecialityFixtures::INFO_A_3_SPECIALITY, SpecialityFixtures::NIDS_3_SPECIALITY, SpecialityFixtures::NIDSM_3_SPECIALITY],
        ];
        foreach ($arr as $path) {
            $learningPath = new Path($this->getReference(SpecialityFixtures::MASTER_INFO_3_SPECIALITY));
            $manager->persist($learningPath);
            foreach ($path as $index => $speciality) {
                $pathBranch = new PathBranch($learningPath, $this->getReference($speciality), $index + 1);
                $manager->persist($pathBranch);
            }
        }

        $manager->flush();
    }

    public function getDependencies() {

        return array(
            SpecialityFixtures::class,
        );
    }
}

<?php

namespace App\DataFixtures\ORM;

use App\Entity\Department;
use App\Entity\Institute;
use App\Entity\Subject;
use App\Entity\SubjectClasse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class SubjectClassesFixtures extends Fixture implements DependentFixtureInterface {

    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);

        $professor = $this->getReference(UserFixtures::PROFESSOR_USER);
        $classe1 = $this->getReference(ClassesFixtures::DSA1_CLASSE);
        $classe2 = $this->getReference(ClassesFixtures::DSA2_CLASSE);
        $subjectNames = [
            'Probablity',
            'Statistics',
            'Linear Algebra',
            'Machine Learning',
            'Deep Learning',
            'Tensorflow & PyTorch',
        ];
        for ($i = 0; $i < 4; $i++) {
            $subject = $manager->getRepository('App:Subject')->findOneBy(['name'=> $subjectNames[$i]]);
            if ($subject) {
                $subClass = new SubjectClasse($subject, $classe1, $professor);
                $manager->persist($subClass);
            }
        }

        for ($i = 2; $i < 6; $i++) {
            $subject = $manager->getRepository('App:Subject')->findOneBy(['name'=> $subjectNames[$i]]);
            if ($subject) {
                $subClass = new SubjectClasse($subject, $classe2, $professor);
                if ($subjectNames[$i] === 'Deep Learning') {
                    $prof2 = $manager->getRepository('App:User')->findOneBy(['username' => 'professor2']);
                    $prof3 = $manager->getRepository('App:User')->findOneBy(['username' => 'professor3']);

                    $subClass2 = new SubjectClasse($subject, $classe2, $prof2, 2);
                    $subClass3 = new SubjectClasse($subject, $classe2, $prof3, 2);
                    $manager->persist($subClass2);
                    $manager->persist($subClass3);
                }
                $manager->persist($subClass);
            }
        }

        $manager->flush();
    }

    public function getDependencies() {

        return array(
            ClassesFixtures::class,
            SubjectFixtures::class,
        );
    }
}

<?php

namespace App\DataFixtures\ORM;

use App\Entity\Classe;
use App\Entity\Infrastructure;
use App\Entity\Institute;
use App\Entity\Speciality;
use App\Entity\UserClasse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class InfrastructureFixtures extends Fixture implements DependentFixtureInterface {

    public const CLASSROOM_INFRASTRUCTURE = 'classroom-infrastructure';

    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);

        /**
         * @var Institute $institute
         */
        $institute = $this->getReference(InstituteFixtures::DEMO_INSTITUTE_REFERENCE);

        $block = new Infrastructure();
        $block->setName('Block A');
        $block->setAlias('A');
        $block->setInstitute($institute);
        $block->setType('BLOCK');

        $manager->persist($block);

        $floor = new Infrastructure();
        $floor->setName('Floor 1');
        $floor->setAlias('1');
        $floor->setInstitute($institute);
        $floor->setType('FLOOR');
        $floor->setParent($block);
        $floor->setIndex(1);

        $manager->persist($floor);

        for ($i = 1; $i < 11; $i++) {

            $classroom = new Infrastructure();
            $classroom->setInstitute($institute);
            $classroom->setType('CLASSROOM');
            $classroom->setParent($floor);
            $classroom->setIndex($i);

            $manager->persist($classroom);

            if ($i === 1)
                $this->setReference(InfrastructureFixtures::CLASSROOM_INFRASTRUCTURE, $classroom);
        }

        $manager->flush();
    }

    public function getDependencies() {

        return array(
            InstituteFixtures::class,
        );
    }
}

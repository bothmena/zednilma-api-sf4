<?php

namespace App\DataFixtures\ORM;

use App\Entity\Department;
use App\Entity\Group;
use App\Entity\Institute;
use App\Entity\Post;
use App\Entity\UserDepartment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class DepartmentFixtures extends Fixture implements DependentFixtureInterface {


    public const MATH_DEPARTMENT = 'math-department';
    public const PHYSIC_DEPARTMENT = 'physic-department';
    public const EMBEDDED_DEPARTMENT = 'embedded-department';
    public const FINECO_DEPARTMENT = 'fineco-department';
    public const AI_DEPARTMENT = 'ai-department';
    public const CLOUD_DEPARTMENT = 'cloud-department';

    private $departments = [
        'Security', 'Mathematics', 'Physics', 'Embedded Systems', 'Finance & Economy', 'Artificial Intelligence', 'Cloud & Storage'
    ];

    public function load(ObjectManager $manager) {

        $faker = Faker\Factory::create('en_UK');
        $faker->seed(123);
        /**
         * @var Institute $institute
         */
        $institute = $this->getReference(InstituteFixtures::DEMO_INSTITUTE_REFERENCE);

        $depManager = new Post();
        $depManager->setName('Department Manager');
        $depManager->setDescription($faker->realText(250));
        $depManager->setInstitute($institute);
        $depManager->setType('Type');

        $group1 = new Group();
        $group1->setInstitute($institute);
        $group1->setType('post');
        $depManager->setGroupe($group1);

        $manager->persist($depManager);

        $depManagerAssist = new Post();
        $depManagerAssist->setName('Department Manager Assistant');
        $depManagerAssist->setDescription($faker->realText(250));
        $depManagerAssist->setInstitute($institute);
        $depManagerAssist->setType('Type');

        $group = new Group();
        $group->setInstitute($institute);
        $group->setType('post');
        $depManagerAssist->setGroupe($group);

        $manager->persist($depManagerAssist);

        foreach ($this->departments as $dep) {

            $department = new Department();
            $department->setInstitute($institute);
            $department->setName($dep . ' Department');
            $department->setDescription($faker->realText(250));

            $ids = $faker->randomElements(range(1, 10), 2);
            $prof1 = $manager->getRepository('App:User')->findOneBy(['username'=>'professor' . $ids[0]]);
            $prof2 = $manager->getRepository('App:User')->findOneBy(['username'=>'professor' . $ids[1]]);

            $userDep1 = new UserDepartment($prof1, $department, $depManager);
            $userDep2 = new UserDepartment($prof2, $department, $depManagerAssist);

            $group2 = new Group();
            $group2->setInstitute($institute);
            $group2->setType('department');
            $department->setGroupe($group2);

            switch ($dep) {
                case 'Mathematics':
                    $this->setReference(self::MATH_DEPARTMENT, $department);
                    break;
                case 'Physics':
                    $this->setReference(self::PHYSIC_DEPARTMENT, $department);
                    break;
                case 'Embedded Systems':
                    $this->setReference(self::EMBEDDED_DEPARTMENT, $department);
                    break;
                case 'Finance & Economy':
                    $this->setReference(self::FINECO_DEPARTMENT, $department);
                    break;
                case 'Artificial Intelligence':
                    $this->setReference(self::AI_DEPARTMENT, $department);
                    break;
                case 'Cloud & Storage':
                    $this->setReference(self::CLOUD_DEPARTMENT, $department);
                    break;
            }
            $manager->persist($department);
            $manager->persist($userDep1);
            $manager->persist($userDep2);
        }

        $manager->flush();
    }

    public function getDependencies() {

        return array(
            InstituteProfileFixtures::class,
        );
    }
}

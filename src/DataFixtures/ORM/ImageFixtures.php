<?php
/**
 * Created by PhpStorm.
 * User: bothmena
 * Date: 01/07/18
 * Time: 07:25
 */

namespace App\DataFixtures\ORM;


use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ImageFixtures extends Fixture {

    public const ADMIN_IMAGE = 'admin-male-img';
    public const INSTITUTE_M_IMAGE = 'institute-male-img';
    public const INSTITUTE_F_IMAGE = 'institute-female-img';
    public const PARENTS_IMAGE = 'parents-icon-img';
    public const PROFESSOR_M_IMAGE = 'professor-male-img';
    public const PROFESSOR_F_IMAGE = 'professor-female-img';
    public const STUDENT_M_IMAGE = 'student-male-img';
    public const STUDENT_F_IMAGE = 'student-female-img';
    public const LOGO_IMAGE = 'institute-logo-img';
    public const COVER_IMAGE = 'institute-cover-img';

    private $images = [
        'admin-male.png',
        'institute-male.png',
        'institute-female.png',
        'parents-icon.png',
        'professor-male.png',
        'professor-female.png',
        'student-male.png',
        'student-female.png',
        'institute-logo.png',
        'institute-cover.jpg',
    ];

    public function load(ObjectManager $manager) {

        foreach ( $this->images as $key => $image ) {

            $type = 'prf';
            if ( $image === 'institute-cover.png') {
                $type = 'cvr';
            }

            $imageObj = new Image();
            $imageObj->setType($type);
            $imageObj->setName($image);
            $imageObj->setEntity('web');
            $manager->persist($imageObj);
            
            switch ($key) {
                case 0:
                    $this->setReference(self::ADMIN_IMAGE, $imageObj);
                    break;
                case 1:
                    $this->setReference(self::INSTITUTE_M_IMAGE, $imageObj);
                    break;
                case 2:
                    $this->setReference(self::INSTITUTE_F_IMAGE, $imageObj);
                    break;
                case 3:
                    $this->setReference(self::PARENTS_IMAGE, $imageObj);
                    break;
                case 4:
                    $this->setReference(self::PROFESSOR_M_IMAGE, $imageObj);
                    break;
                case 5:
                    $this->setReference(self::PROFESSOR_F_IMAGE, $imageObj);
                    break;
                case 6:
                    $this->setReference(self::STUDENT_M_IMAGE, $imageObj);
                    break;
                case 7:
                    $this->setReference(self::STUDENT_F_IMAGE, $imageObj);
                    break;
                case 8:
                    $this->setReference(self::LOGO_IMAGE, $imageObj);
                    break;
                case 9:
                    $this->setReference(self::COVER_IMAGE, $imageObj);
                    break;
                default:
                    break;
            }
        }
        $manager->flush();
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserPost
 *
 * @ORM\Table(name="user_post")
 * @ORM\Entity(repositoryClass="App\Repository\UserPostRepository")
 */
class UserPost {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Post")
     * @ORM\JoinColumn(nullable=false)
     */
    private $post;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStarted", type="date")
     */
    private $dateStarted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnded", type="date", nullable=true)
     */
    private $dateEnded;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set dateStarted
     *
     * @param \DateTime $dateStarted
     *
     * @return UserPost
     */
    public function setDateStarted( $dateStarted ) {

        $this->dateStarted = $dateStarted;

        return $this;
    }

    /**
     * Get dateStarted
     *
     * @return \DateTime
     */
    public function getDateStarted() {

        return $this->dateStarted;
    }

    /**
     * Set dateEnded
     *
     * @param \DateTime $dateEnded
     *
     * @return UserPost
     */
    public function setDateEnded( $dateEnded ) {

        $this->dateEnded = $dateEnded;

        return $this;
    }

    /**
     * Get dateEnded
     *
     * @return \DateTime
     */
    public function getDateEnded() {

        return $this->dateEnded;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return UserPost
     */
    public function setUser(\App\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set post
     *
     * @param \App\Entity\Post $post
     *
     * @return UserPost
     */
    public function setPost(\App\Entity\Post $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \App\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }
}

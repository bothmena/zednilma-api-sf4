<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Location
 *
 * @ORM\Table(name="location")
 * @ORM\Entity(repositoryClass="App\Repository\LocationRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"id", "country", "state", "city", "address",
 *     "zipCode", "mapLat", "mapLong", "isPrimary"})
 */
class Location {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Institute")
     */
    private $institute;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=2, nullable=true)
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=61, nullable=true)
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=61, nullable=true)
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="zipCode", type="string", length=10, nullable=true)
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=101, nullable=true)
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="mapLat", type="decimal", precision=10, scale=8, nullable=true)
     * @Serializer\Type("float")
     * @Serializer\Expose()
     */
    private $mapLat;

    /**
     * @var string
     *
     * @ORM\Column(name="mapLong", type="decimal", precision=11, scale=8, nullable=true)
     * @Serializer\Type("float")
     * @Serializer\Expose()
     */
    private $mapLong;

    /**
     * @var bool
     *
     * @ORM\Column(name="isPrimary", type="boolean")
     *
     * @Serializer\Type("boolean")
     * @Serializer\Expose()
     */
    private $isPrimary;


    public function __construct() {

        $this->isPrimary = false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Location
     */
    public function setCountry( $country ) {

        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry() {

        return $this->country;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Location
     */
    public function setState( $state ) {

        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState() {

        return $this->state;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Location
     */
    public function setCity( $city ) {

        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity() {

        return $this->city;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     *
     * @return Location
     */
    public function setZipCode( $zipCode ) {

        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode() {

        return $this->zipCode;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Location
     */
    public function setAddress( $address ) {

        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress() {

        return $this->address;
    }

    /**
     * Set mapLat
     *
     * @param string $mapLat
     *
     * @return Location
     */
    public function setMapLat($mapLat)
    {
        $this->mapLat = $mapLat;

        return $this;
    }

    /**
     * Get mapLat
     *
     * @return string
     */
    public function getMapLat()
    {
        return $this->mapLat;
    }

    /**
     * Set mapLong
     *
     * @param string $mapLong
     *
     * @return Location
     */
    public function setMapLong($mapLong)
    {
        $this->mapLong = $mapLong;

        return $this;
    }

    /**
     * Get mapLong
     *
     * @return string
     */
    public function getMapLong()
    {
        return $this->mapLong;
    }

    /**
     * Set institute
     *
     * @param \App\Entity\Institute $institute
     *
     * @return Location
     */
    public function setInstitute(\App\Entity\Institute $institute = null)
    {
        $this->institute = $institute;

        return $this;
    }

    /**
     * Get institute
     *
     * @return \App\Entity\Institute
     */
    public function getInstitute()
    {
        return $this->institute;
    }

    /**
     * Set isPrimary
     *
     * @param boolean $isPrimary
     *
     * @return Location
     */
    public function setIsPrimary($isPrimary)
    {
        $this->isPrimary = $isPrimary;

        return $this;
    }

    /**
     * Get isPrimary
     *
     * @return boolean
     */
    public function getIsPrimary()
    {
        return $this->isPrimary;
    }
}

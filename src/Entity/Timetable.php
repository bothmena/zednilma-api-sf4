<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TimetableRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class Timetable {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Institute")
     * @ORM\JoinColumn(nullable=false)
     */
    private $institute;

    /**
     * @ORM\Column(type="string", length=60)
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Expose()
     */
    private $locked;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SessionContainer", mappedBy="timetable", orphanRemoval=true)
     * @Serializer\Expose()
     */
    private $sessionContainers;

    public function __construct() {
        $this->sessionContainers = new ArrayCollection();
        $this->locked = false;
    }

    public function getId() {
        return $this->id;
    }

    public function getInstitute(): ?Institute {
        return $this->institute;
    }

    public function setInstitute(?Institute $institute): self {
        $this->institute = $institute;

        return $this;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getLocked(): ?bool {
        return $this->locked;
    }

    public function setLocked(bool $locked): self {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @return Collection|SessionContainer[]
     */
    public function getSessionContainers(): Collection {
        return $this->sessionContainers;
    }

    public function addSessionContainer(SessionContainer $sessionContainer): self {
        if (!$this->sessionContainers->contains($sessionContainer)) {
            $this->sessionContainers[] = $sessionContainer;
            $sessionContainer->setTimetable($this);
        }

        return $this;
    }

    public function removeSessionContainer(SessionContainer $sessionContainer): self {
        if ($this->sessionContainers->contains($sessionContainer)) {
            $this->sessionContainers->removeElement($sessionContainer);
            // set the owning side to null (unless already changed)
            if ($sessionContainer->getTimetable() === $this) {
                $sessionContainer->setTimetable(null);
            }
        }

        return $this;
    }
}

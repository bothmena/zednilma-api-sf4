<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * PathBranch
 *
 * @ORM\Table(name="subject_class")
 * @ORM\Entity(repositoryClass="App\Repository\SubjectClasseRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"id", "path", "branch", "index"})
 */
class SubjectClasse {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Subject")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose()
     */
    private $subject;

    /**
     * @ORM\ManyToOne(targetEntity="Classe")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose()
     */
    private $classe;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose()
     */
    private $professor;

    /**
     * @var integer
     *
     * 1: professor, 2: prof-assistant (primary)
     *
     * @ORM\Column(name="role", type="smallint")
     *
     * @Serializer\Expose()
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="subjectClasse")
     *
     * @Serializer\Expose()
     */
    private $tasks;


    public function __construct(Subject $subject = NULL, Classe $classe = NULL, User $professor = NULL, $role = 1) {

        $this->subject = $subject;
        $this->classe = $classe;
        $this->professor = $professor;
        $this->role = $role;
        $this->tasks = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * @return Subject
     */
    public function getSubject() {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject(Subject $subject): void {
        $this->subject = $subject;
    }

    /**
     * @return Classe
     */
    public function getClasse() {
        return $this->classe;
    }

    /**
     * @param mixed $classe
     */
    public function setClasse(Classe $classe): void {
        $this->classe = $classe;
    }

    /**
     * @return mixed
     */
    public function getProfessor() {
        return $this->professor;
    }

    /**
     * @param mixed $professor
     */
    public function setProfessor(User $professor): void {
        $this->professor = $professor;
    }

    /**
     * @return int
     */
    public function getRole(): int {
        return $this->role;
    }

    /**
     * @param int $role
     */
    public function setRole(int $role): void {
        $this->role = $role;
    }

    /**
     * @return Collection|Task[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Task $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setSubjectClasse($this);
        }

        return $this;
    }

    public function removeTask(Task $task): self
    {
        if ($this->tasks->contains($task)) {
            $this->tasks->removeElement($task);
            // set the owning side to null (unless already changed)
            if ($task->getSubjectClasse() === $this) {
                $task->setSubjectClasse(null);
            }
        }

        return $this;
    }
}

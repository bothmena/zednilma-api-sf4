<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserInstitute
 *
 * @ORM\Table(name="user_institute")
 * @ORM\Entity(repositoryClass="App\Repository\UserInstituteRepository")
 */
class UserInstitute {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Institute")
     * @ORM\JoinColumn(nullable=false)
     */
    private $institute;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateIn", type="datetime")
     */
    private $dateIn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateOut", type="datetime", nullable=true)
     */
    private $dateOut;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=15)
     */
    private $role;

    public function __construct(User $user = NULL, Institute $institute = NULL, $role = NULL) {

        $this->dateIn = new \DateTime();
        $this->user = $user;
        $this->institute = $institute;
        $this->role = $role;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set dateIn
     *
     * @param \DateTime $dateIn
     *
     * @return UserInstitute
     */
    public function setDateIn( $dateIn ) {

        $this->dateIn = $dateIn;

        return $this;
    }

    /**
     * Get dateIn
     *
     * @return \DateTime
     */
    public function getDateIn() {

        return $this->dateIn;
    }

    /**
     * Set dateOut
     *
     * @param \DateTime $dateOut
     *
     * @return UserInstitute
     */
    public function setDateOut( $dateOut ) {

        $this->dateOut = $dateOut;

        return $this;
    }

    /**
     * Get dateOut
     *
     * @return \DateTime
     */
    public function getDateOut() {

        return $this->dateOut;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return UserInstitute
     */
    public function setRole( $role ) {

        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole() {

        return $this->role;
    }

    /**
     * Set institute
     *
     * @param \App\Entity\Institute $institute
     *
     * @return UserInstitute
     */
    public function setInstitute( \App\Entity\Institute $institute ) {

        $this->institute = $institute;

        return $this;
    }

    /**
     * Get institute
     *
     * @return Institute
     */
    public function getInstitute(): Institute {

        return $this->institute;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return UserInstitute
     */
    public function setUser( User $user ) {

        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser(): User {

        return $this->user;
    }
}

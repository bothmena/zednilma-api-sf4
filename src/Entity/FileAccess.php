<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FileAccess
 *
 * @ORM\Table(name="file_access")
 * @ORM\Entity(repositoryClass="App\Repository\FileAccessRepository")
 */
class FileAccess {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="File")
     * @ORM\JoinColumn(nullable=false)
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=5)
     */
    private $type;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return FileAccess
     */
    public function setType( $type ) {

        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {

        return $this->type;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return FileAccess
     */
    public function setUser(\App\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set file
     *
     * @param \App\Entity\File $file
     *
     * @return FileAccess
     */
    public function setFile(\App\Entity\File $file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return \App\Entity\File
     */
    public function getFile()
    {
        return $this->file;
    }
}

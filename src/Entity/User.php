<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"context", "id", "email", "username", "firstName", "middleName",
 *     "lastName", "birthday", "gender", "imageUrl", "cin", "language", "location", "role", "groups", "parent", "status"})
 */
class User extends BaseUser {

    /**
     * @ORM\PrePersist()
     */
    public function onPrePersist() {

        if ( empty($this->username) ) {

            $this->setUsername( $this->email );
        }
        $this->setCin( $this->username );
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Location", cascade={"persist", "remove"})
     * @Serializer\Type("App\Entity\Location")
     * @Serializer\Expose()
     */
    private $location;

    /**
     * @ORM\OneToOne(targetEntity="User", cascade={"persist", "remove"})
     * @Serializer\Type("App\Entity\User")
     * @Serializer\Expose()
     */
    private $parent;

    /**
     * @ORM\ManyToOne(targetEntity="Image")
     */
    private $profileImage;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=31, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=31, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=6, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $gender;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     *
     * @Serializer\Expose()
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=3, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="cin", type="string", length=81, unique=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $cin;

    /**
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $email;

    /**
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    protected $username;

    /**
     * @Serializer\Type("array<string>")
     * @Serializer\Expose()
     */
    protected $groups;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TaskUser", mappedBy="user")
     */
    private $tasks;

    public function __construct() {

        parent::__construct();
        $this->location = new Location();
        $this->tasks = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("image_url")
     *
     * @return string
     */
    public function getImageUrl(): string {

        return $this->profileImage ? $this->profileImage->getSource() : '';
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("status")
     *
     * @return string
     */
    public function getStatus(): string {

        if ($this->enabled)
            return 'enabled';
        else if (empty($this->confirmationToken))
            return 'disabled';
        else
            return 'inactive';
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("role")
     *
     * @return string
     */
    public function getRole(): string {

        return strtolower( substr($this->roles[0], 5) );
    }

    /**
     * @Assert\IsTrue(message = "birthday is not valid, user age should be between 3 and 100 years old!")
     */
    public function isBirthdayValid(): bool {

        if ( $this->birthday ) {
            return ( $this->birthday->diff( new \DateTime() )->y >= 3 &&
                $this->birthday->diff( new \DateTime() )->y <= 100 );
        } else {
            return true;
        }
    }

    /**
     * Set location
     *
     * @param \App\Entity\Location $location
     *
     * @return User
     */
    public function setLocation( \App\Entity\Location $location ) {

        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \App\Entity\Location
     */
    public function getLocation() {

        return $this->location;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName( $firstName ) {

        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName() {

        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName( $lastName ) {

        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName() {

        return $this->lastName;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return User
     */
    public function setGender( $gender ) {

        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender() {

        return $this->gender;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return User
     */
    public function setBirthday( $birthday ) {

        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday() {

        return $this->birthday;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return User
     */
    public function setLanguage( $language ) {

        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage() {

        return $this->language;
    }

    /**
     * Set cin
     *
     * @param string $cin
     *
     * @return User
     */
    public function setCin( $cin ) {

        $this->cin = $cin;

        return $this;
    }

    /**
     * Get cin
     *
     * @return string
     */
    public function getCin() {

        return $this->cin;
    }

    /**
     * Set profileImage
     *
     * @param \App\Entity\Image $profileImage
     *
     * @return User
     */
    public function setProfileImage( \App\Entity\Image $profileImage = NULL ) {

        $this->profileImage = $profileImage;

        return $this;
    }

    /**
     * Get profileImage
     *
     * @return \App\Entity\Image
     */
    public function getProfileImage() {

        return $this->profileImage;
    }

    /**
     * Set parent
     *
     * @param \App\Entity\User $parent
     *
     * @return User
     */
    public function setParent( User $parent = NULL ) {

        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return User
     */
    public function getParent() {

        return $this->parent;
    }

    /**
     * @return Collection|TaskUser[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(TaskUser $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setUser($this);
        }

        return $this;
    }

    public function removeTask(TaskUser $task): self
    {
        if ($this->tasks->contains($task)) {
            $this->tasks->removeElement($task);
            // set the owning side to null (unless already changed)
            if ($task->getUser() === $this) {
                $task->setUser(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Notification
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="App\Repository\NotificationRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class Notification {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose()
     * @var $notifier User
     */
    private $notifier;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     *
     * @Serializer\Expose()
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     *
     * @Serializer\Expose()
     */
    private $description;

    /**
     * @var string
     * one for every entity (not all entities have notifications).
     *
     *  0: general purpose notification (custom title + description),
     *  1: Attendance: for student,
     *  2: Attendance: for parent,
     *  6: Classe
     *  11: Club
     *  16: ClubMember
     *  21: Curriculum
     *  26: Department
     *  31: Email
     *  36: Exam
     *  41: File (user have access to new file)
     *  46: FileAccess (user has access to file)
     *  51: FileDeposit (taker (prof) receives new file from a giver (student))
     *  56: FileVersion
     *  61: Group (user enter to new group)
     *  66: Image
     *  71: Infrastructure
     *  76: Institute
     *  81: InstituteSettings
     *  86: Location
     *  91: Module
     *  96: Notification
     *  101: Path
     *  106: PathBranch
     *  111: Phone
     *  116: Post
     *  121: Session: session report for parent
     *  126: SocialProfile
     *  131: Speciality
     *  136: Subject
     *  141: Task: task due, to owner if task is not personal
     *  146: TaskFile
     *  151: TaskList
     *  156: TaskUser: new assignment to user
     *  157: TaskUser: new assignment to parent
     *  158: TaskUser: reminder to user
     *  159: TaskUser: reminder to parent
     *  161: Timetable
     *  166: User
     *  171: UserClasse
     *  176: UserExam (student/parent new mark)
     *
     * @ORM\Column(name="type", type="smallint")
     *
     * @Serializer\Expose()
     */
    private $type;



    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("image")
     *
     * @return string
     */
    public function getDateStr() {

        return $this->notifier->getImageUrl();
    }

    public function __construct() {

        $this->date = new \DateTime();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Notification
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Notification
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param int $type
     *
     * @return Notification
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get notifier
     *
     * @return User
     */
    public function getNotifier() {
        return $this->notifier;
    }

    /**
     * Set notifier
     *
     * @param User $notifier
     *
     * @return Notification
     */
    public function setNotifier(User $notifier) {
        $this->notifier = $notifier;

        return $this;
    }
}

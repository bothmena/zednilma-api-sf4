<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Email
 *
 * @ORM\Table(name="email")
 * @ORM\Entity(repositoryClass="App\Repository\EmailRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"id", "instituteId", "userId", "email", "isPrimary",
 *     "isVerified", "role", "date"})
 */
class Email {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Institute")
     * @Serializer\Type("App\Entity\Institute")
     */
    private $institute;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @Serializer\Type("App\Entity\User")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=51, unique=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="confirmationToken", type="string", length=180, nullable=true, unique=true)
     */
    private $confirmationToken;

    /**
     * @var bool
     *
     * @ORM\Column(name="isPrimary", type="boolean")
     *
     * @Serializer\Type("boolean")
     * @Serializer\Expose()
     */
    private $isPrimary;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=31, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $role;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     * @Serializer\Expose()
     */
    private $date;


    public function __construct() {

        $this->date = new \DateTime();
        $this->isPrimary = false;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("institute_id")
     *
     * @return int|NULL
     */
    public function getInstituteId() {

        return $this->institute ? $this->institute->getId() : NULL;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("user_id")
     *
     * @return int|NULL
     */
    public function getUserId() {

        return $this->user ? $this->user->getId() : NULL;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\Type("boolean")
     * @Serializer\SerializedName("is_verified")
     *
     * @return string
     */
    public function getIsVerified(): string {

        return $this->confirmationToken === NULL;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Email
     */
    public function setEmail( $email ) {

        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {

        return $this->email;
    }

    /**
     * Set isPrimary
     *
     * @param boolean $isPrimary
     *
     * @return Email
     */
    public function setIsPrimary( $isPrimary ) {

        $this->isPrimary = $isPrimary;

        return $this;
    }

    /**
     * Get isPrimary
     *
     * @return bool
     */
    public function getIsPrimary() {

        return $this->isPrimary;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Email
     */
    public function setRole( $role ) {

        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole() {

        return $this->role;
    }

    /**
     * Set institute
     *
     * @param \App\Entity\Institute $institute
     *
     * @return Email
     */
    public function setInstitute( \App\Entity\Institute $institute ) {

        $this->institute = $institute;

        return $this;
    }

    /**
     * Get institute
     *
     * @return \App\Entity\Institute
     */
    public function getInstitute() {

        return $this->institute;
    }

    /**
     * Set confirmationToken
     *
     * @param string $confirmationToken
     *
     * @return Email
     */
    public function setConfirmationToken( $confirmationToken ) {

        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * Get confirmationToken
     *
     * @return string
     */
    public function getConfirmationToken() {

        return $this->confirmationToken;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Email
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return Email
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}

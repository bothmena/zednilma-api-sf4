<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Phone
 *
 * @ORM\Table(name="phone")
 * @ORM\Entity(repositoryClass="App\Repository\PhoneRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("canonicalPhone")
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"id", "instituteId", "userId", "type", "ccode",
 *     "isVerified", "number", "role", "date"})
 */
class Phone {

    /**
     * @ORM\PrePersist()
     */
    public function onPrePersist() {

        $this->setCanonicalPhone();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate() {

        $this->setCanonicalPhone();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Institute")
     */
    private $institute;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=3)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="ccode", type="string", length=5)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $ccode;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=15)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="verificationCode", type="integer", nullable=true)
     */
    private $verificationCode;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=31, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="canonicalPhone", type="string", length=30, unique=true)
     */
    private $canonicalPhone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     * @Serializer\Expose()
     */
    private $date;

    public function __construct() {

        $this->date = new \DateTime();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("institute_id")
     *
     * @return int|NULL
     */
    public function getInstituteId() {

        return $this->institute ? $this->institute->getId() : NULL;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("user_id")
     *
     * @return int|NULL
     */
    public function getUserId() {

        return $this->user ? $this->user->getId() : NULL;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\Type("boolean")
     * @Serializer\SerializedName("is_verified")
     *
     * @return bool
     */
    public function getIsVerified(): bool {

        return $this->verificationCode === NULL;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Phone
     */
    public function setType( $type ) {

        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {

        return $this->type;
    }

    /**
     * Set ccode
     *
     * @param string $ccode
     *
     * @return Phone
     */
    public function setCcode( $ccode ) {

        $this->ccode = $ccode;

        return $this;
    }

    /**
     * Get ccode
     *
     * @return string
     */
    public function getCcode() {

        return $this->ccode;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Phone
     */
    public function setNumber( $number ) {

        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber() {

        return $this->number;
    }

    /**
     * Set institute
     *
     * @param \App\Entity\Institute $institute
     *
     * @return Phone
     */
    public function setInstitute( \App\Entity\Institute $institute ) {

        $this->institute = $institute;

        return $this;
    }

    /**
     * Get institute
     *
     * @return \App\Entity\Institute
     */
    public function getInstitute() {

        return $this->institute;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Phone
     */
    public function setRole( $role ) {

        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole() {

        return $this->role;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Phone
     */
    public function setDate( $date ) {

        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {

        return $this->date;
    }

    /**
     * Set canonicalPhone
     *
     * @return Phone
     */
    public function setCanonicalPhone() {

        $cnPhone = preg_replace( '/\s/', '', $this->ccode );
        $cnPhone = preg_replace( '/^00/', '+', $cnPhone, 1 );

        $cnPhone .= preg_replace( '/\s/', '', $this->number );

        $this->canonicalPhone = $cnPhone;

        return $this;
    }

    /**
     * Get canonicalPhone
     *
     * @return string
     */
    public function getCanonicalPhone() {

        return $this->canonicalPhone;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return Phone
     */
    public function setUser( \App\Entity\User $user = NULL ) {

        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser() {

        return $this->user;
    }

    /**
     * Set verificationCode
     *
     * @return Phone
     */
    public function setVerificationCode() {

        $this->verificationCode = mt_rand( 100000, 999999 );

        return $this;
    }

    /**
     * Set verificationCode
     *
     * @return Phone
     */
    public function setVCodeToNull() {

        $this->verificationCode = NULL;

        return $this;
    }

    /**
     * Get verificationCode
     *
     * @return integer
     */
    public function getVerificationCode() {

        return $this->verificationCode;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Group
 *
 * @ORM\Table(name="groupe")
 * @ORM\Entity(repositoryClass="App\Repository\GroupRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class Group {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Institute")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var Institute
     */
    private $institute;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Group")
     */
    private $parent;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=100)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=15)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     * @Serializer\Expose()
     */
    private $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     */
    private $maxMembers;


    /**
     * Group constructor.
     */
    public function __construct() {

        $this->date = new \DateTime();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("parent_id")
     *
     * @return int|NULL
     */
    public function getParentId() {

        return $this->parent ? $this->parent->getId() : NULL;
    }

    public function setSuffixedName(string $name) {

        $this->name = $name . ' ' . $this->institute->getSettings()->getSchoolYear();
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Group
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Group
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    public function getMaxMembers(): ?int {
        return $this->maxMembers;
    }

    public function setMaxMembers(?int $maxMembers): self {
        $this->maxMembers = $maxMembers;

        return $this;
    }

    public function getParent(): ?self {
        return $this->parent;
    }

    public function setParent(?self $parent): self {
        $this->parent = $parent;

        return $this;
    }

    public function getInstitute(): ?Institute {
        return $this->institute;
    }

    public function setInstitute(?Institute $institute): self {
        $this->institute = $institute;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name) {
        $this->name = $name;
    }
}

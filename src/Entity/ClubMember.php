<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClubMember
 *
 * @ORM\Table(name="club_member")
 * @ORM\Entity(repositoryClass="App\Repository\ClubMemberRepository")
 */
class ClubMember {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Club")
     * @ORM\JoinColumn(nullable=false)
     */
    private $club;

    /**
     * @ORM\ManyToOne(targetEntity="Post")
     * @ORM\JoinColumn(nullable=false)
     */
    private $post;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="joinDate", type="date")
     */
    private $joinDate;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set joinDate
     *
     * @param \DateTime $joinDate
     *
     * @return ClubMember
     */
    public function setJoinDate( $joinDate ) {

        $this->joinDate = $joinDate;

        return $this;
    }

    /**
     * Get joinDate
     *
     * @return \DateTime
     */
    public function getJoinDate() {

        return $this->joinDate;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return ClubMember
     */
    public function setUser(\App\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set club
     *
     * @param \App\Entity\Club $club
     *
     * @return ClubMember
     */
    public function setClub(\App\Entity\Club $club)
    {
        $this->club = $club;

        return $this;
    }

    /**
     * Get club
     *
     * @return \App\Entity\Club
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * Set post
     *
     * @param \App\Entity\Post $post
     *
     * @return ClubMember
     */
    public function setPost(\App\Entity\Post $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \App\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }
}

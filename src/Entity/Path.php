<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Path
 *
 * @ORM\Table(name="path")
 * @ORM\Entity(repositoryClass="App\Repository\PathRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"id", "specialityId", "name", "description", "dateStr"})
 */
class Path {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Speciality")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose()
     */
    private $speciality;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     *
     * @Serializer\Expose()
     */
    private $date;


    public function __construct( Speciality $speciality = NULL ) {

        $this->speciality = $speciality;
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set speciality
     *
     * @param Speciality $speciality
     *
     * @return Path
     */
    public function setSpeciality( Speciality $speciality ) {

        $this->speciality = $speciality;

        return $this;
    }

    /**
     * Get speciality
     *
     * @return Speciality
     */
    public function getSpeciality() {

        return $this->speciality;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Path
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}

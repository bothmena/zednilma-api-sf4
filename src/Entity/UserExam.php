<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserExam
 *
 * @ORM\Table(name="user_exam")
 * @ORM\Entity(repositoryClass="App\Repository\UserExamRepository")
 */
class UserExam
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Exam")
     * @ORM\JoinColumn(nullable=false)
     */
    private $exam;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @var string
     *
     * @ORM\Column(name="mark", type="decimal", precision=5, scale=3)
     */
    private $mark;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="string", length=101)
     */
    private $observation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mark
     *
     * @param string $mark
     *
     * @return UserExam
     */
    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return string
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set observation
     *
     * @param string $observation
     *
     * @return UserExam
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set exam
     *
     * @param \App\Entity\Exam $exam
     *
     * @return UserExam
     */
    public function setExam(\App\Entity\Exam $exam)
    {
        $this->exam = $exam;

        return $this;
    }

    /**
     * Get exam
     *
     * @return \App\Entity\Exam
     */
    public function getExam()
    {
        return $this->exam;
    }

    /**
     * Set student
     *
     * @param \App\Entity\User $student
     *
     * @return UserExam
     */
    public function setStudent(\App\Entity\User $student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student
     *
     * @return \App\Entity\User
     */
    public function getStudent()
    {
        return $this->student;
    }
}

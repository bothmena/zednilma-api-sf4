<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Exam
 *
 * @ORM\Table(name="exam")
 * @ORM\Entity(repositoryClass="App\Repository\ExamRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class Exam {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Subject")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose()
     */
    private $subject;

    /**
     * @ORM\ManyToOne(targetEntity="Classe")
     *
     * @Serializer\Expose()
     */
    private $classe;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     *
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="smallint")
     *
     * @Serializer\Expose()
     *
     * // 1: practical, 2: theoretical, 3: theoretical and practical, 4: oral, 5: discipline (CC: presence, behavior, participation)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="coefficient", type="decimal", precision=5, scale=3)
     *
     * @Serializer\Expose()
     */
    private $coefficient;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     *
     * @Serializer\Expose()
     */
    private $date;


    public function __construct() {

        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Exam
     */
    public function setName( $name ) {

        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {

        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Exam
     */
    public function setType( $type ) {

        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {

        return $this->type;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Exam
     */
    public function setDate( $date ) {

        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {

        return $this->date;
    }

    /**
     * Set coefficient
     *
     * @param string $coefficient
     *
     * @return Exam
     */
    public function setCoefficient( $coefficient ) {

        $this->coefficient = $coefficient;

        return $this;
    }

    /**
     * Get coefficient
     *
     * @return string
     */
    public function getCoefficient() {

        return $this->coefficient;
    }

    /**
     * Set subject
     *
     * @param \App\Entity\Subject $subject
     *
     * @return Exam
     */
    public function setSubject(\App\Entity\Subject $subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \App\Entity\Subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set classe
     *
     * @param \App\Entity\Classe $classe
     *
     * @return Exam
     */
    public function setClasse(\App\Entity\Classe $classe)
    {
        $this->classe = $classe;

        return $this;
    }

    /**
     * Get classe
     *
     * @return \App\Entity\Classe
     */
    public function getClasse()
    {
        return $this->classe;
    }
}

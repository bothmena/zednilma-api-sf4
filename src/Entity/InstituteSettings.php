<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InstituteSettingsRepository")
 */
class InstituteSettings {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * y = 1 * Year
     * s = 2 * Semester
     * t = 3 * Trimester
     * q = 4 * Quarter
     *
     * @ORM\Column(name="terms", type="string", length=1)
     *
     * @Serializer\Expose()
     */
    private $terms;

    /**
     * @var string
     *
     * @ORM\Column(name="schoolYear", type="string", length=9)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $schoolYear;

    /**
     * @ORM\Column(name="minClassSize", type="integer")
     *
     * @Serializer\Expose()
     */
    private $minClassSize;

    /**
     * @ORM\Column(name="maxClassSize", type="integer")
     *
     * @Serializer\Expose()
     */
    private $maxClassSize;


    public function __construct() {

        $this->terms = 's';
        $this->schoolYear = '18/19';
        $this->minClassSize = 25;
        $this->maxClassSize = 35;
    }

    public function getId() {
        return $this->id;
    }

    public function getTerms(): ?string {
        return $this->terms;
    }

    public function setTerms(string $terms): self {
        $this->terms = $terms;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMinClassSize() {
        return $this->minClassSize;
    }

    /**
     * @param mixed $minClassSize
     */
    public function setMinClassSize($minClassSize) {
        $this->minClassSize = $minClassSize;
    }

    /**
     * @return mixed
     */
    public function getMaxClassSize() {
        return $this->maxClassSize;
    }

    /**
     * @param mixed $maxClassSize
     */
    public function setMaxClassSize($maxClassSize) {
        $this->maxClassSize = $maxClassSize;
    }

    /**
     * @return string
     */
    public function getSchoolYear(): string {
        return $this->schoolYear;
    }

    /**
     * @param string $schoolYear
     */
    public function setSchoolYear(string $schoolYear): void {
        $this->schoolYear = $schoolYear;
    }
}

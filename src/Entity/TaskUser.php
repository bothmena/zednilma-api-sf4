<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskUserRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class TaskUser {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task", inversedBy="user")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose()
     */
    private $task;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tasks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose()
     */
    private $doneAt;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     * @Serializer\Expose()
     */
    private $dueDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Serializer\Expose()
     */
    private $isValidated;


    public function __construct(Task $task = NULL, User $user = NULL) {

        $this->user = $user;
        $this->task = $task;
        if ($task && $task->getRepeat() === 's')
            $this->dueDate = $task->getDueDate();
    }

    public function getId() {
        return $this->id;
    }

    public function getTask(): ?Task {
        return $this->task;
    }

    public function setTask(?Task $task): self {
        $this->task = $task;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDoneAt() {
        return $this->doneAt;
    }

    public function setDoneAt(?\DateTime $doneAt): self {
        $this->doneAt = $doneAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDueDate() {
        return $this->dueDate;
    }

    public function setDueDate(\DateTime $dueDate): self {
        $this->dueDate = $dueDate;

        return $this;
    }

    public function getIsValidated(): ?bool {
        return $this->isValidated;
    }

    public function setIsValidated(?bool $isValidated): self {
        $this->isValidated = $isValidated;

        return $this;
    }
}

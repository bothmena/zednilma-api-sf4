<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * SocialProfile
 *
 * @ORM\Table(name="social_profile")
 * @ORM\Entity(repositoryClass="App\Repository\SocialProfileRepository")
 * @UniqueEntity("url")
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"context", "id", "instituteId", "userId", "website", "url", "dateStr"})
 */
class SocialProfile {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Institute")
     */
    private $institute;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=141, unique=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=71)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=12)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $website;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    public function __construct() {

        $this->date = new \DateTime();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("context")
     *
     * @return string
     */
    public function getContext(): string {

        if ( $this->institute ) {

            return '/api/v1/institutes/' . $this->institute->getId() . '/social-profiles/' . $this->id;
        } else if ( $this->user ) {

            return '/api/v1/users/' . $this->user->getId() . '/social-profiles/' . $this->id;
        }
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("institute_id")
     *
     * @return int|NULL
     */
    public function getInstituteId() {

        return $this->institute ? $this->institute->getId() : NULL;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("user_id")
     *
     * @return int|NULL
     */
    public function getUserId() {

        return $this->user ? $this->user->getId() : NULL;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("date")
     *
     * @return string
     */
    public function getDateStr(): string {

        return $this->date ? $this->date->format( 'd/m/Y H:i:s' ) : '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return SocialProfile
     */
    public function setUrl( $url ) {

        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl() {

        return $this->url;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return SocialProfile
     */
    public function setWebsite( $website ) {

        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite() {

        return $this->website;
    }

    /**
     * Set institute
     *
     * @param \App\Entity\Institute $institute
     *
     * @return SocialProfile
     */
    public function setInstitute( \App\Entity\Institute $institute = NULL ) {

        $this->institute = $institute;

        return $this;
    }

    /**
     * Get institute
     *
     * @return \App\Entity\Institute
     */
    public function getInstitute() {

        return $this->institute;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return SocialProfile
     */
    public function setUser( \App\Entity\User $user = NULL ) {

        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser() {

        return $this->user;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return SocialProfile
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return SocialProfile
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}

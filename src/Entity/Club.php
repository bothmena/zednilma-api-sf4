<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Club
 *
 * @ORM\Table(name="club")
 * @ORM\Entity(repositoryClass="App\Repository\ClubRepository")
 */
class Club {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Institute")
     * @ORM\JoinColumn(nullable=false)
     */
    private $institute;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=51)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="openingDate", type="date")
     */
    private $openingDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="closingDate", type="date", nullable=true)
     */
    private $closingDate;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Club
     */
    public function setName( $name ) {

        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {

        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Club
     */
    public function setDescription( $description ) {

        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {

        return $this->description;
    }

    /**
     * Set openingDate
     *
     * @param \DateTime $openingDate
     *
     * @return Club
     */
    public function setOpeningDate( $openingDate ) {

        $this->openingDate = $openingDate;

        return $this;
    }

    /**
     * Get openingDate
     *
     * @return \DateTime
     */
    public function getOpeningDate() {

        return $this->openingDate;
    }

    /**
     * Set closingDate
     *
     * @param \DateTime $closingDate
     *
     * @return Club
     */
    public function setClosingDate( $closingDate ) {

        $this->closingDate = $closingDate;

        return $this;
    }

    /**
     * Get closingDate
     *
     * @return \DateTime
     */
    public function getClosingDate() {

        return $this->closingDate;
    }

    /**
     * Set institute
     *
     * @param \App\Entity\Institute $institute
     *
     * @return Club
     */
    public function setInstitute(\App\Entity\Institute $institute)
    {
        $this->institute = $institute;

        return $this;
    }

    /**
     * Get institute
     *
     * @return \App\Entity\Institute
     */
    public function getInstitute()
    {
        return $this->institute;
    }
}

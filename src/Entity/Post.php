<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Post
 *
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"id", "instituteId", "name", "description", "type", "date"})
 */
class Post {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Institute")
     * if this field is null then the post is public!
     */
    private $institute;
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Group", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $groupe;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=41)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $description;
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16)
     *
     * Department, specialty, ins_admin, etc...
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $type;
    /**
     * @var bool
     *
     * @ORM\Column(name="isMultiple", type="boolean")
     *
     * @Serializer\Type("boolean")
     * @Serializer\Expose()
     */
    private $isMultiple;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     * @Serializer\Expose()
     */
    private $date;

    public function __construct() {

        $this->date = new \DateTime();
        $this->isMultiple = false;
    }

    /**
     * @ORM\PrePersist()
     */
    public function onPrePersist() {

        $this->groupe->setSuffixedName($this->name);
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("institute_id")
     *
     * @return int|NULL
     */
    public function getInstituteId() {

        return $this->institute ? $this->institute->getId() : NULL;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {

        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Post
     */
    public function setName($name) {

        $this->name = $name;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {

        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Post
     */
    public function setDescription($description) {

        $this->description = $description;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {

        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Post
     */
    public function setType($type) {

        $this->type = $type;

        return $this;
    }

    /**
     * Get institute
     *
     * @return Institute
     */
    public function getInstitute() {
        return $this->institute;
    }

    /**
     * Set institute
     *
     * @param Institute $institute
     *
     * @return Post
     */
    public function setInstitute(Institute $institute = null) {
        $this->institute = $institute;

        return $this;
    }

    /**
     * Get isMultiple
     *
     * @return bool
     */
    public function getIsMultiple() {

        return $this->isMultiple;
    }

    /**
     * Set isMultiple
     *
     * @param boolean $isMultiple
     *
     * @return Post
     */
    public function setIsMultiple($isMultiple) {

        $this->isMultiple = $isMultiple;

        return $this;
    }

    public function getGroupe(): ?Group {
        return $this->groupe;
    }

    public function setGroupe(Group $groupe): self {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date) {
        $this->date = $date;
    }
}

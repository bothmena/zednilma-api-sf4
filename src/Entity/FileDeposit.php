<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FileDeposit
 *
 * @ORM\Table(name="file_deposit")
 * @ORM\Entity(repositoryClass="App\Repository\FileDepositRepository")
 */
class FileDeposit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $giver;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $taker;

    /**
     * @ORM\ManyToOne(targetEntity="File")
     * @ORM\JoinColumn(nullable=false)
     */
    private $directory;

    /**
     * @ORM\ManyToOne(targetEntity="Subject")
     * @ORM\JoinColumn(nullable=false)
     */
    private $subject;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="datetime")
     */
    private $deadline;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=41)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     *
     * @return FileDeposit
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FileDeposit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return FileDeposit
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set giver
     *
     * @param \App\Entity\User $giver
     *
     * @return FileDeposit
     */
    public function setGiver(\App\Entity\User $giver)
    {
        $this->giver = $giver;

        return $this;
    }

    /**
     * Get giver
     *
     * @return \App\Entity\User
     */
    public function getGiver()
    {
        return $this->giver;
    }

    /**
     * Set taker
     *
     * @param \App\Entity\User $taker
     *
     * @return FileDeposit
     */
    public function setTaker(\App\Entity\User $taker)
    {
        $this->taker = $taker;

        return $this;
    }

    /**
     * Get taker
     *
     * @return \App\Entity\User
     */
    public function getTaker()
    {
        return $this->taker;
    }

    /**
     * Set directory
     *
     * @param \App\Entity\File $directory
     *
     * @return FileDeposit
     */
    public function setDirectory(\App\Entity\File $directory)
    {
        $this->directory = $directory;

        return $this;
    }

    /**
     * Get directory
     *
     * @return \App\Entity\File
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * Set subject
     *
     * @param \App\Entity\Subject $subject
     *
     * @return FileDeposit
     */
    public function setSubject(\App\Entity\Subject $subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \App\Entity\Subject
     */
    public function getSubject()
    {
        return $this->subject;
    }
}

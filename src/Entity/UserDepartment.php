<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Null_;

/**
 * UserDepartment
 *
 * @ORM\Table(name="user_department")
 * @ORM\Entity(repositoryClass="App\Repository\UserDepartmentRepository")
 */
class UserDepartment {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Department")
     * @ORM\JoinColumn(nullable=false)
     */
    private $department;

    /**
     * @ORM\ManyToOne(targetEntity="Post")
     * @ORM\JoinColumn(nullable=false)
     */
    private $post;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStarted", type="date")
     */
    private $dateStarted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnded", type="date", nullable=true)
     */
    private $dateEnded;


    public function __construct(User $user = NULL, Department $department = NULL, Post $post = NULL) {

        $this->user = $user;
        $this->department = $department;
        $this->post = $post;
        $this->dateStarted = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set dateStarted
     *
     * @param \DateTime $dateStarted
     *
     * @return UserDepartment
     */
    public function setDateStarted( $dateStarted ) {

        $this->dateStarted = $dateStarted;

        return $this;
    }

    /**
     * Get dateStarted
     *
     * @return \DateTime
     */
    public function getDateStarted() {

        return $this->dateStarted;
    }

    /**
     * Set dateEnded
     *
     * @param \DateTime $dateEnded
     *
     * @return UserDepartment
     */
    public function setDateEnded( $dateEnded ) {

        $this->dateEnded = $dateEnded;

        return $this;
    }

    /**
     * Get dateEnded
     *
     * @return \DateTime
     */
    public function getDateEnded() {

        return $this->dateEnded;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return UserDepartment
     */
    public function setUser(\App\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set department
     *
     * @param \App\Entity\Department $department
     *
     * @return UserDepartment
     */
    public function setDepartment(\App\Entity\Department $department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \App\Entity\Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set post
     *
     * @param \App\Entity\Post $post
     *
     * @return UserDepartment
     */
    public function setPost(\App\Entity\Post $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \App\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Curriculum
 *
 * @ORM\Table(name="curriculum")
 * @ORM\Entity(repositoryClass="App\Repository\CurriculumRepository")
 */
class Curriculum {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Subject")
     * @ORM\JoinColumn(nullable=false)
     */
    private $subject;

    /**
     * @var int
     *
     * @ORM\Column(name="indx", type="smallint")
     */
    private $indx;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=61)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=8)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="complexity", type="smallint")
     */
    private $complexity;

    /**
     * @var string
     *
     * @ORM\Column(name="duration", type="decimal", precision=5, scale=2)
     */
    private $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="durationUnit", type="string", length=3)
     */
    private $durationUnit;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set indx
     *
     * @param integer $indx
     *
     * @return Curriculum
     */
    public function setIndx( $indx ) {

        $this->indx = $indx;

        return $this;
    }

    /**
     * Get indx
     *
     * @return int
     */
    public function getIndx() {

        return $this->indx;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Curriculum
     */
    public function setName( $name ) {

        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {

        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Curriculum
     */
    public function setType( $type ) {

        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {

        return $this->type;
    }

    /**
     * Set complexity
     *
     * @param integer $complexity
     *
     * @return Curriculum
     */
    public function setComplexity( $complexity ) {

        $this->complexity = $complexity;

        return $this;
    }

    /**
     * Get complexity
     *
     * @return int
     */
    public function getComplexity() {

        return $this->complexity;
    }

    /**
     * Set duration
     *
     * @param string $duration
     *
     * @return Curriculum
     */
    public function setDuration( $duration ) {

        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string
     */
    public function getDuration() {

        return $this->duration;
    }

    /**
     * Set durationUnit
     *
     * @param string $durationUnit
     *
     * @return Curriculum
     */
    public function setDurationUnit( $durationUnit ) {

        $this->durationUnit = $durationUnit;

        return $this;
    }

    /**
     * Get durationUnit
     *
     * @return string
     */
    public function getDurationUnit() {

        return $this->durationUnit;
    }

    /**
     * Set subject
     *
     * @param \App\Entity\Subject $subject
     *
     * @return Curriculum
     */
    public function setSubject(\App\Entity\Subject $subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \App\Entity\Subject
     */
    public function getSubject()
    {
        return $this->subject;
    }
}

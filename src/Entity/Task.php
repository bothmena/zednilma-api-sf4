<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class Task {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose()
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TaskList")
     *
     * @Serializer\Expose()
     */
    private $list;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SubjectClasse", inversedBy="tasks")
     */
    private $subjectClasse;

    /**
     * @ORM\Column(type="string", length=60)
     *
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Serializer\Expose()
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     *
     * @Serializer\Expose()
     */
    private $dueDate;

    /**
     * @ORM\Column(name="repeatt", type="string", length=4)
     * repeat periods:
     * s: single time, no repeat
     * d: every day
     * w: every week
     * m: every month
     * combinations: 5d: every 5 days, 2w: every two weeks, ...
     *
     * @Serializer\Expose()
     */
    private $repeat;

    /**
     * @ORM\Column(type="smallint")
     * 1: exam
     * 2: homework
     * 3: file submission
     * 4: project
     * 5: personal
     *
     * @Serializer\Expose()
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose()
     */
    private $link;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TaskFile", mappedBy="task")
     */
    private $files;


    public function __construct() {

        $this->repeat = 's';
        $this->files = new ArrayCollection();
    }

    public function getId() {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription(?string $description): self {
        $this->description = $description;

        return $this;
    }

    public function getDueDate(): ?\DateTimeInterface {
        return $this->dueDate;
    }

    public function setDueDate(\DateTimeInterface $dueDate): self {
        $this->dueDate = $dueDate;

        return $this;
    }

    public function getRepeat(): ?string {
        return $this->repeat;
    }

    public function setRepeat(string $repeat): self {
        $this->repeat = $repeat;

        return $this;
    }

    public function getType(): ?string {
        return $this->type;
    }

    public function setType(string $type): self {
        $this->type = $type;

        return $this;
    }

    public function getLink(): ?string {
        return $this->link;
    }

    public function setLink(?string $link): self {
        $this->link = $link;

        return $this;
    }

    /**
     * @return Collection|TaskFile[]
     */
    public function getFiles(): Collection {
        return $this->files;
    }

    public function addFile(TaskFile $file): self {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setTask($this);
        }

        return $this;
    }

    public function removeFile(TaskFile $file): self {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
            // set the owning side to null (unless already changed)
            if ($file->getTask() === $this) {
                $file->setTask(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getList(): ?TaskList {
        return $this->list;
    }

    public function setList(?TaskList $list): self {
        $this->list = $list;

        return $this;
    }

    /**
     * @return SubjectClasse
     */
    public function getSubjectClasse() {
        return $this->subjectClasse;
    }

    public function setSubjectClasse(?SubjectClasse $subjectClasse): self {
        $this->subjectClasse = $subjectClasse;

        return $this;
    }
}

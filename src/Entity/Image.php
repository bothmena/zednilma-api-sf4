<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Image
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 */
class Image {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=3)
     * ins => Institute
     * usr => User
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=3)
     * prf => profile
     * cvr => cover
     * glr => gallery
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $date;

    /**
     * @var File
     */
    protected $file;

    /**
     * @var ArrayCollection
     */
    protected $files;

    public function __construct() {

        $this->date = new \DateTime();
    }

    public function getSource(): string {

        if ( $this->entity === 'usr' ) {

            return "/images/user/$this->type/$this->name";
        } else if ( $this->entity === 'ins' ) {

            return "/images/institute/$this->type/$this->name";
        } else if ( $this->entity === 'web' ) {

            return "/images/website/$this->name";
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return Image
     */
    public function setFile( File $file = NULL ): Image {

        $this->file = $file;

        return $this;
    }

    /**
     * @return File|NULL
     */
    public function getFile() {

        return $this->file;
    }

    /**
     * @return ArrayCollection
     */
    public function getFiles(): ArrayCollection {

        return $this->files;
    }

    /**
     * @param ArrayCollection $files
     */
    public function setFiles( ArrayCollection $files ) {

        $this->files = $files;
    }

    /**
     * @return string
     */
    public function getName(): string {

        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName( string $name ) {

        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEntity(): string {

        return $this->entity;
    }

    /**
     * @param string $entity
     */
    public function setEntity( string $entity ) {

        $this->entity = $entity;
    }

    /**
     * @return mixed
     */
    public function getType() {

        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType( $type ) {

        $this->type = $type;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime {

        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate( \DateTime $date ) {

        $this->date = $date;
    }
}

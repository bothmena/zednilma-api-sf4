<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="session")
 * @ORM\Entity(repositoryClass="App\Repository\SessionRepository")
 */
class Session {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $professor;

    /**
     * @ORM\ManyToOne(targetEntity="Classe")
     * @ORM\JoinColumn(nullable=false)
     */
    private $classe;

    /**
     * @ORM\ManyToOne(targetEntity="Subject")
     * @ORM\JoinColumn(nullable=false)
     */
    private $subject;

    /**
     * @ORM\ManyToOne(targetEntity="Infrastructure")
     * @ORM\JoinColumn(nullable=false)
     */
    private $classroom;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SessionContainer")
     * @ORM\JoinColumn(nullable=false)
     */
    private $container;

    /**
     * @var int
     *
     * @ORM\Column(name="week", type="smallint")
     */
    private $week;

    /**
     * @var string
     *
     * @ORM\Column(name="schoolYear", type="string", length=9)
     */
    private $schoolYear;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $report;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set week
     *
     * @param integer $week
     *
     * @return Session
     */
    public function setWeek( $week ) {

        $this->week = $week;

        return $this;
    }

    /**
     * Get week
     *
     * @return int
     */
    public function getWeek() {

        return $this->week;
    }

    /**
     * Set schoolYear
     *
     * @param string $schoolYear
     *
     * @return Session
     */
    public function setSchoolYear( $schoolYear ) {

        $this->schoolYear = $schoolYear;

        return $this;
    }

    /**
     * Get schoolYear
     *
     * @return string
     */
    public function getSchoolYear() {

        return $this->schoolYear;
    }

    /**
     * Set classe
     *
     * @param Classe $classe
     *
     * @return Session
     */
    public function setClasse(Classe $classe = null)
    {
        $this->classe = $classe;

        return $this;
    }

    /**
     * Get classe
     *
     * @return Classe
     */
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     * Set professor
     *
     * @param User $professor
     *
     * @return Session
     */
    public function setProfessor(User $professor = null)
    {
        $this->professor = $professor;

        return $this;
    }

    /**
     * Get professor
     *
     * @return User
     */
    public function getProfessor()
    {
        return $this->professor;
    }

    /**
     * Set subject
     *
     * @param Subject $subject
     *
     * @return Session
     */
    public function setSubject(Subject $subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return Subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return Infrastructure
     */
    public function getClassroom() {
        return $this->classroom;
    }

    /**
     * @param Infrastructure $classroom
     */
    public function setClassroom(Infrastructure $classroom = null) {
        $this->classroom = $classroom;
    }

    public function getContainer(): ?SessionContainer
    {
        return $this->container;
    }

    public function setContainer(?SessionContainer $container = null): self
    {
        $this->container = $container;

        return $this;
    }

    public function getReport(): ?string
    {
        return $this->report;
    }

    public function setReport(?string $report): self
    {
        $this->report = $report;

        return $this;
    }
}

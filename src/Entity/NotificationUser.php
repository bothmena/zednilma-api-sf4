<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * NotificationUser
 *
 * @ORM\Table(name="notification_user")
 * @ORM\Entity(repositoryClass="App\Repository\NotificationUserRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class NotificationUser {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $notified;

    /**
     * @ORM\ManyToOne(targetEntity="Notification")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose()
     */
    private $notification;

    /**
     * @var bool
     *
     * @ORM\Column(name="isRead", type="boolean")
     *
     * @Serializer\Expose()
     */
    private $isRead;

    /**
     * @ORM\Column(type="datetime")
     *
     * @Serializer\Expose()
     */
    private $date;

    /**
     * NotificationUser constructor.
     * @param $notified
     * @param $notification
     */
    public function __construct(User $notified = NULL, Notification $notification = NULL) {

        $this->notified = $notified;
        $this->notification = $notification;
        $this->isRead = False;
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set isRead
     *
     * @param boolean $isRead
     *
     * @return NotificationUser
     */
    public function setIsRead( $isRead ) {

        $this->isRead = $isRead;

        return $this;
    }

    /**
     * Get isRead
     *
     * @return bool
     */
    public function getIsRead() {

        return $this->isRead;
    }

    /**
     * Set notified
     *
     * @param User $notified
     *
     * @return NotificationUser
     */
    public function setNotified(User $notified)
    {
        $this->notified = $notified;

        return $this;
    }

    /**
     * Get notified
     *
     * @return User
     */
    public function getNotified()
    {
        return $this->notified;
    }

    /**
     * Set notification
     *
     * @param Notification $notification
     *
     * @return NotificationUser
     */
    public function setNotification(Notification $notification)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Get notification
     *
     * @return Notification
     */
    public function getNotification()
    {
        return $this->notification;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}

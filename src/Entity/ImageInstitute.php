<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImageInstitute
 *
 * @ORM\Table(name="image_institute")
 * @ORM\Entity(repositoryClass="App\Repository\ImageInstituteRepository")
 */
class ImageInstitute {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Image", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="Institute")
     * @ORM\JoinColumn(nullable=false)
     */
    private $institute;

    public function __construct() {}

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set image
     *
     * @param \App\Entity\Image $image
     *
     * @return ImageInstitute
     */
    public function setImage(\App\Entity\Image $image) {

        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \App\Entity\Image
     */
    public function getImage() {

        return $this->image;
    }

    /**
     * Set institute
     *
     * @param \App\Entity\Institute $institute
     *
     * @return ImageInstitute
     */
    public function setInstitute( \App\Entity\Institute $institute) {

        $this->institute = $institute;

        return $this;
    }

    /**
     * Get institute
     *
     * @return \App\Entity\Institute
     */
    public function getInstitute() {

        return $this->institute;
    }
}

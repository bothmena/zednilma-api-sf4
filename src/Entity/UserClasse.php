<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * UserClasse
 *
 * @ORM\Table(name="user_classe")
 * @ORM\Entity(repositoryClass="App\Repository\UserClasseRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class UserClasse {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var User $user
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Classe")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var Classe $classe
     */
    private $classe;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateIn", type="datetime")
     *
     * @Serializer\Expose()
     */
    private $dateIn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateOut", type="datetime", nullable=true)
     *
     * @Serializer\Expose()
     */
    private $dateOut;


    /**
     * UserClasse constructor.
     * @param User|null $user
     * @param Classe|null $classe
     */
    public function __construct(User $user = NULL, Classe $classe = NULL) {

        $this->classe = $classe;
        $this->user = $user;
        $this->dateIn = new \DateTime();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("user_id")
     *
     * @return int
     */
    public function getUserId() {

        return $this->user->getId();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("group_id")
     *
     * @return int
     */
    public function getClasseId() {

        return $this->classe->getId();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDateIn(): \DateTime {
        return $this->dateIn;
    }

    /**
     * @param \DateTime $dateIn
     */
    public function setDateIn(\DateTime $dateIn) {
        $this->dateIn = $dateIn;
    }

    /**
     * @return \DateTime
     */
    public function getDateOut(): \DateTime {
        return $this->dateOut;
    }

    /**
     * @param \DateTime $dateOut
     */
    public function setDateOut(\DateTime $dateOut) {
        $this->dateOut = $dateOut;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return UserClasse
     */
    public function setUser(User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get classe
     *
     * @return Classe
     */
    public function getClasse() {
        return $this->classe;
    }

    /**
     * Set classe
     *
     * @param Classe $classe
     *
     * @return UserClasse
     */
    public function setClasse(Classe $classe) {
        $this->classe = $classe;

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImageUser
 *
 * @ORM\Table(name="image_user")
 * @ORM\Entity(repositoryClass="App\Repository\ImageUserRepository")
 */
class ImageUser {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Image", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct() {}

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set image
     *
     * @param \App\Entity\Image $image
     *
     * @return ImageUser
     */
    public function setImage(\App\Entity\Image $image) {

        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \App\Entity\Image
     */
    public function getImage() {

        return $this->image;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return ImageUser
     */
    public function setUser(\App\Entity\User $user) {

        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser() {

        return $this->user;
    }
}

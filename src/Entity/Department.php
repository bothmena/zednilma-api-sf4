<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;

/**
 * Department
 *
 * @ORM\Table(name="department")
 * @ORM\Entity(repositoryClass="App\Repository\DepartmentRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"id", "instituteId", "name", "slug", "description", "date"})
 */
class Department {

    /**
     * @ORM\PrePersist()
     */
    public function onPrePersist() {

        $this->groupe->setSuffixedName( $this->name );
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Institute")
     * @ORM\JoinColumn(nullable=false)
     */
    private $institute;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Group", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     *
     * @var Group $groupe
     */
    private $groupe;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=81, unique=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     *
     * @Serializer\Expose()
     */
    private $date;

    /**
     * @Serializer\Type("ArrayCollection")
     * @Serializer\Expose()
     */
    private $subjects;

    /**
     * @Serializer\Type("ArrayCollection")
     * @Serializer\Expose()
     */
    private $members;


    public function __construct() {

        $this->date = new \DateTime();
        $this->groupe = new Group();
        $this->groupe->setType('department');
        $this->subjects = [];
        $this->members = [];
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("institute_id")
     *
     * @return int
     */
    public function getInstituteId() {

        return $this->institute->getId();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Department
     */
    public function setName( $name ) {

        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {

        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Department
     */
    public function setSlug( $slug ) {

        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {

        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Department
     */
    public function setDescription( $description ) {

        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {

        return $this->description;
    }

    /**
     * Set institute
     *
     * @param Institute $institute
     *
     * @return Department
     */
    public function setInstitute(Institute $institute)
    {
        $this->institute = $institute;

        return $this;
    }

    /**
     * Get institute
     *
     * @return Institute
     */
    public function getInstitute()
    {
        return $this->institute;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Department
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    public function setSubjects($subjects) {

        $this->subjects = $subjects;
        return $this;
    }

    public function getSubjects() {

        return $this->subjects;
    }

    public function setMembers($members) {

        $this->members = $members;
        return $this;
    }

    public function getMembers() {

        return $this->members;
    }

    public function getGroupe(): ?Group
    {
        return $this->groupe;
    }

    public function setGroupe(Group $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Infrastructure
 *
 * @ORM\Table(name="infrastructure")
 * @ORM\Entity(repositoryClass="App\Repository\InfrastructureRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"id", "instituteId", "parentId", "departmentId", "name", "alias",
 *     "index", "capacity", "type", "dateStr"})
 */
class Infrastructure {

    /**
     * @ORM\PrePersist()
     */
    public function onPrePersist() {

        if ( $this->type === 'BLOCK' ) {

            return;
        } else if ( $this->type === 'FLOOR' ) {

            $this->name = 'Floor ${F}';
            if ( $this->index === 0 )
                $this->name = 'ground_floor';
            else {
                $this->name = preg_replace( '/\$\{F\}/', $this->index, $this->name );
                $this->name = preg_replace( '/\$\{B\}/', $this->parent->getAlias(), $this->name );
                if ( empty( $this->alias ) ) {
                    $this->alias = $this->index;
                }
            }
        } else {

            $this->name = '${B}${F}${C,2}';
            $this->name = preg_replace( '/\$\{F\}/', $this->parent->getAlias(), $this->name );
            $this->name = preg_replace( '/\$\{B\}/', $this->parent->getParent()->getAlias(), $this->name );

            if ( strpos($this->name, '${C}') !== false ) {

                $this->name = preg_replace( '/\$\{C\}/', $this->index, $this->name );
            } else if ( strpos($this->name, '${C,2}') !== false ) {

                if ( $this->index < 10 )
                    $index = '0' . $this->index;
                else
                    $index = '' . $this->index;
                $this->name = preg_replace( '/\$\{C,2\}/', $index, $this->name );
            } else if ( strpos($this->name, '${C,3}') !== false ) {

                if ( $this->index < 10 )
                    $index = '00' . $this->index;
                else if ( $this->index < 100 )
                    $index = '0' . $this->index;
                else
                    $index = '' . $this->index;
                $this->name = preg_replace( '/\$\{C,3\}/', $index, $this->name );
            }
        }
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Department")
     */
    private $department;

    /**
     * @ORM\ManyToOne(targetEntity="Institute")
     * @ORM\JoinColumn(nullable=false)
     */
    private $institute;

    /**
     * @ORM\ManyToOne(targetEntity="Infrastructure")
     * @var $parent Infrastructure
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=15)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=5, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $alias;

    /**
     * @var int
     *
     * @ORM\Column(name="indx", type="smallint", nullable=true)
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $index;

    /**
     * @var int
     *
     * @ORM\Column(name="capacity", type="smallint", nullable=true)
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $capacity;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=15)
     * // types: BLOCK, FLOOR, [CLASSROOM, LABORATORY, WORKSHOP, LIBRARY]
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    public function __construct() {

        $this->date = new \DateTime();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("institute_id")
     *
     * @return int
     */
    public function getInstituteId() {

        return $this->institute->getId();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("department_id")
     *
     * @return int|NULL
     */
    public function getDepartmentId() {

        return $this->department ? $this->department->getId() : NULL;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("parent_id")
     *
     * @return int|NULL
     */
    public function getParentId() {

        return $this->parent ? $this->parent->getId() : NULL;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("date")
     *
     * @return string
     */
    public function getDateStr() {

        return $this->date->format( 'd/m/Y H:i:s' );
    }

    /**
     * @Assert\IsTrue(message = "Required fields should not be left blank.")
     */
    public function isInfrastructureValid(): bool {

        switch ( $this->type ) {
            case 'BLOCK':
                return ( !empty( $this->name ) && !empty( $this->alias ) );
            case 'FLOOR':
                return ( ( $this->index >= 0 ) && !empty( $this->parent ) );
                // !empty( $this->name ) && !empty( $this->alias )
            default:
                return ( ( $this->index >= 0 ) && !empty( $this->parent ) );
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Infrastructure
     */
    public function setName( $name ) {

        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {

        return $this->name;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Infrastructure
     */
    public function setAlias( $alias ) {

        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias() {

        return $this->alias;
    }

    /**
     * Set index
     *
     * @param integer $index
     *
     * @return Infrastructure
     */
    public function setIndex( $index ) {

        $this->index = $index;

        return $this;
    }

    /**
     * Get index
     *
     * @return integer
     */
    public function getIndex() {

        return $this->index;
    }

    /**
     * Set capacity
     *
     * @param integer $capacity
     *
     * @return Infrastructure
     */
    public function setCapacity( $capacity ) {

        $this->capacity = $capacity;

        return $this;
    }

    /**
     * Get capacity
     *
     * @return integer
     */
    public function getCapacity() {

        return $this->capacity;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Infrastructure
     */
    public function setType( $type ) {

        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {

        return $this->type;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Infrastructure
     */
    public function setDate( $date ) {

        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {

        return $this->date;
    }

    /**
     * Set department
     *
     * @param \App\Entity\Department $department
     *
     * @return Infrastructure
     */
    public function setDepartment( \App\Entity\Department $department = NULL ) {

        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \App\Entity\Department
     */
    public function getDepartment() {

        return $this->department;
    }

    /**
     * Set institute
     *
     * @param \App\Entity\Institute $institute
     *
     * @return Infrastructure
     */
    public function setInstitute( \App\Entity\Institute $institute ) {

        $this->institute = $institute;

        return $this;
    }

    /**
     * Get institute
     *
     * @return \App\Entity\Institute
     */
    public function getInstitute() {

        return $this->institute;
    }

    /**
     * Set parent
     *
     * @param \App\Entity\Infrastructure $parent
     *
     * @return Infrastructure
     */
    public function setParent( \App\Entity\Infrastructure $parent ) {

        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \App\Entity\Infrastructure
     */
    public function getParent() {

        return $this->parent;
    }
}

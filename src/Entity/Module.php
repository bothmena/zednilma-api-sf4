<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Module
 *
 * @ORM\Table(name="module")
 * @ORM\Entity(repositoryClass="App\Repository\ModuleRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"id", "specialityId", "name", "description", "coefficient", "dateStr"})
 */
class Module {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Speciality")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var $speciality Speciality
     */
    private $speciality;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=61)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="coefficient", type="decimal", precision=4, scale=2)
     *
     * @Serializer\Type("float")
     * @Serializer\Expose()
     */
    private $coefficient;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="integer")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="term", type="string", length=2)
     *
     * s1 - s2 / t1 - t2 - t3 / q1 - q2 - q3 - q4
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $term;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    public function __construct() {

        $this->date = new \DateTime();
        $this->coefficient = 0;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("speciality_id")
     *
     * @return int|NULL
     */
    public function getSpecialityId() {

        return $this->speciality->getId();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("date")
     *
     * @return string
     */
    public function getDateStr(): string {

        return $this->date ? $this->date->format( 'd/m/Y H:i:s' ) : '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Module
     */
    public function setName( $name ) {

        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {

        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Module
     */
    public function setDescription( $description ) {

        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {

        return $this->description;
    }

    /**
     * Set coefficient
     *
     * @param float $coefficient
     *
     * @return Module
     */
    public function setCoefficient( $coefficient ) {

        $this->coefficient = $coefficient;

        return $this;
    }

    /**
     * Get coefficient
     *
     * @return float
     */
    public function getCoefficient() {

        return $this->coefficient;
    }

    /**
     * @return int
     */
    public function getYear() {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear(int $year) {
        $this->year = $year;
    }

    /**
     * @return string
     */
    public function getTerm() {
        return $this->term;
    }

    /**
     * @param string $term
     */
    public function setTerm(string $term) {
        $this->term = $term;
    }

    /**
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date) {
        $this->date = $date;
    }

    /**
     * Set speciality
     *
     * @param Speciality $speciality
     *
     * @return Module
     */
    public function setSpeciality( Speciality $speciality = NULL ) {

        $this->speciality = $speciality;

        return $this;
    }

    /**
     * Get speciality
     *
     * @return Speciality
     */
    public function getSpeciality() {

        return $this->speciality;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ModuleSubject
 *
 * @ORM\Table(name="module_subject")
 * @ORM\Entity(repositoryClass="App\Repository\ModuleSubjectRepository")
 */
class ModuleSubject {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Module")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose()
     */
    private $module;

    /**
     * @ORM\ManyToOne(targetEntity="Subject")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose()
     */
    private $subject;

    /**
     * @var int
     *
     * @ORM\Column(name="coefficient", type="decimal", precision=4, scale=2)
     *
     * @Serializer\Expose()
     */
    private $coefficient;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     *
     * @Serializer\Expose()
     */
    private $date;

    public function __construct(Module $module = NULL, Subject $subject = NULL, float $coefficient = NULL) {

        $this->date = new \DateTime();
        $this->module = $module;
        $this->subject = $subject;
        $this->coefficient = $coefficient;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ModuleSubject
     */
    public function setDate( $date ) {

        $this->date = $date;

        return $this;
    }

    /**
     * @return float
     */
    public function getCoefficient() {
        return $this->coefficient;
    }

    /**
     * @param float $coefficient
     */
    public function setCoefficient(float $coefficient) {
        $this->coefficient = $coefficient;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {

        return $this->date;
    }

    /**
     * Set module
     *
     * @param \App\Entity\Module $module
     *
     * @return ModuleSubject
     */
    public function setModule( \App\Entity\Module $module ) {

        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return \App\Entity\Module
     */
    public function getModule() {

        return $this->module;
    }

    /**
     * Set subject
     *
     * @param \App\Entity\Subject $subject
     *
     * @return ModuleSubject
     */
    public function setSubject( \App\Entity\Subject $subject ) {

        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \App\Entity\Subject
     */
    public function getSubject() {

        return $this->subject;
    }
}

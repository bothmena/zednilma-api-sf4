<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * PathBranch
 *
 * @ORM\Table(name="path_branch")
 * @ORM\Entity(repositoryClass="App\Repository\PathBranchRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"id", "path", "branch", "index"})
 */
class PathBranch {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Path")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var Path $path
     * @Serializer\Expose()
     */
    private $path;

    /**
     * @ORM\ManyToOne(targetEntity="Speciality")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var Speciality $branch
     * @Serializer\Expose()
     */
    private $branch;

    /**
     * @var int
     *
     * @ORM\Column(name="indx", type="smallint")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $index;


    public function __construct(Path $path = NULL, Speciality $speciality = NULL, int $index = NULL) {

        $this->path = $path;
        $this->branch = $speciality;
        $this->index = $index;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set index
     *
     * @param integer $index
     *
     * @return PathBranch
     */
    public function setIndex( $index ) {

        $this->index = $index;

        return $this;
    }

    /**
     * Get index
     *
     * @return int
     */
    public function getIndex() {

        return $this->index;
    }

    /**
     * Set path
     *
     * @param \App\Entity\Path $path
     *
     * @return PathBranch
     */
    public function setPath(\App\Entity\Path $path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return \App\Entity\Path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set branch
     *
     * @param \App\Entity\Speciality $branch
     *
     * @return PathBranch
     */
    public function setBranch(\App\Entity\Speciality $branch)
    {
        $this->branch = $branch;

        return $this;
    }

    /**
     * Get branch
     *
     * @return \App\Entity\Speciality
     */
    public function getBranch()
    {
        return $this->branch;
    }
}

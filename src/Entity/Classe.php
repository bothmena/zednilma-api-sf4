<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;

/**
 * Classe
 *
 * @ORM\Table(name="classe")
 * @ORM\Entity(repositoryClass="App\Repository\ClasseRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"id", "branch", "name", "schoolYear"})
 */
class Classe {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Speciality")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose()
     */
    private $branch;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Group", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     *
     * @var Group
     */
    private $groupe;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=81)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="schoolYear", type="string", length=9)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $schoolYear;

    /**
     * @Gedmo\Slug(fields={"name", "schoolYear"}, updatable=false)
     * @ORM\Column(length=81, unique=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $slug;

    /**
     * @ORM\Column(name="students", type="integer")
     *
     * @Serializer\Expose()
     */
    private $students;

    /**
     * @ORM\Column(type="smallint")
     *
     * @Serializer\Expose()
     */
    private $level;


    public function __construct() {

        $this->students = 0;
        $this->groupe = new Group();
        $this->groupe->setType('class');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {

        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Classe
     */
    public function setName($name) {

        $this->name = $name;
        if ($this->groupe->getInstitute())
            $this->groupe->setSuffixedName($name);
        else
            $this->name = $name;

        return $this;
    }

    /**
     * Get schoolYear
     *
     * @return string
     */
    public function getSchoolYear() {

        return $this->schoolYear;
    }

    /**
     * Set schoolYear
     *
     * @param string $schoolYear
     *
     * @return Classe
     */
    public function setSchoolYear($schoolYear) {

        $this->schoolYear = $schoolYear;

        return $this;
    }

    /**
     * Get speciality
     *
     * @return \App\Entity\Speciality
     */
    public function getBranch() {
        return $this->branch;
    }

    /**
     * Set speciality
     *
     * @param Speciality $branch
     *
     * @return Classe
     */
    public function setBranch(Speciality $branch) {

        $this->branch = $branch;
        $this->groupe->setInstitute($branch->getInstitute());
        if ($this->name)
            $this->groupe->setSuffixedName($this->name);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void {
        $this->slug = $slug;
    }

    /**
     * @return int
     */
    public function getStudents() {
        return $this->students;
    }

    /**
     * @param int $students
     */
    public function setStudents($students) {
        $this->students = $students;
    }

    public function getGroupe(): ?Group {
        return $this->groupe;
    }

    public function setGroupe(Group $groupe): self {
        $this->groupe = $groupe;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }
}

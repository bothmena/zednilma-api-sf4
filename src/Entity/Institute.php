<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;

/**
 * Institute
 *
 * @ORM\Table(name="institute")
 * @ORM\Entity(repositoryClass="App\Repository\InstituteRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"context", "id", "name", "slug", "slogan",
 *     "description", "profileImageUrl", "coverImageUrl", "foundationDate", "website", "type", "parentalControl", "joinDate"})
 */
class Institute {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="InstituteSettings", cascade={"persist", "remove"})
     */
    private $settings;

    /**
     * @ORM\ManyToOne(targetEntity="Image")
     */
    private $profileImage;

    /**
     * @ORM\ManyToOne(targetEntity="Image")
     */
    private $coverImage;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=101)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=81, unique=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="slogan", type="string", length=101, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $slogan;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="foundationDate", type="date", nullable=true)
     * @Serializer\Expose()
     */
    private $foundationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="joinDate", type="datetime")
     * @Serializer\Expose()
     */
    private $joinDate;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=41, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=18, nullable=true)
     * // PRE_SCHOOL
     * // PRIMARY_SCHOOL, Elementary
     * // SECONDARY_SCHOOL, Junior, MIDDLE
     * // HIGH_SCHOOL, COLLEGE, , UNIVERSITY, Senior
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="parentalControl", type="string", length=6, nullable=true)
     * // NONE, MEDIUM, FULL, CUSTOM
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $parentalControl;


    public function __construct() {

        $this->joinDate = new \DateTime();
        $this->settings = new InstituteSettings();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("context")
     *
     * @return string
     */
    public function getContext(): string {

        return '/api/v1/institutes/' . $this->id;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("logo_url")
     *
     * @return string
     */
    public function getProfileImageUrl(): string {

        return $this->profileImage ? $this->profileImage->getSource() : '';
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("cover_url")
     *
     * @return string
     */
    public function getCoverImageUrl(): string {

        return $this->coverImage ? $this->coverImage->getSource() : '';
    }

    public function getValueByKey( string $key ) {

        switch ( $key ) {

            case 'id':
                return $this->getId();
            case 'context':
                return $this->getContext();
            case 'name':
                return $this->getName();
            case 'slug':
                return $this->getSlug();
            case 'slogan':
                return $this->getSlogan();
            case 'description':
                return $this->getDescription();
            case 'foundation_date':
                return $this->getFoundationDate();
            case 'website':
                return $this->getWebsite();
            case 'type':
                return $this->gettype();
            case 'parental_control':
                return $this->getParentalControl();
            case 'join_date':
                return $this->getJoinDate();
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Institute
     */
    public function setName( $name ) {

        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {

        return $this->name;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return Institute
     */
    public function setWebsite( $website ) {

        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite() {

        return $this->website;
    }

    /**
     * Set slogan
     *
     * @param string $slogan
     *
     * @return Institute
     */
    public function setSlogan( $slogan ) {

        $this->slogan = $slogan;

        return $this;
    }

    /**
     * Get slogan
     *
     * @return string
     */
    public function getSlogan() {

        return $this->slogan;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Institute
     */
    public function setDescription( $description ) {

        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {

        return $this->description;
    }

    /**
     * Set foundationDate
     *
     * @param \DateTime $foundationDate
     *
     * @return Institute
     */
    public function setFoundationDate( $foundationDate ) {

        $this->foundationDate = $foundationDate;

        return $this;
    }

    /**
     * Get foundationDate
     *
     * @return \DateTime
     */
    public function getFoundationDate() {

        return $this->foundationDate;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Institute
     */
    public function setSlug( $slug ) {

        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {

        return $this->slug;
    }

    /**
     * Set joinDate
     *
     * @param \DateTime $joinDate
     *
     * @return Institute
     */
    public function setJoinDate( $joinDate ) {

        $this->joinDate = $joinDate;

        return $this;
    }

    /**
     * Get joinDate
     *
     * @return \DateTime
     */
    public function getJoinDate() {

        return $this->joinDate;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Institute
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set parentalControl
     *
     * @param string $parentalControl
     *
     * @return Institute
     */
    public function setParentalControl($parentalControl)
    {
        $this->parentalControl = $parentalControl;

        return $this;
    }

    /**
     * Get parentalControl
     *
     * @return string
     */
    public function getParentalControl()
    {
        return $this->parentalControl;
    }

    /**
     * Set profileImage
     *
     * @param Image $profileImage
     *
     * @return Institute
     */
    public function setProfileImage(Image $profileImage = null)
    {
        $this->profileImage = $profileImage;

        return $this;
    }

    /**
     * Get profileImage
     *
     * @return Image
     */
    public function getProfileImage()
    {
        return $this->profileImage;
    }

    /**
     * Set coverImage
     *
     * @param Image $coverImage
     *
     * @return Institute
     */
    public function setCoverImage(Image $coverImage = null)
    {
        $this->coverImage = $coverImage;

        return $this;
    }

    /**
     * Get coverImage
     *
     * @return Image
     */
    public function getCoverImage()
    {
        return $this->coverImage;
    }

    /**
     * @return InstituteSettings
     */
    public function getSettings() {
        return $this->settings;
    }

    /**
     * @param InstituteSettings $settings
     */
    public function setSettings(InstituteSettings $settings) {
        $this->settings = $settings;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Speciality
 *
 * @ORM\Table(name="speciality")
 * @ORM\Entity(repositoryClass="App\Repository\SpecialityRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"id", "instituteId", "name", "alias", "description", "studyCycle",
 *     "years", "type", "date"})
 */
class Speciality {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Institute")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var Institute $institute
     */
    private $institute;

    /**
     * @ORM\ManyToOne(targetEntity="Speciality")
     *
     * @var $parent Speciality
     */
    private $parent;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Group", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     *
     * @var Group
     */
    private $groupe;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=61)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=120, unique=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=10, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="studyCycle", type="string", length=41, nullable=true)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $studyCycle;

    /**
     * @var int
     *
     * @ORM\Column(name="years", type="smallint")
     *
     * @Serializer\Type("integer")
     * @Serializer\Expose()
     */
    private $years;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=21)
     *
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     * @Serializer\Expose()
     */
    private $date;


    public function __construct() {

        $this->date = new \DateTime();
        $this->groupe = new Group();
        $this->groupe->setType('speciality');
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("institute_id")
     *
     * @return int|NULL
     */
    public function getInstituteId() {

        return $this->institute->getId();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("parent_id")
     *
     * @return int|NULL
     */
    public function getParentId() {

        return $this->parent ? $this->parent->getId() : NULL;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * @Assert\IsTrue(message = "years must be less or equal than parent years.")
     */
    public function isYearsValid(): bool {

        if ($this->parent)
            return $this->years <= $this->parent->years;
        return true;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {

        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Speciality
     */
    public function setName($name) {

        if ($this->groupe->getInstitute())
            $this->groupe->setSuffixedName($name);
        else
            $this->groupe->setName($name);
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void {
        $this->slug = $slug;
    }

    /**
     * Get studyCycle
     *
     * @return string
     */
    public function getStudyCycle() {

        return $this->studyCycle;
    }

    /**
     * Set studyCycle
     *
     * @param string $studyCycle
     *
     * @return Speciality
     */
    public function setStudyCycle($studyCycle) {

        $this->studyCycle = $studyCycle;

        return $this;
    }

    /**
     * Get years
     *
     * @return int
     */
    public function getYears() {

        return $this->years;
    }

    /**
     * Set years
     *
     * @param integer $years
     *
     * @return Speciality
     */
    public function setYears($years) {

        $this->years = $years;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {

        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Speciality
     */
    public function setType($type) {

        $this->type = $type;

        return $this;
    }

    /**
     * Get institute
     *
     * @return Institute
     */
    public function getInstitute() {
        return $this->institute;
    }

    /**
     * Set institute
     *
     * @param Institute $institute
     *
     * @return Speciality
     */
    public function setInstitute(Institute $institute) {

        $this->institute = $institute;
        $this->groupe->setInstitute($institute);
        if ($this->name)
            $this->groupe->setSuffixedName($this->name);

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Speciality
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Speciality
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get parent
     *
     * @return Speciality
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * Set parent
     *
     * @param Speciality $parent
     *
     * @return Speciality
     */
    public function setParent(Speciality $parent = null) {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias() {
        return $this->alias;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Speciality
     */
    public function setAlias($alias) {
        $this->alias = $alias;

        return $this;
    }

    public function getGroupe(): ?Group {
        return $this->groupe;
    }

    public function setGroupe(Group $groupe): self {
        $this->groupe = $groupe;

        return $this;
    }
}

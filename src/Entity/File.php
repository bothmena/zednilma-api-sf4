<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * File
 *
 * @ORM\Table(name="file")
 * @ORM\Entity(repositoryClass="App\Repository\FileRepository")
 */
class File {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Subject")
     */
    private $subject;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="Group")
     */
    private $group;

    /**
     * @ORM\ManyToOne(targetEntity="File")
     */
    private $directory;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=41)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="isPublic", type="boolean")
     */
    private $isPublic;

    /**
     * @var bool
     *
     * @ORM\Column(name="isDirectory", type="boolean")
     */
    private $isDirectory;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return File
     */
    public function setName( $name ) {

        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {

        return $this->name;
    }

    /**
     * Set isPublic
     *
     * @param boolean $isPublic
     *
     * @return File
     */
    public function setIsPublic( $isPublic ) {

        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic
     *
     * @return bool
     */
    public function getIsPublic() {

        return $this->isPublic;
    }

    /**
     * Set isDirectory
     *
     * @param boolean $isDirectory
     *
     * @return File
     */
    public function setIsDirectory( $isDirectory ) {

        $this->isDirectory = $isDirectory;

        return $this;
    }

    /**
     * Get isDirectory
     *
     * @return bool
     */
    public function getIsDirectory() {

        return $this->isDirectory;
    }

    /**
     * Set subject
     *
     * @param \App\Entity\Subject $subject
     *
     * @return File
     */
    public function setSubject(\App\Entity\Subject $subject = null)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \App\Entity\Subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set owner
     *
     * @param \App\Entity\User $owner
     *
     * @return File
     */
    public function setOwner(\App\Entity\User $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \App\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set directory
     *
     * @param \App\Entity\File $directory
     *
     * @return File
     */
    public function setDirectory(\App\Entity\File $directory = null)
    {
        $this->directory = $directory;

        return $this;
    }

    /**
     * Get directory
     *
     * @return \App\Entity\File
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * Set group
     *
     * @param \App\Entity\Group $group
     *
     * @return File
     */
    public function setGroup(\App\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \App\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }
}

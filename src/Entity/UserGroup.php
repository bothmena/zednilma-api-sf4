<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * UserGroup
 *
 * @ORM\Table(name="user_group")
 * @ORM\Entity(repositoryClass="App\Repository\UserGroupRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("custom", custom = {"id", "group", "userId", "date"})
 */
class UserGroup {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var User $user
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Group")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose()
     * @var Group $group
     */
    private $group;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     * @Serializer\Expose()
     */
    private $level;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     * @Serializer\Expose()
     */
    private $date;


    public function __construct(User $user = NULL, Group $group = NULL) {

        $this->user = $user;
        $this->group = $group;
        $this->date = new \DateTime();
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("user_id")
     *
     * @return int
     */
    public function getUserId() {

        return $this->user->getId();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return UserGroup
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return UserGroup
     */
    public function setUser(User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get group
     *
     * @return Group
     */
    public function getGroup() {
        return $this->group;
    }

    /**
     * Set group
     *
     * @param Group $group
     *
     * @return UserGroup
     */
    public function setGroup(Group $group) {
        $this->group = $group;

        return $this;
    }

    public function getLevel(): ?int {
        return $this->level;
    }

    public function setLevel(?int $level): self {
        $this->level = $level;

        return $this;
    }
}

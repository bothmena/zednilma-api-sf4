<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Attendance
 *
 * @ORM\Table(name="attendance")
 * @ORM\Entity(repositoryClass="App\Repository\AttendanceRepository")
 */
class Attendance {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Session")
     * @ORM\JoinColumn(nullable=false)
     */
    private $session;

    /**
     * @var bool
     *
     * @ORM\Column(name="isPresent", type="boolean")
     */
    private $isPresent;

    /**
     * @var integer
     *
     * 1: student, 2: prof-assistant, 3: professor (primary)
     *
     * @ORM\Column(name="role", type="smallint")
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="certification", type="string", length=21, nullable=true)
     */
    private $certification;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {

        return $this->id;
    }

    /**
     * Set isPresent
     *
     * @param boolean $isPresent
     *
     * @return Attendance
     */
    public function setIsPresent( $isPresent ) {

        $this->isPresent = $isPresent;

        return $this;
    }

    /**
     * Get isPresent
     *
     * @return bool
     */
    public function getIsPresent() {

        return $this->isPresent;
    }

    /**
     * Set certification
     *
     * @param string $certification
     *
     * @return Attendance
     */
    public function setCertification( $certification ) {

        $this->certification = $certification;

        return $this;
    }

    /**
     * Get certification
     *
     * @return string
     */
    public function getCertification() {

        return $this->certification;
    }

    /**
     * @return int
     */
    public function getRole() {
        return $this->role;
    }

    /**
     * @param int $role
     */
    public function setRole(int $role) {
        $this->role = $role;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Attendance
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set session
     *
     * @param Session $session
     *
     * @return Attendance
     */
    public function setSession(Session $session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }
}

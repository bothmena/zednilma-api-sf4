<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SessionContainerRepository")
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class SessionContainer {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Timetable", inversedBy="sessionContainers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $timetable;

    /**
     * @ORM\Column(type="time")
     * @Serializer\Expose()
     */
    private $startAt;

    /**
     * @ORM\Column(type="time")
     * @Serializer\Expose()
     */
    private $endAt;

    /**
     * @ORM\Column(type="smallint")
     * @Serializer\Expose()
     */
    private $day;


    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("timetable_id")
     *
     * @return int|NULL
     */
    public function getTimetableId() {

        return $this->timetable->getId();
    }

    /**
     * SessionContainer constructor.
     * @param $timetable
     */
    public function __construct(Timetable $timetable = NULL) {
        $this->timetable = $timetable;
    }

    public function getId() {
        return $this->id;
    }

    public function getTimetable(): ?Timetable {
        return $this->timetable;
    }

    public function setTimetable(?Timetable $timetable): self {
        $this->timetable = $timetable;

        return $this;
    }

    public function getStartAt(): ?\DateTimeInterface {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeInterface $startAt): self {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface {
        return $this->endAt;
    }

    public function setEndAt(\DateTimeInterface $endAt): self {
        $this->endAt = $endAt;

        return $this;
    }

    public function getDay(): ?int {
        return $this->day;
    }

    public function setDay(int $day): self {
        $this->day = $day;

        return $this;
    }
}
